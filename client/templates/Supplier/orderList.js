/**
 * Created by zhangxuhui on 16/3/24.
 */

Template.orderList.onRendered(function () {
    // Session.set("currentOrderSearchVal", null);
    // IonLoading.show();
    // var self = this;
    // TableSkipKey = "tableSkipKey";
    // TableLimitKey = "tableLimitKey";
    // // Session.set(TableLimitKey, 200);
    // // Session.set(TableSkipKey, 0);
    // this.subscribe("orderListInfo", Router.current().params.type, function () {
    //     IonLoading.hide()
    // });
    var orderType = Router.current().params.type;
    loadingSystemContent();
    this.subscribe("publishOrderList", orderType, function () {
        loadingHide();
    })
});
Template.orderList.helpers({
    headObj: function () {
        return {}
    },
    'titleName': function () {
        var type = Router.current().params.type;
        if (type == "newOrder") {
            return "新订单";
        } else if (type == "proceedOrder") {
            return "进行中的订单";
        } else if (type == "finishOrder") {
            return "已完成订单";
        } else if (type == "suspendOrder") {
            return "已中止订单";
        }
    },
    'orderListInfo': function () {
        var selectinfo = Session.get("currentOrderSearchVal");
        var select = {};
        if (selectinfo) {
            select.受理编号 = new RegExp(selectinfo, "i");
        }
        var info = erpViewVerifyOrderAll.find(select, {$sort: {定单号: 1, 受理编号: 1}});
        if (info) {
            return info;
        }
    },
    getCustomerName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    getCustomerAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    }
});

Template.orderList.events({
    'click .gocustomerDetail': function (event, template) {
        var acnumber = $(event.currentTarget).children("span").attr("class").trim();
        var orderNo = $(event.currentTarget).children("span").children(".orderNo").text().trim();
        var type = Router.current().params.type;
        Session.set(CurrentBackTemplate, "/orderList/" + type)
        Router.go("/orderDetail/type/" + type + "/acnumber/" + acnumber + "/orderNo/" + orderNo);
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentOrderSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentOrderSearchVal", null);
        $(".search").val("");
    },
    'click .backMenuList': function () {
        Router.go("/orderType");
    }
});
 