/**
 * Created by zhangxuhui on 15/10/29.
 */
Template.hejiadan.rendered = function () {
    var Acnumber;
    if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
        Acnumber = Session.get("customerLogin");
    } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "designer", "districtManager", "vicePresident", "StoreManager", "test", "ConstructionCaptainBoss"], "user-group")) {
        Acnumber = Session.get("customerDecorateInfo").ACNumber;
    }

    this.subscribe("NuclearPriceSheet", Acnumber, 32, function () {

    });
};


Template.hejiadan.helpers({
    headObj: function () {
        return {
            title: '核价单类型',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    }, 'hejiaInfo': function () {
        var info = erpRemoteFileStorage.find({});
        if (info) {
            return info;
        }
    },

});

Template.hejiadan.events({
    'click .openfile': function () {
        var id = $(event.currentTarget).attr("id").trim();
        Session.set("backURL","hejiadan");
        Router.go("/pdfFiles/" + id);
    },

});