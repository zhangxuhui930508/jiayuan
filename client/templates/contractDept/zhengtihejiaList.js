/**
 * Created by zhangxuhui on 16/7/13.
 */
Template.zhengtihejiaList.onRendered(function () {
    var id = Router.current().params.id;
    var type = Router.current().params.type;
    this.subscribe("publishzhengtihejiaList", id, type);

});

Template.zhengtihejiaList.helpers({
    headObj: function () {
        return {
            // title: '客户列表',
            leftButton: true,
            backTemplate: 'contractPricing'
        }
    },
    'titleName': function () {
        var info = erpInfoSupplier.findOne();
        if (info && info.名称) {
            return info.名称;
        }
    }, erpViewOrder: function () {
        return erpViewVerifyOrderAll.find();
    }, 'customerInfo': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber})
        if (info) {
            return info.CustName;
        }
    },
    'customerAddress': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber})
        if (info) {
            return info.Address;
        }
    }
});

Template.zhengtihejiaList.events({
    'click .gocustomerDetail': function () {
        Router.go("/zhengtihejiaDetaila/" + this._id);

    },
    'click .backMenuList': function () {
        Router.go("/contractPricing")
    }
});