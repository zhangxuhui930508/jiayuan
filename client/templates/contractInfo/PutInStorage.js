/**
 * Created by zhangxuhui on 16/4/27.
 */



Template.PutInStorage.rendered = function () {
    Session.set("contractPricing", "undeliverGoods");
    var self = this;
    IonLoading.show();
    this.autorun(function () {
        self.subscribe("publishCheckPutInStorageOrderInfo", Session.get("contractPricing"), function () {
            IonLoading.hide();
        });

    });
};


Template.PutInStorage.helpers({
    headObj: function () {
        return {
            title: '入库'
        }
    },
    isActive: function (val) {
        var select = Session.get("contractPricing");
        if (select && select == val) {
            return "active";
        }
    },
    //========================更改为以订单数据为主,供应商为辅=========================================
    erpBudgetMaterialInfo: function () {
        var info = ERP_Budget_Material.find();
        if (info) {
            return info;
        }
    },
    budgetsupplierNameInfo: function (ID) {
        console.log(ID);
        console.log("------------------------------");
        var info = ERP_Budget_Material.findOne({ID: ID});
        if (info) {
            var acnumber = info.受理编号;
            var orderNo = info.定单编号;
            var orderInfo = erpViewVerifyOrderAll.findOne({受理编号: acnumber, 定单号: orderNo});
            if (orderInfo) {
                return erpInfoSupplier.findOne({ID: orderInfo.供货商编号}).名称;
            }
        }
    },
    erpViewOrder: function () {
        var info = erpViewVerifyOrderAll.find();
        if (info)return info;
    },

    'erpInfoSupplierInfo': function () {
        return erpInfoSupplier.find({}, {sort: {ID: 1}});
    },
    supplierCount: function (id) {
        return ERP_Budget_Material.find({}).count() + erpViewVerifyOrderAll.find({供货商编号: id}).count();
    },
});

Template.PutInStorage.events({
    "click .tab-item": function (event) {
        var value = $(event.currentTarget).attr("value");
        Session.set("contractPricing", value);
    },
    'click .gocustomerDetail': function (event) {
        var id = $(event.currentTarget).children("span").attr("class");
        console.log(id);
        console.log("---------------------------------");
        if (Session.get("contractPricing") == "undeliverGoods") {
            Router.go("/PutInStorageDetail/" + id);
        }
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/contractDeptLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .item': function () {
        console.log(this.ID);
        Session.set("contractBackTemplate", "PutInStorage");

        var info = erpInfoSupplier.findOne({ID: this.ID});
        console.log("-----------------------------------", info);

        if (info && info.putInStorageType == "whole") {
            //    查看单项入库还是整体入库
            var type;
            if (Session.get("contractPricing") == "todaydeliverGoods") {
                type = 12;
            } else if (Session.get("contractPricing") == "undeliverGoods") {
                type = 11;
            }
            Router.go("/contractMaterailInfo/id/" + this.ID + "/type/" + type);

        } else {
            if (Session.get("contractPricing") == "todaydeliverGoods") {
                type = 12;
            } else if (Session.get("contractPricing") == "undeliverGoods") {
                type = 11;
            }
            Router.go("/orderDetailContract/id/" + this.ID + "/type/" + type);
        }


    }
})