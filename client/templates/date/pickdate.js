/**
 * Created by pcbeta on 16-3-3.
 */


Template.pickDate.rendered = function () {
    var selectDate = Router.current().params.selectDate;
    if (selectDate) {
        Session.set(selectDateKey, new Date(selectDate));
    } else if (!Session.get(selectDateKey)) {
        Session.set(selectDateKey, new Date());
    }
    Meteor.call('getServerDate', function (err, result) {
        if (err) return;
        var serverDate = result;
        var startDate = serverDate;
        if (serverDate.getHours() >= 17) {
            // 日期加一 ，小时初始为早上九点
            startDate = new Date(new Date(moment(serverDate).add(1, 'days')).Format('yyyy-MM-dd') + " 09:00:00");
        } else if (serverDate.getHours() < 9) {
            startDate = new Date(new Date(moment(serverDate).add(0, 'days')).Format('yyyy-MM-dd') + " 09:00:00");
        }
        var selectDate = Session.get(selectDateKey);
        if (isSameDate(selectDate, startDate) || selectDate > startDate) {
            Session.set(selectDateKey, selectDate);
        } else {
            Session.set(selectDateKey, startDate);
        }
        Session.set(SERVERDATE, startDate);
        $('#date').pickadate({
            monthsFull: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
            weekdaysShort: ['一', '二', '三', '四', '五', '六', '日'],
            today: '',
            clear: '',
            close: '关闭',
            format: 'yyyy-mm-dd',
            formatSubmit: 'yyyy-mm-dd',
            min: Session.get(SERVERDATE),
            onSet: function (context) {
                var nowSelectDate = $('[name=_submit]').val();
                console.log(nowSelectDate)
                //var serverDate = Session.get(SERVERDATE);
                if (nowSelectDate == serverDate.Format('yyyy-MM-dd'))
                    Session.set(selectDateKey, serverDate);
                else
                    Session.set(selectDateKey, new Date(nowSelectDate + " 09:00:00"));
            }
        });
    });
};
Template.pickDate.helpers({
    startDate: function () {
        console.log(Template.currentData().data)
        return Template.currentData().data;
    }
});
Template.pickDate.events({
    "click .yesterday": function () {
        var selectedDate = Session.get(selectDateKey);
        var serverDate = Session.get(SERVERDATE);
        if (canBeBack(selectedDate, serverDate)) {
            IonPopup.alert({
                title: '温馨提示',
                template: '<p style="text-align:center">时间已过期!!!!</p>',
                okText: '确定'
            });
            return;
        }
        Session.set(OCCUPATION_COUNTS, 1)
        var lastDate = new Date(moment(selectedDate).add(-1, 'days'));
        Session.set(selectDateKey, new Date(lastDate));

    },
    "click .tomorrow": function () {
        Session.set(OCCUPATION_COUNTS, 1)
        var date = $('[id=date]').text();
        var nextDate = new Date(moment(date).add(1, 'days'));
        Session.set(selectDateKey, new Date(nextDate.Format('yyyy-MM-dd') + " " + "09:00:00"));
        //Session.set(selectDateKey, nextDate);
    }
});