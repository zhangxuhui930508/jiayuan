/**
 * Created by zhangxuhui on 16/7/12.
 */

Template.getOrderProceedList.onRendered(function () {
    var type = Router.current().params.type;
    IonLoading.show();
    this.subscribe("publishGetOrderProceedList", parseInt(type), function () {
        IonLoading.hide();
    });
});

Template.getOrderProceedList.helpers({
    headObj: function () {
        return {
            // title: '进行中的订单',
            leftButton: true,
            backTemplate: 'proceedOrderType'
        }
    },
    'titleName': function () {
        var type = Router.current().params.type;
        if (type == 2) {
            return "待核价";
        } else if (type == 3) {
            return "待批准";
        } else if (type == 4) {
            return "核价批准";
        } else if (type == 5) {
            return "被退回核价";
        } else if (type == 6) {
            return "备货中";
        } else if (type == 7) {
            return "安排发货";
        } else if (type == 8) {
            return "发货中";
        } else if (type == 9) {
            return "待安装";
        } else if (type == 10) {
            return "安装中";
        } else if (type == 14) {
            return "待入库";
        } else if (type == 11) {
            return "入库待审核";
        }
    },
    'otherProceedList': function () {
        var select = {};
        if (Session.get("currentOrderSearchVal")) {
            select.受理编号 = new RegExp(Session.get("currentOrderSearchVal"), "i");
        }
        var info = erpViewVerifyOrderAll.find(select);
        if (info) {
            return info;
        }
    },
    getCustomerName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    getCustomerAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    }
});


Template.getOrderProceedList.events({
    'click .gocustomerDetail': function (event) {
        var info = erpViewVerifyOrderAll.findOne();
        if (info)
            Router.go("/orderDetail/type/newOrder/acnumber/" + info.受理编号 + '/orderNo/' + info.定单号);
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentOrderSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentOrderSearchVal", null);
        $(".search").val("");
    },
});

Template.getOrderProceedList.onDestroyed(function () {
    Session.set("currentOrderSearchVal", null);
});