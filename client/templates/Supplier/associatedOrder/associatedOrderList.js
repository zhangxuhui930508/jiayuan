/**
 * Created by zhangxuhui on 16/7/13.
 */
Template.associatedOrderList.onRendered(function () {
    this.subscribe("publishAssociatedOrderList", Router.current().params.acnumber, Router.current().params.materialId)
});

Template.associatedOrderList.helpers({
    headObj: function () {
        return {
            title: '关联订单',
            leftButton: true,
            backTemplate: 'orderType'
        }
    },
    orderMaterialList: function () {
        return ERP_Budget_Material.find();
    },
    isSalesReturn: function (id) {
        var info = ERP_Budget_Material.findOne({_id: id});
        if (info && info.增减 == -1) {
            return true;
        }
        return false;
    }

});

Template.associatedOrderList.events({
    'click .orderMaterialDetailInfo': function () {
        console.log(this._id);
        Router.go("/StandardMaterialDetail/" + this._id);
    }
});