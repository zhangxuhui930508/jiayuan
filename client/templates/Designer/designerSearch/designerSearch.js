/**
 * Created by zhangxuhui on 16/6/25.
 */
Template.designerSearch.onRendered(function () {
    Session.set("currentSearchClient", null);
    this.subscribe("designerSearchClientInfo");
});

Template.designerSearch.helpers({
    headObj: function () {
        return {
            title: '查找客户'
        }
    },
    'Info': function () {
        var select = {};
        if (Session.get("currentSearchClient")) {
            var info = Session.get("currentSearchClient");
            var select = {$or: [{ACNumber: new RegExp(info, "i")}, {CustName: new RegExp(info, "i")}, {Address: new RegExp(info, "i")}]};

            var custInfo = Cust_Info.find(select, {sort: {CreatTime: -1}});
            return custInfo;
        }
    }
});

Template.designerSearch.events({
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentSearchClient", currentSearch);
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/designerLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .closed': function () {
        Session.set("currentSearchClient", null);
        $(".search").val("");
        console.log("-------------------------------------");
    },
    'click .item .gocustomerDetail': function (event) {
        var cid = $(event.currentTarget).children("h2").attr("id").trim();
        var ctype = "designer";

        Router.go("/customerDetail/customerType/" + ctype + '/customerId/' + cid);
    },
});

Template.designerSearch.onDestroyed(function () {
    Session.set("currentSearchClient", null);
});