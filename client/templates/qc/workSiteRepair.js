/**
 * Created by zhangxuhui on 16/4/18.
 */
Template.workSiteRepair.onRendered(function () {
    Session.set("isFinish", false);
    this.subscribe("publishQCImage");
    var self = this;
    this.subscribe("publishErpControl");
});

Template.workSiteRepair.helpers({
    headObj: function () {
        return {
            title: '发布整改单/协调单',
            leftButton: true,
        }
    }
});


Template.workSiteRepair.events({
    'click .backMenuList': function () {
        var id = Router.current().params.id;
        Router.go("/constructionDetail/constructionId/" + id + "/type/notExamined")
    },
    'click .goOn': function () {
        var id = Router.current().params.id;
        Router.go("/constructionDetail/constructionId/" + id + "/type/notExamined")
    },
    'click .onEnd': function () {

        Session.set("isFinish", true);
    },
    'change select[name="reason.0.errorCode1"]': function (event) {
        console.log("---------------------", $(event.currentTarget).val())
        Session.set("selectBigType", $(event.currentTarget).val());
    }
});


Schemas = {};
Schemas.workSiteRepairInfo = new SimpleSchema({
    repairType: {
        type: String,
        label: "整改单/协调单类型",
        autoform: {
            type: "select",
            options: function () {
                if (Session.get("workSiteRepairType") == "Rectification") {
                    return [
                        {label: "A类", value: "A", selected: "selected"},
                        {label: "B类", value: "B"}
                    ];
                } else if (Session.get("workSiteRepairType") == "Coordination") {
                    return [
                        {label: "C类", value: "C", selected: "selected"}
                    ];
                }
            }
        },
    },
    acnumber: {
        type: String,
        autoValue: function () {
            return Router.current().params.acnumber;
        }
    },
    reason: {
        type: Array,
        label: "整改原因",
        optional: true
    },
    'reason.$': {
        type: Object
    },
    'reason.$.area': {
        type: String,
        label: "区域",
        autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "主卧", value: "主卧"},
                    {label: "次卧", value: "次卧"},
                    {label: "客卧", value: "客卧"},
                    {label: "主卫", value: "主卫"},
                    {label: "次卫", value: "次卫"},
                    {label: "厨房", value: "厨房"},
                    {label: "厨房（二扇门）", value: "厨房（二扇门）"},
                    {label: "阳台", value: "阳台"},
                    {label: "工作阳台", value: "工作阳台"},
                    {label: "进户门", value: "进户门"},
                    {label: "保姆房", value: "保姆房"},
                    {label: "主卧阳台", value: "主卧阳台"},
                    {label: "客卧阳台", value: "客卧阳台"},
                    {label: "客厅阳台", value: "客厅阳台"},
                    {label: "客厅", value: "客厅"},
                    {label: "餐厅", value: "餐厅"},
                    {label: "过道", value: "过道"},
                    {label: "书房", value: "书房"},
                    {label: "衣帽间", value: "衣帽间"},
                    {label: "老人房", value: "老人房"},
                    {label: "储藏间", value: "储藏间"},
                    {label: "盥洗间", value: "盥洗间"}
                ]
            }
        }
    },
    'reason.$.supplementrea': {
        type: String,
        label: "补充",
        optional: true,
    },
    'reason.$.errorCode1': {
        type: String,
        label: "整改项目大分类",
        autoform: {
            type: "select",
            options: function () {
                var returnValue = [];
                ERP_Info_Quality_Control.find().forEach(function (info) {
                    returnValue.push({label: info.内容, value: info._id})
                });
                return returnValue;
            }
        }
    }, 'reason.$.errorCode2': {
        type: String,
        label: "整改项目小分类",
        autoform: {
            type: "select",
            options: function () {
                var sessionInfo = Session.get("selectBigType");
                console.log(sessionInfo);
                var returnValue = [];
                var info = ERP_Info_Quality_Control.findOne({_id: sessionInfo});
                if (info && info.classList) {
                    _.each(info.classList, function (classInfo) {
                        returnValue.push({label: classInfo.内容, value: classInfo.ID})
                    })
                }
                return returnValue;
            }
        }

    },
    'reason.$.picture': {
        type: [String],
        label: "上传图片",
        optional: true
    },
    'reason.$.picture.$': {
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'QCImage',
                accept: 'image/*',
                label: '浏览'
            }
        }
    }, createAt: {
        type: Date,
        autoValue: function () {
            return new Date();
        }
    },
    createBy: {
        type: String,
        autoValue: function () {
            return Meteor.userId();
        }
    },
    checkId: {
        type: String,
        autoValue: function () {
            return Router.current().params.id;
        }
    }
});
correctiveReason.attachSchema(
    Schemas.workSiteRepairInfo
);

AutoForm.hooks({
    correctiveReasonInsert: {
        onSubmit: function (insertDoc, updateDoc, currentDoc) {
        },

        // Called when any submit operation succeeds
        onSuccess: function (formType, result) {
            QcTemporary.insert({
                checkId: Router.current().params.id,
                reasonId: result
            });
            if (Session.get("isFinish")) {
                Session.set("isFinish", false);
                var id = Router.current().params.id;

                var checkId = Router.current().params.id;
                var acnumber = Router.current().params.acnumber;
                console.log(checkId, acnumber);

                Meteor.call("IssueRectificationList", checkId, acnumber, Meteor.userId(), function (err, result) {
                    if (!err) {
                        Session.set("workSiteRepairType", null);
                    }
                });
                Router.go("/notExaminedList");
            }
        },

        // Called when any submit operation fails
        onError: function (formType, error) {
            console.log(formType, error);
        },
    }
});