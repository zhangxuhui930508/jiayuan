/**
 * Created by zhangxuhui on 16/4/11.
 */
var selectTabsTopKey = "selectTabsToJsonKey";

Template.messageInfo.rendered = function () {
    Session.set(selectTabsTopKey, "unread");
    var ACNumber;
    if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
        ACNumber = Session.get("customerLogin");
    } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "test", "designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptainBoss"], "user-group")) {
        ACNumber = Session.get("customerDecorateInfo").ACNumber;
    }

    this.autorun(function () {
        Meteor.subscribe("chatMessageCenter", Meteor.userId(), Session.get(selectTabsTopKey), ACNumber, function (err, result) {
            if (!err) {
                var messageInfo = chatMessages.find().fetch();
                var roomArrs = [];
                _.each(messageInfo, function (info) {
                    roomArrs.push(info.roomId);
                });
                Meteor.call("searchMaterialName", roomArrs, function (err, result) {
                    if (!err) {
                        Session.set("searchMaterialNameValue", result);
                    }
                })
            }
        });

        // Meteor.subscribe("systemMessageInfo", ACNumber, Session.get(selectTabsTopKey));
    })

};

Template.messageInfo.helpers({
    headObj: function () {
        return {
            title: '消息中心',
            leftButton: true,
            // backTemplate: 'ContractListType'
        }
    },
    'messageInfo': function () {
        var info = chatMessages.find({}, {sort: {createDate: -1}}).fetch();
        if (info) {
            return info;
        }
    },
    isActive: function (val) {
        var select = Session.get(selectTabsTopKey);
        if (select && select == val) {
            return "active";
        }
    },
    'getMaterialName': function (roomId) {
        try {
            if (roomId) {
                if (Session.get("searchMaterialNameValue")) {
                    return Session.get("searchMaterialNameValue")[roomId];
                }
            }
        } catch (e) {

        }
    },
    'systemMessageInfo': function () {
        return systemMessage.find();
    }
});

Template.messageInfo.events({
    'click .chatMessage': function (event) {
        var roomId = $(event.currentTarget).children("span").attr("class").trim();
        Router.go("/chat/roomId/" + roomId + '/materialId/messageCenter');
    },
    'click .backMenuList': function (event) {
        Router.go("/mycustomer");
    },
    "click .tab-item": function (event) {
        var val = $(event.currentTarget).attr("value");
        Session.set(selectTabsTopKey, val.trim());
    }
});