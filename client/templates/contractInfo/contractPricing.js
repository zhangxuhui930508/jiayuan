/**
 * Created by zhangxuhui on 16/4/27.
 */

Template.contractPricing.rendered = function () {
    Session.set("contractPricing", "undeliverGoods");
    var self = this;
    IonLoading.show();
    this.autorun(function () {
        self.subscribe("publishContractPricingInfo", Session.get("contractPricing"), function () {
            IonLoading.hide();
        });
    })

};


Template.contractPricing.helpers({
    headObj: function () {
        return {
            title: '核价',
            // leftButton: true,
            // backTemplate: 'proceedOrderType'
        }
    },
    isActive: function (val) {
        var select = Session.get("contractPricing");
        if (select && select == val) {
            return "active";
        }
    },
    'erpInfoSupplierInfo': function () {
        return erpInfoSupplier.find({}, {sort: {ID: 1}});
    },
    supplierCount: function (id) {
        return ERP_Budget_Material.find({供货商ID: id}).count();
    }
});

Template.contractPricing.events({
    "click .tab-item": function (event) {
        var value = $(event.currentTarget).attr("value");
        Session.set("contractPricing", value);
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/contractDeptLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .item': function () {
        console.log(this.ID);
        Session.set("contractBackTemplate", "contractPricing");
        ///contractMaterailInfo/id/:id/type/:type
        // Router.go("/contractMaterailInfo/id/" + this.ID + "/type/" + Session.get("contractPricing"));
        var info = erpInfoSupplier.findOne({ID: this.ID});
        console.log("-----------------------------------", info);

        if (info && info.putInStorageType == "whole") {
            //    查看单项入库还是整体入库
            var type;
            if (Session.get("contractPricing") == "todaydeliverGoods") {
                type = 4;
            } else if (Session.get("contractPricing") == "undeliverGoods") {
                type = 3;
            }
            Router.go("/zhengtihejiaList/id/" + this.ID + '/type/' + type);

        } else {
            var type;
            if (Session.get("contractPricing") == "todaydeliverGoods") {
                type = 4;
            } else if (Session.get("contractPricing") == "undeliverGoods") {
                type = 3;
            }
            Router.go("/danxianghejiaList/id/" + this.ID + "/type/" + type);
        }

    }

});