/**
 * Created by zhangxuhui on 16/4/29.
 */

Template.suspendOrderList.onRendered(function () {
    Session.set("currentOrderSearchVal", null);
    this.subscribe("publishMeasureList", Meteor.userId(), 13, function (err, result) {
        var info = ERP_Budget_Material.find().fetch();
        console.log(info);
        var arr = [];
        _.each(info, function (erpInfo) {
            arr.push(erpInfo.受理编号);
        });
        if (arr.length > 0) {
            Meteor.call("getCustomerNameInfo", arr, function (err, result) {
                console.log(err, result);
                Session.set("customerNameAndAddress", result);
                IonLoading.hide();
            })
        }
    })
});

Template.suspendOrderList.helpers({
    headObj: function () {
        return {
            title: '退回订单',
            leftButton: true,
            backTemplate: 'orderType'
        }
    },
    'otherProceedList': function () {
        var select = {};
        if (Session.get("currentOrderSearchVal")) {
            select.受理编号 = new RegExp(Session.get("currentOrderSearchVal"), "i");
        }
        var info = ERP_Budget_Material.find(select);
        if (info) {
            return info;
        }
    },
    'getCustomerName': function (acnumber) {
        try {
            return Session.get("customerNameAndAddress")[acnumber].CustName;
        } catch (e) {

        }

    },
    'getCustomerAddress': function (acnumber) {
        try {
            return Session.get("customerNameAndAddress")[acnumber].Address;
        } catch (e) {

        }
    }
});

Template.suspendOrderList.events({
    'click .gocustomerDetail': function (event) {
        var id = $(event.currentTarget).children("span").attr("class").trim();
        console.log("id:", id);
        Router.go("/scheduleList/" + id);
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();

        Session.set("currentOrderSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentOrderSearchVal", null);
        $(".search").val("");
    }
})


