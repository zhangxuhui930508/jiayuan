/**
 * Created by zhangxuhui on 15/11/10.
 */
Template.customerParts.rendered = function () {
    //Session.set("classifyback", "DecorateArea");
}

Template.customerParts.helpers({
    'headObj': function () {
        return {
            title: '选择部位后选材',
            //leftButton: true,
            //backTemplate: 'mycustomer'
        }
    },
    'Selectparts': function () {
        return [
            {parts: "主卧"},
            {parts: "次卧"},
            {parts: "客卧"},
            {parts: "主卫"},
            {parts: "次卫"},
            {parts: "厨房"},
            {parts: "厨房（二扇门）"},
            {parts: "阳台"},
            {parts: "工作阳台"},
            {parts: "进户门"},
            {parts: "保姆房"},
            {parts: "主卧阳台"},
            {parts: "客卧阳台"},
            {parts: "客厅阳台"},
            {parts: "客厅"},
            {parts: "餐厅"},
            {parts: "过道"},
            {parts: "书房"},
            {parts: "衣帽间"},
            {parts: "老人房"},
            {parts: "储藏间"},
            {parts: "盥洗间"}
        ]
    }
});

Template.customerParts.events({
    'click .item': function (event) {
        var parts = $(event.currentTarget).text().trim();
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        customerDecorateInfo.parts = parts;
        Session.set("customerDecorateInfo", customerDecorateInfo);


        //var parts = $(event.currentTarget).text().trim();
        //Session.set("Decoration_parts", parts);
        var itemval = Session.get("customerSelectMaterial");
        switch (itemval) {
            case "materialClassify01":
                Router.go("/materialClassify01");
                break;
            case "materialClassify02":
                Router.go("/materialClassify02");
                break;
            default :
                Router.go("/materialClassify01");
                break;
        }
    },
    'click .backTemplates': function (event) {
        Router.go("/mycustomer");


        //var back = Session.get("backtemplate");
        //console.log("==========back=============", back);
        //switch (back) {
        //    case "MaterialClassify1":
        //        Router.go("/MaterialClassify");
        //        break;
        //    case "MaterialClassify2":
        //        Router.go("/MaterialClassify2");
        //        break;
        //    case "MaterialClassify3":
        //        Router.go("/MaterialClassify3");
        //        break;
        //    case "MaterialClassify4":
        //        Router.go("/MaterialClassify4");
        //        break;
        //    case "customerDetail":
        //        Router.go("/customerDetail/" + Session.get("currentCustomerID"));
        //        break;
        //    case "customerindex":
        //        Router.go("/customerindex");
        //        break;
        //    case "mycustomer":
        //        Router.go("/mycustomer");
        //        break;
        //    default:
        //    {
        //        if (!Session.get("currentCustomerID")) {
        //            Router.go("/customerindex");
        //            break;
        //        }else{
        //            Router.go("/customerDetail/" + Session.get("currentCustomerID"));
        //        }
        //    }
        //    //default :Router.go("/customerDetail/"+Session.get("currentCustomerID"));break;
        //}
    }
});