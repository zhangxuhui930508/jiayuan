/**
 * Created by zhangxuhui on 16/2/14.
 */
Template.construtionList.onRendered(function () {
    IonLoading.show();
    var type = Router.current().params.type;
    var id = Session.get("currentConstructionId");
    var cId;
    if (id) cId = id.toString();

    this.subscribe("construtionWorkCustomer", cId, type, function () {
        IonLoading.hide();
    });
});

Template.construtionList.helpers({
    headObj: function () {
        return {
            leftButton: true,
            backTemplate: 'constructionCType'
        }
    },
    'titleName': function () {
        var title = Router.current().params.type;
        if (title) {
            return title;
        }
    },
    construtionListInfo: function () {
        var info = Session.get("currentCustomerSearchVal");
        var select = {};
        if (info) {
            select = {$or: [{ACNumber: new RegExp(info, "i")}, {CustName: new RegExp(info, "i")}]};
            // select.CustName = new RegExp(info, "i");
        }
        var customerInfo = Cust_Info.find(select, {sort: {CreatTime: -1}});
        if (customerInfo) {
            return customerInfo;
        }
    },
    getStoreInfo: function (branch) {
        var info = acSet.findOne({ACType: "营业网点", ACNumb: branch});
        if (info) {
            return info.ACName;
        }
    },
    'getCustomerMode': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info && info.BusiType == "整体翻新") {
            return "#0000CD"
        } else if (info && info.BusiType == "局部翻新") {
            return "#000000";
        }
    },
});

Template.construtionList.events({
    'click .item .gocustomerDetail': function (event) {
        var cid = $(event.currentTarget).children("h2").attr("id").trim();
        var type = Router.current().params.type;
        Router.go("/customerDetail/customerType/" + type + '/customerId/' + cid);
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
        console.log("-------------------------------------");
    }
});