/**
 * Created by chendongdong on 16/5/27.
 */
/**
 * 时间转换为指定格式
 * time :new Date()  format : YYYY-MM-DD || YYYY-MM-DD hh:mm:ss|| YYYY-MM-DD HH:mm:ss
 */
Template.registerHelper("DataFormat", function (time, format) {
    if (time) {
        return moment(time).format(format);
    }
});