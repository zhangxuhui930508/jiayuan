/**
 * Created by zhangxuhui on 16/7/13.
 */



Template.danxianghejiaDetail.helpers({
    headObj: function () {
        return {
            // title: '核价详情',
            // leftButton: true,
            // backTemplate: 'proceedOrderType'
        }
    },
    'titleName': function () {
        return "核价审核详情"
    },
    imageInfo: function () {
        var info = SupplierImages.find();
        if (info) {
            return info
        }
    },
    'budgetMaterialInfo': function () {
        var currentid = Router.current().params.id;
        var info = ERP_Budget_Material.findOne({_id: currentid});
        return info;
    },
    'supplierName': function () {
        return erpInfoSupplier.findOne();
    },
    putInStorageDetailInfo: function () {
        var currentid = Router.current().params.id;
        var info = ERP_Budget_Material.findOne({_id: currentid});
        if (info && info.orderState == 3) {
            return true;
        }
        return false;
    }

});

Template.danxianghejiaDetail.events({
    'click .approved': function () {
        //    审核通过
        IonPopup.confirm({
            title: '温馨提示',
            template: "确认审核通过?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var id = Router.current().params.id;
                Meteor.call("checkOrderInfo", id, Meteor.userId(), 4, function (err, result) {
                    if (!err) {
                        Router.go("/contractPricing")
                    } else {
                        console.log(err);
                    }
                });
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .notApproved': function () {

        $(".reasonRemark").css("display", "");
        console.log($(".textareaReason").val());
        var textarea = $(".textareaReason").val().trim();
        if (!textarea) {
            IonLoading.show({
                customTemplate: "<h4>请输入审核不通过的理由!</h4>",
                duration: 1000
            });
            return;

        }
        IonPopup.confirm({
            title: '温馨提示',
            template: "确认审核不通过?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var id = Router.current().params.id;
                Meteor.call("checkOrderInfo", id, Meteor.userId(), 5, function (err, result) {
                    if (!err) {
                        Router.go("/contractPricing")
                    }
                });
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .backMenuList': function () {
        Router.go("/contractPricing")
    }
});