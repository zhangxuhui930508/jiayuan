/**
 * Created by pcbeta on 15-8-4.
 */

Template.modeSelect.helpers({
    headObj: function () {
        return {
            title: '选择装修模式',
            leftButton: true,
         }
    },

});

Template.modeSelect.events({
    'click .Local_renovation': function (event) {
        var mode = $(event.currentTarget).text().trim();
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        customerDecorateInfo.mode = mode;
        Session.set("customerDecorateInfo", customerDecorateInfo);


    }
});
