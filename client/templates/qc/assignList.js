/**
 * Created by zhangxuhui on 16/4/17.
 */

Template.assignList.onRendered(function () {
    Meteor.call("qcIsCheckInfo", Router.current().params.id)
})
Template.assignList.helpers({
    headObj: function () {
        return {
            title: '客户基本信息',
            leftButton: true,
            backTemplate: 'assignQC'
        }
    },
    Info: function () {
        return Cust_Info.findOne();
    },
    employeeInfo: function () {
        return AC_Personnel.find();
    },
    'checkProject': function () {
        var info = checkConstructionProject.findOne();
        if (info) {
            var value = info.project;
            if (value) {
                switch (value) {
                    case "completion":
                        return "竣工报检";
                        break;
                    case "hydropower":
                        return "水电报检";
                        break;
                    case "mudWood":
                        return "泥木报检";
                        break;
                }
            }
        }
    },
    assignerDate: function () {
        var info = checkConstructionProject.findOne();
        if (info && info.checkDate) {
            return info.checkDate;
        }
    }
});

Template.assignList.events({
    'click .assigner': function (event) {
        var checkId = Router.current().params.id;
        var qc = $(".qcList option:selected").val().trim();
        var assignDate = $(".assignDate").val().trim();
        console.log(checkId, qc, assignDate);
        if (!qc) {
            IonLoading.show({
                customTemplate: "<h4>请选择分配质检人员!</h4>",
                duration: 1000
            });
            return;
        }
        if (!assignDate) {
            IonLoading.show({
                customTemplate: "<h4>请设置质检时间!</h4>",
                duration: 1000
            });
            return;
        }
        var assignerInfo = $(".qcList option:selected").text().trim();
        var customerName = $(".custnameInfo").text();
        IonPopup.confirm({
            title: '温馨提示',
            template: "将客户" + customerName + "分配给质检人员" + assignerInfo + "?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                Meteor.call("assignQCInfo", checkId, qc, assignDate, Meteor.userId(), function (err, result) {
                    if (!err) {
                        Router.go("/assignQC");
                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>分配失败!</h4>",
                            duration: 1000
                        });
                    }
                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });


    }
});
