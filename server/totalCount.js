/**
 * Created by zhangxuhui on 15/12/16.
 */
Meteor.methods({

    'customerInfoTotal': function (designerId, cType) {
        var CustomerInfo = DesignerRelationClient.find({dId: designerId, cType: cType}).count();
        return CustomerInfo;
    },
    supplierMaterialCount: function (createDate, endDate) {
        console.log(createDate, endDate);
        var supplierInfo = Meteor.users.findOne({_id: this.userId});
        if (supplierInfo) {
            console.log(supplierInfo.ID);
            var erpViewOrderAll = erpViewVerifyOrderAll.find({
                供货商编号: supplierInfo.ID,
                预警确认时间: {
                    $gte: createDate,
                    $lte: endDate,
                }
            });
            var totalPrice = 0;
            var total8 = 0;
            var total12 = 0;
            var total8Count = 0;
            var total12Count = 0;

            var arr = [];
            var arr2 = [];

            erpViewOrderAll.forEach(function (info) {
                var orderState8 = ERP_Budget_Material.findOne({
                    受理编号: info.受理编号,
                    定单编号: info.定单号,
                    orderState: 8,
                    队长确认: true
                });
                if (orderState8) {
                    arr.push(orderState8._id);
                    total8Count = total8Count + 1;
                    total8 += info.总价;
                }
                var orderState12 = ERP_Budget_Material.findOne({
                    受理编号: info.受理编号,
                    定单编号: info.定单号,
                    orderState: 12,
                    队长确认: true
                });
                if (orderState12) {
                    arr2.push(orderState12._id);
                    total12Count = total12Count + 1;
                    total12 += info.总价;
                    console.log(info._id);
                }
            });
            var select = {};
            select.total8 = total8;
            select.total8Count = total8Count;
            select.total12 = total12;
            select.total12Count = total12Count;
            select.arr = arr;
            select.arr2 = arr2;
            console.log(arr)
            console.log("======================")
            console.log(arr2)
            console.log(select, "select");
            return select;

        }
    },
    'getOrderListTotalPrice': function (prior) {
        console.log(prior, "===============================", this.userId);

        var date = prior.getFullYear() + "-" + (prior.getMonth() + 1) + "-1";
        var totalPrice = 0;
        console.log(date);
        console.log(Order.find({"personnel": this.userId, createDate: getMonthPrior(date)}).count());
        Order.find({"personnel": this.userId, createDate: getMonthPrior(date)}).forEach(function (info) {
            totalPrice += info.price;
        });
        return totalPrice;
    }
});