/**
 * Created by zhangxuhui on 16/4/27.
 */

Template.proceedOrderType.onRendered(function () {
    Meteor.call("getProceedOrderTypeCount", Meteor.userId(), function (err, result) {
        console.log(err, result);
        Session.set("proceedOrderTypeCount", result);
    })
});

Template.proceedOrderType.helpers({
    headObj: function () {
        return {
            title: '进行中的订单',
            leftButton: true,
            backTemplate: 'orderType'
        }
    },
    infoCount: function () {
        try {
            return Session.get("proceedOrderTypeCount");
        } catch (e) {

        }
    },
});

Template.proceedOrderType.events({
    'click .quantityMeasure': function () {
        var info = erpInfoSupplier.findOne();
        if (info && info.putInStorageType == "whole") {
            Router.go("/measureListByOrder");
        } else {
            Router.go("/measureList");
        }
    },
    'click .deliverGoods': function () {
        var info = erpInfoSupplier.findOne();
        if (info && info.putInStorageType == "whole") {
            Router.go("/deviceGoodsByOrder");
        } else {
            Router.go("/deliverGoodsList");
        }
    },
    'click .otherOrder': function (event) {
        var info = erpInfoSupplier.findOne();
        var type = $(event.currentTarget).children("span").attr("class").trim();
        console.log(type);
        if (info && info.putInStorageType == "whole") {
            Router.go("/getOrderProceedList/" + type);
        } else {
            Router.go("/otherProceedOrder/" + type);
        }
    }

});