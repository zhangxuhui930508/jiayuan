/**
 * Created by zhangxuhui on 16/4/25.
 */

Template.contractDeptLogin.events({
    'click .account-button': function (event, template) {
        var userName = template.$('[id=username]').val().replace(/\s+/g, "");
        console.log("userName:" + userName);
        var password = template.$('[id=pwd]').val().replace(/\s+/g, "");
        console.log("password:" + password);
        //让return起作用
        event.preventDefault();

        Meteor.loginWithPassword(userName, password, function (error, result) {

            if (error) {
                console.log(error.message);
                IonLoading.show({
                    customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                    duration: 1000
                });
            } else {

                if (Roles.userIsInRole(Meteor.userId(), ['contractDept'], "user-group")) {
                    Session.set("loginRole", "contractDept");

                    Router.go("/contractPricing");
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                        duration: 1000
                    });
                }


            }
        })
    }
});
