/**
 * Created by zhangxuhui on 16/4/7.
 */
var SKIP = "skip";
var LIMIT = "limit";


Template.chat.onCreated(function () {
    Session.set("chatViewMaterial", null);
    Session.set("selectChatDate", null);
    Session.set("selectChatDateButton", true);
    var id = Router.current().params.id;
    this.subscribe("getSupplierName", Router.current().params.id);
    var template = this;
    Session.set(LIMIT, 15);
    template.initializing = true;
    template.autorun(function () {
        template.subscribing = true;
        Session.set("loading", true);
        var select = Session.get("selectChatDate");
        template.subscribe("chatMessagesInfo", id, select, Session.get(LIMIT), function () {
            Session.set("loading", false);
            // var userArr = [];
            // var info = chatMessages.find().fetch();
            // _.each(info, function (messageInfo) {
            //     userArr.push(messageInfo.sender);
            // });
            // Meteor.call("getuserRole", userArr, function (err, result) {
            //     console.log(result);
            //     Session.set("currentUserRoles", result);
            // })
        });
    });
});


Template.chat.rendered = function () {

    var template = this;
    Session.set('sending', false);
    readChatMessages(); //读消息

    template.endScroll = true;
    this.autorun(function () {
        if (template.subscriptionsReady()) {
            template.subscribing = false;
            template.$('.overflow-scroll').scrollTop(template.$('#chatList')[0].scrollHeight - template.scroll)
        } else {
            template.subscribing = true;
            if (template.initializing)
                Meteor.setTimeout(function () {
                    template.initializing = false;
                    scrollToBottom(template);
                });
        }
    });

    $(window).on('sendChatMessage', function (e, id, doc) {
        if (template.endScroll) {
            scrollToBottom(template);
        }
    });
};

Template.chat.helpers({
    messages: function () {
        var template = Template.instance();
        var chatMessagesinfo = chatMessages.find({}, {sort: {createDate: 1}});
        if (chatMessagesinfo) {
            chatMessagesinfo.observeChanges({
                added: function (id, doc) {
                    if (!template.subscribing) {
                        $(window).trigger('sendChatMessage', [id, doc]);
                    }
                }
            });
            return chatMessagesinfo;
        }
    },
    selfSend: function (uid) {
        return Meteor.userId() == uid;
    },
    sendDisabled: function () {
        return Session.get("sending") ? "disabled" : "";
    },
    loading: function () {
        return Session.get("loading");
    },
    'titleName': function () {
        var name;
        var info = erpInfoSupplier.findOne();
        if (info) {
            name = info.名称;
        }
        return "材料对话-" + name;
    },
    //
    // 'rolesInfo': function (info) {
    //     // return Session.get("currentUserRoles")[info].name;
    //     console.log("===========================================")
    //     if (Roles.userIsInRole(info, ['test'], 'user-group')) {
    //         return '测试帐号';
    //     } else if (Roles.userIsInRole(info, ['designer'], 'user-group')) {
    //         return '设计师';
    //     } else if (Roles.userIsInRole(info, ['client'], 'user-group')) {
    //         return '客户';
    //     } else if (Roles.userIsInRole(info, ['ConstructionCaptain', "ConstructionCaptainBoss"], 'user-group')) {
    //         return '施工队长';
    //     } else if (Roles.userIsInRole(info, ['vicePresident'], 'user-group')) {
    //         return '副总经理';
    //     } else if (Roles.userIsInRole(info, ['districtManager'], 'user-group')) {
    //         return '大区经理';
    //     } else if (Roles.userIsInRole(info, ['StoreManager'], 'user-group')) {
    //         return '店长';
    //     } else if (Roles.userIsInRole(info, ['Supplier'], 'user-group')) {
    //         return '供应商';
    //     }
    // },
    // rolesInfoIcon: function (info) {
    //     console.log(info);
    //     return Session.get("currentUserRoles")[info].icon;
    //
    //     if (Roles.userIsInRole(info, ['test', 'vicePresident', 'districtManager', 'StoreManager', 'designer'], 'user-group')) {
    //         return "/image/designer.jpg";
    //     } else if (Roles.userIsInRole(info, ['client'], 'user-group')) {
    //         return "/image/57875.gif"
    //     } else if (Roles.userIsInRole(info, ['ConstructionCaptain'], 'user-group')) {
    //         return "/image/26103129861.jpg"
    //     } else if (Roles.userIsInRole(info, ['Supplier'], 'user-group')) {
    //         return "/image/supplierIcon.jpg";
    //     }
    // },
    more: function () {
        return Session.get(LIMIT) == chatMessages.find({}).count();
    },
    selectDateOrAll: function () {
        return Session.get("selectChatDateButton");
    }
});

Template.chat.events({
    "click .send": function (event, tempalte) {
        var id = Router.current().params.id;
        var message = tempalte.$(".saySomething").val();
        if (Session.get('sending') && message && message != 0 && !id) {
            return;
        }
        Session.set('sending', true);
        scrollToBottom(tempalte);
        Meteor.call("sendChatMessage", message, Meteor.userId(), id, Session.get("materialName"), Meteor.userId(), function () {
            tempalte.$(".saySomething").val("");
            Session.set('sending', false);
        });
    },
    "click .pull-left": function () {
        var mid = Router.current().params.mid;
        readChatMessages();
        if (mid == "messageCenter") {
            Router.go("/messageInfo");
        } else {
            history.back();
            // Router.go("/StandardMaterialDetail/" + mid);
        }
    },
    "scroll .overflow-scroll": function (event, template) {
        var top = event.currentTarget.scrollTop;
        var scrollHeight = event.currentTarget.scrollHeight;
        var offsetHeight = event.currentTarget.offsetHeight;
        if (scrollHeight - top - offsetHeight < 10) {
            template.endScroll = true;
        } else {
            template.endScroll = false;
        }
    },
    "click .loadMore": function (event, template) {
        if (!Session.get("loading")) {
            Session.set("loading", true);
            Session.set(LIMIT, Session.get(LIMIT) + 20);
            template.scroll = template.$('#chatList')[0].scrollHeight;
            template.$(".overflow-scroll").animate({scrollTop: 0}, 0);
            template.$('.overflow-scroll').trigger('scroll')
        }
    },
    'click .chatItem': function () {
        console.log(this._id);
        console.log(chatMessages.findOne({_id: this._id}));
        var info = chatMessages.findOne({_id: this._id})
        var username = info.userInfo.name;
        IonPopup.confirm({
            title: info.createDate.Format("yyyy-MM-dd"),
            template: info.userInfo.name + ":" + info.message,
            okText: '查看材料',
            cancelText: "取消",
            onOk: function () {
                var roomInfo = roomMessage.findOne({_id: Router.current().params.id});
                Session.set("chatViewMaterial", true);
                Router.go("/StandardMaterialDetail/" + roomInfo.materialId);
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .selectDate': function (event) {
        IonPopup.prompt({
            title: '设置搜索时间',
            template: '请设置搜索时间',
            okText: '确定',
            cancelText: '取消',
            inputType: 'date',
            onOk: function () {
                var newDate = $('.popup-body input').val().trim();
                Session.set("selectChatDate", newDate);
                Session.set("selectChatDateButton", false);
            }
        });
    },
    'click .selectAll': function () {
        Session.set("selectChatDate", null);
        Session.set("selectChatDateButton", true);
    }
});


function scrollToBottom(template) {
    console.log("bottom");
    $(".overflow-scroll").animate({scrollTop: $("#chatList").height()}, 30);
    $(".overflow-scroll").trigger('scroll');
    return;
}


Template.chat.destroyed = function () {
    Session.set(LIMIT, null);
};

Template.chat.onDestroyed(function () {
    readChatMessages();
});

function readChatMessages() {
    var roomid = Router.current().params.id;
    Meteor.call("updateChatMessageState", Meteor.userId(), roomid, function (err, result) {
        if (result = 0) {
            IonLoading.show({
                customTemplate: "<h4>更新失败!</h4>",
                duration: 1000
            });
        }
    })
}

Template.chat.onDestroyed(function () {
    Session.set("selectChatDate", null);

});