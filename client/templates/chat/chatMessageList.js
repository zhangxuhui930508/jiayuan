/**
 * Created by zhangxuhui on 16/4/21.
 */
Template.chatMessageList.onRendered(function () {
    var messageState = Router.current().params.messageState;
    this.subscribe("chatMessageListInfo", messageState, function () {
    })
});

Template.chatMessageList.helpers({
    headObj: function () {
        return {
            title: '消息列表',
            leftButton: true,
            backTemplate: 'materialChatList'
        }
    },
    'messageInfo': function () {
        var info = chatMessages.find();
        if (info) {
            return info;
        }
    },
});

Template.chatMessageList.events({
    'click .gocustomerDetail': function (event) {
        var roomId = $(event.currentTarget).children("h2").attr("class").trim();
        console.log(this.roomId);
        if (roomId) {
            var roomInfo = roomMessage.findOne({_id: roomId});
            if (roomInfo) {
                var acnumber = {};
                acnumber.ACNumber = roomInfo.clientId;
                Session.set("customerDecorateInfo", acnumber);
                Router.go("/chat/roomId/" + roomInfo._id + "/materialId/" + roomInfo.materialId);
            }
        }
    }
})