/**
 * Created by chendongdong on 16/4/7.
 */
var startTouche;
var lock;
var option = {};
HeytzSlideButton = {
    MinimumDistance: 5,
    buttonWidth: 50,
    /**
     *
     * @param option
        {
              editText:"", default "编辑"
              editClass:"", default ""
              editCallback:function(event,template){},
              delText:"", default "删除"
              delClass:"", default ""
              delCallback:function(event,template){},
              
              editModelName:"",
         }
     */
    init: function (o) {
        option = o;
    }
};
var events = {
    'touchstart .heytz-del-row': function (event) {
        var touchesStart = event.originalEvent.touches;
        if (touchesStart.length == 1) {
            lock = false;
            startTouche = {pageX: touchesStart[0].pageX, pageY: touchesStart[0].pageY};
        }

    },
    'touchmove  .heytz-del-row': function (event) {
        event.stopPropagation();
        if (lock)return;
        var touches = event.originalEvent.changedTouches;
        var itemDel = $(event.currentTarget).children("div.heytz-right-buttons");
        var spanCount = itemDel.children("span").length;
        var item = $(event.currentTarget).children("div.item");
        if (startTouche) {
            lock = true;
            if ((startTouche.pageX - touches[0].pageX) > HeytzSlideButton.MinimumDistance) {
                if (itemDel.css("display") == "none") {
                    itemDel.css("display", "block");
                    itemDel.animate({"width": (HeytzSlideButton.buttonWidth * spanCount) + "px"}, 300,
                        function () {
                        }
                    );
                    itemDel.children("span").animate({"width": HeytzSlideButton.buttonWidth + "px"}, 300,
                        function () {
                        }
                    );
                    item.animate({"margin-left": "-" + (HeytzSlideButton.buttonWidth * spanCount) + "px"}, 300);
                }
            } else {
                if (itemDel.css("display") == "block") {
                    itemDel.animate({"width": "0px"}, 300,
                        function () {
                            itemDel.removeAttr("style");
                        });
                    itemDel.children("span").animate({"width": "0px"}, 300,
                        function () {
                        }
                    );
                    item.animate({"margin-left": "0px"}, 300);
                }
            }
        }
    },
    "touchend .heytz-del-row": function (event) {
        // console.log("touchend", event);
    },
    "click .heytz-edit-button": function (event, template) {
        if (option && option.editCallback) {
            var self = this;
            option.editCallback(event, template, self);
        }
    },
    "click .heytz-del-button": function (event, template) {
        if (option && option.delCallback) {
            option.delCallback(event, template, this);
        }
    }
};
var helpers = {};
Template.HeytzSlideButton.helpers(helpers);
Template.HeytzSlideButton.events(events);
Template["HeytzDelButton"].helpers({
    delText: function () {
        return option.delText ? option.delText : "删除";
    },
    delClass: function () {
        return option.delClass ? option.delClass : "";
    },
});
Template["HeytzEditButton"].helpers({
    editText: function () {
        return option.editText ? option.editText : "编辑";
    },
    editClass: function () {
        return option.editClass ? option.editClass : "";
    },
    editModelName: function () {
        return option.editModelName ? option.editModelName : "";
    }
});