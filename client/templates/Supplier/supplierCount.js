/**
 * Created by zhangxuhui on 16/4/26.
 */


var selectitems = "selectTabsTopKey";

Template.supplierCount.onRendered(function () {
    Session.set("supplierTotalCount", null);

    if (!Session.get("datePickerSelectTime")) {
        Session.set("datePickerSelectTime", new Date());
    }

    // this.autorun(function () {
    //     Meteor.subscribe("publishSupplierCount", Meteor.userId(), Session.get(selectitems), function (err, result) {
    //         var budgetMaterial = ERP_Budget_Material.find().fetch();
    //         var acnumberArr = [];
    //         _.each(budgetMaterial, function (info) {
    //             acnumberArr.push(info.受理编号);
    //         })
    //         Meteor.call("getCustomerNameInfo", acnumberArr, function (err, result) {
    //             console.log(result);
    //             Session.set("SupplierCountGetCustomer", result);
    //         })
    //     })
    // }
    $('#date').pickadate({
        monthsFull: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        weekdaysShort: ['一', '二', '三', '四', '五', '六', '日'],
        today: '今天',
        clear: '清除',
        close: '关闭',
        format: 'yyyy-mm-dd',
        formatSubmit: 'yyyy-mm-dd',
        selectMonths: true,
        selectYears: true,
        onSet: function (context) {
            var nowSelectDate = $('[name=_submit]').val();
            console.log(nowSelectDate);
            console.log("==========================");
            Session.set("datePickerSelectTime", new Date(nowSelectDate));

            // Meteor.call("getOrderListTotalPrice", new Date(nowSelectDate), function (err, result) {
            //     Session.set("orderTotalPrice", result);
            // })
        }
    });
    // Meteor.call("supplierMaterialCount", new Date(moment(moment()).format("YYYY-01-01")), new Date(), function (err, result) {
    //     Session.set("yearTotalCount", result);
    // })

    IonLoading.show()
    Meteor.call("supplierMaterialCount", new Date(moment(moment().add(-1, 'months')).format("YYYY-MM-26")), new Date(), function (err, result) {
        console.log(result, err);
        IonLoading.hide()
        Session.set("supplierTotalCount", result);
    });
});


Template.supplierCount.helpers({
    headObj: function () {
        return {
            title: '本月统计',
            // leftButton: true,
            // backTemplate: 'customerType'
        }
    },
    totalResult: function () {
        return Session.get("supplierTotalCount");
    },
    'selectDate': function () {
        var timeDate = new Date(Session.get("datePickerSelectTime"));
        var time3 = moment(moment(timeDate).add(-1, 'months')).format("YYYY-MM-26");
        console.log(time3, "time3");
        var time4 = moment(moment(timeDate)).format("YYYY-MM-DD");
        console.log(time4, "time4");
        Session.set("countDate", {createDate: new Date(time3), endDate: new Date(time4)});
        return time3 + "~" + time4
    },
    // yearTotalCount:function () {
    //     return Session.get("yearTotalCount");
    // }
});


Template.supplierCount.events({
    'click .goDetail8': function () {
        Router.go("/countDetail/arr");
    },
    'click .goDetail12': function () {
        Router.go("/countDetail/arr2");
    },

    'click .search': function () {
        IonLoading.show()
        Meteor.call("supplierMaterialCount", Session.get("countDate").createDate, Session.get("countDate").endDate, function (err, result) {
            console.log(result, err);
            IonLoading.hide();
            Session.set("supplierTotalCount", result);
        });
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/supplierLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },

});

Template.supplierCount.onDestroyed(function () {
    // Session.set("supplierTotalCount", null);
})