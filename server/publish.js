/**
 * Created by pcbeta on 15-8-7.
 */

//材料分类  Material_Classify页面调用
Meteor.publish("MaterialClassifyList", function () {
    return Material_Classify_List.find();
});

//todo 客户首页使用的,代码需要查询优化
Meteor.publish("CustomerID", function (ACNumber) {
    return Cust_Info.find({ACNumber: ACNumber});
});
//客户详情界面调用
//返回客户的详情
Meteor.publish("custInfoDetail", function (_id) {
    var customerInfo = [];
    var info = Cust_Info.find({_id: _id});
    if (info.count() > 0) {
        customerInfo.push(info);
        var custInfo = Cust_Info.findOne({_id: _id});
        var DesignerRelase = DesignerRelationClient.findOne({cId: custInfo.ACNumber});
        customerInfo.push(AC_Personnel.find({ID: parseInt(DesignerRelase.dId)}));

    }
    return customerInfo;
});

//返回图片
Meteor.publish('images', function () {
    return Images.find();
});


//材料分类多余的六大类属性的分类名称查询
//查询出来,暂无使用,需要详细查询. todo
Meteor.publish("orderType", function (ordertypeid) {
    return OrderTypes.find({OrderType: ordertypeid});
});

//返回客户的类型
//
// Meteor.publish("currentCurrentCType", function (id) {
//     var custInfo = Cust_Info.findOne({_id: id});
//     if (custInfo) {
//         return DesignerRelationClient.find({cId: custInfo.ACNumber});
//     }
// })

Meteor.publish(null, function () {
    return Meteor.roles.find({})
});
//todo 客户首页调用  customerindex页面
Meteor.publish("publis_user", function (userId) {
    // check(userId, String);
    if (userId == this.userId) {
        return Meteor.users.find({_id: this.userId}, {fields: {services: 0}});
    }
});


//================CUstomer classify
//todo materialClassify02
Meteor.publish("customerClassify", function (Mark) {
    return CustomerMaterialclassify.find({移动标志: Mark});
});

Meteor.publish("customerClassifyall", function () {
    return CustomerMaterialclassify.find();
});


//==============合同材料列表
//todo 在StandardMaterial.js调用
Meteor.publish("ERP_Budget_Material", function (AcNumber, phase) {
    return ERP_Budget_Material.find({"受理编号": AcNumber, 阶段: phase}, {
        fields: {
            _id: 1,
            部位号: 1,
            材料品牌: 1,
            增减: 1,
            材料数量: 1,
            材料单位: 1,
            材料名称: 1,
        }
    });
});

Meteor.publish("erpBudgetMaterialInfo", function (acnumber, phase, type) {
    var materialDetail = [];
    console.log(type);

    var info = ERP_Budget_Material.find({"受理编号": acnumber, 阶段: phase, 定单分类: type}, {
        fields: {
            _id: 1,
            部位号: 1,
            材料品牌: 1,
            增减: 1,
            材料数量: 1,
            材料单位: 1,
            材料名称: 1,
            定单分类: 1
        }
    });
    materialDetail.push(info);
    var erpBudgetRationinfo = erpBudgetRation.find({受理编号: acnumber});
    materialDetail.push(erpBudgetRationinfo);
    if (info.count() > 0) {
        var materialIdArr = [];
        var partsIds = [];
        var roomIds = [];
        info.forEach(function (minfo) {
            materialIdArr.push(minfo._id);
            partsIds.push(minfo.部位号);
        });
        erpBudgetRationinfo.forEach(function (rinfo) {
            partsIds.push(rinfo.部位号);
        });
        var messageStateInfo = MessageState.find({uid: this.userId, state: "unread"});
        materialDetail.push(messageStateInfo);
        if (messageStateInfo.count() > 0) {
            messageStateInfo.forEach(function (info) {
                if (info.roomId) {
                    roomIds.push(info.roomId);
                    // materialDetail.push(roomMessage.find({_id: info.roomId}));
                }
            });
        }
        materialDetail.push(roomMessage.find({_id: {$in: roomIds}}));
        if (partsIds.length > 0) {
            materialDetail.push(erpBudgetLocation.find({受理编号: acnumber, 部位号: {$in: partsIds}}, {
                fields: {
                    部位名称: 1,
                    部位号: 1
                }
            }));
        }

    }
    return materialDetail;

});


//todo 在StandardMaterialDetail  router中调用
Meteor.publish("ERP_Budget_MaterialACNumber_ID", function (_id) {
    var budgetMaterialInfo = [];
    var info = ERP_Budget_Material.find({_id: _id});

    budgetMaterialInfo.push(info);
    if (info) {
        var erpInfo = ERP_Budget_Material.findOne({_id: _id});
        if (erpInfo && erpInfo.材料编号) {
            var materialInfo = MaterialDetailList.findOne({材料编号: erpInfo.材料编号});
            if (materialInfo && materialInfo.picture)
                budgetMaterialInfo.push(Images.find({_id: {$in: materialInfo.picture}}));
            budgetMaterialInfo.push(MaterialDetailList.find({材料编号: erpInfo.材料编号}));
        }
        if (erpInfo && erpInfo.受理编号 && erpInfo.定单编号) {
            var orderInfo = erpViewVerifyOrderAll.findOne({受理编号: erpInfo.受理编号, 定单号: erpInfo.定单编号});
            if (orderInfo && orderInfo.供货商编号) {
                budgetMaterialInfo.push(erpInfoSupplier.find({ID: orderInfo.供货商编号}));
            }
        }
    }
    return budgetMaterialInfo;
});

//todo 在StandardMaterial.js中调用,立项
Meteor.publish("ERP_Budget_Ration", function (ACNumber) {
    return erpBudgetRation.find({"受理编号": ACNumber}, {fields: {部位号: 1, 立项项目: 1}});
});

//====产品01-06

Meteor.publish("erpCustomizeMaterialInfo", function (acnumber) {
    var customizeMaterial = [];
    customizeMaterial.push(ERP_Product_Order.find({"受理编号": acnumber}));
    customizeMaterial.push(ERP_Product_Order01.find({"受理编号": acnumber}));
    customizeMaterial.push(ERP_Product_Order02.find({"受理编号": acnumber}));
    customizeMaterial.push(ERP_Product_Order03.find({"受理编号": acnumber}));
    customizeMaterial.push(ERP_Product_Order04.find({"受理编号": acnumber}));
    customizeMaterial.push(ERP_Product_Order05.find({"受理编号": acnumber}));
    customizeMaterial.push(ERP_Product_Order06.find({"受理编号": acnumber}));
    return customizeMaterial;
});

//todo 在product01Detail.js中调用
Meteor.publish("Product_Order01", function (id) {
    var productOrder = [];
    productOrder.push(ERP_Product_Order01.find({"_id": id}));
    var info = ERP_Product_Order01.findOne({_id: id});
    if (info) {
        productOrder.push(roomMessage.find({clientId: info.受理编号, materialId: id}))
    }
    return productOrder;
});

//todo 在product02Detail.js中调用
Meteor.publish("Product_Order02", function (id) {
    var productOrder = [];
    productOrder.push(ERP_Product_Order02.find({"_id": id}));
    var info = ERP_Product_Order02.findOne({_id: id});
    if (info) {
        productOrder.push(roomMessage.find({clientId: info.受理编号, materialId: id}))
    }
    return productOrder;
});

//todo 在product03Detail.js中调用
Meteor.publish("Product_Order03", function (id) {
    var productOrder = [];
    productOrder.push(ERP_Product_Order03.find({"_id": id}));
    var info = ERP_Product_Order03.findOne({_id: id});
    if (info) {
        productOrder.push(roomMessage.find({clientId: info.受理编号, materialId: id}))
    }
    return productOrder;
});

//todo 在product04Detail.js中调用
Meteor.publish("Product_Order04", function (id) {
    var productOrder = [];
    productOrder.push(ERP_Product_Order04.find({"_id": id}));
    var info = ERP_Product_Order04.findOne({_id: id});
    if (info) {
        productOrder.push(roomMessage.find({clientId: info.受理编号, materialId: id}))
    }
    return productOrder;
});
//todo 在product05Detail.js中调用
Meteor.publish("Product_Order05", function (id) {
    var productOrder = [];
    productOrder.push(ERP_Product_Order05.find({"_id": id}));
    var info = ERP_Product_Order05.findOne({_id: id});
    if (info) {
        productOrder.push(roomMessage.find({clientId: info.受理编号, materialId: id}))
    }
    return productOrder;

});

//todo 在product06Detail.js中调用
Meteor.publish("Product_Order06", function (id) {
    var productOrder = [];
    productOrder.push(ERP_Product_Order06.find({"_id": id}));
    var info = ERP_Product_Order06.findOne({_id: id});
    if (info) {
        productOrder.push(roomMessage.find({clientId: info.受理编号, materialId: id}))
    }
    return productOrder;
});

//todo 新闻消息列表
Meteor.publish("newsInfo", function () {
    return newsInfo.find();
});

//todo 消息详情
Meteor.publish("newsInfoId", function (id) {
    return newsInfo.find({_id: id});
});

//todo 宣传图片
Meteor.publish("AdvertisementPic", function () {
    return AdvertisementPic.find();
});

Meteor.publish("AdvertisementPicinfo", function () {
    var info = AdvertisementPic.findOne();
    var collections = [];
    collections.push(AdvertisementPic.find());
    if (info && info.picture) {
        collections.push(OtherImages.find({_id: {$in: info.picture}}));
    }
    return collections;
})

Meteor.publish("myCustomerPageInfo", function (acnumber) {
    var customerPage = [];
    // return erpRemoteFileStorage.find({受理编号: acnumber, 类型: type});
    var erpRemoteFileStorage1 = erpRemoteFileStorage.find({受理编号: acnumber});
    if (erpRemoteFileStorage1.count() > 0) {
        customerPage.push(erpRemoteFileStorage1);
    }
    // var erpRemoteFileStorage2 = erpRemoteFileStorage.find({受理编号: acnumber, 类型: 2});
    // if (erpRemoteFileStorage2.count() > 0) {
    //     customerPage.push(erpRemoteFileStorage2);
    // }
    // var erpRemoteFileStorage3 = erpRemoteFileStorage.find({受理编号: acnumber, 类型: 8});
    // if (erpRemoteFileStorage3.count() > 0) {
    //     customerPage.push(erpRemoteFileStorage3);
    // }
    var construtionRelationClientInfo = construtionRelationClient.find({cId: acnumber});
    if (construtionRelationClientInfo.count() > 0) {
        customerPage.push(construtionRelationClientInfo);
    }
    var DesignerRelationClientInfo = DesignerRelationClient.find({cId: acnumber});
    if (DesignerRelationClientInfo.count() > 0) {
        customerPage.push(DesignerRelationClientInfo);
    }
    var shippingCartInfo = ShoppingCart.find({ACNumber: acnumber});
    if (shippingCartInfo.count() > 0) {
        customerPage.push(shippingCartInfo);
    }
    var SolutionsInfo = Solutions.find({ACNumber: acnumber});
    if (SolutionsInfo.count() > 0) {
        customerPage.push(SolutionsInfo);
    }
    //系统消息和聊天回话消息统计
    var systemMessageInfo = systemMessage.find({acnumber: acnumber, "state": "unread"});
    if (systemMessageInfo) {
        customerPage.push(systemMessageInfo);
    }
    var messageStateInfo = MessageState.find({"state": "unread", uid: this.userId});
    //聊天消息
    if (messageStateInfo) {
        customerPage.push(messageStateInfo);
    }

    return customerPage;
});


//
// newsInfo.allow({
//     insert: function (userId, doc) {
//
//         return true;
//     },
//     update: function (userId, doc, fieldName, modifier) {
//
//         return true;
//     },
// });

// AdvertisementPic.allow({
//     insert: function (userId, doc) {
//
//         return true;
//     },
//     update: function (userId, doc, fieldName, modifier) {
//
//         return true;
//     },
// });

//todo 需要查询代码评价
Meteor.publish("Evaluation", function (acnumber) {
    return Evaluation.find({acnumber: acnumber});
});

// Evaluation.allow({
//     insert: function (userId, doc) {
//         return true;
//     },
//     update: function (userId, doc, fieldName, modifier) {
//
//         return true;
//     },
// });

//
////==============MaterialImage===================
//Meteor.publish("MaterialImage",function(ID){
//    return MaterialImage.find({materialID:id});
//});


//=============================2016-1-8=======================
//todo 在customerList.js中调用
Meteor.publish("designerByCustomerType", function (did, customerType) {
    var collections = [];
    var acnumbers = [];
    var relation = DesignerRelationClient.find({dId: did.toString(), cType: customerType});
    if (relation.count() > 0) {
        relation.forEach(function (relationInfo) {
            acnumbers.push(relationInfo.cId);
        });
    }
    return Cust_Info.find({ACNumber: {$in: acnumbers}}, {
        fields: {
            _id: 1,
            ACNumber: 1,
            CustName: 1,
            Address: 1
        }
    });
});

//
//todo 在updateprojDetai.js中调用
Meteor.publishComposite("MaterialimagesByIds", function (id) {
    return {
        find: function () {
            return MaterialDetailList.find({ID: id});
        },
        children: [
            {
                find: function (images) {
                    if (images.picture) {
                        return Images.find({_id: {$in: images.picture}})
                    }
                }
            }
        ]
    }
});

//todo 在updateprojDetai.js中调用
Meteor.publish("materialDetailInfoById", function (id) {
    return MaterialDetailList.find({ID: parseInt(id), 是否可用: true});
});
//todo 在 materialList.js的router中调用
Meteor.publish("materialListByclassifyId", function (id) {
    return MaterialDetailList.find({分类ID: parseInt(id), 是否可用: true, 结束日期: {$gte: new Date()}});
});


//publish images according to IDs
//todo 在customerindex.js中调用
Meteor.publish('imagesByIds', function (ids) {
    if (ids.length > 0) {
        return Images.find({_id: {$in: ids}});
    }

});
//todo 在material_detail.js  materialList.js  mycustomer.js中调用
Meteor.publish("ShoppingCartInfo", function (acNumber) {
    return ShoppingCart.find({ACNumber: acNumber});
});

//根据受理编号查找购物车中对应的材料详细信息
//todo  在shopping中调用
Meteor.publishComposite("shoppingPageInfo", function (acNumber) {
    return {
        find: function () {
            return ShoppingCart.find({ACNumber: acNumber});
        },
        children: [
            {
                find: function (shoppingInfo) {
                    return MaterialDetailList.find({ID: parseInt(shoppingInfo.materialID)});
                }
            }
        ]
    }
});

//根据customer的_id查找到相应的客户方案
//todo 在customerDetail   constructionCustDetail中调用
Meteor.publishComposite("solutionsBycustomerid", function (_id) {
    return {
        find: function () {
            return Cust_Info.find({_id: _id});
        },
        children: [
            {
                find: function (customerInfo) {
                    return Solutions.find({ACNumber: customerInfo.ACNumber});
                }
            }
        ]
    }
});


//根据方案的ID的材料ID查找材料的详细信息
//todo 在proj_detail.js  updateprojDetai.js  CSolutionDetail.js  CSMaterialDetail中调用
Meteor.publish("meterialDetailBySolutionid", function (_id) {
    return Solutions.find({_id: _id});
});

//todo 在shoppingDetail.js中调用
Meteor.publish("shoppingByACnumberandmId", function (acnumber, mId) {
    return ShoppingCart.find({ACNumber: acnumber, materialID: mId})
})
//----------------------customer login------------------------------------------
//todo 在designer_tabs.js中调用
Meteor.publishComposite("getCustomerType", function (Muid) {
    return {
        find: function () {
            return Meteor.users.find({_id: this.userId}, {fields: {services: 0}});
        },
        children: [
            {
                find: function (usersInfo) {
                    return DesignerRelationClient.find({cId: usersInfo.clientId});
                }
            }
        ]
    }
});
//todo 在mycustomer.js CsolutionsList中调用
Meteor.publish("getSolutionInfoByAcnumber", function (acnumber) {
    return Solutions.find({ACNumber: acnumber})
});

//在designer_tabs.js   customerindex.js中调用
Meteor.publishComposite("CustomerLogingetCustomerInfo", function (Muid) {
    return {
        find: function () {
            return Meteor.users.find({_id: this.userId}, {fields: {services: 0}});
        },
        children: [
            {
                find: function (usersInfo) {
                    return Cust_Info.find({ACNumber: usersInfo.clientId});
                }
            }
        ]
    }
});
//根据受理编号查找设计师的ID
//todo 在evaluationPage.js中调用
Meteor.publish("publishDesignerId", function (acnumber) {
    return DesignerRelationClient.find({cId: acnumber});
});

//根据acnumber查询施工队长的ID
//todo 在evaluationPage.js中调用
Meteor.publish("publishConstrationId", function (acnumber) {
    return construtionRelationClient.find({cId: acnumber});
});

//todo  shopManagerSearch.js中调用
Meteor.publishComposite("ManagerSearch", function (uid, custNameInfo) {
    return {
        find: function () {
            return Meteor.users.find({_id: uid}, {fields: {services: 0}});
        },
        children: [{
            find: function (userInfo) {
                return AC_Personnel.find({ID: parseInt(userInfo.ID)});
            }, children: [{
                find: function (info) {
                    var select = {};
                    select = {$or: [{ACNumber: custNameInfo}, {CustName: custNameInfo}]};
                    select.Branch = {$in: bitConversion(info.pBranch)};
                    return Cust_Info.find(select);

                }
            }]
        }]
    }
});

//todo 测试账户页面筛选使用
Meteor.publish("testAccountSearch", function (custNameInfo) {
    var select = {};
    select = {$or: [{ACNumber: custNameInfo}, {CustName: custNameInfo}]};
    return Cust_Info.find(select);
});

//将一个数字转换成成一个数组  如:传入1026返回[1024,2]

function bitConversion(pBreach) {
    console.log("pBreach:", pBreach);
    if (!(pBreach instanceof Number)) {
        pBreach = Number(pBreach)
    }
    var pbBit = pBreach.toString(2);
    var arr = [];
    _.each(_.range(pbBit.length), function (n) {

        if (pbBit.substring(n, n + 1) !== "0") {
            console.log("pow:", Math.pow(2, pbBit.length - n - 1));
            arr.push(Math.pow(2, pbBit.length - n - 1));
        }
    });

    console.log("arr:", arr);
    return arr;
}


//施工队长按照客户类型进行订阅信息.
//未开工
Meteor.publish("construtionWorkCustomer", function (id, type) {
    var returnValue = [];
    var construction = construtionRelationClient.find({construtionId: id});
    if (construction.count() > 0) {
        var constructionIds = [];
        construction.forEach(function (info) {
            constructionIds.push(info.cId);
        });
        var select = {};
        select.ACNumber = {$in: constructionIds};
        if (type == "未开工") {
            select.startTime = {$gt: new Date()};
        } else if (type == "已开工") {
            select.startTime = {$lte: new Date()};
            select.endTime = {$gt: new Date()};
        } else if (type == "已完成") {
            select.endTime = {$lte: new Date(), $gte: new Date("2016-01-01")};
        }
        var customerInfo = Cust_Info.find(select, {
            fields: {
                _id: 1,
                ACNumber: 1,
                CustName: 1,
                Address: 1,
                Branch: 1,
                BusiType: 1
            }
        });
        returnValue.push(customerInfo);
        if (customerInfo.count() > 0) {
            var branch = [];
            customerInfo.forEach(function (info) {
                branch.push(info.Branch);
            });
            returnValue.push(acSet.find({ACType: "营业网点", ACNumb: {$in: branch}}))
        }
    }
    return returnValue;

});


// Meteor.publishComposite("construtionWorkCustomer", function (id, type) {
//     return {
//         find: function () {
//             return construtionRelationClient.find({construtionId: id});
//         }, children: [{
//             find: function (construtionInfo) {
//                 var select = {};
//                 select.ACNumber = construtionInfo.cId;
//                 if (type == "未开工") {
//                     select.startTime = {$gt: new Date()};
//                 } else if (type == "已开工") {
//                     select.startTime = {$lte: new Date()};
//                     select.endTime = {$gt: new Date()};
//                 } else if (type == "已完成") {
//                     select.endTime = {$lte: new Date(), $gte: new Date("2016-01-01")};
//                 }
//                 var returnValue = [];
//                 var customerInfo = Cust_Info.find(select, {
//                     fields: {
//                         _id: 1,
//                         ACNumber: 1,
//                         CustName: 1,
//                         Address: 1
//                     }
//                 })
//                 returnValue.push(customerInfo);
//                 if (customerInfo.count() > 0) {
//                     var branch = [];
//                     customerInfo.forEach(function (info) {
//                         branch.push(info.Branch);
//                     });
//                     returnValue.push(acSet.find({ACType: "营业网点", ACNumb: {$in: branch}}))
//                 }
//                 return returnValue;
//             }
//         }]
//     }
// });

//没有使用
//Meteor.publishComposite("workCustomer", function (id,ctype) {
//    return {
//        find: function () {
//            if (id) {
//                return construtionRelationClient.find({construtionId: id});
//            }
//        }, children: [{
//            find: function (construtionInfo) {
//                //var select = {};
//                //select.ACNumber = construtionInfo.cId;
//                //select.startTime = {$lt: new Date()};
//                //select.endTime = {$gt: new Date()};
//                //
//                //return Cust_Info.find(select);
//            }
//        }]
//    }
//});

//没有使用
//Meteor.publishComposite("Completed", function (id) {
//    return {
//        find: function () {
//            if (id) {
//                return construtionRelationClient.find({construtionId: id});
//            }
//        }, children: [{
//            find: function (construtionInfo) {
//                //var select = {};
//                //select.ACNumber = construtionInfo.cId;
//                //select.endTime = {$lt: new Date(), $gte: new Date("2016/01/01")};
//                ////select.startTime = {$gte: new Date("2016/01/01")};
//                //return Cust_Info.find(select);
//            }
//        }]
//    }
//});

//todo 在borrowingType.js borrowingList中调用
Meteor.publishComposite("borrowingInfo", function (id, type) {
    return {
        find: function () {
            if (id) {
                return construtionRelationClient.find({construtionId: id});
            }
        }, children: [{
            find: function (construtionInfo) {
                var value;

                var select = {};
                select.申请日期 = {$gt: new Date("2016/01/01")};
                select.受理编号 = construtionInfo.cId;

                if (type == "true") {
                    select.财务审核 = true;
                }
                if (type == "false") {
                    select.财务审核 = false;
                }
                return erpProjectLoan.find(select);
            }
        }]
    }
});


//todo 在ConstrectionType中调用
Meteor.publishComposite("constructionCostInfo", function (id, type) {
    return {
        find: function () {
            if (id) {
                return construtionRelationClient.find({construtionId: id});
            }
        }, children: [{
            find: function (construtionInfo) {
                var value;

                var select = {};
                select.工程部审核日期 = {$gt: new Date("2016/01/01")};
                select.受理编号 = construtionInfo.cId;

                if (type == "true") {
                    select.财务审核 = true;
                }
                if (type == "false") {
                    select.财务审核 = false;
                }
                return erpCostAccountingPlan.find(select);
            }
        }]
    }
});

Meteor.publish("publishConstructionCost", function (type) {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    var collections = [];
    if (userInfo) {
        var acnumbers = [];
        var select = {};
        construtionRelationClient.find({construtionId: userInfo.ID.toString()}).forEach(function (info) {
            acnumbers.push(info.cId);
        });
        if (type == "true") {
            select.财务审核 = true;
        }
        if (type == "false") {
            select.财务审核 = false;
        }
        select.受理编号 = {$in: acnumbers};
        select.工程部审核日期 = {$gt: new Date("2016/01/01")};

        console.log("-------------------")
        console.log(select);
        console.log(erpCostAccountingPlan.find(select).count());
        console.log("-------------------")

        collections.push(erpCostAccountingPlan.find(select));
    }
    return collections;
});

Meteor.publish("publishConstructionCostInfo", function (type) {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    var collections = [];
    if (userInfo) {
        var acnumbers = [];
        construtionRelationClient.find({construtionId: userInfo.ID.toString()}).forEach(function (info) {
            acnumbers.push(info.cId);
        });
        collections.push(erpCostAccountingPlan.find({
            工程部审核: {$in: [true, false]},
            受理编号: {$in: acnumbers},
            工程部审核日期: {$gt: new Date("2016/01/01")}
        }))

    }
    return collections;
});


Meteor.publish("erpCostAccountingPlanInfo", function (id) {
    var custInfo = Cust_Info.findOne({_id: id});
    if (custInfo) {
        console.log(custInfo);
        return erpCostAccountingPlan.find({"受理编号": custInfo.ACNumber}, {fields: {受理编号: 1, 合计: 1}});
    }
});


//********************************************************
//todo  ChangeOrder.js中调用
Meteor.publish("NuclearPriceSheet", function (acnumber, type) {
    console.log("===========NuclearPriceSheet=========", acnumber, type);
    console.log(erpRemoteFileStorage.find({受理编号: acnumber, 类型: type}).count());
    return erpRemoteFileStorage.find({受理编号: acnumber, 类型: type});
});

//pdf文件
//todo pdfFiles页面
Meteor.publish("pdfFilesInfo", function (_id) {
    return erpRemoteFileStorage.find({_id: _id});
});

Meteor.publish("getSupplierPutType", function () {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    if (userInfo) {
        return erpInfoSupplier.find({ID: userInfo.ID});
    }
});

Meteor.publish("publishGetOrderProceedList", function (type) {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    var collections = [];
    var info;
    if (userInfo) {
        console.log("-----", type)
        if (type == 14) {
            if (!Roles.userIsInRole(this.userId, 'install', 'supplier')) {
                info = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: {$in: [9, 10, 14]}});
            } else {
                info = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 14});
            }
        } else
            info = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: type});
        collections.push(info);
        var acnumber = [];
        info.forEach(function (orderCustomer) {
            acnumber.push(orderCustomer.受理编号);
        });
        collections.push(Cust_Info.find({ACNumber: {$in: acnumber}}));
    }
    return collections;
});

//供应商订单列表
//todo 在orderList中调用
Meteor.publish("orderListInfo", function (type, searchVal) {
    // var returnValue = [];
    // var userInfo = Meteor.users.findOne({_id: this.userId});
    // if (userInfo) {
    //     //todo 锁定时间需要修改为2016-06-01
    //     var select = {};
    //     select.供货商编号 = userInfo.ID;
    //     select.orderType = type;
    //     select.预警确认时间 = {$gte: new Date("2016-06-01")};
    //     if (searchVal) {
    //         select.受理编号 = new RegExp(selectinfo, "i");
    //     }
    //     var erpViewVerifyOrderAllInfo = erpViewVerifyOrderAll.find(select, {
    //         limit: limit,
    //         skip: skip,
    //         $sort: {受理编号: -1, 定单号: 1}
    //     });
    //     var acnumbers = [];
    //     var orderIds = [];
    //     console.log("==================", erpViewVerifyOrderAllInfo.count());
    //     erpViewVerifyOrderAllInfo.forEach(function (info) {
    //         var orderID = [];
    //         erpViewVerifyOrderAll.find({受理编号: info.受理编号}).forEach(function (orderinfo) {
    //             orderID.push(orderinfo.定单号);
    //         });
    //         var sortOrderId = _.sortBy(orderID, function (num) {
    //             return num;
    //         });
    //         if (_.first(sortOrderId) == 1) {
    //             orderIds.push(info.定单号);
    //             acnumbers.push(info.受理编号);
    //         }
    //
    //     });
    //
    //     returnValue.push(erpViewVerifyOrderAll.find({_id: {$in: orderIds}}, {
    //         fields: {
    //             ID: 1,
    //             预警确认时间: 1,
    //             供货商编号: 1,
    //             "受理编号": 1,
    //             "定单号": 1
    //         }, $sort: {预警确认时间: -1, "受理编号": 1, 定单号: 1}
    //     }));
    //     returnValue.push(Cust_Info.find({ACNumber: {$in: acnumbers}}, {
    //         fields: {
    //             Address: 1,
    //             CustName: 1,
    //             ACNumber: 1
    //         }
    //     }));
    //     // }
    // }
    // return returnValue;
    var returnValue = [];
    var userInfo = Meteor.users.findOne({_id: this.userId});
    if (userInfo) {
        // var acnumber = [];
        // Cust_Info.find().forEach(function (info) {
        //     acnumber.push(info.ACNumber);
        // });
        var erpViewVerifyOrderAllInfo = erpViewVerifyOrderAll.find({
            供货商编号: userInfo.ID,
            orderType: type,
            // 受理编号: {$in: acnumber},
            // 预警确认时间: {$gte: new Date("2016-08-25")}
        }, {
            fields: {
                "受理编号": 1,
                "定单号": 1,
                "预警确认时间": 1,
            }, $sort: {预警确认时间: -1, 定单号: 1}
        });

        var acnumbers = [];
        returnValue.push(erpViewVerifyOrderAllInfo);
        if (erpViewVerifyOrderAllInfo.count() > 0) {
            erpViewVerifyOrderAllInfo.forEach(function (info) {
                acnumbers.push(info.受理编号)
            });
            returnValue.push(Cust_Info.find({ACNumber: {$in: acnumbers}}, {
                fields: {
                    Address: 1,
                    CustName: 1,
                    ACNumber: 1
                }
            }));
        }
    }
    return returnValue;
});


//供货商材料信息显示
//todo 在orderMaterialList中调用
Meteor.publishComposite("orderMaterialListInfo", function (suplierId) {
    return {
        find: function () {
            return erpOrdersDeploy.find({供货商编号: suplierId});
        }, children: [{
            find: function (orderDeployInfo) {
                return MaterialDetailList.find({分类ID: orderDeployInfo.材料分类ID, 是否可用: true}, {
                    fields: {
                        材料品牌: 1,
                        材料名称: 1,
                        ID: 1
                    }
                });
            }
        }]
    }
});
//显示订单材料列表
//todo 在orderDetail中调用
Meteor.publish("orderDetailInfo", function (acnumber, orderNo) {
    return ERP_Budget_Material.find({"受理编号": acnumber, "定单编号": orderNo, isCancel: false}, {
        fields: {
            材料名称: 1,
            材料品牌: 1,
            材料规格: 1
        }
    })
});

//订单详情页面
Meteor.publish("orderDetailInfoPublish", function (acnumber, orderNo) {
    var returnValue = [];
    var info = ERP_Budget_Material.find({"受理编号": acnumber, "定单编号": orderNo}, {
        fields: {
            材料名称: 1,
            材料品牌: 1,
            材料规格: 1,
            增减: 1
        }
    })
    returnValue.push(info);
    if (info.count() > 0) {
        var supplierinfo = Meteor.users.findOne({_id: this.userId});
        if (info) {
            returnValue.push(erpInfoSupplier.find({ID: supplierinfo.ID}, {fields: {putInStorageType: 1}}));
        }
    }
    returnValue.push(ConsolidatedOrders.find({acnumber: acnumber, orderNo: orderNo}));
    returnValue.push(Cust_Info.find({ACNumber: acnumber}));
    returnValue.push(DesignerRelationClient.find({cId: acnumber}));
    returnValue.push(construtionRelationClient.find({cId: acnumber}));
    var designerInfo = DesignerRelationClient.findOne({cId: acnumber});
    var construction = construtionRelationClient.findOne({cId: acnumber});
    var IDs = [];
    if (designerInfo) IDs.push(designerInfo.dId);
    if (construction) IDs.push(construction.construtionId);
    console.log(IDs);
    console.log("==============================")
    returnValue.push(AC_Personnel.find({_id: {$in: IDs}}));


    return returnValue;
});

//供应商材料详细信息
//todo 在orderMaterialDetailInfo中调用
Meteor.publish("currentOrderMaterialDetail", function (id) {
    return MaterialDetailList.find({ID: id})
});

//todo 在orderMaterialDetailInfo中调用
Meteor.publishComposite("currentSelectMaterialImg", function (id) {
    return {
        find: function () {
            return MaterialDetailList.find({ID: id});
        }, children: [{
            find: function (materialInfo) {
                if (materialInfo.picture) {
                    return Images.find({_id: {$in: materialInfo.picture}});
                }
            }
        }]
    }
});

//todo 在constructionCType router中调用
Meteor.publish("constructionCtype", function (constructionId) {

    return construtionRelationClient.find({construtionId: constructionId}, {
        fields: {
            _id: 1,
            construtionId: 1,
            cType: 1
        }
    });
});

//订阅聊天信息
//todo 在chat.js中调用
Meteor.publish("chatMessagesInfo", function (id, selectDate, limit) {
    var select = {};
    select.roomId = id;
    // if (selectDate) {
    //     select.createDate = new Date(selectDate)
    // }
    console.log(selectDate);
    if (selectDate) {
        select.createDate = {$gte: new Date(selectDate + " 00:00:00"), $lte: new Date(selectDate + " 23:59:59")};
    }
    console.log("=========", select);

    return chatMessages.find(select, {limit: limit, skip: 0, sort: {createDate: -1}})

});

// todo 消息中心 在messageInfo.js中调用
Meteor.publishComposite("chatMessageCenter", function (uid, state, acnumber) {
    return {
        find: function () {
            return roomMessage.find({clientId: acnumber});
        }, children: [{
            find: function (roomMessageInfo) {
                var select = {};
                switch (state) {
                    case 'unread':
                        select = {state: "unread"};
                        break;
                    case 'read':
                        select = {state: "read"};
                        break;
                    case 'all':
                        select = {};
                        break;
                }

                console.log(select, state);
                select.uid = uid;
                select.roomId = roomMessageInfo._id;
                console.log(select);
                return MessageState.find(select);
            }, children: [{
                find: function (messageStateInfo) {
                    return chatMessages.find({roomId: messageStateInfo.roomId}, {sort: {createDate: -1}, limit: 1})
                }
            }]
        }]
    }
});


Meteor.publish("checkConstructionProject", function (cId) {
    var custInfo = Cust_Info.findOne({_id: cId});
    if (custInfo) {
        return checkConstructionProject.find({acnumber: custInfo.ACNumber, isDeleted: false});
    }
});

Meteor.publish("systemMessageInfo", function (acnumber, state) {
    return systemMessage.find({acnumber: acnumber});
});

Meteor.publish("assignQCInfo", function (qc) {
    var assignQC = [];
    var checkInfo = checkConstructionProject.find({
        isDeleted: false,
        assigner: this.userId,
        checkResult: 1,
        status: 2,
        bossCheck: 1
    });
    assignQC.push(checkInfo);
    if (checkInfo.count() > 0) {
        var acnumbers = [];
        var constructionUids = [];
        checkInfo.forEach(function (info) {
            acnumbers.push(info.acnumber);
            constructionUids.push(info.sender);
        });
        //得到门店的信息
        var custinfo = Cust_Info.find({ACNumber: {$in: acnumbers}});
        assignQC.push(custinfo);

        var branchs = [];
        custinfo.forEach(function (info) {
            branchs.push(info.Branch);
        });
        assignQC.push(acSet.find({ACType: "营业网点", ACNumb: {$in: branchs}}));
        //得到施工队长信息

        var employeeIDs = [];
        Meteor.users.find({_id: {$in: constructionUids}}).forEach(function (userInfo) {
            employeeIDs.push(userInfo.ID);
        });
        assignQC.push(Meteor.users.find({_id: {$in: constructionUids}}));
        assignQC.push(AC_Personnel.find({ID: {$in: employeeIDs}}));


    }
    return assignQC;
    // return checkConstructionProject.find({checkResult: 1, status: 1});
});

Meteor.publish("assignerListPage", function (id) {
    var assignerList = [];
    var checkinfo = checkConstructionProject.find({_id: id, isDeleted: false});
    assignerList.push(checkinfo);
    if (checkinfo) {
        var check = checkConstructionProject.findOne({_id: id, isDeleted: false});
        if (check && check.acnumber) {
            console.log(Cust_Info.find({ACNumber: check.acnumber}).count());
            assignerList.push(Cust_Info.find({ACNumber: check.acnumber}));
        }
        //查询质检人员信息
        //得到当前质检人员的ID
        var userInfo = Meteor.users.findOne({_id: this.userId});
        if (userInfo) {
            var qcEmployeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
            if (qcEmployeeInfo && qcEmployeeInfo.pBranch2) {
                //    查找相同branch值的员工
                var qcUserInfo = Roles.getUsersInRole("qc", "user-group");
                console.log(qcUserInfo.count());
                var qcIds = [];
                qcUserInfo.forEach(function (info) {
                    qcIds.push(info.ID);
                })
                assignerList.push(AC_Personnel.find({pBranch2: qcEmployeeInfo.pBranch2, ID: {$in: qcIds}}));
            }
        }
    }
    return assignerList;
});

Meteor.publish("getCustInfoDetail", function (id) {
    var checkinfo = checkConstructionProject.findOne({_id: id, isDeleted: false});
    if (checkinfo) {
        return Cust_Info.find({ACNumber: checkinfo.acnumber});
    }
});

Meteor.publish("publishCheckProject", function (acnumber, project) {
        var collections = [];
        var checkInfo = checkConstructionProject.find({acnumber: acnumber, project: project, isDeleted: false});
        collections.push(checkInfo);
        if (checkInfo.count() > 0) {
            var info = checkConstructionProject.findOne({acnumber: acnumber, project: project, isDeleted: false});
            var images = [];
            if (info && info.reasonID) {
                var reasonInfo1 = correctiveReason.find({_id: {$in: info.reasonID}});

                reasonInfo1.forEach(function (info) {
                    if (info && info.reason) {
                        _.each(info.reason, function (info1) {
                            _.each(info1.picture, function (picInfo) {
                                images.push(picInfo);
                            });
                        });
                    }
                });
                // var reasonInfo = correctiveReason.find({_id: info.reasonID});
                collections.push(reasonInfo1);
                collections.push(QCImage.find({_id: {$in: images}}));
            }
        }
        collections.push(Cust_Info.find({ACNumber: info.acnumber}))
        return collections;
    }
);

Meteor.publish("publishCheckProjectDetailInfo", function (id) {
    var collections = [];
    var checkInfo = checkConstructionProject.find({_id: id, isDeleted: false});
    collections.push(checkInfo);
    if (checkInfo.count() > 0) {
        var info = checkConstructionProject.findOne({_id: id, isDeleted: false});
        var images = [];
        if (info) {
            var reasonInfo1 = correctiveReason.find({checkId: id});

            reasonInfo1.forEach(function (info) {
                if (info && info.reason) {
                    _.each(info.reason, function (info1) {
                        _.each(info1.picture, function (picInfo) {
                            images.push(picInfo);
                        });
                    });
                }
            });
            // var reasonInfo = correctiveReason.find({_id: info.reasonID});
            collections.push(reasonInfo1);
            collections.push(QCImage.find({_id: {$in: images}}));
            collections.push(Cust_Info.find({ACNumber: info.acnumber}))
        }
    }
    return collections;
})


//根据角色返回相应质检人员的信息

Meteor.publishComposite("publishEmployees", function () {
    return {
        find: function () {
            return Meteor.users.find({
                "roles": {
                    "user-group": [
                        "qc"
                    ]
                }
            });

        }, children: [{
            find: function (userinfo) {
                return AC_Personnel.find({ID: userinfo.ID, $nor: [{hidden: true}]}, {fields: {pName: 1, ID: 1}});
            }
        }]
    }
});

Meteor.publishComposite("publishEmployeesMacanger", function () {
    return {
        find: function () {
            return Meteor.users.find({
                "roles": {
                    "user-group": [
                        "qcManager"
                    ]
                }
            });

        }, children: [{
            find: function (userinfo) {
                return AC_Personnel.find({ID: userinfo.ID}, {fields: {pName: 1, ID: 1}});
            }
        }]
    }

});

Meteor.publish("consturctionInfo", function (uid, type) {
    var uid;
    if (Roles.userIsInRole(this.userId, ["qc", "qcManager"], "user-group")) { //质检人员
        uid = this.userId;
    } else if (Roles.userIsInRole(this.userId, ["qcBoss"], "user-group")) {
        uid = null;
    }

    var status;
    var returnValue = [];
    if (type == "notExamined") {
        status = 1;
    } else if (type == "inspected") {
        status = {$nin: [1]};
    }
    var select = {};
    select.bossCheck = 1;
    if (uid) {
        select.assigner = uid;
    }
    var ids = [];

    select.checkResult = status;
    select.isDeleted = false;

    var info = checkConstructionProject.find(select);
    // console.log(info.count());
    var acnumbers = [];
    if (info.count() > 0) {
        info.forEach(function (checkInfo) {
            ids.push(checkInfo.sender);
            acnumbers.push(checkInfo.acnumber);
        })
    }
    returnValue.push(info);

    var employeeIDs = [];
    Meteor.users.find({_id: {$in: ids}}).forEach(function (userInfo) {
        employeeIDs.push(userInfo.ID);
    });
    if (ids.length > 0) {
        returnValue.push(Meteor.users.find({_id: {$in: ids}}));
        returnValue.push(AC_Personnel.find({ID: {$in: employeeIDs}}));
        var custinfo = Cust_Info.find({ACNumber: {$in: acnumbers}});
        var branchs = [];
        custinfo.forEach(function (info) {
            branchs.push(info.Branch);
        });
        console.log(acSet.find({ACType: "营业网点", ACNumb: {$in: _.union(branchs)}}).count());
        returnValue.push(acSet.find({ACType: "营业网点", ACNumb: {$in: _.union(branchs)}}));
        returnValue.push(custinfo);
    }
    return returnValue;
});

Meteor.publish("construction", function (uid, type) {
    var status;
    var returnValue = [];
    if (type == "notExamined") {
        status = 1;
    } else if (type == "inspected") {
        status = {$nin: [1]};
    }
    var select = {};
    select.bossCheck = 1;

    if (uid) {
        select.assigner = uid;
    }
    select.checkResult = status;
    select.isDeleted = false;
    var info = checkConstructionProject.find(select, {sort: {jumpTheQueue: 1, updateDate: 1,}});
    returnValue.push(info);
    if (info.count() > 0) {
        var acnumbers = [];
        info.forEach(function (checkInfo) {
            acnumbers.push(checkInfo.acnumber);
        });
        console.log("0---------------------------------")
        console.log(acnumbers);
        console.log("0---------------------------------")
        var custinfo = Cust_Info.find({ACNumber: {$in: acnumbers}}, {
            fields: {
                ACNumber: 1,
                CustName: 1,
                Address: 1,
                Branch: 1,
                BusiType: 1
            }
        });
        returnValue.push(custinfo);
        //    根据branch查找门店
        var branchs = [];
        custinfo.forEach(function (info) {
            branchs.push(info.Branch);
        });
        returnValue.push(acSet.find({ACType: "营业网点", ACNumb: {$in: branchs}}));
    }
    return returnValue;
});
//
// Meteor.publishComposite("publishRepairList", function (id) {
//     return {
//         find: function () {
//             return checkConstructionProject.find({_id: id})
//         }, children: [{
//             find: function (checkInfo) {
//                 return correctiveReason.find({_id: checkInfo.reasonID})
//             }
//         }]
//     }
// });

Meteor.publish("publishRepairList", function (id) {
    var collections = [];
    var checkInfo = checkConstructionProject.findOne({_id: id, isDeleted: false});
    var reason = correctiveReason.find({_id: checkInfo.reasonID});
    collections.push(reason);
    var reasonInfo = correctiveReason.findOne({_id: checkInfo.reasonID});

    collections.push(Images.find({_id: {$in: reasonInfo.picture}}));
    return collections;

})


Meteor.publish("publishImageInfo", function (imageId) {
    return Images.find({_id: imageId});
});

Meteor.publish("publishChatMessage", function (acnumber, materialID) {
    console.log(acnumber, materialID);
    return roomMessage.find({clientId: acnumber, materialId: materialID})
});

Meteor.publish("systemMessageDetail", function (id) {
    return systemMessage.find({_id: id});
});

Meteor.publish("publishConstrutionRelationClient", function (acnumber) {
    return construtionRelationClient.find({cId: acnumber});
});
Meteor.publish("publishDesigner", function (acnumber) {
    return DesignerRelationClient.find({"cId": acnumber})
});

Meteor.publishComposite("publishSearchCustomer", function (id, type) {
    return {
        find: function () {
            return construtionRelationClient.find({construtionId: id});

        }, children: [{
            find: function (userInfo) {
                var checkType = [];
                if (type == 1) {
                    checkType = [1, 2]
                } else {
                    checkType = [3]
                }
                return checkConstructionProject.find({
                    checkResult: {$in: checkType},
                    acnumber: userInfo.cId,
                    isDeleted: false
                });
            }, children: [{
                find: function (checkInfo) {
                    return Cust_Info.find({ACNumber: checkInfo.acnumber})
                }
            }]
        }]
    }
});

Meteor.publish("publishemployeeInfo", function (ID) {

    return AC_Personnel.find({ID: parseInt(ID)});

})


Meteor.publish("typicalSentencesInfo", function () {
    return typicalSentences.find();
});


Meteor.publishComposite("chatQuickReply", function (uid, state) {
    return {
        find: function () {
            return MessageState.find({uid: uid, state: state});
        }, children: [{
            find: function (messageInfo) {
                return roomMessage.find({_id: messageInfo.roomId});
            }, children: [{
                find: function (roomMessageInfo) {
                    return Cust_Info.find({
                        ACNumber: roomMessageInfo.clientId
                    }, {fields: {_id: 1, ACNumber: 1, CustName: 1, Address: 1}})
                }
            }]
        }]
    }
});

Meteor.publish("chatQucikReplayInfo", function (state) {
    var collection = [];
    var messageStateInfo = MessageState.find({uid: this.userId, state: state});
    collection.push(messageStateInfo);
    if (messageStateInfo.count()) {
        var roomIdArr = [];
        messageStateInfo.forEach(function (info) {
            roomIdArr.push(info.roomId);
        });
        var roomMessageInfo = roomMessage.find({_id: {$in: roomIdArr}});

        var clientIds = [];
        roomMessageInfo.forEach(function (info) {
            clientIds.push(info.clientId);
        })
        collection.push(Cust_Info.find({ACNumber: {$in: clientIds}}));
    }
    return collection;

});

//材料部门 对话

Meteor.publish("materialDeptChatCustInfo", function (uid) {

});
//
// Meteor.publishComposite("chatMessageListInfo", function (uid) {
//     return {
//         find: function () {
//             return MessageState.find({uid: uid});
//         }, children: [{
//             find: function (messageInfo) {
//                 return roomMessage.find({_id: messageInfo.roomId});
//             }, children: [{
//                 find: function (messageStateInfo) {
//                     return chatMessages.find({roomId: messageStateInfo._id}, {sort: {createDate: -1}, limit: 1})
//                 }
//             }]
//         }]
//     }
// });

Meteor.publish("chatMessageListInfo", function (status) {
    var collections = [];
    var messageStateInfo = MessageState.find({uid: this.userId, state: status});
    collections.push(messageStateInfo);
    if (messageStateInfo.count() > 0) {
        var roomArr = [];
        var messageId = []
        messageStateInfo.forEach(function (info) {
            roomArr.push(info.roomId);
            messageId.push(info.messageId);
        });
        collections.push(roomMessage.find({_id: {$in: roomArr}}));
        collections.push(chatMessages.find({_id: {$in: messageId}}));
    }
    return collections;
});

Meteor.publish("unReadMessage", function () {
    var collectons = [];
    collectons.push(MessageState.find({uid: this.userId, "state": "unread"}));
    collectons.push(constructionAndSupplierChat.find({supplierUID: this.userId, "status": "unread"}))
    return collectons
});

// Meteor.publish("designerCustomerTypeCount", function () {
//     var customer;
//     var messageInfo = MessageState.find({uid: this.userId, state: "unread"});
//     if (messageInfo.count() > 0) {
//         customer.push(messageInfo);
//     }
//     var userInfo = Meteor.users.find({_id: this.userId});
//     console.log(userInfo.count());
//     // if (userInfo && userInfo.ID) {
//     //     var id = userInfo.ID.toString();
//     //     var DesignerRelationClientInfo = DesignerRelationClient.find({dId: id});
//     //     console.log("===========", DesignerRelationClientInfo.count());
//     //     if (DesignerRelationClientInfo.count() > 0) {
//     //         customer.push(DesignerRelationClientInfo);
//     //     }
//     // }
//     return customer;
// });

Meteor.publish("publishSupplierCount", function (uid, type) {
    var userInfo = Meteor.users.findOne({_id: uid});
    if (userInfo) {
        if (type == "Delivered") {
            return ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 8})
        } else if (type == "AlreadyInStorage") {
            console.log(uid);
            return ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 12})
        }
    }
});

//经销商进度
Meteor.publish("getErpBudgetMaterialByID", function (id) {

    var collections = [];
    var erpBudgetMaterial = ERP_Budget_Material.find({_id: id});
    if (erpBudgetMaterial.count() > 0) {
        collections.push(erpBudgetMaterial);
        var info = ERP_Budget_Material.findOne({_id: id});
        if (info && info.材料编号) {
            var costInfo = erpInfoMaterialCost.find({
                "材料编号": info.材料编号
            });
            collections.push(costInfo);

        }

    }
    return collections
})
;

Meteor.publish("publishMeasureList", function (uid, type) {
    var userInfo = Meteor.users.findOne({_id: uid});
    //根据角色来进行查询 如果出现角色不匹配对情况 根据角色来进行显示相应的字段信息
    if (!Roles.userIsInRole(this.userId, 'install', 'supplier') && type == '14') {
        return ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: {$in: [14, 9, 10]}});
    }

    if (userInfo) {
        return ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: type});
    }
});

Meteor.publish("publishMeasureListByOrder", function () {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    var collections = [];
    //待测量订单
    if (userInfo) {
        var info = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 1});
        var acnumber = [];
        info.forEach(function (orderAll) {
            acnumber.push(orderAll.受理编号);
        });
        collections.push(info);
        collections.push(Cust_Info.find({ACNumber: {$in: acnumber}}));
    }
    return collections;
});


Meteor.publish("publishSupplierInfo", function (uid) {
    var info = Meteor.users.findOne({_id: uid});
    if (info) {
        return erpInfoSupplier.find({
                ID: info.ID
            }
        );
    }
})
;
//=====================根据订单进行进度更新 数据发布=================================================
Meteor.publish("publishOrderProgressList", function (oid, acnumber) {
    var collections = [];
    var userInfo = Meteor.users.findOne({_id: this.userId});
    if (userInfo) {
        console.log(oid, acnumber);
        collections.push(erpInfoSupplier.find({ID: userInfo.ID}, {fields: {putInStorageType: 1}}));
        collections.push(erpViewVerifyOrderAll.find({受理编号: acnumber, 定单号: parseInt(oid)}))
    }
    console.log(erpViewVerifyOrderAll.find({受理编号: acnumber, 定单号: parseInt(oid)}).count());
    return collections;
});
//=====================根据订单进行进度更新 数据发布=================================================

Meteor.publish("publishCheckOrderInfo", function (type, uid) {
    var select = {};
    if (type == "undeliverGoods") {
        select.orderState = 3;
    } else {
        select.orderState = {$in: [4, 5]};
        select.PricingChecker = uid;
    }
    return ERP_Budget_Material.find(select)
});

Meteor.publish("publishContractPricingInfo", function (type) {
//    供应商列表
    var collections = [];
    var select = {};
    var supplierId = [];
    if (type == "undeliverGoods") {
        select.orderState = 3;
    } else {
        select.orderState = 4;
        select.PricingChecker = this.userId;
    }
    var info = ERP_Budget_Material.find(select);
    var orderInfo = erpViewVerifyOrderAll.find(select);

    if (info.count() > 0) {
        collections.push(ERP_Budget_Material.find(select));
        info.forEach(function (materialInfo) {
            var acnumber = materialInfo.受理编号;
            var orderNo = materialInfo.定单编号;
            var erpViewOrder = erpViewVerifyOrderAll.findOne({受理编号: acnumber, 定单号: orderNo});
            if (erpViewOrder) {
                supplierId.push(erpViewOrder.供货商编号);
            }
        });

    }
    if (orderInfo.count() > 0) {
        collections.push(orderInfo);
        orderInfo.forEach(function (order) {
            supplierId.push(order.供货商编号)
        })
    }
    collections.push(erpInfoSupplier.find({ID: {$in: supplierId}}, {fields: {ID: 1, 名称: 1, putInStorageType: 1}}))


    return collections;
});
//***********************************
Meteor.publish("publishOrderDetailContractInfo", function (id, type) {
    var collections = [];
    console.log(id, type);
    var erpInfo = erpInfoSupplier.find({_id: id});
    collections.push(erpInfo);
    var materialInfo = ERP_Budget_Material.find({供货商ID: parseInt(id), orderState: parseInt(type)});
    collections.push(materialInfo);
    if (materialInfo.count() > 0) {
        var acnumbers = [];
        materialInfo.forEach(function (info) {
            acnumbers.push(info.受理编号);
        });
        collections.push(Cust_Info.find({ACNumber: {$in: acnumbers}}));
    }
    return collections;
});


Meteor.publish("publishzhengtihejiaList", function (id, type) {
    var collections = [];
    var info = erpInfoSupplier.find({_id: id});
    collections.push(info);
    var orderInfo = erpViewVerifyOrderAll.find({供货商编号: parseInt(id), orderState: parseInt(type)})
    var acnumber = [];
    orderInfo.forEach(function (order) {
        acnumber.push(order.受理编号);
    })
    collections.push(orderInfo);
    collections.push(Cust_Info.find({ACNumber: {$in: acnumber}}));
    return collections;

})

//=======================================================================
Meteor.publish("publishContractMaterialCustomer", function (id, type) {
    var collections = [];
    var select = {};
    var info = erpInfoSupplier.find({_id: id});
    collections.push(info);
    var orderInfo = erpViewVerifyOrderAll.find({供货商编号: parseInt(id), orderState: parseInt(type)})
    var acnumber = [];
    orderInfo.forEach(function (order) {
        acnumber.push(order.受理编号);
    })
    collections.push(orderInfo);
    collections.push(Cust_Info.find({ACNumber: {$in: acnumber}}));
    return collections;

});


//=======================================================================


//todo
// Meteor.publish("publishContractMaterialCustomer", function (id, type, selectType) {
//     var collections = [];
//     var select = {};
//     collections.push(erpInfoSupplier.find({ID: parseInt(id), hide: {$ne: 1}}, {fields: {ID: 1, 名称: 1}}));
//     if (selectType == "PutInStorage") {
//         if (type == "undeliverGoods") {
//             select.orderState = 11;
//             select.供货商ID = parseInt(id)
//         } else {
//             select.供货商ID = parseInt(id);
//             select.orderState = 12;
//             select.PutInStrongChecker = this.userId;
//         }
//     } else {
//         if (type == "undeliverGoods") {
//             select.orderState = 3;
//             select.供货商ID = parseInt(id)
//         } else {
//             select.供货商ID = parseInt(id);
//             select.orderState = 4;
//             select.PutInStrongChecker = this.userId;
//         }
//     }
//
//
//     var info = ERP_Budget_Material.find(select);
//     collections.push(info);
//     if (info.count() > 0) {
//         var acnumbers = [];
//         info.forEach(function (materialInfo) {
//             acnumbers.push(materialInfo.受理编号);
//         });
//         collections.push(Cust_Info.find({ACNumber: {$in: acnumbers}}));
//     }
//
//     return collections;
// })

Meteor.publish("publishCheckPutInStorageOrderInfo", function (type, uid) {
    var collections = [];
    var select = {};
    var supplierId = [];

    if (type == "undeliverGoods") {
        select.orderState = 11;
    } else {
        select.orderState = 12;
        select.PutInStrongChecker = this.userId;
    }
    var acnumbers = [];

    var info = ERP_Budget_Material.find(select);
    var orderInfo = erpViewVerifyOrderAll.find(select);
    if (info.count() > 0) {
        collections.push(info);
        info.forEach(function (materialInfo) {
            var acnumber = materialInfo.受理编号;
            var orderNo = materialInfo.定单编号;
            acnumbers.push(materialInfo.受理编号);
            var erpViewOrder = erpViewVerifyOrderAll.findOne({受理编号: acnumber, 定单号: orderNo});
            if (erpViewOrder) {
                supplierId.push(erpViewOrder.供货商编号);
            }
        });
        select.受理编号 = {$in: _.uniq(acnumbers)};
    }
    if (orderInfo.count() > 0) {
        orderInfo.forEach(function (order) {
            supplierId.push(order.供货商编号)
        })
    }
    collections.push(erpViewVerifyOrderAll.find(select));
    collections.push(erpInfoSupplier.find({ID: {$in: supplierId}}, {fields: {ID: 1, 名称: 1, putInStorageType: 1}}));
    return collections;
});


Meteor.publish("publishCheckOrder", function (id) {
    var materialId = ERP_Budget_Material.findOne({_id: id});
    if (materialId && materialId.corePrice) {
        return Images.find({_id: {$in: materialId.corePrice}});
    }
});

Meteor.publish("publishCheckPutInStrongOrder", function (id) {
    console.log(id);
    var collections = [];
    collections.push(ERP_Budget_Material.find({_id: id}));
    console.log("---------------------------")
    var materialId = ERP_Budget_Material.findOne({_id: id});
    collections.push(erpInfoSupplier.find({ID: materialId.供货商ID}));
    if (materialId && materialId.putInStorageFile) {
        collections.push(SupplierImages.find({_id: {$in: materialId.putInStorageFile}}));
    }
    return collections;
});
Meteor.publish("publishCheckPutInStrongOrder2", function (id) {
    console.log(id);
    var collections = [];
    collections.push(ERP_Budget_Material.find({_id: id}));
    console.log("---------------------------")
    var materialId = ERP_Budget_Material.findOne({_id: id});
    collections.push(erpInfoSupplier.find({ID: materialId.供货商ID}));
    if (materialId && materialId.corePrice) {
        collections.push(SupplierImages.find({_id: {$in: materialId.corePrice}}));
    }
    return collections;
});

Meteor.publish("publishOrderViewAllInfo", function (id) {
    var collections = [];
    var info = erpViewVerifyOrderAll.find({_id: id});
    if (info.count() > 0) {
        collections.push(info);
        var viewOrderInfo = erpViewVerifyOrderAll.findOne({_id: id})
        if (viewOrderInfo) {
            var acnumber = viewOrderInfo.受理编号;
            var orderNo = viewOrderInfo.定单号;
            collections.push(ERP_Budget_Material.find({受理编号: acnumber, 定单编号: orderNo}))
            if (viewOrderInfo.putInStorageFile) {
                collections.push(SupplierImages.find({_id: {$in: viewOrderInfo.putInStorageFile}}));
            }
        }
    }
    return collections;
});


Meteor.publish("publishOrderViewAllInfo2", function (id) {
    var collections = [];
    var info = erpViewVerifyOrderAll.find({_id: id});
    if (info.count() > 0) {
        collections.push(info);
        var viewOrderInfo = erpViewVerifyOrderAll.findOne({_id: id})
        if (viewOrderInfo) {
            var acnumber = viewOrderInfo.受理编号;
            var orderNo = viewOrderInfo.定单号;
            collections.push(ERP_Budget_Material.find({受理编号: acnumber, 定单编号: orderNo}))
            if (viewOrderInfo.corePrice) {
                collections.push(SupplierImages.find({_id: {$in: viewOrderInfo.corePrice}}));
            }
        }
    }
    return collections;
});


Meteor.publish("publishCheckPricingOrder", function (id) {
    console.log(id);
    var collections = [];
    collections.push(ERP_Budget_Material.find({_id: id}));
    var materialId = ERP_Budget_Material.findOne({_id: id});
    collections.push(erpInfoSupplier.find({ID: materialId.供货商ID}));
    if (materialId && materialId.corePrice) {
        collections.push(Images.find({_id: {$in: materialId.corePrice}}));
    }
    return collections;
});

Meteor.publish("publishFinishOrderInfo", function (uid, limit) {
    var returnValue = [];
    var userInfo = Meteor.users.findOne({_id: uid});
    console.log(limit, "limit");
    if (userInfo) {
        console.log(userInfo);
        var erpBudgetMaterial = ERP_Budget_Material.find({orderState: 12}, {
            fields: {受理编号: 1, ID: 1}
        });
        var info = vieVerifyOrderStorageA.find({供货商编号: userInfo.ID});
        returnValue.push(info);
        returnValue.push(erpBudgetMaterial);
        if (info.count() > 0) {
            var acnumbers = [];
            info.forEach(function (orderStorageAInfo) {
                acnumbers.push(orderStorageAInfo.受理编号);
            });
            erpBudgetMaterial.forEach(function (info) {
                acnumbers.push(info.受理编号);
            });
            returnValue.push(Cust_Info.find({ACNumber: {$in: acnumbers}}, {
                fields: {
                    ACNumber: 1,
                    CustName: 1,
                    Address: 1
                }
            }))
        }
    }
    return returnValue;

});
//
// Meteor.publish("publishSupplierInfo",function (uid) {
//     var userInfo=Meteor.users.findOne({_id:uid});
//     if(userInfo){
//         return erpInfoSupplier.find({ID:userInfo.ID});
//     }
// })

//todo 任何门店的关系是写死的

Meteor.publish("meterialDeptConstructionType", function (uid, type) {
    var userInfo = Meteor.users.findOne({_id: uid});
    if (userInfo) {
        var employee = AC_Personnel.findOne({ID: userInfo.ID});
        // if (employee && employee.pName) {
        //     console.log(employee.pName);
        //
        //     var store;
        //
        //     switch (employee.pName) {
        //         case  "潘国荣":
        //             store = "浦东";
        //             break;
        //         case  "俞希凡":
        //             store = "旗舰";
        //             break;
        //         case  "归瑜佳":
        //             store = "西区";
        //             break;
        //         case  "孔建松":
        //             store = "北区";
        //             break;
        //     }
        //
        //     var acSetInfo = acSet.findOne({"ACType": "营业网点", "ACName": store});
        //     console.log(acSetInfo);
        if (employee && employee.pBranch2) {
            var select = {};
            select.Branch = {$in: bitConversion(employee.pBranch2)};
            if (type) {
                switch (type) {
                    case "未开工":
                        select.startTime = {$gt: new Date()};
                        break;
                    case "已开工": {
                        select.startTime = {$lte: new Date()};
                        select.endTime = {$gt: new Date()};
                        break;
                    }
                    case "已完成": {
                        select.endTime = {$lte: new Date(), $gte: new Date("2016-01-01")};
                        break;
                    }
                }
            }
            ;
            var customerInfo = Cust_Info.find(select, {
                fields: {
                    _id: 1,
                    ID: 1,
                    ACNumber: 1,
                    CustName: 1,
                    Address: 1,
                    Branch: 1
                }
            })
            var returnValue = [];
            if (customerInfo.count() > 0) {
                returnValue.push(customerInfo);
                var acnumbers = [];
                var branch = [];
                customerInfo.forEach(function (info) {
                    acnumbers.push(info.ACNumber);
                    branch.push(info.Branch);
                });
                var Relationship = construtionRelationClient.find({cId: {$in: acnumbers}});
                returnValue.push(Relationship);

                var CIds = [];
                Relationship.forEach(function (info) {
                    CIds.push(info.construtionId);
                });

                console.log(branch);
                returnValue.push(AC_Personnel.find({_id: {$in: CIds}}));
                var store = acSet.find({ACNumb: {$in: branch}, "ACType": "营业网点"});
                returnValue.push(store);
            }

            return returnValue;
            // console.log(Cust_Info.find(select).count());
            // console.log("--------------------------------------------")
            // return Cust_Info.find(select, {fields: {_id: 1, ID: 1, ACNumber: 1, CustName: 1, Address: 1}});
        }

    }
})
;


Meteor.publish("constructionCaptainListInfo", function () {
    var ConstructionCaptainID = [];
    Roles.getUsersInRole("ConstructionCaptain", "user-group").forEach(function (info) {
        if (info.username != "chenwei")
            ConstructionCaptainID.push(info.ID);
    });
    console.log(ConstructionCaptainID)
    return AC_Personnel.find({ID: {$in: ConstructionCaptainID}}, {sort: {pName: 1, pUser: 1, ID: 1}});
});

Meteor.publish("orderSortSupplierMaterial", function (acnumber) {
    var collections = [];
    var erpMaterial = ERP_Budget_Material.find({受理编号: acnumber, 阶段: 8});
    collections.push(erpMaterial);
    if (erpMaterial.count() > 0) {
        var supplierIds = [];
        erpMaterial.forEach(function (info) {
            supplierIds.push(info.供货商ID);
        });
        var supplierInfo = erpInfoSupplier.find({ID: {$in: supplierIds}}, {fields: {ID: 1, 名称: 1, 电话: 1, 联系人: 1}});
        collections.push(supplierInfo);
    }
    return collections;

});

Meteor.publish("publishCandSChat", function () {
    var collections = [];
    if (Roles.userIsInRole(this.userId, ["Supplier"], "user-group")) {
        var info = constructionAndSupplierChat.find({supplierUID: this.userId}, {sort: {createAt: -1}});
        collections.push(info);
        if (info) {
            var construction = [];
            info.forEach(function (chatInfo) {
                construction.push(chatInfo.constructionID);
            });
            collections.push(AC_Personnel.find({ID: {$in: construction}}))
        }

    } else if (Roles.userIsInRole(this.userId, ["ConstructionCaptain", "ConstructionCaptainBoss"], "user-group")) {
        var info = constructionAndSupplierChat.find({constructionUID: this.userId}, {sort: {createAt: -1}});
        collections.push(info);
        if (info) {
            var construction = [];
            info.forEach(function (chatInfo) {
                construction.push(parseInt(chatInfo.supplierId));
            });
            collections.push(AC_Personnel.find({ID: {$in: construction}}))
        }
    }
    return collections;
});

//6.18日修改
Meteor.publish("getEmployeesInfo", function (id) {
    var collections = [];
    if (Roles.userIsInRole(this.userId, ['ConstructionCaptain'], 'user-group')) {
        var userInfo = Meteor.users.findOne({_id: this.userId});
        collections.push(AC_Personnel.find({ID: userInfo.ID}, {fields: {pName: 1}}));
        collections.push(construtionRelationClient.find({construtionId: userInfo.ID.toString()}));
    } else if (Roles.userIsInRole(this.userId, ['ConstructionCaptainBoss'], 'user-group')) {
        collections.push(AC_Personnel.find({ID: parseInt(id)}, {fields: {pName: 1}}));
        collections.push(construtionRelationClient.find({construtionId: id.toString()}));

    }
    return collections
});

Meteor.publish("getSupplierName", function (id) {
    var collection = [];
    var roomMessageInfo = roomMessage.findOne({_id: id});
    collection.push(roomMessage.find({_id: id}));
    if (roomMessageInfo) {

        var info = ERP_Budget_Material.findOne({_id: roomMessageInfo.materialId});
        if (info) {
            collection.push(erpInfoSupplier.find({ID: info.供货商ID}, {fields: {名称: 1}}));
        }
    }
    return collection;
});

Meteor.publish("contractCustomerInfo", function (acnumber) {
    return Cust_Info.find({ACNumber: acnumber});
})
//


//customerDatail数据订阅修改
Meteor.publish("publishCustomerDetailPage", function (_id) {
    var collections = [];
    var info = Cust_Info.find({_id: _id});
    if (info.count() > 0) {
        collections.push(info);
        //    查找设计师的名字
        var custInfo = Cust_Info.findOne({_id: _id});
        var DesignerRelase = DesignerRelationClient.findOne({cId: custInfo.ACNumber});
        if (DesignerRelase) {
            collections.push(AC_Personnel.find({ID: parseInt(DesignerRelase.dId)}));
        }
        //方案信息
        collections.push(Solutions.find({ACNumber: custInfo.ACNumber}));
        collections.push(DesignerRelationClient.find({cId: custInfo.ACNumber}));
        collections.push(checkConstructionProject.find({acnumber: custInfo.ACNumber, isDeleted: false}));
        collections.push(ERP_Quarantine_Acceptance.find({受理编号: custInfo.ACNumber}, {
            fields: {
                受理编号: 1,
                报验类型: 1,
                质检合格: 1,
                ID: 1,
                _id: 1
            }
        }));
        collections.push(erpCostAccountingPlan.find({"受理编号": custInfo.ACNumber}, {fields: {受理编号: 1, 合计: 1}}))
        collections.push(ERP_Budget_Material.find({"受理编号": custInfo.ACNumber}));
    }
    return collections;
});


Meteor.publish("publishQcCheckPage", function () {
    var assignQC = [];
    var checkInfo = checkConstructionProject.find({checkResult: 1, status: 2, bossCheck: 0, isDeleted: false});
    assignQC.push(checkInfo);
    if (checkInfo.count() > 0) {
        var acnumbers = [];
        var constructionUids = [];
        checkInfo.forEach(function (info) {
            acnumbers.push(info.acnumber);
            constructionUids.push(info.sender);
        });
        //得到门店的信息
        var custinfo = Cust_Info.find({ACNumber: {$in: acnumbers}});
        assignQC.push(custinfo);

        var branchs = [];
        custinfo.forEach(function (info) {
            branchs.push(info.Branch);
        });
        assignQC.push(acSet.find({ACType: "营业网点", ACNumb: {$in: branchs}}));
        //得到施工队长信息

        var employeeIDs = [];
        Meteor.users.find({_id: {$in: constructionUids}}).forEach(function (userInfo) {
            employeeIDs.push(userInfo.ID);
        });
        assignQC.push(Meteor.users.find({_id: {$in: constructionUids}}));
        assignQC.push(AC_Personnel.find({ID: {$in: employeeIDs}}));


    }
    return assignQC;
});


Meteor.publish("designerSearchClientInfo", function () {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    if (userInfo) {
        var acnumbers = [];
        var dId = userInfo.ID.toString()
        DesignerRelationClient.find({dId: dId}).forEach(function (info) {
            acnumbers.push(info.cId);
        });

        return Cust_Info.find({ACNumber: {$in: acnumbers}});

    }
});
//
// Meteor.publish("publishSupplierCountDetail", function (type, createDate, endDate) {
//     var userInfo = Meteor.users.findOne({_id: this.userId});
//     var collections = [];
//
//     var erpViewOrderAll = erpViewVerifyOrderAll.find({
//         供货商编号: userInfo.ID,
//         预警确认时间: {
//             $gte: createDate,
//             $lte: endDate
//         }
//     });
//
//     if (erpViewOrderAll) {
//         console.log(erpViewOrderAll.count());
//         console.log("-------------------")
//         var acnumbers = [];
//         var orderId = [];
//         erpViewOrderAll.forEach(function (info) {
//             acnumbers.push(info.受理编号);
//             orderId.push(info.定单号);
//         });
//         collections.push(Cust_Info.find({ACNumber: {$in: acnumbers}}))
//         var select = {};
//         select.受理编号 = {$in: acnumbers};
//         select.orderState = type;
//         select.供货商ID = userInfo.ID;
//         collections.push(ERP_Budget_Material.find(select))
//     }
//     return collections;
// });

Meteor.publish("publishSupplierCountDetail", function (idArr) {
    var info = ERP_Budget_Material.find({_id: {$in: idArr}});
    var acnumbers = [];
    var collections = []
    collections.push(info)
    info.forEach(function (acnumber) {
        acnumbers.push(acnumber.受理编号);
    })
    collections.push(Cust_Info.find({ACNumber: {$in: acnumbers}}));
    return collections;
});

Meteor.publish("publishDesignerInfo", function () {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    if (userInfo) {
        return AC_Personnel.find({ID: userInfo.ID}, {fields: {pName: 1}});
    }
});

Meteor.publish("publishGetOrderdeviceGoodsList", function () {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    var collections = [];
    var info;
    //待测量订单
    if (userInfo) {
        // if (!Roles.userIsInRole(this.userId, 'install', 'supplier')) {
        //     info = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: {$in: [14, 9, 10]}});
        // } else {
        info = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 8});
        // }

        var acnumber = [];
        info.forEach(function (orderAll) {
            acnumber.push(orderAll.受理编号);
        });
        collections.push(info);
        collections.push(Cust_Info.find({ACNumber: {$in: acnumber}}));

    }
    return collections;
});


//publishOtherProceedOrder 经销商查看审核材料的审核情况情况
Meteor.publish("publishOtherProceedOrder", function (type) {
    var userInfo = Meteor.users.findOne({_id: this.userId});
    var collections = [];
    var info;
    if (userInfo) {
        console.log("-----", type)
        if (type == 14) {
            if (!Roles.userIsInRole(this.userId, 'install', 'supplier')) {
                info = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: {$in: [9, 10, 14]}});
            } else {
                info = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 10});
            }
        } else
            info = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: type});
        collections.push(info);
        var acnumber = [];
        info.forEach(function (orderCustomer) {
            acnumber.push(orderCustomer.受理编号);
        });
        collections.push(Cust_Info.find({ACNumber: {$in: acnumber}}));
    }
    return collections;
});

Meteor.publish("publishAssociatedOrderList", function (acnumber, materialID) {
    return ERP_Budget_Material.find({受理编号: acnumber, 材料编号: materialID});
});


Meteor.publish("publishQCEditWorkSitePage", function (checkId) {
    var collections = [];
    var checkInfo = checkConstructionProject.find({_id: checkId, isDeleted: false});
    if (checkId) {
        collections.push(checkInfo);
        var reasonArr = [];
        QcTemporary.find({checkId: checkId}).forEach(function (info) {
            reasonArr.push(info.reasonId);
        });
        var errInfo = [];
        correctiveReason.find({_id: {$in: reasonArr}}).forEach(function (info) {
            if (info && info.reason) {
                _.each(info.reason, function (reason1) {
                    errInfo.push(reason1.errorCode1);
                })
            }
        });
        collections.push(ERP_Info_Quality_Control.find({_id: {$in: errInfo}}));
        collections.push(correctiveReason.find({_id: {$in: reasonArr}}));
    }
    return collections;
});


Meteor.publish("publishQcCheckDetail", function (id) {
    var collections = [];
    var checkInfo = checkConstructionProject.find({_id: id});
    collections.push(checkInfo);
    if (checkInfo.count() > 0) {
        var acnumbers = [];
        checkInfo.forEach(function (info) {
            acnumbers.push(info.acnumber);
        });
        var acnumbers = [];
        var constructionUids = [];
        checkInfo.forEach(function (info) {
            acnumbers.push(info.acnumber);
            constructionUids.push(info.sender);
        });
        //得到门店的信息
        var custinfo = Cust_Info.find({ACNumber: {$in: acnumbers}});
        collections.push(custinfo);

        var branchs = [];
        custinfo.forEach(function (info) {
            branchs.push(info.Branch);
        });
        collections.push(acSet.find({ACType: "营业网点", ACNumb: {$in: branchs}}));
        //得到施工队长信息

        var employeeIDs = [];
        Meteor.users.find({_id: {$in: constructionUids}}).forEach(function (userInfo) {
            employeeIDs.push(userInfo.ID);
        });
        collections.push(Meteor.users.find({_id: {$in: constructionUids}}));
        collections.push(AC_Personnel.find({ID: {$in: employeeIDs}}));
    }

    return collections;
});


Meteor.publish("publishQCImage", function () {
    return QCImage.find();
});
Meteor.publish("publishOtherImages", function () {
    return OtherImages.find();
});

Meteor.publish("publishSupplierImages", function () {
    return SupplierImages.find();
});

Meteor.publish("publishErpControl", function () {
    return ERP_Info_Quality_Control.find({
        删除: false, 可用: true
    })
})
;

Meteor.publish("publishCancel", function () {
    var collections = [];

    var userData = Meteor.users.findOne({_id: this.userId});
    if (userData && userData.ID) {

        var info = ERP_Budget_Material.find({isCancel: true, 供货商ID: userData.ID});
        collections.push(info);
        var acnumber = [];
        info.forEach(function (info) {
            acnumber.push(info.受理编号);
        });
        collections.push(Cust_Info.find({ACNumber: {$in: acnumber}}));
    }
    return collections;
});


Meteor.publish("publishOrderList", function (type) {
    console.log(type);
    var collections = [];
    var userInfo = Meteor.users.findOne({_id: this.userId});
    if (userInfo) {
        var orderInfo = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderType: type}, {
            fields: {
                orderListInfo: 1,
                状态: 1,
                定单号: 1,
                ID: 1,
                受理编号: 1
            }
        });
        var newOrderACNumber = [];

        orderInfo.forEach(function (info) {
            newOrderACNumber.push(info.受理编号)
        });
        collections.push(orderInfo);
        collections.push(Cust_Info.find({ACNumber: {$in: newOrderACNumber}}, {
            fields: {
                CustName: 1,
                ACNumber: 1,
                Address: 1,
                ID: 1
            }
        }));
    }
    return collections;
})