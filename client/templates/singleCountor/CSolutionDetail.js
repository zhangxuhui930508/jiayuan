/**
 * Created by zhangxuhui on 15/10/28.
 */

var currItem;
Template.CSolutionDetail.rendered = function () {
    Session.set("classifyback", "CSolutionDetail");
    var _id = Router.current().params.sId;
    this.subscribe("meterialDetailBySolutionid", _id, function (err, result) {
        var solutionsInfo = Solutions.findOne({_id: _id});
        var materialIds = [];
        _.each(solutionsInfo.selectMaterial, function (value) {
            materialIds.push(parseInt(value.materialID));
        });
        Meteor.call("getMaterialNameByIDs", materialIds, function (err, result) {
            console.log("result:", result);
            Session.set("materialNameByIds", result);
        })
    });
    ////根据方案的ID订阅数据
    //this.subscribe("selectedSolution", Session.get("CustomersolutionListID"));
    //console.log("solutionsId:", Session.get("CustomersolutionListID"));
    //this.subscribe("images");
    //
    //var currentCType = Session.get("currentCustomeCType");//获得客户的类型 提取出 装修的type
    //if (currentCType) {
    //    if (currentCType.cType == '已签' || currentCType.cType == '已归档') {
    //        $(".ion-search").css("color", "#969696");
    //    }
    //};
};

Template.CSolutionDetail.helpers({
    headObj: function () {
        return {
            leftButton: true,
            title: Solutions.findOne().solutionName,
            backTemplate: 'CsolutionsList'
        }
    },
    //获取数据的列表
    'projectDeataiList': function () {
        var _id = Router.current().params.sId;
        var solutionsInfo = Solutions.findOne({_id: _id})
        if (solutionsInfo && solutionsInfo.selectMaterial) {
            return solutionsInfo.selectMaterial;
        }

    },
    'getMaterialName': function (mId) {
        var materialInfo = Session.get("materialNameByIds");
        if (mId && materialInfo) {
            return materialInfo[mId];
        }
    },
    'currentCustomerState': function () {
        var info = DesignerRelationClient.findOne();
        if (info && info.cType) {
            var cType = info.cType;
            if (cType == "已签" || cType == "已归档") {
                return false
            } else {
                return true;
            }
        }
    }

});


Template.CSolutionDetail.events({
    'click .solutionListDel': function (event) {
        //var MaterialId = $(event.currentTarget).siblings("div").children("h2").attr("id");
        //console.log("solutionID:", Session.get("CustomersolutionListID"), "materialID", MaterialId);
        //Meteor.call("deleteMaterial", Session.get("CustomersolutionListID"), MaterialId, function (err, result) {
        //    //Session.set("currentCustomerdeleteMaterial",'success');
        //});
        if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
            var _id = Router.current().params.sId;
            console.log(_id);
            var MaterialId = $(event.currentTarget).attr("id");
            console.log(MaterialId);
            Meteor.call("delSolutionMaterial", _id, MaterialId, function (err) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>删除材料成功!</h4>",
                        duration: 1000
                    });
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>删除材料失败!</h4>",
                        duration: 1000
                    });
                }
            });
        } else {
            IonLoading.show({
                customTemplate: "<h4>你没有权限操作!</h4>",
                duration: 1000
            });
        }
    },
    "click .add_Material": function (event) {
        //新增材料
        if (Roles.userIsInRole(Meteor.userId(), ["designer", 'client'], "user-group")) {

            var CustomerInfo = Cust_Info.findOne();
            var info = DesignerRelationClient.findOne();

            var customerDecorateInfo = {};
            customerDecorateInfo.ACNumber = Session.get("customerLogin");
            customerDecorateInfo.sId = Router.current().params.sId;
            customerDecorateInfo.mode = CustomerInfo.BusiType;
            customerDecorateInfo.cType = info.cType;
            customerDecorateInfo.cId = CustomerInfo._id;
            Session.set("customerDecorateInfo", customerDecorateInfo);

            Router.go("/customerParts");
        } else {
            IonLoading.show({
                customTemplate: "<h4>你没有权限操作!</h4>",
                duration: 1000
            });
        }
    },
    'click .saveButton': function () {
        Router.go("/CsolutionsList/" + Session.get("customerLogin"))
    },
    'click .nextDetailPage': function (event) {
        var materialOrderType = $(event.currentTarget).children("input").val();
        var materialId = $(event.currentTarget).children("h2").attr("id").trim();
        var sId = Router.current().params.sId;
        if (!materialOrderType) {
            materialOrderType = 0;
        }
        Router.go("/CSMaterialDetail/solutionId/" + sId + "/materialId/" + materialId + "/OrderType/" + materialOrderType);//跳转到材料详情页面
    },
    'click .backMenuList': function () {
        Router.go("/CsolutionsList/" + Session.get("customerLogin"))
    }
});