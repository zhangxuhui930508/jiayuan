/**
 * Created by zhangxuhui on 16/3/24.
 */
Template.supplierLogin.events({
    'click .account-button': function (event, template) {
        var userName = template.$('[id=username]').val().replace(/\s+/g, "");
        console.log("userName:" + userName);
        var password = template.$('[id=pwd]').val().replace(/\s+/g, "");
        console.log("password:" + password);
        //让return起作用
        event.preventDefault();

        loadingContent("正在为您登录系统, 请稍候...");

        Meteor.loginWithPassword(userName, password, function (error, result) {
            loadingHide();
            if (error) {
                console.log(error.message);
                IonLoading.show({
                    customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                    duration: 1000
                });
            } else {
                if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
                    Router.go("/orderType");
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                        duration: 1000
                    });
                    Meteor.logout(function (err) {
                        if (!err) {
                            Router.go("/supplierLogin");
                        } else {
                            loadingTime("系统错误", 1000);
                        }
                    });
                }
            }
        })
    }
});