/**
 * Created by zhangxuhui on 16/4/19.
 */

Template.constructionDetail.rendered = function () {
    Meteor.call("qcIsCheckInfo", Router.current().params.id)
    IonLoading.show();
    this.subscribe("publishCheckProjectDetailInfo", Router.current().params.id, function () {
        IonLoading.hide();
    });
};


Template.constructionDetail.helpers({
    headObj: function () {
        return {
            title: '客户基本信息',
            leftButton: true,
            // backTemplate: 'assignQC'
        }
    },
    Info: function () {
        return checkConstructionProject.findOne({})
    },
    getCustInfoCustName: function () {
        return Cust_Info.findOne();
    },
    'getQCProject': function (value) {
        if (value) {
            switch (value) {
                case "completion":
                    return "竣工报检";
                    break;
                case "hydropower":
                    return "水电报检";
                    break;
                case "mudWood":
                    return "泥木报检";
                    break;
            }
        }
    },
    'colorValue': function (date) {
        if (date <= new Date().Format("yyyy-MM-dd")) {
            return "red"
        } else {
            return "rgb(0,0,0)"
        }
    },
    'buttonStatus': function (id) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info && info.checkResult) {
            if (info.checkResult == 1) {
                return true;
            } else {
                return false;
            }
        }
    },
    checkResult: function (value) {
        if (value == "2") {
            return "检验合格"
        } else if (value == "3") {
            return "整改单"
        }
    },

    'correctiveReasonInfo': function () {
        return correctiveReason.findOne();
    },
    'imageAll': function () {
        var info = correctiveReason.findOne();
        // if (info && info.reason.picture) {
        //     return info.reason.picture;
        // }

    },
    'imageInfo': function (picture) {
        if (picture)
            console.log(picture);
        return Images.find({_id: {$in: picture}});
    },
    reasonInfo: function () {
        var arr = [];
        correctiveReason.find().forEach(function (info) {
            _.each(info.reason, function (reasonInfo) {
                arr.push(reasonInfo)
            })
        });
        return arr;
    }
});


Template.constructionDetail.events({

    'click .updateQCDate': function (event) {
        var id = $(event.currentTarget).attr("id").trim();
        console.log("id:", id);
        IonPopup.prompt({
            title: '更新质检时间',
            template: '请设置新的时间',
            okText: '确定',
            cancelText: '取消',
            inputType: 'date',
            onOk: function () {
                var newDate = $('.popup-body input').val().trim();
                if (!newDate) {
                    IonLoading.show({
                        customTemplate: "<h4>请设置新的质检时间!</h4>",
                        duration: 1000
                    });
                } else {
                    Meteor.call("updateCheckDate", id, newDate, Meteor.userId(), function (err, result) {
                        if (!err) {
                            IonLoading.show({
                                customTemplate: "<h4>质检时间更新成功!</h4>",
                                duration: 1000
                            });
                        }
                    });
                }
            }
        });
    },
    'click .checkResultInspection': function (event) {
        var id = $(event.currentTarget).attr("id").trim();

        IonPopup.confirm({
            title: '温馨提示',
            template: "确定检验合格?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                Meteor.call("updateCheckResultInspection", id, Meteor.userId(), function (err, result) {
                    console.log("=========================", err, result);
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>检验结果提交成功!</h4>",
                            duration: 1000
                        });
                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>检验结果提交失败!</h4>",
                            duration: 1000
                        });
                    }
                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });


    },
    'click .checkResultIssure': function (event) {
        var id = $(event.currentTarget).attr("id").trim();
        var acnumber = $(event.currentTarget).parent("div").parent("div").attr("id").trim();
        console.log(acnumber);
        var type = $(event.currentTarget).children("span").attr("class").trim();
        Session.set("workSiteRepairType", type);
        Router.go("/workSiteRepair/checkId/" + id + "/acnumber/" + acnumber);
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/qualityLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .backMenuList': function () {
        var type = Router.current().params.type;
        if (type == "inspected") {
            Router.go("/inspectedList")
        } else if (type = "notExamined") {
            Router.go("/notExaminedList")

        }
    },
    'click .satisfaction': function () {
        var info = checkConstructionProject.findOne();
        if (info) {
            Router.go("/satisfaction/acnumber/" + info.acnumber + "/construction/" + info.sender + "/checkId/" + info._id);
        }
    },
    'click .edit': function () {
        var checkId = Router.current().params.id;
        Router.go("/qcEditWorkSiteRepairList/" + checkId);
    }

});