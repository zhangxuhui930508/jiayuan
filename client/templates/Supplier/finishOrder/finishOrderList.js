/**
 * Created by zhangxuhui on 16/4/28.
 */
Template.finishOrderList.rendered = function () {
    IonLoading.show();
    Session.set(TableSkipKey, 0);
    Session.set(TableLimitKey, 20);

    this.subscribe("publishFinishOrderInfo", Meteor.userId(), Session.get(TableLimitKey), function () {
        IonLoading.hide();
    });
};


Template.finishOrderList.helpers({
    headObj: function () {
        return {
            title: '已入库订单',
            leftButton: true,
            backTemplate: 'orderType'
        }
    },
    orderListInfo: function () {
        var select = {};
        if (Session.get("currentOrderSearchVal")) {
            select.受理编号 = new RegExp(Session.get("currentOrderSearchVal"), "i");
        }
        console.log(select);
        var info = vieVerifyOrderStorageA.find(select);
        if (info) {
            return info
        }
    },
    'erpBudgetMaterial': function () {
        var select = {};
        if (Session.get("currentOrderSearchVal")) {
            select.受理编号 = new RegExp(Session.get("currentOrderSearchVal"), "i");
        }
        console.log(select);
        var info = ERP_Budget_Material.find(select);
        if (info) {
            return info;
        }
    },
    getCustomerName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    getCustomerAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    }
});


Template.finishOrderList.events({
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();

        Session.set("currentOrderSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentOrderSearchVal", null);
        $(".search").val("");
        console.log("-------------------------------------");
    },
    'scroll .home': function (event) {
        var list = $(".home");
        var viewH = list.height();
        var contentH = list.get(0).scrollHeight;//内容高度
        console.log("contentH:", contentH);
        var scrollTop = list.scrollTop();//滚动高度
        console.log("scrollTop:", scrollTop);
        if (contentH - viewH - scrollTop <= 10) {
            console.log("==========================");
            Session.set(TableLimitKey, parseInt(Session.get(TableLimitKey) + 20));
        }
    },
})