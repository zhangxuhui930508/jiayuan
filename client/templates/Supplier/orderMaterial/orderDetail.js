/**
 * Created by zhangxuhui on 16/3/25.
 */

Template.orderDetail.onRendered(function () {
    var acnumber = Router.current().params.acnumber;
    var orderNo = parseInt(Router.current().params.orderNo);
    loadingSystemContent();
    this.subscribe("orderDetailInfoPublish", acnumber, orderNo, function () {
        loadingHide();
    });
});


Template.orderDetail.helpers({
    isMerge: function () {
        var info = ConsolidatedOrders.find({
            acnumber: Router.current().params.acnumber,
            orderNo: parseInt(Router.current().params.orderNo)
        });
        if (info.count() > 0) {
            return false;
        }
        return true;
    },
    headObj: function () {
        return {
            title: '订单详情',
            // leftButton: true,
            // backTemplate: 'orderType'
        }
    },

    'orderMaterialList': function () {
        var info = ERP_Budget_Material.find();
        if (info) {
            return info;
        }
    },
    isSalesReturn: function (id) {
        var info = ERP_Budget_Material.findOne({_id: id});
        if (info && info.增减 == -1) {
            return true;
        }
        return false;

    },
    'putInStorageTypeInfo': function () {
        var info = erpInfoSupplier.findOne();
        if (info) {
            if (info.putInStorageType == "single") {
                return false;
            }
        }
        return true;
    },
    orderIsMerge: function () {
        var info = ConsolidatedOrders.find({
            acnumber: Router.current().params.acnumber,
            orderNo: parseInt(Router.current().params.orderNo),
            isDelete: false
        });
        if (info.count() > 0) {
            return "#836FFF"
        }
        return "#FFFFFF";
    },
    designerInfo: function () {
        var acnumber = Router.current().params.acnumber;
        var designerInfo = DesignerRelationClient.findOne({cId: acnumber});
        if (designerInfo) {
            return AC_Personnel.findOne({_id: designerInfo.dId});
        }
    },
    customerInfo: function () {
        return Cust_Info.findOne();
    },
    construtionInfo: function () {
        var acnumber = Router.current().params.acnumber;
        var construction = construtionRelationClient.findOne({cId: acnumber});
        if (construction) {
            return AC_Personnel.findOne({_id: construction.construtionId});

        }
    },
    mergeOrCancel: function () {
        var info = ConsolidatedOrders.find({
            acnumber: Router.current().params.acnumber,
            orderNo: parseInt(Router.current().params.orderNo),
            isDelete: false
        });
        if (info.count() > 0) {
            return true;
        }
        return false;

    },


});

Template.orderDetail.events({
    'click .cancelMergeOrder': function () {
        var acnumber = Router.current().params.acnumber;
        var orderNo = Router.current().params.orderNo;
        IonPopup.confirm({
            title: '温馨提示',
            template: "确定取消合并订单吗?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                Meteor.call("cancelSupplierMergeOrder", acnumber, orderNo);
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .orderMaterialDetailInfo': function (event) {
        var id = $(event.currentTarget).children("span").attr("id");
        var acnumber = Router.current().params.acnumber;
        var orderNo = Router.current().params.orderNo;
        var orderType = Router.current().params.ordertype;
        Session.set("orderBackURL", {acnumber: acnumber, orderNo: orderNo, type: orderType});


        Router.go("/StandardMaterialDetail/" + id);


    },
    'click .backMenuList': function () {
        var orderType = Router.current().params.ordertype;
        Router.go("/orderList/" + orderType);
    },
    'click .mergeOrder': function (event) {
        //    是否合并
        var acnumber = Router.current().params.acnumber;
        var orderNo = Router.current().params.orderNo;
        IonPopup.confirm({
            title: '温馨提示',
            template: "确定合并订单吗?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                Meteor.call("SupplierMergeOrder", acnumber, orderNo);
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .orderProgress': function () {
        var acnumber = Router.current().params.acnumber;
        var orderNo = Router.current().params.orderNo;

        var moreOrLess = [];
        ERP_Budget_Material.find().forEach(function (info) {
            moreOrLess.push(info.增减);
        });
        if (_.first(_.uniq(moreOrLess)) == -1 && _.uniq(moreOrLess).length == 1) {
            Meteor.call("returnOfTheTreasury", acnumber, orderNo)
        }

        Router.go("/orderProgressList/acnumber/" + acnumber + "/orderId/" + orderNo);
    },
    'click .backMenuList': function () {
        Router.go(Session.get(CurrentBackTemplate));
    }
});