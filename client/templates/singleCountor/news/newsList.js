/**
 * Created by zhangxuhui on 15/11/10.
 */
Template.newsList.rendered = function () {
    var id = Router.current().params.id;
    Session.set("newsInfoIDVal", id);
    this.subscribe("newsInfoId", id, function () {
    });
};


Template.newsList.helpers({
    headObj: function () {
        return {
            leftButton: true,
            title: "最新动态",
            backTemplate: 'customerindex'
        }
    },
    'newInfoList': function () {
        var info = newsInfo.findOne({_id: Session.get("newsInfoIDVal")});
        return info;
    },
    getDataformat: function (data) {
        if (data) {
            return data.Format("yyyy-MM-dd");
        }
    }
});