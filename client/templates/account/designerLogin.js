/**
 * Created by zhangxuhui on 15/11/2.
 */
//Template.designerLogin.rendered=function(){
//    if(Meteor.userId()){
//        Router.go("/customerType");
//    }
//}
Template.designerLogin.rendered = function () {
    console.log("=========================")
    console.log(Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group"))
    console.log("=========================")
};


Template.designerLogin.events({
    'click .account-button': function (event, template) {
        var userName = template.$('[id=username]').val().replace(/\s+/g, "");
        console.log("userName:" + userName);
        var password = template.$('[id=pwd]').val().replace(/\s+/g, "");
        console.log("password:" + password);
        //让return起作用
        event.preventDefault();

        Meteor.loginWithPassword(userName, password, function (error, result) {

            if (error) {
                console.log(error.message);
                IonLoading.show({
                    customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                    duration: 1000
                });
            } else {
                //if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
                //    console.log(result);
                //    Router.go('/customerType');
                //} else {
                //    IonLoading.show({
                //        customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                //        duration: 1000
                //    });
                //}
                //经营副总

                if (Roles.userIsInRole(Meteor.userId(), ["vicePresident"], "user-group")) { //副总
                    Router.go("/shopManagerSearch");
                } else if (Roles.userIsInRole(Meteor.userId(), ["districtManager"], "user-group")) {//大区经理
                    Router.go("/shopManagerSearch");
                } else if (Roles.userIsInRole(Meteor.userId(), ["StoreManager"], "user-group")) {//店长
                    Router.go("/shopManagerSearch");
                } else if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
                    Session.set("currentDesignerID", null);
                    Router.go('/customerType');
                } else if (Roles.userIsInRole(Meteor.userId(), ['test'], "user-group")) {
                    Router.go("/shopManagerTestPage");
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                        duration: 1000
                    });
                }

                //if(Roles.userIsInRole(Meteor.userId(),['StoreManager'],'user-group')){
                //
                //}else if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
                //    Session.set("currentDesignerID", null);
                //    Router.go('/customerType');
                //}else{
                //    IonLoading.show({
                //        customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                //        duration:1000
                //    });
                //}

            }
        })
    }
});