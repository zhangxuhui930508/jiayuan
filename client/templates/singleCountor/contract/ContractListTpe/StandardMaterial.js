/**
 * Created by zhangxuhui on 15/11/10.
 */
Template.StandardMaterial.onRendered(function () {
    var ACNumber;
    IonLoading.show({});
    if (Roles.userIsInRole(Meteor.userId(), ["client", "materialDept", "contractDept"], "user-group")) {
        ACNumber = Session.get("customerLogin");
    } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptainBoss", "ConstructionCaptain", "test", "designer", "districtManager", "vicePresident", "StoreManager"], "user-group")) {
        ACNumber = Session.get("customerDecorateInfo").ACNumber;
    }
    var t;
    if (Session.get("captainCustom") == "captain_custom") {
        t = 1
    } else {
        t = 2;
    }

    this.subscribe("erpBudgetMaterialInfo", ACNumber, 8, t, function () {
        IonLoading.hide();
    });
});

Template.StandardMaterial.helpers({
    iscaptainCustom: function () {
        if (Session.get("captainCustom") == "captain_custom") {
            return false;
        }
        return true;

    },
    headObj: function () {
        if (Session.get("captainCustom") == "captain_custom") {
            return {
                title: '队长定制',
                leftButton: true,
                backTemplate: 'ContractListType'
            }
        } else {
            return {
                title: '标准材料',
                leftButton: true,
                backTemplate: 'ContractListType'
            };
        }

    },
    'standardMaterialList': function () {
        var info = ERP_Budget_Material.find({}, {
            sort: {
                '部位号': 1
            }
        });
        if (info) {
            return info;
        }
    },
    'itemColor': function (moreOrless) {
        if (moreOrless == 1) {
            return "#f1b8e4";
        } else if (moreOrless == -1) {
            return "#b8f1cc";
        }
    },
    'symboladd': function (moreorless) {
        if (moreorless == 1) {
            return true;
        } else {
            return false;
        }
    },
    'symboljian': function (moreorless) {
        if (moreorless == -1) {
            return true;
        } else {
            return false;
        }
    },
    'erpBudgetRationList': function () {
        var info = erpBudgetRation.find({}, {sort: {部位号: 1}});
        if (info) {
            return info
        }
    },
    'getpartsName': function (PartsNumber) {
        var info = erpBudgetLocation.findOne({部位号: PartsNumber});
        if (info) {
            return info.部位名称;
        }
    },
    'materialMessageState': function (id) {
        var messageInfo = roomMessage.findOne({materialId: id});
        if (messageInfo) {
            var messageState = MessageState.findOne({roomId: messageInfo._id, state: "unread"})
            if (messageState) {
                return true;
            }
        }
        return false;
    }
});

Template.StandardMaterial.events({
    'click .StandardMaterialgoDetail': function (event) {
        var id = $(event.currentTarget).children("span").attr("id");
        if (!Roles.userIsInRole(Meteor.userId(), ["contractDept"], "user-group")) {
            Router.go("/StandardMaterialDetail/" + id);
        }
    },
    'click .orderSortSupplierList': function () {
        var ACNumber;
        if (Roles.userIsInRole(Meteor.userId(), ["client", "materialDept", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "test", "designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptainBoss"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        }
        Router.go("/orderSortSupplierList/" + ACNumber)
    }
});


Template.StandardMaterial.onDestroyed(function () {
    Session.set("captainCustom", null);
});