/**
 * Created by zhangxuhui on 16/4/27.
 */

Template.deliverGoodsList.rendered = function () {
    Session.set("deliverGoodsList", "undeliverGoods");
    IonLoading.show({
        duration: 1500
    });
    this.subscribe("publishMeasureList", Meteor.userId(), 8, function (err, result) {
        var info = ERP_Budget_Material.find().fetch();
        console.log(info);
        var arr = [];
        _.each(info, function (erpInfo) {
            arr.push(erpInfo.受理编号);
        });
        if (arr.length > 0) {
            Meteor.call("getCustomerNameInfo", arr, function (err, result) {
                console.log(err, result);
                Session.set("customerNameAndAddress", result);
                IonLoading.hide();
            })
        }
    })
};


Template.deliverGoodsList.helpers({
    headObj: function () {
        return {
            title: '发货中订单',
            leftButton: true,
            backTemplate: 'proceedOrderType'
        }
    },
    isActive: function (val) {
        var select = Session.get("deliverGoodsList");
        if (select && select == val) {
            return "active";
        }
    },
    'deliverGoodsInfo': function () {
        var info = Session.get("deliverGoodsList");
        var select = {};
        if (info == "undeliverGoods") {
            select.deliveryDate = {$lt: new Date().Format("yyyy-MM-dd")}
        } else if (info == "todaydeliverGoods") {
            select.deliveryDate = new Date().Format("yyyy-MM-dd");
        } else if (info == "futuredeliverGoods") {
            select.deliveryDate = {$gt: new Date().Format("yyyy-MM-dd")}
        }

        var info = ERP_Budget_Material.find(select);
        if (info) {
            return info;
        }
    }, 'getCustomerName': function (acnumber) {
        try {
            return Session.get("customerNameAndAddress")[acnumber].CustName;
        } catch (e) {

        }

    },
    'getCustomerAddress': function (acnumber) {
        try {
            return Session.get("customerNameAndAddress")[acnumber].Address;
        } catch (e) {

        }
    }
});

Template.deliverGoodsList.events({
    "click .tab-item": function (event) {
        var val = $(event.currentTarget).attr("value");
        Session.set("deliverGoodsList", val);
    },
    'click .gocustomerDetail': function (event) {
        var id = $(event.currentTarget).children("span").attr("class").trim();
        console.log("id:", id);
        Router.go("/scheduleList/" + id);
    }
});