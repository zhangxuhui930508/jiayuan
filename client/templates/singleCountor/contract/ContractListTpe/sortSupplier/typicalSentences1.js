/**
 * Created by zhangxuhui on 5/26/16.
 */
Template.typicalSentences1.onRendered(function () {
    this.subscribe("typicalSentencesInfo");
});

Template.typicalSentences1.helpers({
    headObj: function () {
        return {
            title: "常用语",
            // leftButton: true,
            // backTemplate: 'constructionCType'
        }
    },
    typicalSentencesInfo: function () {
        var info = typicalSentences.find();
        if (info) {
            return info;
        }
    },


});


Template.typicalSentences1.events({
    'click .backMenuList': function () {
        var acnumber = Router.current().params.acnumber;
        Router.go("/orderSortSupplierList/" + acnumber);
    },
    'click .item': function (event) {
        var message = $(event.currentTarget).children("h2").text().trim();
        // var mid = Router.current().params.mid;
        // var acnumber = Router.current().params.acnumber;
        var acnumber = Router.current().params.acnumber;
        var supplierId = Router.current().params.supplierId;

        IonPopup.show({
            title: "确定发送?",
            template: message,
            buttons: [{
                text: '发送',
                type: 'button-positive',
                onTap: function () {
                    Meteor.call("constructionAndSupplier", supplierId, acnumber, message, function (err, result) {
                        if (!err) {
                            IonLoading.show({
                                customTemplate: "<h4>催材料信息发送成功!</h4>",
                                duration: 1000
                            });
                        } else {
                            IonLoading.show({
                                customTemplate: "<h4>催材料信息发送失败!</h4>",
                                duration: 1000
                            });
                        }
                    })

                    //
                    //
                    // Meteor.call("sendMessageOrCreate", mid, acnumber, Meteor.userId(), message, Session.get("materialName"), function (err, result) {
                    //     if (!err) {
                    //         IonLoading.show({
                    //             customTemplate: "<h4>消息发送成功!</h4>",
                    //             duration: 1000
                    //         });
                    //     } else {
                    //         IonLoading.show({
                    //             customTemplate: "<h4>消息发送失败!</h4>",
                    //             duration: 1000
                    //         });
                    //     }
                    // });
                    IonPopup.close();
                    Router.go("/orderSortSupplierList/" + acnumber);
                }
            }]
        });
    }
})