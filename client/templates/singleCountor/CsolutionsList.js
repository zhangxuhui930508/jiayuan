/**
 * Created by zhangxuhui on 15/10/26.
 */

Template.CsolutionsList.helpers({
    headObj: function () {
        return {
            title: '我的方案',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    },
    solutionsList: function () {
        var info = Solutions.find({}, {sort: {Date: -1}});
        return info;
    },
    getformatDate: function (data) {
        if (data) {
            var time = data.Format("yyyyMMdd");
            console.log(time);
            return time;
        }
    }
});

Template.CsolutionsList.events({
    'click .solutionListDel': function (event) {
        if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
            var id = $(event.currentTarget).attr("id");
            console.log(id);
            Meteor.call("delSolution", id, function (err, result) {
                if (err) {
                    IonLoading.show({
                        customTemplate: "<h4>删除方案失败!</h4>",
                        duration: 1000
                    });
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>删除方案成功!</h4>",
                        duration: 1000
                    });
                }
            })
        } else {
            IonLoading.show({
                customTemplate: "<h4>您没有权限操作!</h4>",
                duration: 1000
            });
        }
    },
    'click .solutionName': function (event) {
        console.log("-----------")
        var id = $(event.currentTarget).children("span").attr("id").trim();
        //Session.set("CustomersolutionListID", id);
        //Session.set("scheneName", ScheneName);
        //console.log(ScheneName);
        //console.log(id);
        Router.go("/CSolutionDetail/" + id);
    },

});

