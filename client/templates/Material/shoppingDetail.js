/**
 * Created by zhangxuhui on 16/1/12.
 */
Template.shoppingDetail.rendered = function () {
    var orderType = Router.current().params.orderType;
    var mId = Router.current().params.mId;
    //this.subscribe("materialDetailInfoById", mId, function () {
    //});
    var ACNumber;

    if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
        ACNumber = Session.get("customerDecorateInfo").ACNumber;
    } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
        ACNumber = Session.get("customerLogin");
    }
    this.subscribe("shoppingByACnumberandmId", ACNumber, mId);
    this.subscribe("images");
};

Template.shoppingDetail.helpers({
    headObj: function () {
        return {
            title: '产品详细页'
        }
    },
    materialDetailList: function () {
        var info = MaterialDetailList.findOne();
        if (info) {
            return info;
        }
    },

    schemaDetail: function () {
        var currentOrderType = Router.current().params.orderType;
        var ordertype = parseInt(currentOrderType);
        if (ordertype) {
            if (ordertype === 6) {
                ordertype = 5;
            }
        } else {
            ordertype = 0;
        }
        return arr[ordertype]
    },
    'materialCount': function () {
        var shoppingInfo = ShoppingCart.findOne();
        if (shoppingInfo) {
            return shoppingInfo.materialCount;
        }
    },
    solutionDetail: function () {
        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        var materialID = Router.current().params.mId;
        var shoppingInfo = ShoppingCart.findOne({ACNumber: ACNumber, materialID: materialID})
        if (shoppingInfo) {
            return shoppingInfo.otherAttributes;
        }
        //var info = MaterialDetailList.findOne();
        //if (info && info.产品订单分类) {
        //    var idx = info.产品订单分类;
        //    console.log(idx);
        //    var infoext = {};
        //    if (idx === 6) {
        //        idx = 6;
        //    }
        //    infoext = materialTypeData[idx];
        //    return _.extend(info, infoext);
        //}
    },
    images: function (id) {
        var imageInfo = Images.find({_id: id});
        return imageInfo;
    },
    isimage: function () {
        var MaterialListInfo = MaterialDetailList.findOne();
        if (MaterialListInfo && MaterialListInfo.picture) {
            return true;
        } else {
            return false;
        }
    },

    'images1': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image1:", materialInfo.picture[0]);
            if (materialInfo.picture[0]) {
                return materialInfo.picture[0];
            }
        }
    },
    'images2': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image2:", materialInfo.picture[1]);
            if (materialInfo.picture[1]) {
                return materialInfo.picture[1];
            }
        }
    },
    'images3': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image3:", materialInfo.picture[2]);
            if (materialInfo.picture[2]) {
                return materialInfo.picture[2];
            }
        }
    },
    'images4': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image4:", materialInfo.picture[3]);
            if (materialInfo.picture[3]) {
                return materialInfo.picture[3];
            }
        }
    },
    'images5': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image5:", materialInfo.picture[4]);
            if (materialInfo.picture[4]) {
                return materialInfo.picture[4];
            }
        }
    },
    'images6': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image6:", materialInfo.picture[5]);
            if (materialInfo.picture[5]) {
                return materialInfo.picture[5];
            }
        }
    },
    'isimages1': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[0]) {
                return false
            } else {
                return true;
            }
        }
    }, 'isimages2': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[1]) {
                return false;
            } else {
                return true;
            }
        }
    }, 'isimages3': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[2]) {
                return false
            } else {
                return true;
            }
        }
    }, 'isimages4': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[3]) {
                return false
            } else {
                return true;
            }
        }
    }, 'isimages5': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[4]) {
                return false
            } else {
                return true;
            }
        }
    }, 'isimages6': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[5]) {
                return false
            } else {
                return true;
            }
        }
    },
});


Template.shoppingDetail.events({
    //数量减
    'click .addd': function (event) {
        var countval = $(event.currentTarget).siblings(".add").children("input").val();
        var jisuan = Number(countval) + 1;
        $(event.currentTarget).siblings(".add").children("input").val(jisuan);

    },
    //    数量减
    'click .jian': function (event) {
        var countval = $(event.currentTarget).siblings(".add").children("input").val();
        console.log(countval);
        var jianvalue = Number(countval) - 1;
        if (jianvalue <= 0) {
            IonLoading.show({
                customTemplate: "<h4>商品数量不能小于1!</h4>",
                duration: 1000
            });
            $(event.currentTarget).siblings(".add").children("input").val(1);
        } else {
            $(event.currentTarget).siblings(".add").children("input").val(jianvalue);
        }
    },
    'click .backMenuList': function () {

        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        Router.go("/shopping/" + ACNumber);

        //if (Session.get("backendTemplate")) {
        //    Session.set("backendTemplate", null);
        //    Router.go("/shopping/" + Session.get("customerDecorateInfo").ACNumber);
        //} else {
        //    var id = Router.current().params.mCid;
        //    Router.go("/materialList/" + id);
        //}
    }
});

solutionDetailsSchema1 = new SimpleSchema({
    '门（窗）': {
        type: String, label: "门（窗）", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"}
                ];
            }
        }
    },
    '门（窗）套': {
        type: String, label: "门（窗）套", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"}
                ];
            }
        }
    },
    '踢脚线': {
        type: String, label: "踢脚线", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"}
                ];
            }
        }
    },
    '饰面板': {
        type: String, label: "饰面板", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "白色混水", value: "白色混水"},
                    {label: "橡木-本色-H", value: "橡木-本色-H"},
                    {label: "橡木-1号色-H", value: "橡木-1号色-H"},
                    {label: "橡木-2号色-H", value: "橡木-2号色-H"},
                    {label: "橡木-3号色-H", value: "橡木-3号色-H"},
                    {label: "沙比利-本色-H", value: "沙比利-本色-H"},
                    {label: "沙比利-1号色-H", value: "沙比利-1号色-H"},
                    {label: "沙比利-2号色-H", value: "沙比利-2号色-H"},
                    {label: "沙比利-3号色-H", value: "沙比利-3号色-H"},
                    {label: "黑胡桃木-本色-H", value: "黑胡桃木-本色-H"},
                    {label: "黑胡桃木-1号色-H", value: "黑胡桃木-1号色-H"},
                    {label: "黑胡桃木-2号色-H", value: "黑胡桃木-2号色-H"},
                    {label: "黑胡桃木-3号色-H", value: "黑胡桃木-3号色-H"},
                    {label: "北美樱桃木-本色-H", value: "北美樱桃木-本色-H"},
                    {label: "北美樱桃木-1号色-H", value: "北美樱桃木-1号色-H"},
                    {label: "北美樱桃木-2号色-H", value: "北美樱桃木-2号色-H"},
                    {label: "北美樱桃木-3号色-H", value: "北美樱桃木-3号色-H"},
                    {label: "南美樱桃木-本色-H", value: "南美樱桃木-本色-H"},
                    {label: "南美樱桃木-1号色-H", value: "南美樱桃木-1号色-H"},
                    {label: "柚木-本色", value: "柚木-本色"},
                    {label: "柚木-1号色", value: "柚木-1号色"},
                    {label: "柚木-2号色", value: "柚木-2号色"},
                    {label: "柚木-3号色", value: "柚木-3号色"},
                    {label: "沙比利01号色-980", value: "沙比利01号色-980"},
                    {label: "沙比利02号色-980", value: "沙比利02号色-980"},
                    {label: "沙比利03号色-980", value: "沙比利03号色-980"},
                    {label: "金玫瑰04号色-980", value: "金玫瑰04号色-980"},
                    {label: "金玫瑰05号色-980", value: "金玫瑰05号色-980"},
                    {label: "金玫瑰06号色-980", value: "金玫瑰06号色-980"},
                    {label: "柚木07号色-980", value: "柚木07号色-980"},
                    {label: "柚木08号色-980", value: "柚木08号色-980"},
                    {label: "柚木09号色-980", value: "柚木09号色-980"},
                    {label: "柚木09号色-980", value: "柚木09号色-980"},
                    {label: "黑胡桃10号色-980", value: "黑胡桃10号色-980"},
                    {label: "黑胡桃11号色-980", value: "黑胡桃11号色-980"},
                    {label: "黑胡桃12号色-980", value: "黑胡桃12号色-980"},
                    {label: "花梨木13号色-980", value: "花梨木13号色-980"},
                    {label: "花梨木14号色-980", value: "花梨木14号色-980"},
                    {label: "花梨木15号色-980", value: "花梨木15号色-980"},
                    {label: "樱桃木16号色-980", value: "樱桃木16号色-980"},
                    {label: "樱桃木17号色-980", value: "樱桃木17号色-980"},
                    {label: "樱桃木18号色-980", value: "樱桃木18号色-980"},
                    {label: "西南桦19号色-980", value: "西南桦19号色-980"},
                    {label: "南美樱桃木-2号色-H", value: "南美樱桃木-2号色-H"},
                    {label: "西南桦20号色-980", value: "西南桦20号色-980"},
                    {label: "西南桦21号色-980", value: "西南桦21号色-980"},
                    {label: "橡木22号色-980", value: "橡木22号色-980"},
                    {label: "橡木23号色-980", value: "橡木23号色-980"},
                    {label: "橡木24号色-980", value: "橡木24号色-980"},
                    {label: "南美樱桃木-3号色-H", value: "南美樱桃木-3号色-H"}

                ];
            }
        }
    },
    '玻璃': {
        type: String, label: "玻  璃", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "白玻璃", value: "白玻璃"},
                    {label: "磨砂玻璃", value: "磨砂玻璃"},
                    {label: "香梨玻璃", value: "香梨玻璃"},
                    {label: "布纹玻璃", value: "布纹玻璃"},
                    {label: "钻石玻璃", value: "钻石玻璃"}
                ];
            }
        }
    },
    '门套线规格': {
        type: String, label: "门套线规格", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "60mm", value: "60mm"},
                    {label: "无", value: "无"},
                    {label: "JYW-MX-18(60mm)", value: "JYW-MX-18(60mm)"},
                    {label: "JYW-MX-12(60mm)", value: "JYW-MX-12(60mm)"},
                    {label: "JYW-MX-16(60mm)", value: "JYW-MX-16(60mm)"},
                    {label: "JYW-MX-14(60mm)", value: "JYW-MX-14(60mm)"},
                    {label: "JYW-MX-9(60mm)", value: "JYW-MX-9(60mm)"},
                    {label: "JYW-MX-2(60mm)", value: "JYW-MX-2(60mm)"},
                    {label: "JYW-MX-10(60mm)", value: "JYW-MX-10(60mm)"},
                    {label: "JYW-MX-19(60mm)", value: "JYW-MX-19(60mm)"},
                    {label: "JYW-MX-7(60mm)", value: "JYW-MX-7(60mm)"},
                    {label: "JYW-MX-8(60mm)", value: "JYW-MX-8(60mm)"},
                    {label: "JYW-MX-20(60mm)", value: "JYW-MX-20(60mm)"},
                    {label: "JYW-MX-11(60mm)", value: "JYW-MX-11(60mm)"},
                    {label: "JYW-MX-17(60mm)", value: "JYW-MX-17(60mm)"},
                    {label: "JYW-MX-21(60mm)", value: "JYW-MX-21(60mm)"},
                    {label: "JYW-MX-13(60mm)", value: "JYW-MX-13(60mm)"},
                ];
            }
        }
    },
    '踢脚线规格': {
        type: String, label: "踢脚线规格", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "150mm内", value: "150mm内"},
                    {label: "100mm内", value: "100mm内"}
                ];
            }
        }
    },
    '线条型号': {
        type: String, label: "线条型号", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "开门 贝犀(仅用于变更)", value: "开门 贝犀(仅用于变更)"},
                    {label: "无", value: "无"},
                    {label: "门套线-1-H", value: "门套线-1-H"},
                    {label: "门套线-2-H", value: "门套线-2-H"},
                    {label: "门套线-3-H", value: "门套线-3-H"},
                    {label: "门套线-4-H", value: "门套线-4-H"},
                    {label: "门套线-5-H", value: "门套线-5-H"},
                    {label: "门套线-6-H", value: "门套线-6-H"},
                    {label: "门套线-7-H", value: "门套线-7-H"},
                    {label: "门套线-8-H", value: "门套线-8-H"},
                    {label: "门套线-1-980", value: "门套线-1-980"},
                    {label: "门套线-2-980", value: "门套线-2-980"},
                    {label: "门套线-3-980", value: "门套线-3-980"},
                    {label: "门套线-4-980", value: "门套线-4-980"},
                    {label: "门套线-5-980", value: "门套线-5-980"},
                    {label: "门套线-6-980", value: "门套线-6-980"},
                    {label: "门套线-7-980", value: "门套线-7-980"},
                    {label: "门套线-8-980", value: "门套线-8-980"},
                    {label: "移门 贝犀(仅用于变更)", value: "移门 贝犀(仅用于变更)"}
                ];
            }
        }
    }
});
solutionDetailsSchema2 = new SimpleSchema({
    '白灰板材': {
        type: String, label: "白灰板材", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "白色板", value: "白色板"},
                    {label: "灰色板", value: "灰色板"}
                ];
            }
        }
    },
    '非标': {
        type: String, label: "非标", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "白色板", value: "白色板"},
                    {label: "胡桃木色", value: "胡桃木色"},
                    {label: "樱桃木色", value: "樱桃木色"},
                    {label: "枫木色", value: "枫木色"},
                    {label: "白橡木色", value: "白橡木色"},
                    {label: "灰色板", value: "灰色板"}
                ];
            }
        }
    },
    '柜体五金': {
        type: String, label: "柜体五金", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "有", value: "有"}
                ];
            }
        }
    },
    '拉篮': {
        type: String, label: "拉篮", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "有", value: "有"}
                ];
            }
        }
    },
    '木纹板材': {
        type: String, label: "木纹板材", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "胡桃木色", value: "胡桃木色"},
                    {label: "樱桃木色", value: "樱桃木色"},
                    {label: "枫木色", value: "枫木色"},
                    {label: "白橡木色", value: "白橡木色"}
                ];
            }
        }
    },
    '深度': {
        type: String, label: "深度", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "600", value: "600"}
                ];
            }
        }
    }
});
solutionDetailsSchema3 = new SimpleSchema({
    '边框材料': {
        type: String, label: "边框材料", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "白色", value: "白色"},
                    {label: "古铜色", value: "古铜色"},
                    {label: "胡桃木色", value: "胡桃木色"},
                    {label: "亮银", value: "亮银"},
                    {label: "砂银", value: "砂银"},
                    {label: "香槟色", value: "香槟色"}
                ];
            }
        }
    },
    '边框造型': {
        type: String, label: "边框造型", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "方型", value: "方型"},
                    {label: "双蝶", value: "双蝶"},
                    {label: "鸭嘴", value: "鸭嘴"},
                    {label: "腰鼓式", value: "腰鼓式"},
                    {label: "一字型", value: "一字型"}
                ];
            }
        }
    },
    '玻璃钢化': {
        type: String, label: "玻璃钢化", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "玻璃钢化", value: "玻璃钢化"}
                ];
            }
        }
    },
    '门型': {
        type: String, label: "门型", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "平板式", value: "平板式"},
                    {label: "等腰式", value: "等腰式"},
                    {label: "双腰式", value: "双腰式"},
                    {label: "下腰式", value: "下腰式"},
                    {label: "等分式", value: "等分式"},
                    {label: "和式", value: "和式"}
                ];
            }
        }
    },
    '开门': {
        type: String, label: "门型", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                ];
            }
        }
    },
    '配件': {
        type: String, label: "配件", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"}
                ];
            }
        }
    },
    '藤编': {
        type: String, label: "藤编", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "藤编", value: "藤编"}
                ];
            }
        }
    },
    '折门': {
        type: String, label: "折门", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},

                ];
            }
        }
    },
    "贴纸喷砂": {
        type: String, label: "贴纸喷砂", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "贴纸喷砂", value: "贴纸喷砂"}
                ];
            }
        }
    },
    "线条规格": {
        type: String, label: "线条规格", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "6mm", value: "6mm"},
                    {label: "35mm", value: "35mm"},
                    {label: "40mm", value: "40mm"},
                    {label: "50mm", value: "50mm"}
                ];
            }
        }
    },
    "移门": {
        type: String, label: "移门", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "90030101", value: "90030101"},
                    {label: "90030102", value: "90030102"},
                    {label: "90030103", value: "90030103"},
                    {label: "90030105", value: "90030105"},
                    {label: "90030106", value: "90030106"},
                    {label: "90030107", value: "90030107"},
                    {label: "90030108", value: "90030108"},
                    {label: "90030109", value: "90030109"},
                    {label: "90030110", value: "90030110"},
                    {label: "90030111", value: "90030111"},
                    {label: "90030112", value: "90030112"},
                    {label: "90030113", value: "90030113"},
                    {label: "90030114", value: "90030114"},
                    {label: "90030116", value: "90030116"},
                    {label: "90030117", value: "90030117"},
                    {label: "90030118", value: "90030118"},
                    {label: "90030115", value: "90030115"}
                ];
            }
        }
    }
});
solutionDetailsSchema4 = new SimpleSchema({
    "安装部位": {
        type: String, label: "安装部位", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "主卧", value: "主卧"},
                    {label: "次卧", value: "次卧"},
                    {label: "过道", value: "过道"},
                    {label: "客厅", value: "客厅"},
                    {label: "餐厅", value: "餐厅"},
                    {label: "阳台", value: "阳台"},
                    {label: "厨房", value: "厨房"},
                    {label: "卫生间", value: "卫生间"},
                    {label: "进户门", value: "进户门"}
                ];
            }
        }
    },
    "壁柜门": {
        type: String, label: "壁柜门", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"}
                ];
            }
        }
    },
    "铰链阻尼器": {
        type: String, label: "铰链阻尼器", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"},
                    {label: "有", value: "有"}
                ];
            }
        }
    },
    "扇数": {
        type: String, label: "扇数", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "1", value: "1"},
                    {label: "2", value: "2"},
                    {label: "3", value: "3"},
                    {label: "4", value: "4"}
                ];
            }
        }
    },
    "饰面型号": {
        type: String, label: "饰面型号", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "胡桃木色", value: "胡桃木色"},
                    {label: "樱桃木色", value: "樱桃木色"},
                    {label: "枫木色", value: "枫木色"},
                    {label: "白橡木色", value: "白橡木色"},
                    {label: "白色混水", value: "白色混水"}
                ];
            }
        }
    }
});
solutionDetailsSchema5 = new SimpleSchema({
    "楼梯": {
        type: String, label: "楼梯", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"}
                ];
            }
        }
    }
});
solutionDetailsSchema6 = new SimpleSchema({
    "厨具": {
        type: String, label: "厨具", optional: true, autoform: {
            type: "select",
            options: function () {
                return [
                    {label: "无", value: "无"}
                ];
            }
        }
    }
});
solutionDetailsSchema0 = new SimpleSchema({});
arr = [solutionDetailsSchema0, solutionDetailsSchema1, solutionDetailsSchema2, solutionDetailsSchema3, solutionDetailsSchema4, solutionDetailsSchema5, solutionDetailsSchema6];

function processDoc(insertDoc) {
    try {
        // customerDecorateInfo   Object {ACNumber: "132781", mode: "局部翻新", parts: "主卫"}
        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        var materialID = $(".currentMaterialId").val().trim();//材料ID
        var materialCount = $(".input_text").val().trim(); //材料的数量
        var materialOrderType = $(".orderID").val().trim();//订单分类


        var currentCustomerInfo = ShoppingCart.find({ACNumber: ACNumber, materialID: materialID}).count();
        if (currentCustomerInfo) {
            Meteor.call("updateShoppingCartInfo", ACNumber, materialID, materialCount, insertDoc, function (err, result) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>添加购物车成功!</h4>",
                        duration: 1000
                    });

                    var ACNumber;

                    if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
                        ACNumber = Session.get("customerDecorateInfo").ACNumber;
                    } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
                        ACNumber = Session.get("customerLogin");
                    }
                    Router.go("/shopping/" + ACNumber);
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>添加购物车失败!</h4>",
                        duration: 1000
                    });
                }
            });
        }
        //if (currentCustomerInfo) {
        //    //判断购物车里面是否已经添加了此材料
        //    var shoppingCart = ShoppingCart.find({
        //        ACNumber: customerDecorateInfo.ACNumber,
        //        materialID: materialID
        //    }).count();
        //    if (shoppingCart) {
        //        ShoppingCart.find({
        //            ACNumber: customerDecorateInfo.ACNumber,
        //            materialID: materialID
        //        }).forEach(function (shopingCart) {
        //            if (shopingCart.materialID == materialID) {
        //                materialCount = parseInt(shopingCart.materialCount) + parseInt(materialCount);
        //                ShoppingCart.update({
        //                    _id: shopingCart._id
        //                }, {
        //                    $set: {
        //                        materialCount: materialCount,
        //                        otherAttributes: insertDoc
        //                    }
        //                }, function (err, result) {
        //                    if (!err) {
        //                        IonLoading.show({
        //                            customTemplate: "<h4>添加购物车成功!</h4>",
        //                            duration: 1000
        //                        });
        //                    } else {
        //                        IonLoading.show({
        //                            customTemplate: "<h4>添加购物车失败!</h4>",
        //                            duration: 1000
        //                        });
        //                    }
        //                });
        //            }
        //        });
        //    }
        return true;
        //}
    }
    catch
        (e) {
        console.log(e);
        return false;
    }
}

AutoForm.hooks({
    sd: {
        onSubmit: function (insertDoc, updateDoc, currentDoc) {
            if (processDoc(insertDoc)) {
                var id = Router.current().params.mCid;
                //Router.go("/materialList/" + id);
                this.done();
            }
            return false;
        }
    }
});