/**
 * Created by pcbeta on 15-8-5.
 */

Template.shopping.helpers({
    headObj: function () {
        return {
            title: '选材车'
        }
    },
    shopList: function () {
        var shoppingMaterialInfo = MaterialDetailList.find();
        if (shoppingMaterialInfo) {
            return shoppingMaterialInfo;
        }
    },
});

Template.shopping.events({
    'click .save_shop': function (event) {
        if (Roles.userIsInRole(Meteor.userId(), ["designer", "client"], "user-group")) {

            var count = ShoppingCart.find().fetch();
            if (count.length == 0) {
                IonLoading.show({
                    customTemplate: "<h4>方案材料不能为空!</h4>",
                    duration: 1000
                });
                return;
            }

            IonPopup.prompt({
                title: '设置方案名称',
                template: '请输入方案名称',
                // template: '<input type="text" value="我的方案1">',
                okText: '确定',
                cancelText: '取消',
                inputType: 'text',
                inputPlaceholder: '我的方案1',
                // inputValue: "我的方案1",
                onOk: function () {
                    var name = $('.popup-body input').val().trim();
                    if (!name) {
                        IonLoading.show({
                            customTemplate: "<h4>方案名称不能为空!</h4>",
                            duration: 1000
                        });
                    } else {
                        var ACNumber;
                        if (Session.get("customerDecorateInfo")) {
                            ACNumber = Session.get("customerDecorateInfo").ACNumber
                        } else {
                            ACNumber = Session.get("customerLogin");
                        }
                        Meteor.call("addSolution", ACNumber, name, function (err, result) {
                            if (!err) {
                                IonLoading.show({
                                    customTemplate: "<h4>方案保存成功!</h4>",
                                    duration: 1000
                                });
                                if (Session.get("customerLogin")) {
                                    Router.go("/mycustomer")
                                } else {
                                    Router.go("/customerDetail/customerType/" + Session.get("customerDecorateInfo").cType + '/customerId/' + Session.get("customerDecorateInfo").cId);
                                }
                            } else {
                                IonLoading.show({
                                    customTemplate: err,
                                    duration: 1000
                                });
                            }
                        });
                    }
                }
            });
        } else {
            IonLoading.show({
                customTemplate: "<h4>您没有权限操作!</h4>",
                duration: 1000
            });
        }
    },
    //todo 其他角色 检查
    'click .backTemplates': function () {
        var back = Session.get("shoppingbackTemplate");
        if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager"], "user-group")) {
            if (back == "mycustomer") {
                Router.go("/mycustomer");
            } else {
                var materialClassifyId = Session.get("currentMaterialClassifyId");
                Router.go("/materialList/" + materialClassifyId);
            }
        } else if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
            if (back == "mycustomer") {
                Router.go("/mycustomer")
            } else if (back == "materialList") {
                var materialClassifyId = Session.get("currentMaterialClassifyId");
                Router.go("/materialList/" + materialClassifyId);
            }
        }
    },
    'click .delShoppingCartMaterial': function (event) {
        if (Roles.userIsInRole(Meteor.userId(), ["designer", "client"], "user-group")) {

            var materialId = $(event.currentTarget).attr("id");
            var ACNumber;
            if (Session.get("customerDecorateInfo")) {
                ACNumber = Session.get("customerDecorateInfo").ACNumber
            } else {
                ACNumber = Session.get("customerLogin");
            }
            Meteor.call("delShoppingCartMaterialInfo", ACNumber, materialId, function (err, result) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>删除成功!</h4>",
                        duration: 1000
                    });
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>删除失败!</h4>",
                        duration: 1000
                    });
                }
            });
        } else {
            IonLoading.show({
                customTemplate: "<h4>您没有权限操作!</h4>",
                duration: 1000
            });
        }
    }
    ,
    'click .materialInfo': function (event) {
        var ID = $(event.currentTarget).children("h2").attr("id");
        console.log(ID);
        var orderType = $(event.currentTarget).children("input").val();
        var materialClassifyId = Session.get("currentMaterialClassifyId");
        Router.go("/shoppingDetail/mOrderType/" + orderType + "/materialID/" + ID);
    }
});