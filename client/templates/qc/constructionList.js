/**
 * Created by zhangxuhui on 16/4/15.
 */
Template.constructionList.rendered = function () {
    this.autorun(function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
        var type = Router.current().params.type;
        Meteor.subscribe("construction", Meteor.userId(), type, function (err, result) {
            var checkProjectInfo = checkConstructionProject.find().fetch();
            var acnumbers = [];
            _.each(checkProjectInfo, function (info) {
                acnumbers.push(info.acnumber);
            });
            console.log(acnumbers);
            Meteor.call("getCustInfo", acnumbers, function (err, result) {
                console.log(result, "result");
                Session.set("custInfo", result);
            })
        });
    })
};

Template.constructionList.helpers({
    headObj: function () {
        return {}
    },
    titleName: function () {
        var titleName = Router.current().params.type;
        if (titleName == "notExamined") {
            return '未检工地'
        } else {
            return '已检工地'
        }
    }, Info: function () {
        var select = {};
        if (Session.get("currentCustomerSearchVal")) {
            select.acnumber = new RegExp(Session.get("currentCustomerSearchVal"), "i");
        }
        return checkConstructionProject.find(select, {sort: {updateDate: -1}})
    },

    getCustInfoAddress: function (acnumber) {
        try {
            return Session.get("custInfo")[acnumber].Address;
        } catch (e) {

        }
    },

});

Template.constructionList.events({
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/qualityLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .gocustomerDetail': function (event) {
        var id = $(event.currentTarget).attr("id").trim();
        console.log(id);
        console.log("----------------------------------------");
        Router.go("/constructionDetail/" + id);
    }
});

Template.constructionList.onDestroyed(function () {
    Session.set("currentCustomerSearchVal", null);
    Session.set("custInfo", null);
});