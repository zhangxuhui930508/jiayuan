/**
 * Created by zhangxuhui on 16/3/18.
 */

Template.constructionCType.onRendered(function () {
    this.subscribe("getEmployeesInfo", Session.get("currentConstructionId"));
    Meteor.call("getConstructionCount", Session.get("currentConstructionId"), function (err, result) {
        Session.set("constructionCTypeCount", result);
    })
});

Template.constructionCType.helpers({
    headObj: function () {
        return {}
    },
    'titleName': function () {
        var info = AC_Personnel.findOne();
        if (info) {
            return info.pName;
        }
    },
    notWorkCount: function () {
        return construtionRelationClient.find({cType: "未开工"}).count();
        // if (Session.get("constructionCTypeCount")) {
        //     return Session.get("constructionCTypeCount").notServiceCount;
        // }
    }, WorkCount: function () {
        // if (Session.get("constructionCTypeCount")) {
        //     return Session.get("constructionCTypeCount").ServiceCount;
        // }
        return construtionRelationClient.find({cType: "已开工"}).count();

    }, CompletedCount: function () {
        // if (Session.get("constructionCTypeCount")) {
        //     return Session.get("constructionCTypeCount").finishCount;
        // }
        return construtionRelationClient.find({cType: "已完成"}).count();

    },
    'isUnReadInfo': function () {
        var info = MessageState.find().count();
        if (info > 0) {
            return true;
        }
    }
});

Template.constructionCType.events({
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/constructionLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .notWork': function (event, template) {
        Router.go("/construtionList/未开工")
    },
    'click .work': function (event) {
        Router.go("/construtionList/已开工")
    },
    'click .Completed': function (event) {
        Router.go("/construtionList/已完成");
    },
    'click .submitQC': function (event) {
        event.stopPropagation();
        Router.go("/searchCustomer/1")
    },
    'click .RectificationList': function (event) {
        event.stopPropagation();

        Router.go("/searchCustomer/3")
    }, 'click .materialChat': function () {
        Router.go("/materialChatList");
    },
    'click .backMenuList': function () {
        Session.set("currentConstructionId", null);
        Router.go("/constructionCaptainList");
    },
    'click .CandSChatInfo': function () {
        Router.go("/CandSChat");
    }
});

Template.constructionCType.onDestroyed(function () {
    Session.set("constructionCTypeCount", null);
});