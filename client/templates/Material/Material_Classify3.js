/**
 * Created by pcbeta on 15-8-21.
 */


Template.MaterialClassify3.rendered = function () {
    Session.set("customerMaterialListCountListKey03", {})
    Session.set("backtemplate", "MaterialClassify3");
    Session.set("Nav_third", null);
    var info = Session.get("second_level");
    if (info) {
        var arr = [];
        var infoList = info.classList;
        _.each(info, function (t) {
            arr.push(t.ID)
        });

        Meteor.call("materialListCountList", arr, function (err, result) {
            if (!err) {
                console.log(result);
                Session.set("customerMaterialListCountListKey03", result);
            }
        })

    }


};

Template.MaterialClassify3.helpers({
    'headObj': function () {
        return {
            title: '材料分类',
            //leftButton: true,
            //backTemplate: 'MaterialClassify2'
        }
    },
    // 第二级分类中删选后的数据，在第三级分类中显示的数据
    'MaterialClassifyList': function () {
        var info = Session.get("second_level");
        if (info) {
            return info;
        }
    },
    //   第二级分类的名称
    upper_level: function () {
        var Mclassify2 = Router.current().params.Mclassify2;
        if (Mclassify2) {
            return Mclassify2;
        }
    },
    // 第一级分类的名称
    oneupper_level: function () {
        var Mclassify1 = Router.current().params.Mclassify1;
        if (Mclassify1) {
            return Mclassify1;
        }
    },
    titleName: function () {
        var parts = "材料分类";
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        if (Session.get("customerDecorateInfo")) {
            parts = Session.get("customerDecorateInfo").parts;
        } else {
            parts = "材料分类";
        }
        return parts;
    },
    'materialCount': function (id, classList) {
        try {
            if (classList.length == 0) {
                var json = Session.get("customerMaterialListCountListKey03");
                if (id && json[id]) {
                    return json[id];
                } else {
                    return "0";
                }
            } else {
                return classList.length;
            }
        } catch (e) {
            return "0";
        }
    }
});

Template.MaterialClassify3.events({
    'click .materClassify3 .item': function (event) {
        var getMaterialCount = $(event.currentTarget).children("span").children("span").text();
        //if (getMaterialCount == "（ 0 ）") {
        //    IonLoading.show({
        //        customTemplate: "<h4>没有此类材料，请选择其他材料!</h4>",
        //        duration: 500
        //    });
        //    return;
        //}


        var itemName = $(event.currentTarget).children("h2").text().trim();
        var classifyName = Session.get("classifyName");
        classifyName.three = itemName;
        Session.set("classifyName", classifyName);

        var id = $(event.currentTarget).children("i").text().trim();//材料分类中的ID
        var info = Session.get("second_level");//获取到第二级分类筛选后到数据
        for (var i = 0; i < info.length; i++) {
            if (info[i].分类名称 == itemName) {
                if (info[i].classList.length == 0) {
                    Router.go("/materialList/" + id);
                } else {
                    Session.set("third_level", info[i].classList);

                    var classify1 = Router.current().params.Mclassify1;
                    var classify2 = Router.current().params.Mclassify2;
                    Router.go("/MaterialClassify4/Mclassify1/" + classify1 + "/Mclassify2/" + classify2 + "/Mclassify3/" + itemName);
                }
            }
        }
    },
    'click .go_marerialClassify1': function (event) {
        Router.go("/MaterialClassify");//跳转到分类列表1
    },
    'click .go_marerialClassify2': function (event) {
        var classify1 = Router.current().params.Mclassify1;
        Router.go("/MaterialClassify2/" + classify1);
    },
    'click .DecorateArea': function () {
        Router.go("/DecorateArea");
    },
    'click .backMenuList': function () {
        var classify1 = Router.current().params.Mclassify1;
        Router.go("/MaterialClassify2/" + classify1);
    }
});

Template.MaterialClassify2.destroy = function () {
    Session.set("customerMaterialListCountListKey03", {});
};