/**
 * Created by zhangxuhui on 16/8/15.
 */

Template.satisfaction.onRendered(function () {
    this.subscribe("publishOtherImages")
});


Template.satisfaction.helpers({
    headObj: function () {
        return {
            title: "满意度",
            leftButton: true,
            // backTemplate: 'customerType'
        }
    },

});

Template.satisfaction.events({
    'click .backMenuList': function () {
        history.back();
    }
});

Schemas = {};
Schemas.satisfactionInfo = new SimpleSchema({
    acnumber: {
        type: String,
        autoValue: function () {
            return Router.current().params.acnumber;
        }
    },
    send: {
        type: String,
        autoValue: function () {
            return Router.current().params.constructionId;
        }
    }, checkId: {
        type: String,
        autoValue: function () {
            return Router.current().params.checkId;
        }
    },
    'picture': {
        type: [String],
        label: "上传图片",
        optional: true
    },
    'picture.$': {
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'OtherImages',
                accept: 'image/*',
                label: '浏览'
            }
        }
    },
    createBy: {
        type: String,
        autoValue: function () {
            return Meteor.userId();
        }
    },
    createAt: {
        type: Date,
        autoValue: function () {
            return new Date();
        }
    },
    isEnter: {
        type: Boolean,
        autoValue: function () {
            if (this.isInsert) {
                return false;
            }
        },
        denyUpdate: true
    }
});
Satisfaction.attachSchema(Schemas.satisfactionInfo);


AutoForm.hooks({
    insertSatisfaction: {
        onSubmit: function (insertDoc, updateDoc, currentDoc) {

        },

        // Called when any submit operation succeeds
        onSuccess: function (formType, result) {
            history.back();
        },

        // Called when any submit operation fails
        onError: function (formType, error) {
        },
    }
})