/**
 * Created by zhangxuhui on 16/3/19.
 */

Template.borrowingType.rendered = function () {

    this.subscribe("borrowingInfo", Session.get("currentConstructionId"), true, function (err, result) {});
    this.subscribe("borrowingInfo", Session.get("currentConstructionId"), false, function (err, result) {});

};

Template.borrowingType.helpers({
    headObj: function () {
        return {
            title: '借款类型'
        }
    },
    'notissuedCount': function () {
        var info = erpProjectLoan.find({财务审核: false}).count();
        return info;
    },
    'issuedCount': function () {
        var info = erpProjectLoan.find({财务审核: true}).count();
        return info;
    }
});

Template.borrowingType.events({
    'click .issued': function () {
        Router.go("/borrowingList/true");
    },
    'click .notissued': function () {
        Router.go("/borrowingList/false");
    }, 'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/constructionLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
});

