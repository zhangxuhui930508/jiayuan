/**
 * Created by zhangxuhui on 16/4/11.
 */

createRoomRelation = {
    /**
     * 返回当前客户的设计师的uid
     * @param acnumber 客户受理编号
     * @returns {*}
     *
     */
    "getDesignRelation": function (acnumber) {
        var designerUserIds = [];
        var designerinfo = DesignerRelationClient.findOne({cId: acnumber});
        if (designerinfo) {
            var userInfo = Meteor.users.findOne({ID: parseInt(designerinfo.dId)});
            if (userInfo) {
                designerUserIds.push(userInfo._id);
            }
        }
        return designerUserIds;
    },
    /**
     * 返回当前客户的施工队长的uid
     * @param acnumber 客户受理编号
     * @param userIds 可以参与聊天的uid
     * @returns {*}
     */
    "getBuildRelation": function (acnumber) {
        var construtionUserIds = [];
        var construtionInfo = construtionRelationClient.findOne({cId: acnumber});
        if (construtionInfo) {
            var users = Meteor.users.findOne({ID: parseInt(construtionInfo.construtionId)});
            if (users) {
                construtionUserIds.push(users._id);
            }
        }
        return construtionUserIds;
    },
    /**
     * 返回当前客户 当前材料的供应商的uid
     * @param acnumber
     * @param materialId
     * @param userIds
     * @returns {*}
     */
    'getSupplierRelation': function (acnumber, materialId) {
        var supplierUserIds = [];
        var materialInfo = ERP_Budget_Material.findOne({_id: materialId});
        if (materialInfo) {
            var erpViewVerifyOrderAllInfo = erpViewVerifyOrderAll.findOne({
                "受理编号": acnumber,
                "定单号": materialInfo.定单编号
            });
            if (erpViewVerifyOrderAllInfo) {
                var username = erpViewVerifyOrderAllInfo.供货商编号.toString();
                var user = Meteor.users.findOne({ID: erpViewVerifyOrderAllInfo.供货商编号, username: username});
                if (user) {
                    supplierUserIds.push(user._id);
                }
            }
            return supplierUserIds;
        }
    },
    /**
     * 获得客户的店长和大区经理的uid
     * @param acnumber
     * @returns {Array}
     */
    'getOtherRelation': function (acnumber) {
        var employeeInfo = AC_Personnel.find({pBranch: {$nin: [0, null]}}).fetch();
        if (employeeInfo) {
            var custInfo = Cust_Info.findOne({ACNumber: acnumber});

            console.log("****************************************************************")
            console.log(custInfo);
            console.log(acnumber);
            console.log("****************************************************************")


            var userId = [];
            _.each(employeeInfo, function (employee) {
                var pBranchValue = [];
                if (employee.pBranch) {
                    pBranchValue = bitConversion(employee.pBranch);
                    _.each(pBranchValue, function (info) {
                        if (info == custInfo.Branch) {
                            var userInfo = Meteor.users.findOne({ID: employee.ID});
                            if (userInfo) {
                                userId.push(userInfo._id);
                            }
                        }
                    });
                }
            });
        }
        return userId;
    },
    'getMaterialDeptUid': function (acnumber) {
        var clientInfo = Cust_Info.findOne({ACNumber: acnumber});
        if (clientInfo) {
            var materialDeptUid;
            Roles.getUsersInRole("materialDept", "user-group").forEach(function (info) {
                if (info.username) {
                    console.log("-------------", info.username)
                    var store;
                    switch (info.username) {
                        case  "panguorong":
                            store = "浦东";
                            break;
                        case  "xujunbiao":
                            store = "旗舰";
                            break;
                        case  "woruei":
                            store = "北区";
                            break;
                        case  "kongjiansong":
                            store = "西区";
                            break;
                    }
                    var acSetInfo = acSet.findOne({"ACType": "营业网点", "ACName": store});
                    console.log("store:", store);
                    console.log(acSetInfo);
                    if (acSetInfo && acSetInfo.ACNumb) {
                        var branchValue = bitConversion(acSetInfo.ACNumb);

                        console.log("-------------------------branchValue---------------------------------------------")
                        console.log(branchValue);
                        console.log(clientInfo.Branch);
                        console.log("-------------------------branchValue---------------------------------------------")

                        _.each(branchValue, function (branchInfo) {
                            if (branchInfo == clientInfo.Branch) {
                                console.log("***********************getMaterialDeptUid*************************", info._id)
                                materialDeptUid = info._id;
                            }
                        })
                    }
                }
            });
            return materialDeptUid;
        }
    }
};


function bitConversion(pBreach) {
    if (!(pBreach instanceof Number)) {
        pBreach = Number(pBreach)
    }
    var pbBit = pBreach.toString(2);
    var arr = [];
    _.each(_.range(pbBit.length), function (n) {
        // console.log("======", n, "=======");
        // console.log(pbBit.substring(n, n + 1));
        if (pbBit.substring(n, n + 1) !== "0") {
            // console.log("pow:", Math.pow(2, pbBit.length - n - 1));
            arr.push(Math.pow(2, pbBit.length - n - 1));
        }
    });
    // console.log("arr:", arr);
    return arr;
}