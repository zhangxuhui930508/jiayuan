/**
 * Created by zhangxuhui on 15/11/10.
 */
Template.complaintsList.rendered = function () {
    this.subscribe("publishOtherImages")
};

Template.complaintsList.helpers({
    headObj: function () {
        return {
            title: '投诉电话',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    },
});


Schemes = {};
Schemes.complaints = new SimpleSchema({
    acnumber: {
        type: String,
        autoValue: function () {
            return Session.get("customerLogin");
        }
    },
    reason: {
        type: String,
        label: "投诉原因",
        optional: true,
        autoform: {
            afFieldInput: {
                type: "textarea"
            }
        }
    },
    "picture": {
        type: [String],
        label: '上传图片',
        // fix: it's optional
        // optional: true
    },
    "picture.$": {
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'OtherImages',
                accept: 'image/*',
                label: '浏览'
            }
        }
    },
    createAt: {
        type: Date,
        autoValue: function () {
            return new Date();
        }
    }
});

Complaints.attachSchema(Schemes.complaints);

AutoForm.hooks({
    complaintsInsert: {
        onSuccess: function (formType, result) {
            IonLoading.show({
                customTemplate: "<h4>投诉已受理,请耐心等待!</h4>",
                duration: 1500
            });
        },
    }
});