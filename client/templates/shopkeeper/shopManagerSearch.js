/**
 * Created by zhangxuhui on 16/2/15.
 */
Template.shopManagerSearch.rendered = function () {
    Session.set("ManagercurrentSearchVal", null);
    this.autorun(function () {
        var info = Session.get("ManagercurrentSearchVal");
        Meteor.subscribe("ManagerSearch", Meteor.userId(), info, function () {
            var info = Cust_Info.find().fetch();
            console.log(info);
        });
    })
};

Template.shopManagerSearch.helpers({
    headObj: function () {
        return {
            title: '客户列表'
        }
    },
    'Info': function () {
        var select = {};
        if (Session.get("ManagercurrentSearchVal")) {
            var info = Session.get("ManagercurrentSearchVal");
            var select = {$or: [{ACNumber: info}, {CustName: info}]};

            var custInfo = Cust_Info.find(select, {sort: {CreatTime: -1}});
            return custInfo;
        }
    },

});

Template.shopManagerSearch.events({
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("ManagercurrentSearchVal", currentSearch);
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/designerLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .closed': function () {
        Session.set("ManagercurrentSearchVal", null);
        $(".search").val("");
        console.log("-------------------------------------");
    },
    'click .item .gocustomerDetail': function (event) {
        var cid = $(event.currentTarget).children("h2").attr("id").trim();
        //var ctype = $(event.currentTarget).children("p").attr("id").trim();
        var ctype = "null";

        Router.go("/customerDetail/customerType/" + ctype + '/customerId/' + cid);
    },
});