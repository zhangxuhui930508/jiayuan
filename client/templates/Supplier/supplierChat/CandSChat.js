/**
 * Created by zhangxuhui on 5/26/16.
 */



Template.CandSChat.helpers({
    headObj: function () {
        return {
            title: '催材料消息',
            // leftButton: true,
            // backTemplate: 'orderType'
        }
    },
    'messageInfo': function () {
        return constructionAndSupplierChat.find();
    },
    // 'dateFormat': function (date) {
    //     if (date) {
    //         return date.Format("yyyy-MM-dd");
    //     }
    // },
    'fontColor': function (status) {
        console.log(status);
        if (status == "unread") {
            return "blue"
        } else if (status == "read") {
            return "#D3D3D3"
        }
    },
    getConstructionName: function (id) {
        var info = AC_Personnel.findOne({ID: parseInt(id)});
        if (info) {
            return info.pName;
        }
    }
});

Template.CandSChat.events({
    'click .chatMessage': function (event) {

        if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {

            var id = $(event.currentTarget).children("div").attr("class").trim();
            var info = constructionAndSupplierChat.findOne({_id: id});
            console.log(id);

            // Router.go('/chatMessage/' + id);
            if (info) {
                var date = info.createAt.Format("yyyy-MM-dd");
                IonPopup.show({
                    title: date,
                    template: info.message,
                    buttons: [{
                        text: '已读',
                        type: 'button-positive',
                        onTap: function () {
                            Meteor.call("updateConstructionAndSupplierChat", id, function (err, result) {
                                IonPopup.close();
                            });
                        }
                    }]
                });
            }
        }
    },
    'click .backMenuList': function () {
        if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
            Router.go("/orderType")
        } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "ConstructionCaptainBoss"], "user-group")) {
            Router.go("/constructionCType")

        }
    }
})