/**
 * Created by zhangxuhui on 16/4/27.
 */

Template.measureList.rendered = function () {
    IonLoading.show({
        duration: 1500
    });
    Session.set("measureState", "unMeasure");
    Session.set("customerNameAndAddress", null);

    this.autorun(function () {
        Meteor.subscribe("publishMeasureList", Meteor.userId(), 1, function (err, result) {
            var info = ERP_Budget_Material.find().fetch();
            console.log(info);
            var arr = [];
            _.each(info, function (erpInfo) {
                arr.push(erpInfo.受理编号);
            });
            if (arr.length > 0) {
                Meteor.call("getCustomerNameInfo", arr, function (err, result) {
                    console.log(err, result);
                    Session.set("customerNameAndAddress", result);
                    IonLoading.hide();
                })
            }
        })
    })
};


Template.measureList.helpers({
    headObj: function () {
        return {
            title: '待测量订单',
            leftButton: true,
            backTemplate: 'proceedOrderType'
        }
    },
    isActive: function (val) {
        var select = Session.get("measureState");
        if (select && select == val) {
            return "active";
        }
    },
    'deliverGoodsInfo': function () {
        var info = Session.get("measureState");
        var select = {};
        if (info == "unMeasure") {
            select.measureDate = {$lt: new Date().Format("yyyy-MM-dd")}
        } else if (info == "todayMeasure") {
            select.measureDate = new Date().Format("yyyy-MM-dd");
        } else if (info == "futureMeasure") {
            select.measureDate = {$gt: new Date().Format("yyyy-MM-dd")}
        }

        var erpBudgetMaterialInfo = ERP_Budget_Material.find(select);

        if (erpBudgetMaterialInfo) {
            return erpBudgetMaterialInfo;
        }
    },
    'getCustomerName': function (acnumber) {
        try {
            return Session.get("customerNameAndAddress")[acnumber].CustName;
        } catch (e) {

        }

    },
    'getCustomerAddress': function (acnumber) {
        try {
            return Session.get("customerNameAndAddress")[acnumber].Address;
        } catch (e) {

        }
    }
});

Template.measureList.events({
    "click .tab-item": function (event) {
        var val = $(event.currentTarget).attr("value");
        Session.set("measureState", val);
    },
    'click .gocustomerDetail': function (event) {
        var id = $(event.currentTarget).children("span").attr("class").trim();
        console.log("id:", id);
        Router.go("/scheduleList/" + id);
    }
});

Template.measureList.onDestroyed(function () {
    Session.set("measureState", null);
});