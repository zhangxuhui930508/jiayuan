/**
 * Created by zhangxuhui on 16/4/28.
 */
Meteor.methods({
    /**
     * 供应商更新订单状态
     * @param id 产品ID
     * todo 需要系统消息
     * @param type 更新的状态
     *
     * orderState:1  //已经安排测量,待测量
     * orderState:2 //测量完成,待核价
     * orderState:3 //提交核价,待批准
     * orderState:4 //提交核价审批通过
     * orderState:5 //提交核价 被退回
     * orderState:6 //备货中
     * orderState:7 //备货完成,准备发货
     * orderState:8 //发货中
     * orderState:9 //发货完成 等待安装
     *
     * orderState:10 //安装完成
     * orderState:11 //提交入库
     * orderState:12 //入库通过
     * orderState:13 //不通过
     * orderState:14 //安装中
     * "measure",测量
     //     "nuclearPrice", 核价
     //     "prepareGoods",备货
     //     "install",安装
     //     "deliverGoods",送货
     //     "sendGoods"入库

     * [measure,nuclearPrice,prepareGoods,install,deliverGoods,sendGoods]
     */
    supplierNextSetup: function (type) {
        var newType;
        var roles = Roles.getRolesForUser(this.userId, 'supplier');
        switch (type) {
            case 2: {
                if (_.contains(roles, 'nuclearPrice')) {
                    return 2;
                } else {
                    if (_.contains(roles, 'prepareGoods')) {
                        return 4;
                    } else {
                        if (_.contains(roles, 'deliverGoods')) {
                            return 7;
                        } else {
                            if (_.contains(roles, 'install')) {
                                return 9;
                            } else {
                                if (_.contains(roles, 'sendGoods')) {
                                    return 10;
                                }
                            }
                        }
                    }
                }
                break;
            }
            case 4 : {
                if (_.contains(roles, 'prepareGoods')) {
                    return 4;
                } else {
                    if (_.contains(roles, 'deliverGoods')) {
                        return 7;
                    } else {
                        if (_.contains(roles, 'install')) {
                            return 9;
                        } else {
                            if (_.contains(roles, 'sendGoods')) {
                                return 10;
                            }
                        }
                    }
                }
                break;
            }


        }
    },
    'updateERPBudgetStates': function (id, type, date, lockProject) {


        var info = ERP_Budget_Material.findOne({_id: id});
        if (info) {
            var supplierName = "";
            var userInfo = Meteor.users.findOne({_id: this.uid});
            if (userInfo) {
                var supplierInfo = erpInfoSupplier.findOne({ID: userInfo.ID});
                if (supplierInfo) {
                    supplierName = supplierInfo.名称;
                }
            }
            var message;
            console.log(type, id, date);
            var acnumber = info.受理编号;
            var orderNo = info.定单编号;
            erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单编号}, {$set: {orderType: "proceedOrder"}});
            var isMerge = false;
            //合并订单
            var mergeinfo = ConsolidatedOrders.findOne({acnumber: acnumber, orderNo: orderNo});
            if (mergeinfo) isMerge = true;
            console.log("------------isMerge----------------------", isMerge);
            if (type == 1) {
                message = "供应商" + supplierName + "安排在" + date + "进行测量!";
                if (isMerge) {
                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type,
                            measureDate: date,
                            lockProject: lockProject
                        }
                    }, {multi: true})
                } else {
                    ERP_Budget_Material.update({_id: id}, {
                        $set: {
                            orderState: type,
                            measureDate: date,
                            lockProject: lockProject
                        }
                    });
                }
            } else if (type == 2) {
                message = "供应商" + supplierName + "已经完成了测量工作!";
                if (isMerge) {
                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type, finishMeasureDate: date
                        }
                    }, {multi: true})
                } else {
                    ERP_Budget_Material.update({_id: id}, {$set: {orderState: type, finishMeasureDate: date}});
                }
            } else if (type == 3) {
                message = "供应商" + supplierName + "提交了核价单!";
                if (isMerge) {
                    var info = ERP_Budget_Material.findOne({受理编号: acnumber, 定单编号: orderNo, corePrice: {$exists: true}});

                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type, nuclearPriceDate: date, corePrice: info.corePrice
                        }
                    }, {multi: true});
                } else {
                    ERP_Budget_Material.update({_id: id}, {$set: {orderState: type, nuclearPriceDate: date}});
                }
            } else if (type == 4 || type == 5) {
                if (type == 4) {
                    message = "合约部通过了供应商提交的核价单!"
                }
                if (type == 5) {
                    message = "合约部退回了供应商提交的核价单!"
                }

                ERP_Budget_Material.update({_id: id}, {
                    $set: {
                        orderState: type,
                        checkNuclearPriceDate: new Date(date)
                    }
                }, {multi: true});
            } else if (type == 6) {
                message = "供应商" + supplierName + "已经开始备货了!";
                if (isMerge) {
                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type, beihuoDate: date, lockProject: lockProject

                        }
                    }, {multi: true})
                } else {
                    ERP_Budget_Material.update({_id: id}, {
                        $set: {
                            orderState: type, beihuoDate: date, lockProject: lockProject
                        }
                    });
                }
            } else if (type == 7) {
                message = "供应商" + supplierName + "已经备货完毕!";
                if (isMerge) {
                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type, inStockDate: date
                        }
                    }, {multi: true})
                } else {
                    ERP_Budget_Material.update({_id: id}, {$set: {orderState: type, inStockDate: date}})
                }
            } else if (type == 8) {
                message = "材料已经发货,请耐心等待!";
                if (isMerge) {
                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type, deliveryDate: date, lockProject: lockProject
                        }
                    }, {multi: true})
                } else {
                    ERP_Budget_Material.update({_id: id}, {
                        $set: {
                            orderState: type,
                            deliveryDate: date,
                            lockProject: lockProject
                        }
                    });
                }
            } else if (type == 9) {
                message = "材料已经签收!";
                if (isMerge) {
                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type, finishDelivery: date
                        }
                    }, {multi: true})
                } else {
                    ERP_Budget_Material.update({_id: id}, {$set: {orderState: type, finishDelivery: date}});
                }
            } else if (type == 10) {
                message = "材料已经安装完成!";
                if (isMerge) {
                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type, insetalDate: date
                        }
                    }, {multi: true})
                } else {
                    ERP_Budget_Material.update({_id: id}, {$set: {orderState: type, insetalDate: date}});
                }
            } else if (type == 11) {
                message = "供应商" + supplierName + "提交入库单!";
                if (Roles.userIsInRole(this.userId, "nuclearPrice", 'supplier')) {
                    var info = ERP_Budget_Material.findOne({
                        受理编号: acnumber,
                        定单编号: orderNo,
                        putInStorageFile: {$exists: true}
                    });
                    ERP_Budget_Material.update({_id: id}, {
                        $set: {
                            orderState: type,
                            putInStrongDate: date,
                            putInStorageFile: info.putInStorageFile
                        }
                    })
                } else {
                    ERP_Budget_Material.update({_id: id}, {$set: {orderState: type, putInStrongDate: date}});
                }


            } else if (type == 14) {
                message = "材料安装中!";
                if (isMerge) {
                    ERP_Budget_Material.update({受理编号: acnumber, 定单编号: orderNo}, {
                        $set: {
                            orderState: type, finishInstallDate: date
                        }
                    }, {multi: true})
                } else {
                    ERP_Budget_Material.update({_id: id}, {$set: {orderState: type, finishInstallDate: date}});
                }
            } else if (type == 12 || type == 13) {
                if (type == 12) {
                    message = "合约部门审核通过供应商提交的入库单!";
                }
                if (type == 13) {
                    message = "合约部门退回供应商提交的入库单!";
                }
                ERP_Budget_Material.update({_id: id}, {$set: {orderState: type, checkPutInStrongDate: date}});
            }


            systemMessage.insert({
                acnumber: acnumber,
                message: message,
                date: new Date(),
                state: "unread",
                userInfo: {
                    name: '供应商',
                    icon: "/image/supplierIcon.jpg"
                }
            })
        }
    }
    ,
    'skipMeasure': function (id, pwd) {
        var supplierName = "";
        var userInfo = Meteor.users.findOne({_id: this.uid});
        if (userInfo) {
            var supplierInfo = erpInfoSupplier.findOne({ID: userInfo.ID});
            if (supplierInfo) {
                supplierName = supplierInfo.名称;
            }
        }
        var usersInfo = Meteor.users.findOne({_id: this.userId});
        if (usersInfo) {
            var info = erpInfoSupplier.findOne({ID: usersInfo.ID});
            if (info) {
                if (info.代码 == pwd) {
                    ERP_Budget_Material.update({_id: id}, {
                        $set: {
                            orderState: 2,
                            finishMeasureDate: new Date(),
                            updateAt: new Date()
                        }
                    });

                    var info1 = ERP_Budget_Material.findOne({_id: id});
                    if (info1) {
                        systemMessage.insert({
                            acnumber: info1.受理编号,
                            message: "供应商" + supplierName + "跳过了测量过程!",
                            date: new Date(),
                            state: "unread",
                            userInfo: {
                                name: '供应商',
                                icon: "/image/supplierIcon.jpg"
                            }
                        })
                    }

                } else {
                    throw new Meteor.Error("password is error!")
                }
            }
        }
    }
    ,
    'checkOrderInfo': function (id, checker, result, textarea) {
        var supplierName = "";
        var userInfo = Meteor.users.findOne({_id: this.uid});
        if (userInfo) {
            var supplierInfo = erpInfoSupplier.findOne({ID: userInfo.ID});
            if (supplierInfo) {
                supplierName = supplierInfo.名称;
            }
        }
        var info = ERP_Budget_Material.findOne({_id: id});
        if (info) {
            var message;
            if (result == 4) {
                message = "合约部门审核通过供应商提交的核价单!"
            }
            if (result == 5) {

                message = "合约部门退回了供应商提交的核价单!"
            }
            systemMessage.insert({
                acnumber: info.受理编号,
                message: message,
                date: new Date(),
                state: "unread",
                userInfo: {
                    name: '合约部', icon: "/image/designer.jpg"
                }
            });
            ERP_Budget_Material.update({_id: id}, {
                $set: {
                    orderState: result,
                    checkPricingDate: new Date(),
                    PricingChecker: checker,
                    reasontextarea1: textarea
                }
            })

        } else {

            throw new Meteor.Error("material is null")
        }
    }
    ,
    'checkOrderInfo2': function (id, checker, result, textarea) {
        var supplierName = "";
        var userInfo = Meteor.users.findOne({_id: this.uid});
        if (userInfo) {
            var supplierInfo = erpInfoSupplier.findOne({ID: userInfo.ID});
            if (supplierInfo) {
                supplierName = supplierInfo.名称;
            }
        }
        var info = erpViewVerifyOrderAll.findOne({_id: id});
        if (info) {
            var message;
            if (result == 4) {
                message = "合约部门审核通过供应商提交的核价单!"
            }
            if (result == 5) {

                message = "合约部门退回了供应商提交的核价单!"
            }
            systemMessage.insert({
                acnumber: info.受理编号,
                message: message,
                date: new Date(),
                state: "unread",
                userInfo: {
                    name: '合约部', icon: "/image/designer.jpg"
                }
            });
            erpViewVerifyOrderAll.update({_id: id}, {
                $set: {
                    orderState: result,
                    checkPricingDate: new Date(),
                    PricingChecker: checker,
                    reasontextarea1: textarea
                }
            })

        } else {

            throw new Meteor.Error("material is null")
        }
    }
    ,
    'checkPutInStrong': function (id, checker, result, textarea) {
        var supplierName = "";
        var userInfo = Meteor.users.findOne({_id: this.uid});
        if (userInfo) {
            var supplierInfo = erpInfoSupplier.findOne({ID: userInfo.ID});
            if (supplierInfo) {
                supplierName = supplierInfo.名称;
            }
        }
        var info = ERP_Budget_Material.findOne({_id: id});
        if (info) {

            var message;
            if (result == 12) {

                message = "合约部门审核通过了供应商提交的入库单!"
            }
            if (result == 13) {
                message = "合约部门退回了供应商提交的入库单!"
            }
            systemMessage.insert({
                acnumber: info.受理编号,
                message: message,
                date: new Date(),
                state: "unread",
                userInfo: {
                    name: '合约部', icon: "/image/designer.jpg"
                }
            })
            ERP_Budget_Material.update({_id: id}, {
                $set: {
                    orderState: result,
                    checkPutInStrongDate: new Date(),
                    PutInStrongChecker: checker,
                    reasontextarea2: textarea
                }
            });
            if (ERP_Budget_Material.find({受理编号: info.受理编号, 定单编号: info.定单编号, orderState: 12}))
                erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单编号}, {$set: {orderType: "finishOrder"}})
        } else {
            throw new Meteor.Error("material is null")
        }
    }
    ,
//    //整体入库

    'checkPutInStrong2': function (id, checker, result, textarea) {
        var info = erpViewVerifyOrderAll.findOne({_id: id});
        if (info) {

            var message;
            if (result == 12) {
                message = "合约部门审核通过了供应商提交的核价单!"
            }
            if (result == 13) {
                message = "合约部门退回了供应商提交的核价单!"
            }
            systemMessage.insert({
                acnumber: info.受理编号,
                message: message,
                date: new Date(),
                state: "unread",
                userInfo: {
                    name: '材料部', icon: "/image/designer.jpg"
                }
            });
            erpViewVerifyOrderAll.update({_id: id}, {
                $set: {
                    orderState: result,
                    checkPutInStrongDate: new Date(),
                    PutInStrongChecker: checker,
                    reasontextarea2: textarea,
                }
            });

            erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单编号}, {$set: {orderType: "finishOrder"}})
        } else {
            throw new Meteor.Error("material is null")
        }
    }
    ,


    'getProceedOrderTypeCount': function (uid) {
        var json = {};
        var userInfo = Meteor.users.findOne({_id: uid});

        if (userInfo && userInfo.ID) {
            var info = erpInfoSupplier.findOne({ID: userInfo.ID});
            if (info && info.putInStorageType == "whole") {
                json.info1 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 1}).count();
                json.info2 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 2}).count();
                json.info3 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 3}).count();
                json.info4 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 4}).count();
                json.info5 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 5}).count();
                json.info6 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 6}).count();
                json.info7 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 7}).count();
                json.info8 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 8}).count();
                json.info9 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 9}).count();
                json.info10 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 10}).count();
                json.info11 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 11}).count();
                if (!Roles.userIsInRole(this.userId, 'install', 'supplier')) {
                    json.info14 = erpViewVerifyOrderAll.find({
                        供货商编号: userInfo.ID,
                        orderState: {$in: [14, 9, 10]}
                    }).count();
                } else {
                    json.info14 = erpViewVerifyOrderAll.find({供货商编号: userInfo.ID, orderState: 14}).count();
                }
            } else {
                json.info1 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 1}).count();
                json.info2 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 2}).count();
                json.info3 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 3}).count();
                json.info4 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 4}).count();
                json.info5 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 5}).count();
                json.info6 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 6}).count();
                json.info7 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 7}).count();
                json.info8 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 8}).count();
                json.info9 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 9}).count();
                json.info10 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 10}).count();
                json.info11 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 11}).count();
                if (!Roles.userIsInRole(this.userId, 'install', 'supplier')) {
                    json.info14 = ERP_Budget_Material.find({
                        供货商ID: userInfo.ID,
                        orderState: {$in: [14, 9, 10]}
                    }).count();
                } else {
                    json.info14 = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 14}).count();
                }
            }
        }
        console.log(json.info14);
        return json

    }
    ,

//***************************************************************************

    updateOrderStatus: function (acnumber, orderNo, type, date, lockProject) {
        try {
            console.log(acnumber, orderNo);
            var supplierName = "";
            var userInfo = Meteor.users.findOne({_id: this.uid});
            if (userInfo) {
                var supplierInfo = erpInfoSupplier.findOne({ID: userInfo.ID});
                if (supplierInfo) {
                    supplierName = supplierInfo.名称;
                }
            }

            var info = erpViewVerifyOrderAll.findOne({受理编号: acnumber, 定单号: orderNo});
            if (info) {
                var message;
                erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {$set: {orderType: "proceedOrder"}});
                if (type == 1) {
                    message = "供应商" + supplierName + "安排在" + date + "进行测量!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            measureDate: date,
                            lockProject: lockProject
                        }
                    });
                } else if (type == 2) {
                    message = "供应商" + supplierName + "已经完成了测量工作!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            finishMeasureDate: date
                        }
                    });
                } else if (type == 3) {
                    message = "供应商" + supplierName + "提交了核价单!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            nuclearPriceDate: date
                        }
                    });
                } else if (type == 4 || type == 5) {
                    if (type == 4) {
                        message = "合约部通过了供应商提交的核价单!"
                    }
                    if (type == 5) {
                        message = "合约部退回了供应商提交的核价单!"
                    }
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            checkNuclearPriceDate: new Date(date)
                        }
                    }, {multi: true});
                } else if (type == 6) {
                    message = "供应商" + supplierName + "已经开始备货了!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type, beihuoDate: date, lockProject: lockProject

                        }
                    }, {multi: true})

                } else if (type == 7) {
                    message = "供应商" + supplierName + "已经备货完毕!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            inStockDate: date
                        }
                    })
                } else if (type == 8) {
                    console.log("------------------------------------------")
                    message = "材料已经发货,请耐心等待!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            deliveryDate: date,
                            lockProject: lockProject
                        }
                    });

                } else if (type == 9) {
                    message = "材料已经签收!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            finishDelivery: date
                        }
                    });

                } else if (type == 10) {
                    message = "材料已经安装完成!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            insetalDate: date
                        }
                    });
                } else if (type == 11) {
                    message = "供应商" + supplierName + "提交入库单!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            putInStrongDate: date
                        }
                    });
                } else if (type == 14) {
                    message = "材料安装中!";
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            finishInstallDate: date
                        }
                    });
                } else if (type == 12 || type == 13) {
                    if (type == 12) {
                        message = "合约部门审核通过供应商提交的入库单!";
                    }
                    if (type == 13) {
                        message = "合约部门退回供应商提交的入库单!";
                    }
                    erpViewVerifyOrderAll.update({受理编号: info.受理编号, 定单号: info.定单号}, {
                        $set: {
                            orderState: type,
                            checkPutInStrongDate: date
                        }
                    });
                }
                console.log(message);
                systemMessage.insert({
                    acnumber: acnumber,
                    message: message,
                    date: new Date(),
                    state: "unread",
                    userInfo: {
                        name: '供应商',
                        icon: "/image/supplierIcon.jpg"
                    }
                })
            }
        } catch (e) {
            console.log(e.message);
            throw new Meteor.Error(e.message);
        }
    }
    ,
    skipOrderMeasure: function (acnumber, orderNo, pwd) {
        var usersInfo = Meteor.users.findOne({_id: this.userId});
        if (usersInfo) {
            var info = erpInfoSupplier.findOne({ID: usersInfo.ID});
            if (info) {
                if (info.代码 == pwd) {
                    erpViewVerifyOrderAll.update({受理编号: acnumber, 定单号: orderNo}, {
                        $set: {
                            orderState: 2,
                            finishMeasureDate: new Date(),
                            updateAt: new Date()
                        }
                    });

                    var info1 = erpViewVerifyOrderAll.findOne({受理编号: acnumber, 定单号: orderNo});
                    if (info1) {
                        systemMessage.insert({
                            acnumber: info1.受理编号,
                            message: "供应商" + supplierName + "跳过了测量过程!",
                            date: new Date(),
                            state: "unread",
                            userInfo: {
                                name: '供应商',
                                icon: "/image/supplierIcon.jpg"
                            }
                        })
                    }

                } else {
                    throw new Meteor.Error("密码错误!")
                }
            }
        }
    }

})
;