/**
 * Created by chendongdong on 15/8/3.
 */
Material_items = new Mongo.Collection('materialItems');
ERP_Info_Material_Classify = new Mongo.Collection('ERP_Info_Material_Classify');
ERP_Info_Material_Cost = new Mongo.Collection('ERP_Info_Material_Cost');
ERP_Info_Material_Detail = new Mongo.Collection('ERP_Info_Material_Detail');
ERP_Info_Material_NewClassify = new Mongo.Collection('ERP_Info_Material_NewClassify');
ERP_Info_Material_NewDetail = new Mongo.Collection('ERP_Info_Material_NewDetail');
ERP_Info_Material_Parameter = new Mongo.Collection('ERP_Info_Material_Parameter');
ERP_Info_Material_Price = new Mongo.Collection('ERP_Info_Material_Price');

AC_Sour_Pers = new Mongo.Collection('AC_Sour_Pers');

//Items = new Mongo.Collection('items');

//User_Data = new Mongo.Collection('user_data');
erpInfoMaterialCost = new Mongo.Collection("erpInfoMaterialCost");

//类列表
Material_Classify_List = new Mongo.Collection('MaterialClassifyList');
NewMaterial_Classify_List = new Mongo.Collection('NewMaterialClassifyList');

MaterialDetailList = new Mongo.Collection("materialDetailList");
NewMaterialDetailList = new Mongo.Collection("newMaterialDetailList");
//方案表
Scheme = new Mongo.Collection("Scheme");
//uid  mode  list[info,count]

/**
 *_id cid(用户ID) uid selectMaterials(材料id，材料编号，材料名称，装修部位，多余的属性)
 * @type {Mongo.Collection}
 */
Solutions = new Mongo.Collection("solutions");
SolutionDetails = new Mongo.Collection("solution_details");

ShoppingCart = new Mongo.Collection("shoppingcart")

//客户列表
Cust_Info = new Mongo.Collection('custInfo');
//设计师列表
AC_Personnel = new Mongo.Collection('AC_Personnel');
//var imageStore = new FS.Store.GridFS("images", {
//    mongoUrl: 'mongodb://admin:123456@192.168.1.248:27017/jiayuan_test2', // optional, defaults to Meteor's local MongoDB
//    //mongoOptions: {...},  // optional, see note below
//    //transformWrite: myTransformWriteFunction, //optional
//    //transformRead: myTransformReadFunction, //optional
//    //maxTries: 1, // optional, default 5     最大尝试次数
//    //chunkSize: 1024 * 1024  // optional, default GridFS chunk size in bytes (can be overridden per file).
//                            // Default: 2MB. Reasonable range: 512KB - 4MB
//});
//
//Images = new FS.Collection("images", {
//        stores: [imageStore]
//    }
//);
Images = new FS.Collection("images", {
    stores: [new FS.Store.FileSystem("images", {path: "~/meterialImagesUploads/"})]
});

QCImage = new FS.Collection("qcImage", {
    stores: [new FS.Store.FileSystem("qcImage", {path: "~/qcImagesUploads/"})]

});


OtherImages = new FS.Collection("otherImages", {
    stores: [new FS.Store.FileSystem("otherImages", {path: "~/otherImagesUploads/"})]
});

SupplierImages = new FS.Collection("supplierImages", {
    stores: [new FS.Store.FileSystem("supplierImages", {path: "~/supplierImagesUploads/"})]
})

//设计师 跟用户的关系表
DesignerRelationClient = new Mongo.Collection("designerRelationClient");


//订单类型列表
OrderTypes = new Mongo.Collection("orderType");


/**
 *
 * src="data:image/jpeg;base64,"+{{this.Data}}
 {
 "AcceptanceNumber":"100058",
 "AcceptanceName":"100058结算单",
 "Name":"100058结算单0001.jpeg",
 "Tag":1,
 "Path":"C:\\jpegtemp\\100058结算单0001.jpeg",
 "Data":"",
 "OrderType":"结算单",
 "Version":0
 }
 * @type {Mongo.Collection}
 */
PdfImages = new Mongo.Collection("pdfImages");


/**
 * 2015.11.10
 * @type {Mongo.Collection}
 */
CustomerMaterialclassify = new Mongo.Collection("CustomerMaterialclassify");
ERP_Budget_Material = new Mongo.Collection("erpBudgetMaterial");
ERP_Product_Order = new Mongo.Collection("erpProductOrder");
ERP_Product_Order01 = new Mongo.Collection("erpProductOrder01");
ERP_Product_Order02 = new Mongo.Collection("erpProductOrder02");
ERP_Product_Order03 = new Mongo.Collection("erpProductOrder03");
ERP_Product_Order04 = new Mongo.Collection("erpProductOrder04");
ERP_Product_Order05 = new Mongo.Collection("erpProductOrder05");
ERP_Product_Order06 = new Mongo.Collection("erpProductOrder06");


newsInfo = new Mongo.Collection("newsInfo");


Evaluation = new Mongo.Collection("evaluation");


//客户广告
AdvertisementPic = new Mongo.Collection("AdvertisementPic");

//施工队长和客户之间的关系
construtionRelationClient = new Mongo.Collection("construtionRelationClient");

//经营副总 大区经理 店长和设计师之间的关系

//personnelRelationship = new Mongo.Collection("personnelRelationship");

/**
 * 存储店长的基本信息的collections
 * @type {Mongo.Collection}
 *
 * uid创建用户的_id  用于删除账户操作
 * shopManagerName
 * shopManagerAddress
 *
 *
 */

shopManager = new Mongo.Collection("shopManager");

erpBudgetRation = new Mongo.Collection("erpBudgetRation");

erpBudgetLocation = new Mongo.Collection("erpBudgetLocation");
erpProjectLoan = new Mongo.Collection("erpProjectLoan");
erpCostAccountingPlan = new Mongo.Collection("erpCostAccountingPlan");
erpRemoteFileStorage = new Mongo.Collection("erpRemoteFileStorage");
erpViewVerifyOrderAll = new Mongo.Collection("erpViewVerifyOrderAll");
erpOrdersDeploy = new Mongo.Collection("erpOrdersDeploy");


//聊天室

/**
 *  _id
 *  materialID
 *  clientID
 *
 *
 * @type {Mongo.Collection}
 */
roomMessage = new Mongo.Collection("roomMessage");

/**
 * roomId,
 * message
 * createDate
 * sender
 * uidArrs
 *todo uidArrs为可以访问此房间用户的uid
 *
 * @type {Mongo.Collection}
 */
chatMessages = new Mongo.Collection("chatMessages");

/**
 * 消息状态
 *
 * messageId 消息ID
 * roomID      房间ID
 * uid  访问者ID
 * createDate   创建时间
 *  state       访问状态,已读还是未读
 * @type {Mongo.Collection}
 */
MessageState = new Mongo.Collection("MessageState");

/**
 * 检查施工项目
 * acnumber:受理编号
 * project:水电,泥木,竣工
 * state:当前状态  1.未分配 2.已分配
 * checkResult  1.未审批  2.审批通过  3.整改单,
 * 检查结果为整改单,reasonID,
 * assigner: 分配人
 * checkDate: 检查日期
 * @type {Mongo.Collection}
 */
checkConstructionProject = new Mongo.Collection("checkConstructionProject");

/**
 * 整改单,如果质检人员发送整改单会产生一条记录,产生的id将会记录在checkConstructionProject中
 *
 * acnumber,
 * resaon
 * picture:
 * @type {Mongo.Collection}
 */
correctiveReason = new Mongo.Collection("correctiveReason");

/**
 * 系统消息
 * acnumber,
 * message,
 * @type {Mongo.Collection}
 */
systemMessage = new Mongo.Collection("systemMessage");
typicalSentences = new Mongo.Collection("typicalSentences");
erpInfoSupplier = new Mongo.Collection("erpInfoSupplier");
vieVerifyOrderStorageA = new Mongo.Collection("vieVerifyOrderStorageA");
acSet = new Mongo.Collection("acSet");


/**
 * 施工队长的UID
 * 施工队长的ID
 * 供应商的ID
 * 供应商的UID
 * 消息状态
 * @type {Mongo.Collection}
 */
constructionAndSupplierChat = new Mongo.Collection("constructionAndSupplierChat");

Complaints = new Mongo.Collection("complaints");

ConsolidatedOrders = new Mongo.Collection("consolidatedOrders");

ERP_Quarantine_Acceptance = new Mongo.Collection("ERP_Quarantine_Acceptance");

CheckResultPunchListsToSQL = new Mongo.Collection("checkResultPunchListsToSQL");

Satisfaction = new Mongo.Collection("satisfaction");
//质检临时存储
QcTemporary = new Mongo.Collection("qcTemporary");

LogInfo = new Mongo.Collection("logInfo");

ERP_Info_Quality_Control = new Mongo.Collection("ERP_Info_Quality_Control");

SupplierScheduleLog=new Meteor.Collection("supplierScheduleLog");