/**
 * Created by zhangxuhui on 16/4/18.
 */
Template.repairList.onRendered(function () {
    IonLoading.show();
    this.subscribe("publishCheckProject", Router.current().params.acnumber, Router.current().params.project, function () {
        IonLoading.hide();
    });
})

Template.repairList.helpers({
    'headObj': function () {
        return {
            title: '整改单详情',
            leftButton: true,
        }
    },
    'info': function () {
        return correctiveReason.find();
    },
    'pictureInfo': function () {

        return QCImage.find();
    },
    'imageInfo': function (picture) {
        return QCImage.find({_id: {$in: picture}});
    },
    getCurrentInfo: function (id) {
        var info = correctiveReason.findOne({_id: id});
        if (info) {
            return info.reason;
        }
    }
});


Template.repairList.events({
    'click .pictureDetail': function (event) {
        var imageid = $(event.currentTarget).children("span").attr("class");
        console.log(imageid);
        var titleName = $(event.currentTarget).children("span").text();
        Router.go("/repariPicture/titleName/" + titleName + "/imageId/" + imageid)
    },
    'click .backMenuList': function (event) {
        var info = Session.get("goRepairInfoBack");
        if (info) {
            Router.go("/customerDetail/customerType/" + info.cType + "/customerId/" + info.cId)
        }
    },

});




