/**
 * Created by zhangxuhui on 15/9/17.
 */

//Session.get("customerDecorateInfo")  新客户添加材料到购物车中的时候,存储acnumber parts mode
//Session.get("updateSolutionsId")  用户已经有方案了 在方案中添加材料
//Session.get("customerLogin")  客户登录,存储客户的AcNumber等信息
//Session.get("customerLoginDecorateInfo")  客户登录 自己建立方案
//Session.get("classifyName")  设计师选材料的时候的四级分类名称
//Session.get("currentMaterialClassifyId")  当前材料分类的ID
//Session.get("custInfoAddress")  施工队长选地址查看进度
//Session.get("currentConstructionId")  施工队长的ID
//Session.get("currentSuplierID")  经销商的ID
//经销商查看材料详情页面和客户的合同->标准页面公用


selectDateKey = "selectDateKey";
SERVERDATE = "SERVERDATE";
OCCUPATION_COUNTS = 'occupationCounts';


TableSkipKey = "tableSkipKey";
TableLimitKey = "tableLimitKey";
Session.setDefault(TableSkipKey, 0);
Session.setDefault(TableLimitKey, 20);

CurrentBackTemplate="CurrentBackTemplate";
Session.setDefault(CurrentBackTemplate,null);