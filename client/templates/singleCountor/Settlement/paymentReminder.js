/**
 * Created by zhangxuhui on 15/10/29.
 */


Template.paymentReminder.helpers({
    headObj: function () {
        return {
            title: '结算单列表',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    }, 'paymentReminderDetail': function () {
        var info = erpRemoteFileStorage.find({}, {sort: {完成时间: -1}});
        if (info) {
            return info;
        }
    }
});

Template.paymentReminder.events({
    'click .openfile': function (event) {
        var id = $(event.currentTarget).attr("id").trim();
        Session.set("backURL", "paymentReminder");
        Router.go("/pdfFiles/" + id);
    }
});
