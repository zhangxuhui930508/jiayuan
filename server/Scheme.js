/**
 * Created by pcbeta on 15-8-27.
 */
Meteor.methods({

    /**
     *
     * @param uid 用户ID  acnumber
     * @param tye 类型 包含 shoppingcart和solution
     * @param mode 装修的模式
     */
    //'addsolution': function (_id, type, mode) {
    //    //console.log(_id, type, mode);
    //    //
    //    //var info = Solutions.upsert({acnumber: _id, solutionType: "shoppingcart"}, {
    //    //    $set: {
    //    //        solutionType: type,
    //    //        mode: mode,
    //    //        solutionName: "",
    //    //        Data: new Date()
    //    //    }
    //    //});
    //    //
    //    //console.log(info.insertedId, "info=========");
    //    //console.log(info);
    //    //return info.insertedId;
    //    ////var info = Solutions.findOne({acnumber: _id,solutionType:"shoppingcart"});
    //    ////if (info) {
    //    ////    Solutions.update({acnumber: _id}, {$set:{solutionType: type, mode: mode, solutionName: "",Data:new Date()}});
    //    ////} else {
    //    ////    var infos = Solutions.insert({acnumber: _id, solutionType: type, mode: mode, solutionName: "",Data:new Date()});
    //    ////    console.log(infos,'=================');
    //    ////    return infos;
    //    ////}
    //},

    'saveScheme': function (acnumber, solutionType, solutionName) {

        var info = Solutions.findOne({acnumber: acnumber, solutionType: "shoppingcart"});
        //console.log(info);
        if (info) {
            Solutions.update({
                acnumber: acnumber,
                solutionType: "shoppingcart",
                solutionName: ""
            }, {$set: {solutionType: solutionType, solutionName: solutionName, Data: new Date()}});
        } else {
            console.log("========savesolutions err---------");
        }
    },


    /**
     *
     * @param solutionID 方案id
     * @param materialID 材料id
     */
    'deleteMaterial': function (solutionID, materialID) {
        console.log(solutionID, materialID);
        var infolist = Solutions.findOne({_id: solutionID});
        if (infolist) {
            var info = infolist.selectMaterials;
            if (info) {
                for (var key in info) {
                    if (info[key].id == materialID) {
                        info.splice(key, 1);
                    }
                }
                Solutions.update({_id: solutionID}, {$set: {selectMaterials: info}}, function (err, result) {
                    if (err) {
                        throw new Meteor.Error("error");
                    } else {
                        console.log("=====deleteMaterial success======")
                    }
                })
            }
        }

    },
    /**
     *
     * @param acnumber
     * @param deisgner
     * @param designerScore
     * @param construction
     * @param constructionCaptainScore
     * @param content
     */
    insertevaluation: function (acnumber, deisgner, designerScore, constructionCaptain, constructionCaptainScore, content) {
        var info = Evaluation.findOne({acnumber: acnumber});
        if (!info) {
            Evaluation.insert({
                acnumber: acnumber,
                deisgner: deisgner,
                designerScore: designerScore,
                constructionCaptain: constructionCaptain,
                constructionCaptainScore: constructionCaptainScore,
                content: content
            });
        } else {
            Evaluation.update({acnumber: acnumber}, {
                designerScore: designerScore,
                constructionCaptainScore: constructionCaptainScore,
                content: content
            })
        }

    },
    //=================================================new=================================
    //材料基本信息,装修区域,客户受理编号,
    /**
     * 添加到购物车
     * @param ACNumber 受理编号
     * @param mode 装修模式
     * @param parts 装修区域
     * @param materialID 材料ID
     * @param materialCount 材料数量
     * @param otherAttributes 其他属性
     */
    'addShoppingCart': function (ACNumber, mode, parts, materialID, materialCount, materialOrderType, otherAttributes) {

        ShoppingCart.insert({
            ACNumber: ACNumber,
            mode: mode,
            parts: parts,
            materialID: materialID,
            materialCount: materialCount,
            materialOrderType: materialOrderType,
            otherAttributes: otherAttributes
        });

    },
    /**
     * 购物车中删除材料
     * @param materialId 材料ID
     */
    'delShoppingCartMaterialInfo': function (ACNumber, shoppingId) {
        try {
            var shoppingInfo = ShoppingCart.findOne({ACNumber: ACNumber});
            if (shoppingInfo) {
                ShoppingCart.remove({ACNumber: ACNumber, materialID: shoppingId});

            }


        } catch (e) {
            console.log(e.message());
            throw new Meteor.Error("error");
        }
    },
    /**
     *新建方案
     * @param ACNumber
     */
    "addSolution": function (ACNumber, solutionName) {

        var SolutionsInfo = Solutions.findOne({solutionName: solutionName, ACNumber: ACNumber});
        if (SolutionsInfo) {
            throw new Meteor.Error("方案名称不能重复");
        }

        var customerShoppingInfo = ShoppingCart.find({ACNumber: ACNumber}).fetch();
        var selectMaterial = {};
        if (customerShoppingInfo) {
            _.each(customerShoppingInfo, function () {
                selectMaterial = customerShoppingInfo;
            })
        }
        //{ "_id" : "MG3XtcyWdmYqsuRPQ", "ACNumber" : "123594", "mode" : "整体翻新", "parts" : "次卫", "materialID" : "1659", "materialCount" : "1", "otherAttributes" : {  } }

        Solutions.insert({
            ACNumber: ACNumber,
            solutionName: solutionName,
            selectMaterial: selectMaterial,
            Date: new Date()
        }, function (err, result) {
            if (!err) {
                ShoppingCart.remove({ACNumber: ACNumber});
            }
        });


    },
    /**
     * 删除方案
     * @param _id
     */
    'delSolution': function (_id) {
        try {
            var solutionInfo = Solutions.findOne({_id: _id});
            if (solutionInfo) {
                Solutions.remove({_id: _id})
            }
        } catch (e) {
            console.log(e.message());
            throw new Meteor.Error("error");
        }
    },
    /**
     * 删除方案中的材料
     * @param _id 方案_id
     * @param materialID 材料ID
     */
    'delSolutionMaterial': function (_id, materialID) {
        try {
            var solutionInfo = Solutions.findOne({_id: _id});
            if (solutionInfo && solutionInfo.selectMaterial) {
                var info = solutionInfo.selectMaterial;
                if (info) {
                    _.each(info, function (value) {
                        if (value.materialID == materialID) {
                            info.splice(value, 1);
                        }
                    });
                    Solutions.update({_id: _id}, {$set: {selectMaterial: info}}, function (err, result) {
                        if (!err) {
                            console.log("============delete solutions Material Success=============================")
                        } else {
                            throw new Meteor.Error("error");
                        }
                    })
                }
            }

        } catch (e) {
            console.log(e.message());
            throw new Meteor.Error("error");

        }
    },
    /**
     * 更新材料信息
     * @param solutionId
     * @param MaterialId
     * @param materialCount
     * @param insertDoc
     */
    'updateSolution': function (solutionId, MaterialId, materialCount, insertDoc) {
        Solutions.update({
            _id: solutionId,
            'selectMaterial.materialID': MaterialId
        }, {
            $set: {
                'selectMaterial.$.materialCount': materialCount,
                'selectMaterial.$.otherAttributes': insertDoc
            }
        });
    },

    'addSolutionMaterial': function (solutionId, selectMaterialObj) {

        try {
            var solutionsInfo = Solutions.findOne({_id: solutionId});
            if (solutionId) {
                Solutions.update({_id: solutionId}, {$push: {selectMaterial: selectMaterialObj}});
            }
        } catch (e) {
            throw  new Meteor.Error("err");
        }
    },
    'updateShoppingCartInfo': function (ACNumber, materialID, materialCount, insertDoc) {
        try {
            var shoppingCartInfo = ShoppingCart.findOne({ACNumber: ACNumber, materialID: materialID});
            if (shoppingCartInfo) {
                ShoppingCart.update({ACNumber: ACNumber, materialID: materialID}, {
                    $set: {
                        materialCount: materialCount,
                        otherAttributes: insertDoc
                    }
                })
            }

        } catch (e) {

        }
    }
});