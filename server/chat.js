/**
 * Created by zhangxuhui on 16/4/7.
 */
Meteor.methods({
    'sendMessageOrCreate': function (materialId, acnumber, uid, message, materialName) {

        if (!materialId || !acnumber) {
            throw new Meteor.Error("Info error")
        }
        var roomMessageinfo = roomMessage.findOne({materialId: materialId, clientId: acnumber});
        if (roomMessageinfo) {
            Meteor.call("sendChatMessage", message, uid, roomMessageinfo._id, materialName)
        } else {
            Meteor.call("createRoom", materialId, acnumber, uid, function (err, result) {
                if (!err) {
                    Meteor.call("sendChatMessage", message, uid, result, materialName);
                }
            })
        }
    },

    /**
     * 创建聊天房间
     * @param materialId  string  材料ID
     * @param acnumber  string    受理编号
     * @returns {*}
     *
     */
    'createRoom': function (materialId, acnumber, uid) {
        try {
            var userId = [];
            userId.push(uid);
            //客户信息加入
            var clientUserInfo = Meteor.users.findOne({username: acnumber});
            if (clientUserInfo) {
                userId.push(clientUserInfo._id);
            }
            //设计师
            var designerInfo = createRoomRelation.getDesignRelation(acnumber);
            //施工队长
            var construtionInfo = createRoomRelation.getBuildRelation(acnumber);
            //供应商
            var supplierUserInfo = createRoomRelation.getSupplierRelation(acnumber, materialId);
            var otherInfo = createRoomRelation.getOtherRelation(acnumber);
            var materialDeptInfo = createRoomRelation.getMaterialDeptUid(acnumber);
            // console.log("-----------materialDeptInfo----------------");
            // console.log(materialDeptInfo);
            // console.log("-----------materialDeptInfo----------------");
            var info = _.union(userId, designerInfo, construtionInfo, supplierUserInfo, otherInfo, materialDeptInfo);
            // console.log(info, "info");
            var roomMessageinfo = roomMessage.findOne({materialId: materialId, clientId: acnumber});
            if (roomMessageinfo) {
                roomMessage.update({materialId: materialId, clientId: acnumber}, {$set: {uidArrs: info}});
                return roomMessageinfo._id;
            } else {
                // console.log("===============================================================", materialId, acnumber, info);
                var id = roomMessage.insert({materialId: materialId, clientId: acnumber, uidArrs: info});
                return id;
            }
        } catch (e) {
            throw new Meteor.Error(e.message);
        }

    },

    /**
     * 发送聊天消息
     * @param message  string 聊天信息
     * @param sender    string 发送者的uid
     * @param roomId        string 房间id
     *
     *
     */
    sendChatMessage: function (message, sender, roomId, materialName) {
        try {
            var select = {};
            if (Roles.userIsInRole(sender, ['test'], 'user-group')) {
                var select = {};
                select.name = '测试帐号';
                select.icon = "/image/designer.jpg";
            } else if (Roles.userIsInRole(sender, ['designer'], 'user-group')) {
                var select = {};
                select.name = '设计师';
                select.icon = "/image/designer.jpg";
            } else if (Roles.userIsInRole(sender, ['client'], 'user-group')) {
                var select = {};
                select.name = '客户';
                select.icon = "/image/57875.gif"
            } else if (Roles.userIsInRole(sender, ['ConstructionCaptain', "ConstructionCaptainBoss"], 'user-group')) {
                var select = {};
                select.name = '施工队长';
                select.icon = "/image/26103129861.jpg"
            } else if (Roles.userIsInRole(sender, ['vicePresident'], 'user-group')) {
                var select = {};
                select.name = '副总经理';
                select.icon = "/image/designer.jpg";
            } else if (Roles.userIsInRole(sender, ['districtManager'], 'user-group')) {
                var select = {};
                select.name = '大区经理';
                select.icon = "/image/designer.jpg";
            } else if (Roles.userIsInRole(sender, ['StoreManager'], 'user-group')) {
                var select = {};
                select.name = '店长';
                select.icon = "/image/designer.jpg";
            } else if (Roles.userIsInRole(sender, ['Supplier'], 'user-group')) {
                var select = {};
                select.name = '供应商';
                select.icon = "/image/supplierIcon.jpg";
            } else if (Roles.userIsInRole(sender, ['materialDept'], 'user-group')) {
                var select = {};
                select.name = '材料部';
                select.icon = "/image/designer.jpg";
            }

            var messageID = chatMessages.insert({
                message: message,
                sender: sender,
                createDate: new Date(),
                roomId: roomId,
                userInfo: select,
                materialName: materialName,

            });


            var roomInfo = roomMessage.findOne({_id: roomId});
            //todo

            Meteor.call("createRoom", roomInfo.materialId, roomInfo.clientId, this.userId);

            if (roomInfo && roomInfo.uidArrs) {
                _.each(roomInfo.uidArrs, function (info) {
                    var state;
                    if (info == this.userId) {
                        state = "read"
                    } else {
                        state = "unread"
                    }
                    MessageState.insert({
                        messageId: messageID,
                        roomId: roomId,
                        createDate: new Date(),
                        state: state,
                        uid: info,
                        userInfo: select.name,
                        materialName: materialName
                    });
                })
            }
        } catch (e) {
            throw new Meteor.Error(e.message);
        }
    },
    /**
     * 根据uid获得用户的角色
     * @param userArr  用户uid的数组
     * @returns {{}}
     *
     */
    'getuserRole': function (userArr) {
        var json = {};
        _.each(userArr, function (info) {
            if (Roles.userIsInRole(info, ['test'], 'user-group')) {
                var select = {};
                select.name = '测试帐号';
                select.icon = "/image/designer.jpg";
                json[info] = select;
            } else if (Roles.userIsInRole(info, ['designer'], 'user-group')) {
                var select = {};
                select.name = '设计师';
                select.icon = "/image/designer.jpg";
                json[info] = select;
            } else if (Roles.userIsInRole(info, ['client'], 'user-group')) {
                var select = {};
                select.name = '客户';
                select.icon = "/image/57875.gif"
                json[info] = select;
            } else if (Roles.userIsInRole(info, ['ConstructionCaptain', "ConstructionCaptainBoss"], 'user-group')) {
                var select = {};
                select.name = '施工队长';
                select.icon = "/image/26103129861.jpg"
                json[info] = select;
            } else if (Roles.userIsInRole(info, ['vicePresident'], 'user-group')) {
                var select = {};
                select.name = '副总经理';
                select.icon = "/image/designer.jpg";
                json[info] = select;
            } else if (Roles.userIsInRole(info, ['districtManager'], 'user-group')) {
                var select = {};
                select.name = '大区经理';
                select.icon = "/image/designer.jpg";
                json[info] = select;
            } else if (Roles.userIsInRole(info, ['StoreManager'], 'user-group')) {
                var select = {};
                select.name = '店长';
                select.icon = "/image/designer.jpg";
                json[info] = select;
            } else if (Roles.userIsInRole(info, ['Supplier'], 'user-group')) {
                var select = {};
                select.name = '供应商';
                select.icon = "/image/supplierIcon.jpg";
                json[info] = select;

            }
        });
        return json;
    },

    /**
     * 更改消息状态
     * @param uid 用户uid
     * @param roomid 房间id
     */
    'updateChatMessageState': function (uid, roomid) {
        var info = MessageState.update({uid: uid, roomId: roomid}, {$set: {state: "read"}}, {multi: true});
        return info;
    },
    /**
     * 根据材料ID,客户受理编号,登录用户的uid来获取当前房间是否有未读消息
     * @param arr materialArr
     * @param string acnumber
     * @param string uid
     * @returns {*}
     *
     */
    'searchMaterialMessageState': function (materialArr, acnumber, uid) {
        try {
            var json = {};
            _.each(materialArr, function (midInfo) {
                //查询客户对应材料的room是否有未读消息
                var roomInfo = roomMessage.findOne({materialId: midInfo, clientId: acnumber});
                if (roomInfo) {
                    var stateInfo = MessageState.find({roomId: roomInfo._id, state: "unread", uid: uid}).count();
                    json[midInfo] = stateInfo;
                }
            });
            return json;

        } catch (e) {
            throw new Meteor.Error(e.message);
        }
    },
    /**
     *
     * @param roomIdArr
     * @returns {{}}
     */
    'searchMaterialName': function (roomIdArr) {
        var json = {};
        _.each(roomIdArr, function (roomId) {
            var roomInfo = roomMessage.findOne({_id: roomId});
            if (roomInfo) {
                var materialInfo = ERP_Budget_Material.findOne({_id: roomInfo.materialId});
                if (materialInfo) {
                    json[roomId] = materialInfo.材料名称;
                }
            }
        });
        return json;
    },
    /**
     * 系统消息为已读
     * @param id
     */
    'systemMessageRead': function (id) {
        if (!id) {
            throw new Meteor.Error("id is null")
        }

        systemMessage.update({_id: id}, {$set: {"state": "state"}})
    },

    'messageCount': function (acnumber, uid) {
        if (acnumber) {
            var systemMessageInfo = systemMessage.find({acnumber: acnumber, "state": "unread"}).count();
            var messageIds = [];
            roomMessage.find({clientId: acnumber}).forEach(function (info) {
                messageIds.push(info._id);
            });

            var systemCount = MessageState.find({
                roomId: {$in: messageIds},
                "state": "unread",
                uid: uid
            }).count();
            var returnJson = {};
            returnJson.systemMessageCount = systemMessageInfo;
            returnJson.materialCount = systemCount;
            console.log(returnJson);
            console.log("=======================")
            return returnJson;

        }
    },
//    施工队长和供应商的对话
    'constructionAndSupplier': function (ID, acumber, message) {
        if (!ID) {
            throw new Meteor.Error("ID is null")
        }
        //施工队长的信息
        var userInfo = Meteor.users.findOne({_id: this.userId});
        var employee = AC_Personnel.findOne({ID: userInfo.ID});
        if (employee) {
            var constructionName = employee.pName;
            var supplierInfo = Meteor.users.findOne();
            var supplierUID;
            Roles.getUsersInRole("Supplier", "user-group").forEach(function (info) {
                if (info.ID == ID) {
                    supplierUID = info._id;
                }
            });

            constructionAndSupplierChat.insert({
                supplierId: ID,
                acnumber: acumber,
                supplierUID: supplierUID,
                constructionID: userInfo.ID,
                constructionUID: this.userId,
                createAt: new Date(),
                message: message,
                // message: constructionName + "给您发送催受理编号为" + acumber + "客户的材料!",
                status: "unread"
            })
        }
    },
    'updateConstructionAndSupplierChat': function (id) {
        if (!id) {
            throw new Meteor.Error("id is null")
        }
        constructionAndSupplierChat.update({_id: id}, {$set: {status: "read"}})
    }


});

