/**
 * Created by zhangxuhui on 15/9/21.
 */
Template.designer_tabs.onRendered(function () {
    if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
        this.subscribe("getCustomerType", Meteor.userId(), function () {
            var info = DesignerRelationClient.findOne();
            if (info.cType == "已签" || info.cType == "已归档") {
                $(".ion-search").css("color", "#969696");
            }
        });
        this.subscribe("CustomerLogingetCustomerInfo", Meteor.userId(), function () {
        })

    }
});

Template.designer_tabs.helpers({
    'roleInfo': function (value) {
        return Session.get("loginRole") == value;
    }
})


Template.designer_tabs.events({
    'click .selectMaterial': function (event) {

        if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {

            if ($(".selectMaterial").children("i").css("color") == "rgb(150, 150, 150)") {
                IonLoading.show({
                    customTemplate: "<h4>您已签约，无需再选材!</h4>",
                    duration: 1000
                });
            } else {
                console.log('=====================================================');
                var customerInfo = Cust_Info.findOne();
                var customerDecorateInfo = {};
                customerDecorateInfo.ACNumber = customerInfo.ACNumber;
                customerDecorateInfo.mode = customerInfo.BusiType;
                customerDecorateInfo.cId = customerInfo._id;
                var info = DesignerRelationClient.findOne();
                customerDecorateInfo.cType = info.cType;
                Session.set("customerDecorateInfo", customerDecorateInfo);
                if (customerInfo.BusiType) {
                    Router.go("/customerParts");
                } else {
                    Router.go("/modeSelect");
                }

                //var info = Session.get("CurrentCustomerInfo");
                //if (!Session.get("customerAcNumber")) {
                //    IonPopup.alert({
                //        template: '账号异常，请重新登录！',
                //        okText: '确定'
                //    });
                //    Router.go("/");
                //} else {
                //    if (info.BusiType) {
                //        var mode = info.BusiType;
                //        Meteor.call("addsolution", Session.get("customerAcNumber"), "shoppingcart", mode, function (err, result) {
                //            console.log(result);
                //            if (!err) {
                //                if (result) {
                //                    Session.set("solutionID", result);
                //                } else {
                //                    //todo 如果是更新操作，服务器端将不会返回一个id，这时需要查询数据库获得solutionid
                //                    var solutionsInfo = Session.get("currentCustomerSolutionInfo");
                //                    if (solutionsInfo) {
                //                        Session.set("project_Detail", solutionsInfo._id);
                //                    }
                //                    //Session.set("solutionID", id);
                //                }
                //                console.log(result, "======sever callback=====");
                //                Router.go("/customerParts");
                //            }
                //        });
                //    } else {
                //        Router.go("/modeSelect");
                //    }
                //}
            }
        }
    }
});