/**
 * Created by zhangxuhui on 2016/9/26.
 */

Template.cancelOrder.onRendered(function () {
    this.subscribe("publishCancel");
})

Template.cancelOrder.helpers({
    headObj: function () {
        return {
            title: '已取消订单',
            leftButton: true,
            backTemplate: 'orderType'
        }
    },
    cancelOrderList: function () {
        return Cust_Info.find();
    }
});

