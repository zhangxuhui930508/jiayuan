/**
 * Created by zhangxuhui on 15/11/11.
 */
//Template.StandardMaterialDetail.rendered = function () {
//    var currentid = Router.current().params.id;
//    this.subscribe("ERP_Budget_MaterialACNumber_ID", currentid, function () {
//    });
//};
Template.StandardMaterialDetail.rendered = function () {
    var acnumber;
    if (Roles.userIsInRole(Meteor.userId(), ['client', "materialDept", "contractDept"], 'user-group')) {
        acnumber = Session.get("customerLogin");
    } else if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
        acnumber = Session.get("orderBackURL").acnumber;
    } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], 'user-group')) {
        acnumber = Session.get("customerDecorateInfo").ACNumber;
    }
    this.subscribe("publishChatMessage", acnumber, Router.current().params.id)
};

Template.StandardMaterialDetail.helpers({
    headObj: function () {
        return {
            title: '材料详情',
            leftButton: true,
            //backTemplate: 'StandardMaterial'
        }
    },
    isSingle: function () {
        var erpSupplierInfo = erpInfoSupplier.findOne();
        if (erpSupplierInfo && erpSupplierInfo.putInStorageType == "single") {
            return true;
        }
        return false;
    },
    'budgetMaterialInfo': function () {
        var currentid = Router.current().params.id;
        var info = ERP_Budget_Material.findOne({_id: currentid});
        return info;
    },
    getData: function (data) {
        //var data2 = new Date(data);
        if (data) {
            return data.Format("yyyy-MM-dd hh:mm:ss")
        }
    },
    'supplierName': function () {
        return erpInfoSupplier.findOne();
    },
    materialDetailImage: function () {
        var info = MaterialDetailList.findOne({});
        if (info) {
            return info;
        }
        return ''
    },
    imageDetail: function (id) {
        return Images.find({_id: id})
    },
    imagesSlide: function (imgArr) {
        if (imgArr) {
            var imageArr = [];
            for (var i = 0; i < imgArr.length; i++) {
                var img = Images.find({_id: imgArr[i]});
                imageArr.push(img);
            }
            return imageArr;
        }
    },
    colorFont: function () {
        var info = ERP_Budget_Material.findOne();
        if (info && info.增减 == -1) {
            return 'red';
        }
        return '#000000'
    },
    isAssociatedOrder: function () {
        var info = ERP_Budget_Material.findOne();
        if (info && info.增减 == -1) {
            return true;
        }
        return false;
    }


});

Template.StandardMaterialDetail.events({
    'click .backMenuList': function (event) {
        if (Session.get("chatViewMaterial")) {
            history.back();
        }
        if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
            Router.go("/orderDetail/type/" + Session.get("orderBackURL").type + "/acnumber/" + Session.get("orderBackURL").acnumber + "/orderNo/" + Session.get("orderBackURL").orderNo);
        } else {
            Router.go("/StandardMaterial");
        }
    },
    'click .SNINFO': function (event) {
        var materialID = Router.current().params.id;
        var acnumber;
        var materialInfo = ERP_Budget_Material.findOne();
        if (materialInfo) {
            if (Roles.userIsInRole(Meteor.userId(), ['client', "materialDept", "contractDept"], 'user-group')) {
                acnumber = Session.get("customerLogin");
            } else if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
                acnumber = Session.get("orderBackURL").acnumber;
            } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], 'user-group')) {
                acnumber = Session.get("customerDecorateInfo").ACNumber;
            }
            acnumber = materialInfo.受理编号;
            var info = roomMessage.findOne({clientId: acnumber, materialId: materialID});
            console.log("------------------------SNINFO-----------------------------------------");
            console.log(info);
            var materialName = $(".materialName").text().trim();
            Session.set("materialName", materialName);
            console.log("------------------------------------------------------------------------")
            if (!info) {
                Meteor.call("createRoom", materialID, acnumber, Meteor.userId(), function (err, result) {
                    console.log(err, result);
                    if (!err) {
                        Router.go("/chat/roomId/" + result + "/materialId/" + materialID)
                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>创建对话失败,请重新进入!</h4>",
                            duration: 1000
                        });
                    }
                });
            } else {
                Router.go("/chat/roomId/" + info._id + "/materialId/" + materialID)
            }
        }
    },
    'click .rushFactory': function (event) {
        var materialID = Router.current().params.id;
        var acnumber;
        if (Roles.userIsInRole(Meteor.userId(), ['client', "materialDept", "contractDept"], 'user-group')) {
            acnumber = Session.get("customerLogin");
        } else if (Roles.userIsInRole(Meteor.userId(), ['client', "materialDept", "contractDept"], 'user-group')) {
            acnumber = Session.get("orderBackURL").acnumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], 'user-group')) {
            acnumber = Session.get("customerDecorateInfo").ACNumber;
        }
        Meteor.call("sendMessageOrCreate", materialID, acnumber, Meteor.userId(), "催材料进场!", $(".materialName").text().trim(), function (err, result) {
            if (!err) {
                IonLoading.show({
                    customTemplate: "<h4>消息发送成功!</h4>",
                    duration: 1000
                });
            } else {
                IonLoading.show({
                    customTemplate: "<h4>消息发送失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .typicalSentences': function () {
        var id = Router.current().params.id;
        var acnumber;
        if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
            acnumber = Session.get("orderBackURL").acnumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], 'user-group')) {
            acnumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ['client', "materialDept", "contractDept"], 'user-group')) {
            acnumber = Session.get("customerLogin");
        }
        var materialName = $(".materialName").text().trim();
        Session.set("materialName", materialName);
        Router.go("/typicalSentences/materialId/" + id + "/acnumber/" + acnumber);
    },

    'click .goScheduleList': function () {
        var materialID = Router.current().params.id;
        var info = ERP_Budget_Material.findOne();
        if (info && info.增减 == -1) {
            Meteor.call("returnOrder", materialID)
        }
        Router.go("/scheduleList/" + materialID);
    },
    'click .isCancel': function () {
        IonPopup.confirm({
            title: "温馨提示",
            template: "确认全额退货?",
            okText: '确认',
            cancelText: "取消",
            onOk: function () {
                Meteor.call("cancelMaterialDetail", Router.current().params.id);
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .associatedOrder': function () {
        // "/associatedOrderList/acnumber/:acnumber/materialId/:materialId"
        var info = ERP_Budget_Material.findOne();
        if (info) {
            console.log(info.受理编号)
            console.log(info.材料编号);
            Router.go("/associatedOrderList/acnumber/" + info.受理编号 + "/materialId/" + info.材料编号);
        }

    }
});

