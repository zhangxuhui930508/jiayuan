/**
 * Created by zhangxuhui on 2016/9/23.
 */

loadingTime = function (content, time) {
    IonLoading.show({
        customTemplate: "<h5>" + content + "</h5>",
        duration: time
    });
};

loadingContent = function (content) {
    IonLoading.show({
        customTemplate: "<h5>" + content + "</h5>",
    });
};

loadingHide = function () {
    IonLoading.hide();
};

loadingSystemContent = function () {
    IonLoading.show({
        customTemplate: "<h5>正在为主人拼命加载数据中,请稍候...</h5>"
    });
};

