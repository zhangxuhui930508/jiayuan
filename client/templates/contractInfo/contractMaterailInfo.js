/**
 * Created by zhangxuhui on 16/6/3.
 */

Template.contractMaterailInfo.onRendered(function () {
    IonLoading.show();
    this.subscribe("publishContractMaterialCustomer", Router.current().params.id, Router.current().params.type, function () {
        IonLoading.hide();
    })
});

Template.contractMaterailInfo.helpers({
    headObj: function () {
        return {
            // title: '客户列表',
            leftButton: true,
            backTemplate: 'contractPricing'
        }
    },
    'titleName': function () {
        var info = erpInfoSupplier.findOne();
        if (info && info.名称) {
            return info.名称;
        }
    },

    erpViewOrder: function () {
        return erpViewVerifyOrderAll.find();
    },
    'customerInfo': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber})
        if (info) {
            return info.CustName
        }
    },
    'customerAddress': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber})
        if (info) {
            return info.Address
        }
    }


});

Template.contractMaterailInfo.events({
    'click .gocustomerDetail': function () {
        // if (Session.get("contractPricing") == "undeliverGoods") {
        //     if (Session.get("contractBackTemplate") == "PutInStorage") {
        //         Router.go("/PutInStorageDetail2/" + this._id);
        //     } else {
        //         Router.go("/contractPricingDetail2/" + this._id);
        //     }
        // }

        Router.go("/contractPricingDetail2/" + this._id);
    },
    'click .backMenuList': function () {
        var info = Session.get("contractBackTemplate");
        if (info == "contractPricing") {
            Router.go("/contractPricing")
        } else {
            Router.go("/PutInStorage");
        }
    }
});