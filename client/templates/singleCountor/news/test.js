Schemas = {};
Schemas.newsInfoList = new SimpleSchema({
    newsTitle: {
        type: String,
        label: "标题"
    },
    content: {
        type: String,
        label: '内容',
        autoform: {
            rows: 5
        }
    },
    newData:{
        type:Date,
        label:"发布时间",
    }
});
newsInfo.attachSchema(
    Schemas.newsInfoList
);


Template.test.rendered=function(){
    this.subscribe("newsInfo",function(){});
};


