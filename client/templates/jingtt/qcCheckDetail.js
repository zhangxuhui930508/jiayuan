/**
 * Created by zhangxuhui on 16/8/19.
 */

Template.qcCheckDetail.onRendered(function () {
    IonLoading.show();
    this.subscribe("publishQcCheckDetail", Router.current().params.id, function () {
        IonLoading.hide();
    })
});

Template.qcCheckDetail.helpers({
    headObj: function () {
        return {
            title: '待检工地详情',
            leftButton: true,
            backTemplate: 'qcCheckPage'
        }
    },
    Info: function () {
        return checkConstructionProject.findOne();
    },
    getCustInfoCustName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    getCustInfoAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    },
    getStoreInfo: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info && info.Branch) {
            var acSetInfo = acSet.findOne({ACNumb: info.Branch});
            if (acSetInfo) {
                return acSetInfo.ACName;
            }
        }
    },
    'constructionInfo': function (sender) {
        var userInfo = Meteor.users.findOne({_id: sender});
        if (userInfo) {
            var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
            if (employeeInfo) {
                return employeeInfo.pName;
            }
        }
    },
    'checkProject': function (value) {
        if (value) {
            switch (value) {
                case "completion":
                    return "竣工报检";
                    break;
                case "hydropower":
                    return "水电报检";
                    break;
                case "mudWood":
                    return "泥木报检";
                    break;
            }
        }
    },
    getStartTime: function () {
        var info = Cust_Info.findOne();
        if (info) {
            return info.startTime;
        }
    }, getEndTime: function () {
        var info = Cust_Info.findOne();
        if (info) {
            return info.endTime;
        }
    },
    getActualStartTime: function () {
        var info = Cust_Info.findOne();
        if (info) {
            return info.actualStartTime;
        }
    },
    getCustomer: function () {
        return Cust_Info.findOne();
    }


});

Template.qcCheckDetail.events({
    'click .account-button': function () {
        var id = checkConstructionProject.findOne()._id;
        IonPopup.confirm({
            title: "温馨提示",
            template: "确认报检?",
            okText: '确定',
            cancelText: "取消",
            onOk: function () {
                Meteor.call("bossCheckQcInfo", id, function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>报检成功!</h4>",
                            duration: 1000
                        });
                        Router.go("/qcCheckPage");
                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>报检失败!</h4>",
                            duration: 1000
                        });
                    }
                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },

});