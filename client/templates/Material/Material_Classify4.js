/**
 * Created by pcbeta on 15-8-21.
 */
Template.MaterialClassify4.rendered = function () {
    Session.set("backtemplate", "MaterialClassify4");
    Session.set("customerMaterialListCountListKey04", null);

    var info = Session.get("third_level");
    if (info) {
        var infoList = info.classList;
        var arr = [];
        _.each(info, function (t) {
            arr.push(t.ID)
        });
        // console.log(arr, "arr");
        Meteor.call("materialListCountList", arr, function (err, result) {
            if (!err) {
                console.log(result);
                Session.set("customerMaterialListCountListKey04", result);
            }
        })
    }

};

Template.MaterialClassify4.helpers({
    'headObj': function () {
        return {
            title: '材料分类',
            leftButton: true,
            //backTemplate: 'MaterialClassify3'
        }
    },
    'MaterialClassifyList4': function () {
        //第三级筛选含有第四级分类到数据
        var info = Session.get("third_level");
        if (info) {
            return info;
        }
    },
    //第一级分类的名称
    oneupper_level: function () {
        var classify1 = Router.current().params.Mclassify1;
        if (classify1) {
            return classify1;
        }
        //    第二级分类的名称
    }, upper_level: function () {
        var classify2 = Router.current().params.Mclassify2;
        if (classify2) {
            return classify2;
        }
    },
    //第三级分类的名称
    third_level: function () {
        var classify3 = Router.current().params.Mclassify3;
        if (classify3) {
            return classify3;
        }
    },
    titleName: function () {
        var parts = "材料分类";
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        if (Session.get("customerDecorateInfo")) {
            parts = Session.get("customerDecorateInfo").parts;
        } else {
            parts = "材料分类";
        }
        return parts;
    },
    'materialCount': function (id, classList) {
        try {
            if(!classList ||! _.isArray(classList)){return '0'}
            if (classList.length == 0) {
                var json = Session.get("customerMaterialListCountListKey04");
                if (id && json[id]) {
                    return json[id];
                } else {
                    return "0";
                }
            } else {
                return classList.length;
            }
        } catch (e) {
            return "0";
        }
    }
});


Template.MaterialClassify4.events({
    'click .materClassify4 .item': function (event) {
        var getMaterialCount = $(event.currentTarget).children("span").children("span").text();
        if (getMaterialCount == "（ 0 ）") {
            IonLoading.show({
                customTemplate: "<h4>没有此类材料，请选择其他材料!</h4>",
                duration: 500
            });
            return;
        }

        var itemName = $(event.currentTarget).children("h2").text().trim();
        var classifyName = Session.get("classifyName");
        classifyName.four = itemName;
        Session.set("classifyName", classifyName);

        var id = $(event.currentTarget).children("i").text().trim();
        Router.go("/materialList/" + id);
    },
    'click .go_materialClassify1': function () {
        Router.go("/MaterialClassify");
    },
    'click .go_materialClassify2': function () {
        var classify1 = Router.current().params.Mclassify1;
        Router.go("/MaterialClassify2/" + classify1);
    },
    'click .go_materialClassify3': function () {
        var classify1 = Router.current().params.Mclassify1;
        var classify2 = Router.current().params.Mclassify2;

        Router.go("/MaterialClassify3/Mclassify1/" + classify1 + '/Mclassify2/' + classify2);
    },
    'click .DecorateArea': function () {
        Router.go("/DecorateArea");
    },
    'click .backMenuList': function () {
        var classify1 = Router.current().params.Mclassify1;
        var classify2 = Router.current().params.Mclassify2;

        Router.go("/MaterialClassify3/Mclassify1/" + classify1 + '/Mclassify2/' + classify2);
    }
});