/**
 * Created by zhangxuhui on 16/7/13.
 */

Template.orderDetailContract.onRendered(function () {
    var id = Router.current().params.ID;
    var type = Router.current().params.type;
    IonLoading.show();
    this.subscribe("publishOrderDetailContractInfo", id, type, function () {
        IonLoading.hide();
    });
});

Template.orderDetailContract.helpers({
    headObj: function () {
        return {
            // title: '客户列表',
            leftButton: true,
            backTemplate: 'PutInStorage'
        }
    },
    'titleName': function () {
        var info = erpInfoSupplier.findOne();
        if (info && info.名称) {
            return info.名称;
        }
    },
    erpInfoMaterial: function () {
        return ERP_Budget_Material.find();
    }, customerInfo: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    customerAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    },
});

Template.orderDetailContract.events({
    'click .gocustomerDetail': function () {
        Router.go("/PutInStorageDetail/" + this._id);
    }
});