/**
 * Created by zhangxuhui on 16/4/19.
 */


Template.inspectedList.rendered = function () {
    Session.set("currentCustomerSearchVal", null);
    Session.set("getDateNow", null);

    $(".search").val("");
    var uid;
    if (Roles.userIsInRole(Meteor.userId(), ["qc", "qcManager"], "user-group")) { //质检人员
        uid = Meteor.userId();
    } else if (Roles.userIsInRole(Meteor.userId(), ["qcBoss"], "user-group")) {
        uid = null;
    }
    console.log(uid);
    this.subscribe("construction", uid, "inspected");
    this.subscribe("consturctionInfo", uid, "inspected");
    Session.set("selectNowDate1", false);
    Session.set("selectDateButton1", false);
    Session.set("searchNowDateInfo1", null);
    Session.set("selectDateInfo1", null);

};


Template.inspectedList.helpers({
    headObj: function () {
        return {
            title: "已检工地"
        }
    },
    Info: function () {

        var select = {};
        var selectDate = Session.get("searchNowDateInfo1");
        var selectDateButton = Session.get("selectDateInfo1");
        if (selectDate) {
            select.checkDate = selectDate;
        }
        if (selectDateButton) {
            select.checkDate = selectDateButton;
        }
        if (Session.get("currentCustomerSearchVal")) {
            select.acnumber = new RegExp(Session.get("currentCustomerSearchVal"), "i");
        }
        return checkConstructionProject.find(select, {sort: {checkDateInspection: -1}})

    },
    getCustInfoAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    }, getStoreInfo: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info && info.Branch) {
            var acSetInfo = acSet.findOne({ACNumb: info.Branch});
            if (acSetInfo) {
                return acSetInfo.ACName;
            }
        }
    }, 'constructionInfo': function (sender) {
        var userInfo = Meteor.users.findOne({_id: sender});
        if (userInfo) {
            var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
            if (employeeInfo) {
                return employeeInfo.pName;
            }
        }
    },
    'checkProject': function (value) {
        if (value) {
            switch (value) {
                case "completion":
                    return "竣工报检";
                    break;
                case "hydropower":
                    return "水电报检";
                    break;
                case "mudWood":
                    return "泥木报检";
                    break;
            }
        }
    },
    'checkReasult': function (value, type) {
        if (value) {
            if (value == 2) {
                return "检验合格"
            } else if (value == 3) {
                if (type == "A" || type == "B")
                    return "整改单";
                else
                    return "协调单";
            }
        }
    },
    'nowOrAllButton': function () {
        var selectDate = Session.get("selectNowDate1");
        if (selectDate) {
            return false
        } else {
            return true;
        }
    },
    'selectAllButton': function () {
        var selectDate = Session.get("selectDateButton1");
        if (selectDate) {
            return false
        } else {
            return true;
        }
    },
    'getCustomerMode': function (acnumber) {
        console.log(acnumber);
        console.log("=========================");
        var info = Cust_Info.findOne({ACNumber: acnumber});
        console.log(info);

        if (info && info.BusiType == "整体翻新") {
            return "#0000CD"
        } else if (info && info.BusiType == "局部翻新") {
            return "#000000";
        }
    },
});


Template.inspectedList.events({
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/qualityLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .gocustomerDetail': function (event) {
        var id = $(event.currentTarget).attr("id").trim();
        Router.go("/constructionDetail/constructionId/" + id + "/type/inspected");
    },
    'click .nowDate': function () {
        Session.set("selectNowDate1", true);
        Session.set("searchNowDateInfo1", new Date().Format("yyyy-MM-dd"));
    },
    'click .nowAll': function () {
        Session.set("selectNowDate1", false);
        Session.set("searchNowDateInfo1", null);

    },
    'click .selectDate': function () {
        IonPopup.prompt({
            title: '设置搜索时间',
            template: '请设置搜索时间',
            okText: '确定',
            cancelText: '取消',
            inputType: 'date',
            onOk: function () {
                var newDate = $('.popup-body input').val().trim();
                Session.set("selectDateInfo1", newDate);
                Session.set("selectDateButton1", true);
            }
        });
    },
    'click .selectAll': function () {
        Session.set("selectDateInfo1", null);
        Session.set("selectDateButton1", false);
    }
});

Template.constructionList.onDestroyed(function () {
    Session.set("currentCustomerSearchVal", null);
    Session.set("custInfo", null);
    Session.set("selectDateInfo1", null);
    Session.set("selectDateButton1", null);
    Session.set("selectNowDate1", null);
    Session.set("searchNowDateInfo1", null);
});