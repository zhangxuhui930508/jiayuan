/**
 * Created by zhangxuhui on 16/4/26.
 */
Meteor.methods({
    'updateMaterialState': function (id, count, uid) {
        ERP_Budget_Material.update({_id: id}, {
            $set: {
                材料数量: parseInt(count),
                isPutInStorage: true,
                putInStorageDate: new Date(),
                putInStoragePerson: uid
            }
        })
    }
});