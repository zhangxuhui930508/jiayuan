/**
 * Created by zhangxuhui on 15/11/10.
 */
Template.ContractListType.rendered = function () {
    var ACNumber;
    if (Roles.userIsInRole(Meteor.userId(), ["client", "materialDept", "contractDept"], "user-group")) {
        ACNumber = Session.get("customerLogin");
    } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], "user-group")) {
        ACNumber = Session.get("customerDecorateInfo").ACNumber;
    }

    Meteor.call("erpBudgetMaterial", ACNumber, function (err, result) {
        Session.set("ERP_Budget_Material_count", result);
    });
    Meteor.call("ERPProductOrder", ACNumber, function (err, result) {
        Session.set("ERP_Product_Order_count", result);
    });


    Meteor.call("duizhangCount", ACNumber, function (err, result) {
        Session.set("duizhangCount", result);
    });

};

Template.ContractListType.helpers({
    headObj: function () {
        return {
            title: '材料类型',
        }
    },
    'Budget_Material_count': function () {
        return Session.get("ERP_Budget_Material_count");
    },
    'productOrder_Count': function () {
        return Session.get("ERP_Product_Order_count");
    },
    duizhangCount: function () {
        return Session.get("duizhangCount");
    }
});

Template.ContractListType.events({
    'click .backMenuList': function () {
        if (Roles.userIsInRole(Meteor.userId(), ["materialDept"], "user-group")) {
            Router.go("/materialDeptConstructionList/" + Session.get("materialDeptCustomerType"));
        } else {
            Router.go("/mycustomer");
        }
    },
    'click .captain_custom': function () {
        Session.set("captainCustom", "captain_custom");
        Router.go("/StandardMaterial");
    },
    'click .notSign': function () {
        
    }
});

