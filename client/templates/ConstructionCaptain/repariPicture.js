/**
 * Created by zhangxuhui on 16/4/18.
 */


Template.repariPicture.helpers({
    'headObj': function () {
        return {}
    },
    titleName: function () {
        var titleName = Router.current().params.titleName;
        if (titleName) {
            return titleName
        }
    },
    imageInfo: function () {
        return Images.find();
    }


});

Template.repariPicture.events({
    'click .backMenuList': function () {
        history.back();
    }
});