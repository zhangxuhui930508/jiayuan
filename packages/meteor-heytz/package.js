Package.describe({
    name: 'meteor:heytz',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: '',
    // URL to the Git repository containing the source code for this package.
    git: '',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function (api) {
    api.versionsFrom('1.2');
    api.use([
        "templating",
        "underscore",
        // "fastclick", //消除点击延时
        "iron:router@1.0.0",
        "tracker",
        "session",
        "jquery"
    ], "client");
    api.use('ecmascript');
    api.addFiles([
        "components/heytzSlideButton/heytzSlideButton.html",
        "components/heytzSlideButton/heytzRightButtons.html",
        "components/heytzSlideButton/heytzEditButton.html",
        "components/heytzSlideButton/heytzDelButton.html",
        "components/heytzSlideButton/heytzSlideButton.js",
        "stylesheets/heytzSlideBtn/heytzSlideBtn.css",
    ], ["client"]);
    api.export("HeytzSlideButton");
});

Package.onTest(function (api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('meteor-heytz');
    api.addFiles('meteor-heytz-tests.js');
});
