//var subscriptions = new SubsManager();

Router.map(function () {
    this.route("materialDeptLogin", {
        action: function () {
            if (Meteor.userId()) {
                Meteor.call("materialDeptCheckIndex", function (err, result) {
                    console.log(err, result);
                    // alert(result);
                    if (!err && (result.templatePage || _.isNull(result))) {
                        if (result.loginRole) {
                            Session.set("loginRole", result.loginRole);
                        }
                        Router.go("/" + result.templatePage);
                    } else {
                        alert("请从正确入口进入!");
                    }
                });
            } else {
                this.render();
            }
        }
    });
    this.route("contractDeptLogin", {
        action: function () {
            if (Meteor.userId()) {
                Meteor.call("contractDeptCheckIndex", function (err, result) {
                    console.log(err, result);
                    // alert(result);
                    if (!err && (result.templatePage || _.isNull(result))) {
                        if (result.loginRole) {
                            Session.set("loginRole", result.loginRole);
                        }
                        Router.go("/" + result.templatePage);
                    } else {
                        alert("请从正确入口进入!");
                    }
                });
            } else {
                this.render();
            }
        }
    });
    this.route("qualityLogin", {
        action: function () {
            if (Meteor.userId()) {
                Meteor.call("qcCheckIndex", function (err, result) {
                    console.log(err, result);
                    // alert(result);
                    if (!err && (result.templatePage || _.isNull(result))) {
                        if (result.loginRole) {
                            Session.set("loginRole", result.loginRole);
                        }
                        Router.go("/" + result.templatePage);
                    } else {
                        alert("请从正确入口进入!");
                    }
                });
            } else {
                this.render();
            }
        }
    }); //质检人员登陆
    this.route("supplierLogin", {
        action: function () {
            if (Meteor.userId()) {
                Meteor.call("SupplierCheckIndex", function (err, result) {
                    console.log(err, result);
                    // alert(result);
                    if (!err && (result.templatePage || _.isNull(result))) {
                        if (result.loginRole) {
                            Session.set("loginRole", result.loginRole);
                        }
                        Router.go("/" + result.templatePage);
                    } else {
                        alert("请从正确入口进入!");
                    }
                });
            } else {
                this.render();
            }
        }
    });//供应商登录

    this.route("constructionLogin", {
        action: function () {
            if (Meteor.userId()) {
                Meteor.call("constructionCheckIndex", function (err, result) {
                    console.log(err, result);
                    // alert(result);
                    if (!err && (result.templatePage || _.isNull(result))) {
                        if (result.loginRole) {
                            Session.set("loginRole", result.loginRole);
                        }
                        Router.go("/" + result.templatePage);
                    } else {
                        alert("请从正确入口进入!");
                    }
                });
            } else {
                this.render();
            }
        }
    });
    //施工队长进度查询
    this.route('login', {
        path: '/', action: function () {
            if (Meteor.userId()) {
                Meteor.call("clientCheckIndex", function (err, result) {
                    console.log(err, result);
                    // alert(result);
                    if (!err && (result.templatePage || _.isNull(result))) {
                        Router.go("/" + result.templatePage);
                    } else {
                        alert("请从正确入口进入!");

                    }
                });
            } else {
                this.render();
            }
        }
    });//客户登录
    this.route("designerLogin", {
        action: function () {
            if (Meteor.userId()) {
                Meteor.call("checkIndexPage", function (err, result) {
                    console.log(err, result);
                    // alert(result);
                    if (!err && (result.templatePage || _.isNull(result))) {
                        if (result.loginRole) {
                            Session.set("loginRole", result.loginRole);
                        }
                        Router.go("/" + result.templatePage);
                    } else {
                        alert("请从正确入口进入!");
                    }
                });
                // Router.go("/customerType")
            } else {
                this.render();
            }
        }
    });//设计师登录
    this.route("shopManagerTestPage");//


    //客户列表界面
    //dId:表示设计师ID
    //type:表示要查询的客户类型
    this.route('customerList', {
        path: "/customerList/designerID/:dId/customerType/:type",
        //name: 'customerList',
        action: function () {
            this.render();
        }
    });


    this.route('customerType', {
        action: function () {
            if (!Session.get("currentDesignerID")) {
                Meteor.subscribe("publis_user", Meteor.userId(), function () {
                    var info = Meteor.users.findOne({_id: Meteor.userId()});
                    Session.set("currentDesignerID", info.ID);
                });
                // this.subscribe("unReadMessage");
            }
            // this.subscribe("designerCustomerTypeCount");

            this.render();
        }
    });//客户类型列表页面
    this.route('customerDetail', {
        path: '/customerDetail/customerType/:cType/customerId/:cId',
        action: function () {
            this.render();

        }
    });//客户详情界面
    this.route('modeSelect');//装修材料模式选择

    this.route('materialList', {
        path: '/materialList/:id',
        action: function () {
            var MClassifyId = parseInt(this.params.id);
            console.log("MClassifyId:", MClassifyId);
            var t1 = this.subscribe("materialListByclassifyId", MClassifyId);
            if (t1.ready()) {
                IonLoading.hide();
                this.render();
            } else {
                IonLoading.show({
                    duration: 1000
                })
            }
        }
    });//装修材料列表页面

    this.route('materialDetail', {

        path: '/materialDetail/materialClassifyId/:mCid/materialId/:mid', action: function () {
            var materialId = parseInt(this.params.mid);
            console.log(materialId);
            var t1 = this.subscribe("materialDetailInfoById", materialId);
            var handle = this.subscribe("MaterialimagesByIds", materialId);
            if (t1.ready()) {
                this.render();
            }
        }
    });//材料详细页面
    this.route('shopping', {
        path: '/shopping/:AcNumber',
        action: function () {
            var AcNumber = this.params.AcNumber;
            var t1 = this.subscribe("shoppingPageInfo", AcNumber);
            if (t1.ready()) {
                this.render();
            }
        }
    });//选材料车


    this.route("shoppingDetail", {
        path: "/shoppingDetail/mOrderType/:orderType/materialID/:mId",
        action: function () {
            var acNumber = this.params.orderType;
            var mId = this.params.mId;
            var t1 = this.subscribe("materialDetailInfoById", mId);
            var handle = this.subscribe("MaterialimagesByIds", mId);
            if (t1.ready()) {
                this.render();
            }
        }
    });

    this.route('budgetList');//预算列表页面
    this.route('projDetail', {
        path: "/projDetail/customerType/:cType/solutionId/:_id",
        action: function () {

            this.render();
        }
    });//方案详情页面

    this.route('updateProjDetail', {
        path: '/updateProjDetail/solutionId/:sId/materialId/:mId/OrderType/:OType/customerType/:cType',
        action: function () {

            this.render();

        }
    });//方案材料详情
    this.route('DecorateArea');//装修区域


    //购物车
    this.route('MaterialClassify');//材料分类1
    this.route('MaterialClassify2', {
        path: "/MaterialClassify2/:mClassifyName", action: function () {
            this.render();
        }
    });//材料二级分类
    this.route('MaterialClassify3', {
        path: '/MaterialClassify3/Mclassify1/:Mclassify1/Mclassify2/:Mclassify2',
        action: function () {
            this.render();
        }
    });//材料二级分类
    this.route('MaterialClassify4', {
        path: '/MaterialClassify4/Mclassify1/:Mclassify1/Mclassify2/:Mclassify2/Mclassify3/:Mclassify3',
        action: function () {
            this.render();
        }
    });//材料二级分类


    //***************************************************
    this.route('countorView');
    this.route('planList');
    this.route('_tabs');
    this.route('solutionList');
    this.route('materialSelect');


// ***************20151023******customer****************
    this.route("customerindex");//客户首页 包含广告和咨询信息
    this.route("mycustomer", {
        action: function () {
            var Acnumber;
            if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
                Acnumber = Session.get("customerLogin");
            } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "districtManager", "vicePresident", "StoreManager", 'test', "ConstructionCaptainBoss"], "user-group")) {
                Acnumber = Session.get("customerDecorateInfo").ACNumber;
            } else if (Roles.userIsInRole(Meteor.userId(), ['designer'], 'user-group')) {
                Acnumber = Session.get("customerDecorateInfo").ACNumber;
            }


            this.subscribe("myCustomerPageInfo", Acnumber);

            this.render();
        }

    });//客户我的页面 包含 选材车，方案，合同 核价单 变更单 工地 通知 结算 评价
    this.route("CsolutionsList", {
        path: "/CsolutionsList/:acnumber",
        action: function () {
            var AcNumber = this.params.acnumber;
            var t1 = this.subscribe("getSolutionInfoByAcnumber", AcNumber);
            if (t1.ready()) {
                this.render();
            }
        }
    });//客户我的页面  方案
    this.route("CSolutionDetail", {
        path: "/CSolutionDetail/:sId",
        action: function () {
            this.render();
        }

    });//客户我的页面  方案
    this.route("customerShopping");
    this.route('CSMaterialDetail', {
        path: '/CSMaterialDetail/solutionId/:sId/materialId/:mId/OrderType/:OType',
        action: function () {
            var sId = this.params.sId;
            var mId = this.params.mId;
            var t1 = this.subscribe("materialDetailInfoById", mId);
            //var t2 = this.subscribe("MaterialimagesByIds", mId);
            var t3 = this.subscribe("meterialDetailBySolutionid", sId);
            if (t1.ready() && t3.ready()) {
                this.render();
            }
        }
    });//材料详细页面


    this.route("hejiadan");
    this.route("ChangeOrder");

    this.route("SettlementType"); //结算类型  包括催款单和结算单
    this.route("paymentReminder", {
        action: function () {
            var Acnumber;
            if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
                Acnumber = Session.get("customerLogin");
            } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "districtManager", "vicePresident", "StoreManager", 'test', 'designer', "ConstructionCaptainBoss"], "user-group")) {
                Acnumber = Session.get("customerDecorateInfo").ACNumber;
            } else if (Roles.userIsInRole(Meteor.userId(), ['designer'], 'user-group')) {
                Acnumber = Session.get("customerDecorateInfo").ACNumber;
            }
            var t1 = this.subscribe("NuclearPriceSheet", Acnumber, 8);
            if (t1.ready()) {
                IonLoading.hide();
                this.render();
            } else {
                IonLoading.show({
                    duration: 1500
                })
            }
        }
    }); //催款单
    // this.route("settlementOrder",{
    //     action:function () {
    //         var Acnumber;
    //         if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
    //             Acnumber = Session.get("customerLogin");
    //         } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "districtManager", "vicePresident", "StoreManager", 'test','designer'], "user-group")) {
    //             Acnumber = Session.get("customerDecorateInfo").ACNumber;
    //         } else if (Roles.userIsInRole(Meteor.userId(), ['designer'], 'user-group')) {
    //             Acnumber = Session.get("customerDecorateInfo").ACNumber;
    //         }
    //         var t1=this.subscribe("erpRemoteFileStorageSettlement", Acnumber, 8,2);
    //         if (t1.ready()) {
    //             IonLoading.hide();
    //             this.render();
    //         } else {
    //             IonLoading.show({
    //                 duration: 1500
    //             })
    //         }
    //     }
    // }); //竣工结算单


    this.route("evaluationPage"); //评价


//    ===================================客户材料分类
    this.route("customerParts");//客户装修部位选择
    this.route("materialClassify01");//客户材料第一级
    this.route("materialClassify02");//客户材料第一级
    this.route("ContractListType");// 合同材料类型
    this.route("StandardMaterial", {});// 标准材料列表 合同
    this.route("CustomizeMaterial");// 标准材料列表 合同
    this.route("complaintsList");// 投诉电话 合同


    this.route("product01Detail", {
        path: "/product01Detail/:id", action: function () {
            this.render();
        }
    });
    this.route("product02Detail", {
        path: "/product02Detail/:id", action: function () {
            this.render();
        }
    });
    this.route("product03Detail", {
        path: "/product03Detail/:id", action: function () {
            this.render();
        }
    });
    this.route("product04Detail", {
        path: "/product04Detail/:id", action: function () {
            this.render();
        }
    });
    this.route("product05Detail", {
        path: "/product05Detail/:id", action: function () {
            this.render();
        }
    });
    this.route("product06Detail", {
        path: "/product06Detail/:id", action: function () {
            this.render();
        }
    });

    this.route("CustomizeMaterialDetail", {
        path: "/CustomizeMaterialDetail/:id", action: function () {
            this.render();
        }
    });
    //合同->标准材料详情
    this.route("StandardMaterialDetail", {
        path: "/StandardMaterialDetail/:id", action: function () {
            var id = this.params.id;
            var t1 = this.subscribe("ERP_Budget_MaterialACNumber_ID", id);
            if (t1.ready()) {
                this.render();
            }
        }
    });

    this.route("newsList", {
        path: "/newsList/:id", action: function () {
            this.render();
        }
    });// 新闻详细页面
    this.route("AdvertisementPicTest");// 新闻列表


//    =================================施工队长角色======================
//施工队长登录
    this.route("constructionCType", {
        action: function () {
            var id;
            if (!Session.get("currentConstructionId")) {
                this.subscribe("publis_user", Meteor.userId(), function () {
                    var info = Meteor.users.findOne({_id: Meteor.userId()});
                    if (info) {
                        id = info.ID.toString();
                        Session.set("currentConstructionId", id);
                    }
                });
            }
            this.subscribe("unReadMessage", Meteor.userId());

            this.render();
        }
    });//施工队长筛选客户类型 已开工 未开工  已完成客户


//    ------------店长
    this.route("shopManagerSearch");//店长搜索


    //客户列表
    this.route("construtionList", {
        path: "/construtionList/:type", action: function () {
            // var type = Router.current().params.type;
            // var t1 = this.subscribe("construtionWorkCustomer", Session.get("currentConstructionId"), type);
            // if (t1.ready()) {

            this.render();
            // }
        }
    });
    this.route("qualityInspection"); //质检报检
    this.route("borrowingType"); //借款类型  已发放和未发放

    this.route("borrowingList", {
        path: "/borrowingList/:type", action: function () {
            var type = this.params.type;
            var t1 = this.subscribe("borrowingInfo", Session.get("currentConstructionId"), type);
            if (t1.ready()) {
                this.render();
            }
        }
    })

    this.route("ConstrectionType");//工费费类型

    this.route("ConstructionCost", {
        path: "/ConstructionCost/:type", action: function () {
            // var type = this.params.type;
            // this.subscribe("constructionCostInfo", Session.get("currentConstructionId"), type);
            this.render();
        }
    })
    this.route("constructionCustDetail", {
        path: '/constructionCustDetail/customerType/:cType/customerId/:acnumber',
        action: function () {
            var acnumber = this.params.acnumber;
            this.subscribe("CustomerID", acnumber)
            var t2 = this.subscribe("solutionsBycustomerid", acnumber);

            this.render();
        }
    });

//    ==============核价单详情
    this.route("hejiadanDetail", {
        path: "/hejiadanDetail/:childrenType", action: function () {
            var type = this.params.childrenType;
            var Acnumber;
            if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
                Acnumber = Session.get("customerLogin");
            } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "designer", "districtManager", "vicePresident", "StoreManager", "test", "ConstructionCaptainBoss"], "user-group")) {
                Acnumber = Session.get("customerDecorateInfo").ACNumber;
            }

            var t1 = this.subscribe('NuclearPriceSheet', Acnumber, 32, parseInt(type));
            if (t1.ready()) {
                this.render();
            }
        }
    });

    //打开pdf文件
    this.route("pdfFiles", {
        path: "/pdfFiles/:id", action: function () {
            var id = this.params.id;
            this.subscribe("pdfFilesInfo", id);
            this.render();
        }
    });
    //*******************************供应商************************

    this.route("orderType");//供应商订单类型
    // 供应商订单列表
    this.route("orderList", {
        path: "/orderList/:type", action: function () {
            this.render();
        }
    });
    ////供应商材料列表
    this.route("orderMaterialList", {
        action: function () {
            var t1 = this.subscribe("orderMaterialListInfo", Session.get("currentSuplierID"));
            if (t1.ready()) {
                this.render()
            }
        }
    });

    this.route("orderDetail", {
        path: "/orderDetail/type/:ordertype/acnumber/:acnumber/orderNo/:orderNo", action: function () {
            var acnumber = this.params.acnumber;
            var orderNo = parseInt(this.params.orderNo);
            var t1 = this.subscribe("orderDetailInfo", acnumber, orderNo);
            this.subscribe("publishSupplierInfo", Meteor.userId());
            // if (t1.ready()) {
            this.render();
            // }
        }
    });

    this.route("orderMaterialDetailInfo", {
        path: '/orderMaterialDetailInfo/:id', action: function () {
            var id = parseInt(this.params.id);
            var t1 = this.subscribe("currentOrderMaterialDetail", id);
            var t2 = this.subscribe("currentSelectMaterialImg", id);
            if (t1.ready() && t2.ready()) {
                this.render();
            }
        }
    })


//    聊天


    this.route("chat", {
        path: '/chat/roomId/:id/materialId/:mid', action: function () {
            this.render();
        }
    });

    this.route("messageInfo", {
        // path:'/messageInfo/:id',
        action: function () {
            this.render();
        }
    });


    //********************************************************************************

    this.route("constructionList", {
        path: '/constructionList/:type', action: function () {
            this.render();
        }
    }); //检验工地列表

    this.route("assignQC", {
        action: function () {
            this.subscribe("assignQCInfo", function (err, result) {
                // var checkProjectInfo = checkConstructionProject.find().fetch();
                // var acnumbers = [];
                // _.each(checkProjectInfo, function (info) {
                //     acnumbers.push(info.acnumber);
                // });
                // Meteor.call("getCustInfo", acnumbers, function (err, result) {
                //     console.log(result, "result");
                //     Session.set("custInfo", result);
                // })
            });
            this.render();
        }
    }); //分配质检人员
    this.route("assignList", {
        path: "/assignList/:id", action: function () {
            var id = this.params.id;
            // this.subscribe("getCustInfoDetail", id);
            // this.subscribe("publishCheckProject", id);
            // this.subscribe("publishEmployees");
            // this.subscribe("publishEmployeesMacanger", Meteor.userId());


            //====================================================
            this.subscribe("assignerListPage", id);

            this.render();
        }
    }); //分配质检人员

    this.route("workSiteRepair", {
        path: "/workSiteRepair/checkId/:id/acnumber/:acnumber", action: function () {
            this.render();
        }
    });

    this.route("repairList", {
        path: "/repairList/acnumber/:acnumber/project/:project", action: function () {
            // var id = this.params.id;
            // this.subscribe("publishRepairList", id);
            this.render();
        }
    });

    this.route("repariPicture", {
        path: "/repariPicture/titleName/:titleName/imageId/:imageId", action: function () {
            var imageId = this.params.imageId;
            this.subscribe("publishImageInfo", imageId);
            this.render();
        }
    });

    this.route("inspectedList"); //已检工地
    this.route("notExaminedList"); //未检工地
    this.route("systemMessage"); //系统消息
    this.route("typicalSentences", {
        path: "/typicalSentences/materialId/:mid/acnumber/:acnumber", action: function () {
            this.render();
        }
    }); //常用语发送

    this.route("constructionDetail", {
        path: '/constructionDetail/constructionId/:id/type/:type', action: function () {
            this.render();
        }
    });

    this.route("searchCustomer", {
        path: '/searchCustomer/:type', action: function () {
            this.subscribe("publishemployeeInfo", Session.get("currentConstructionId"));
            this.render();
        }
    })


//   =========================快捷材料对话客户列表==========================

    this.route("materialChatList");//对话客户列表
    this.route("chatMessageList", {
        path: "/chatMessageList/:messageState", action: function () {
            this.render();
        }
    });
    //经销商材料统计
    this.route("supplierCount");
    //进度查询
    //todo 传入传入acnumber和id
    this.route("scheduleList", {
        path: "/scheduleList/:mId", action: function () {
            var mid = this.params.mId;
            var t1 = this.subscribe("getErpBudgetMaterialByID", mid);
            var t2 = this.subscribe("publishSupplierInfo", Meteor.userId());

            if (t1.ready)
                this.render();
        }
    });

    //订单进度跟踪
    this.route("orderProgressList", {
        path: "/orderProgressList/acnumber/:acnumber/orderId/:orderId", action: function () {
            var orderId = this.params.orderId;
            var acnumber = this.params.acnumber;
            //order信息  根据受理编号和订单号
            //    根据订单号进行进度更新
            var t1 = this.subscribe("publishOrderProgressList", orderId, acnumber);

            if (t1.ready()) {
                this.render()
            }
        }
    })


    this.route("proceedOrderType", {
        action: function () {
            this.subscribe("getSupplierPutType")
            this.render()
        }
    });
    //带测量列表
    this.route("measureList");
    //发货中的列deliverGoodsList表
    this.route("deliverGoodsList");
    //其他进行中的订单
    this.route("otherProceedOrder", {
        path: "/otherProceedOrder/:type", action: function () {
            this.render();
        }
    });

    this.route("contractPricing");
    this.route("PutInStorage");

    this.route("qcCheckPage");
    this.route("cancelOrder");


    //*******************************************************************************


    //核价详情页面
    this.route("contractPricingDetail", {
        path: "/contractPricingDetail/:id",
        action: function () {
            var id = this.params.id;
            this.subscribe("publishCheckPricingOrder", id);
            this.render();
        }
    });

    this.route('/contractPricingDetail2/', {path: "/contractPricingDetail2/:id"})
    this.route('/zhengtihejiaDetaila/', {path: "/zhengtihejiaDetaila/:id"})
    this.route('/PutInStorageDetail2/', {path: "/PutInStorageDetail2/:id"})

    this.route("PutInStorageDetail", {
        path: "/PutInStorageDetail/:id",
        action: function () {
            var id = this.params.id;
            this.subscribe("publishCheckPutInStrongOrder", id);
            this.render();
        }
    });
    this.route("danxianghejiaDetail", {
        path: "/danxianghejiaDetail/:id",
        action: function () {
            var id = this.params.id;
            this.subscribe("publishCheckPutInStrongOrder2", id);
            this.render();
        }
    });

    this.route("finishOrderList", {
        action: function () {

            this.render();
        }
    });

    this.route("suspendOrderList", {
        action: function () {
            this.render();
        }
    });

    this.route("materialDeptConstructionType", {
        action: function () {
            // var t1 = this.subscribe("meterialDeptConstructionType", Meteor.userId());
            Meteor.call("materialDeptConstructionListCount", function (err, result) {
                Session.set("materialDeptConstructionInfoCount", result)
            });
            this.render();

        }
    });

    this.route("materialDeptConstructionList", {
        path: "/materialDeptConstructionList/:type",
        action: function () {

            this.render()
        }
    });

    this.route("constructionCaptainList", {
        action: function () {
            var t1 = this.subscribe("constructionCaptainListInfo");
            if (t1.ready()) {
                this.render()
            }
        }
    });

    this.route("orderSortSupplierList", {
        path: "/orderSortSupplierList/:acnumber",
        action: function () {
            var acnumber = this.params.acnumber;
            this.subscribe("orderSortSupplierMaterial", acnumber);
            this.render();
        }
    })

    this.route("CandSChat", {
        action: function () {
            this.subscribe("publishCandSChat");
            this.render();
        }
    });


    this.route("typicalSentences1", {
        path: "/typicalSentences1/acnumber/:acnumber/supplierId/:supplierId", action: function () {

            this.render();
        }
    })

    this.route("contractDeptViewCustomer");
    this.route("contractMaterailInfo", {
        path: "/contractMaterailInfo/id/:id/type/:type", action: function () {
            this.render();
        }
    });

    this.route("/zhengtihejiaList", {path: '/zhengtihejiaList/id/:id/type/:type'});


    this.route("designerSearch");

    this.route("countDetail", {
        path: "/countDetail/:id"
    })

    this.route("getOrderProceedList", {path: '/getOrderProceedList/:type'});

    this.route("measureListByOrder");
    this.route("deviceGoodsByOrder");

    this.route("orderDetailContract", {path: "/orderDetailContract/id/:ID/type/:type"});
    this.route("danxianghejiaList", {path: "/danxianghejiaList/id/:ID/type/:type"});

    this.route("associatedOrderList", {path: "/associatedOrderList/acnumber/:acnumber/materialId/:materialId"});

    this.route("satisfaction", {path: "/satisfaction/acnumber/:acnumber/construction/:constructionId/checkId/:checkId"});//满意度
    this.route("qcEditWorkSiteRepairList", {path: "/qcEditWorkSiteRepairList/:checkId"});
    this.route("qcCheckDetail", {path: "/qcCheckDetail/:id"})
});

