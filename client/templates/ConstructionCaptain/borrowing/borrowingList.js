/**
 * Created by zhangxuhui on 16/3/19.
 */
Template.borrowingList.helpers({
    headObj: function () {
        return {
            leftButton: true,
            backTemplate: 'borrowingType'
        }
    },
    'titleName': function () {
        var title = Router.current().params.type;
        if (title) {
            if (title == "true") {
                return "已发放"
            } else {
                return "未发放"
            }
        }
    },
    "erpProjectLoanList": function () {
        var erpProjectLoanInfo = erpProjectLoan.find({}, {sort: {申请日期: -1}});
        if (erpProjectLoanInfo) {
            return erpProjectLoanInfo;
        }
    },
    "getResonDetail": function (reason) {
        if (reason == 1) {
            return "一期工程款";
        } else if (reason == 2) {
            return "二期工程款";
        } else if (reason == 3) {
            return "三期工程款";
        } else if (reason == 4) {
            return "四期工程款";
        } else if (reason == 5) {
            return "五期工程款";
        } else if (reason == 6) {
            return "六期工程款";
        }
    },
    getDateFormat: function (date) {
        if (date) {
            return date.Format("yyyy-MM-dd");
        }
    }
});