/**
 * Created by zhangxuhui on 16/4/21.
 */
var selectTabs = "selectTabsTopKey";
Template.materialChatList.onRendered(function () {
    Session.set(selectTabs, "unread");
    IonLoading.show();
    this.autorun(function () {
        Meteor.subscribe("chatQucikReplayInfo", Session.get(selectTabs), function () {
            IonLoading.hide();
        })
    })

});


Template.materialChatList.helpers({
    headObj: function () {
        return {
            title: '材料会话列表'
        }
    },
    isActive: function (val) {
        var select = Session.get(selectTabs);
        if (select && select == val) {
            return "active";
        }
    },
    'Info': function () {
        var customerInfo = Cust_Info.find({}, {sort: {CreatTime: -1}});
        if (customerInfo) {
            return customerInfo;
        }
    }

});

Template.materialChatList.events({
    'click .gocustomerDetail': function (event) {
        var acnumber = $(event.currentTarget).children("h2").attr("class").trim();
        var messageCount = roomMessage.find({clientId: acnumber}).count();
        if (messageCount == 1) {
            var roomInfo = roomMessage.findOne();
            if (roomInfo) {
                var acnumber = {};
                acnumber.ACNumber = roomInfo.clientId;
                Session.set("customerDecorateInfo", acnumber);
                Router.go("/chat/roomId/" + roomInfo._id + "/materialId/" + roomInfo.materialId);
            }
        } else {
            Router.go("/chatMessageList/" + Session.get(selectTabs));
        }
    },
    "click .tab-item": function (event) {
        var val = $(event.currentTarget).attr("value");
        Session.set(selectTabs, val);
    },
    'click .backMenuList': function () {
        if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
            Router.go("/customerType");
        } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "ConstructionCaptainBoss"], "user-group")) {
            Router.go("/constructionCType");
        } else if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
            Router.go("/orderType");
        }
    }
});
