/**
 * Created by zhangxuhui on 16/4/20.
 */
Template.typicalSentences.onRendered(function () {
    this.subscribe("typicalSentencesInfo");
});

Template.typicalSentences.helpers({
    headObj: function () {
        return {
            title: "常用语",
            leftButton: true,
            backTemplate: 'constructionCType'
        }
    },
    typicalSentencesInfo: function () {
        var info = typicalSentences.find();
        if (info) {
            return info;
        }
    },

});

Template.typicalSentences.events({
    'click .backMenuList': function () {
        history.back();
    },
    'click .item': function (event) {
        var message = $(event.currentTarget).children("h2").text().trim();
        var mid = Router.current().params.mid;
        var acnumber = Router.current().params.acnumber;
        IonPopup.show({
            title: "确定发送?",
            template: message,
            buttons: [{
                text: '发送',
                type: 'button-positive',
                onTap: function () {

                    Meteor.call("sendMessageOrCreate", mid, acnumber, Meteor.userId(), message, Session.get("materialName"), function (err, result) {
                        if (!err) {
                            IonLoading.show({
                                customTemplate: "<h4>消息发送成功!</h4>",
                                duration: 1000
                            });
                        } else {
                            IonLoading.show({
                                customTemplate: "<h4>消息发送失败!</h4>",
                                duration: 1000
                            });
                        }
                    });
                    IonPopup.close();
                    history.back();
                }
            }]
        });
    }
});