/**
 * Created by zhangxuhui on 16/7/13.
 */

Template.zhengtihejiaDetaila.onRendered(function () {
    var id = Router.current().params.id;
    IonLoading.show();
    this.subscribe("publishOrderViewAllInfo2", id, function () {
        IonLoading.hide();
    })
});

Template.zhengtihejiaDetaila.helpers({
    headObj: function () {
        return {}
    },
    'titleName': function () {
        return "审核详情"
    },
    erpBudgetMaterialInfo: function () {
        return ERP_Budget_Material.find();
    },
    imageInfo: function () {
        var info = SupplierImages.find();
        if (info) {
            return info;
        }
    },
    isShowButton: function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info && info.orderState == 4) {
            return false;
        }
        return true;
    }
});


Template.zhengtihejiaDetaila.events({
    'click .approved': function () {
        //    审核通过
        IonPopup.confirm({
            title: '温馨提示',
            template: "确认审核通过?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var id = Router.current().params.id;
                Meteor.call("checkOrderInfo2", id, Meteor.userId(), 4, function (err, result) {
                    if (!err) {
                        Router.go("/contractPricing")
                    }
                });
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .notApproved': function () {
        $(".reasonRemark").css("display", "")
        console.log($(".textareaReason").val());
        var textarea = $(".textareaReason").val().trim();
        if (!textarea) {
            IonLoading.show({
                customTemplate: "<h4>请输入审核不通过的理由!</h4>",
                duration: 1000
            });
            return;

        }
        IonPopup.confirm({
            title: '温馨提示',
            template: "确认审核不通过?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var id = Router.current().params.id;
                Meteor.call("checkOrderInfo2", id, Meteor.userId(), 5, textarea, function (err, result) {
                    if (!err) {
                        Router.go("/contractPricing")
                    }
                });
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .backMenuList': function () {
        history.back();
    }
});