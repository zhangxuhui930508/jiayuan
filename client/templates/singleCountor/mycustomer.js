/**
 * Created by zhangxuhui on 15/10/23.
 */

Template.mycustomer.helpers({
    headObj: function () {
        return {
            title: '我的',
            leftButton: true,
            //backTemplate: 'customerindex'
        }
    },
    solutionCount: function () {
        // var solutionInfo = Solutions.find();
        // if (solutionInfo) {
        //     return solutionInfo.count();
        // }
        return Solutions.find().count() || "0";
    },
    shoppingCount: function () {
        // var shoppingInfo = ShoppingCart.find();
        // if (shoppingInfo) {
        //     return shoppingInfo.count();
        // }
        return ShoppingCart.find().count() || "0";

    },
    'hejiaInfoCount': function () {
        return erpRemoteFileStorage.find({类型: 32}).count() || '0';

        // var info = erpRemoteFileStorage.find({类型: 32});
        // if (info) {
        //     return info.count();
        // }
    },
    'ChangeOrderCount': function () {
        return erpRemoteFileStorage.find({类型: 2}).count() || '0';

        // try {
        //     var info = erpRemoteFileStorage.find({"类型": 2});
        //     if (info) {
        //         return info.count();
        //     }
        // } catch (e) {
        //
        // }
    },
    'titleName': function () {
        try {
            if (Roles.userIsInRole(Meteor.userId(), ["client", "materialDept", "contractDept"], "user-group")) {
                var info = DesignerRelationClient.findOne({cId: Session.get("customerLogin")});
                if (info) {
                    return "我的(" + info.cType + ")";
                }
            } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "ConstructionCaptain", "districtManager", "vicePresident", "StoreManager", 'test', "ConstructionCaptainBoss"], "user-group")) {
                if (Session.get("customerDecorateInfo").cType == "null") {
                    console.log("================================")
                    var info = DesignerRelationClient.findOne({cId: Session.get("customerDecorateInfo").ACNumber});
                    if (info) {
                        return "我的(" + info.cType + ")";
                    }
                }
                return "我的(" + Session.get("customerDecorateInfo").cType + ")";
            }

        } catch (e) {
            console.log(e.message);
            return "我的";
        }
    },
    'Contract_customersType': function () {
        try {
            var designerInfo = DesignerRelationClient.findOne({});
            if (designerInfo) {
                if (designerInfo.cType == "已签" || designerInfo.cType == "已归档") {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (e) {

        }
    },
    'CompletionCustInfo': function () {
        try {
            var designerInfo = DesignerRelationClient.findOne({});
            if (designerInfo) {
                if (designerInfo.cType == "已归档") {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (e) {
        }
    },
    'constructionCount': function () {
        var info = systemMessage.find();
        if (info.count() > 0) {
            return true
        } else {
            return false
        }
        // try {
        //     var info = Session.get("messageCountInfo").systemMessageCount;
        //     if (info > 0) {
        //         return true;
        //     }
        // } catch (e) {
        //
        // }
    },
    "materialMessageCount": function () {
        var info = MessageState.find();
        if (info.count() > 0) {
            return true
        } else {
            return false
        }


        // try {
        //     var info = Session.get("messageCountInfo").materialCount;
        //     if (info > 0) {
        //         return true;
        //     }
        // } catch (e) {
        //
        // }
    },
    'jiesuanCount': function () {

        return erpRemoteFileStorage.find({"类型": 8}).count() || "0";


        /// / try {
        //     var info = erpRemoteFileStorage.find({"类型": 8});
        //     if (info) {
        //         return info.count();
        //     }
        // } catch (e) {
        //
        // }
    },
    iscontractDept: function () {
        // Session.set("loginRole", "contractDept");

        if (Session.get("loginRole") == "contractDept")return true;
    }
});

Template.mycustomer.events({
    'click .backTemplates': function () {
        if (Roles.userIsInRole(Meteor.userId(), ["contractDept"], 'user-group')) {
            Router.go("/contractDeptViewCustomer");
        } else if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
            Router.go("/customerindex")
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], "user-group")) {
            Router.go("/customerDetail/customerType/" + Session.get("customerDecorateInfo").cType + '/customerId/' + Session.get("customerDecorateInfo").cId);
        } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain"], "user-group")) {
            Router.go("/customerDetail/customerType/" + Session.get("customerDecorateInfo").cType + '/customerId/' + Session.get("customerDecorateInfo").cId);
        }
    },
    'click .contractInfo': function () {
        //Router.go("/contractlist");
        Router.go("/ContractListType");
    },
    'click .hejiadan': function () {
        Router.go("/hejiadan");
    },
    'click .ChangeOrder': function () {
        Router.go("/ChangeOrder");
    },
    'click .jiesuan': function () {
        //todo 结算
        Router.go("/paymentReminder");
    },
    'click .evaluation': function () {
        Router.go("/evaluationPage");
    },
    'click .shoppingcart': function () {
        Session.set("shoppingbackTemplate", "mycustomer");
        if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
            Router.go("/shopping/" + Session.get("customerLogin"));
        } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], "user-group")) {
            Router.go("/shopping/" + Session.get("customerDecorateInfo").ACNumber);
        }
    },
    'click .CsolutionsList': function () {
        if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
            Router.go("/CsolutionsList/" + Session.get("customerLogin"));
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "ConstructionCaptain", "StoreManager", 'test', "ConstructionCaptainBoss"], "user-group")) {
            Router.go("/CsolutionsList/" + Session.get("customerDecorateInfo").ACNumber);
        }
    },
    'click .complaintsList': function () {
        Router.go("/complaintsList");
    },

    'click #js-logout': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });

            }
        });
    },
    'click .MessageList': function () {
        Router.go("/messageInfo");
    },
    'click .constructionList': function () {
        Router.go("/systemMessage");
    }
});


