/**
 * Created by zhangxuhui on 15/10/23.
 */
Template.customerindex.rendered = function () {
    //todo 客户首页,代码需优化
    this.subscribe("publis_user", Meteor.userId(), function () {
        var info = Meteor.users.findOne({_id: Meteor.userId()});
        Session.set("customerLogin", info.clientId);//ACNumber
        Meteor.subscribe("CustomerID", info.clientId);
    });
    this.subscribe("newsInfo", function () {
    });
    //imagesByIds
    this.subscribe("AdvertisementPicinfo");

    Session.set("Customerlogin", 'customerindex');
    Session.set("classifyback", "customerindex");
    Session.set("designerorcustomerback", "customer");
    Session.set("shoppingcartbacktemplate", "mycustomer");
    Session.set("backtemplate", "customerindex");
    //this.autorun(function () {
    //    //获得用户ACNumber
    //    Meteor.subscribe("publis_user", Meteor.userId(), function () {
    //        var info = Meteor.users.findOne({_id: Meteor.userId()});
    //        //console.log(info.clientId)
    //        Session.set("customerAcNumber", info.clientId);//ACNumber
    //    });
    //    console.log(typeof (Session.get("customerAcNumber")));
    //    //获得客户的类型
    //    var customerID = Session.get("customerAcNumber");
    //    Meteor.subscribe("currentCurrentCType", customerID, function () {
    //        var currentCType = DesignerRelationClient.findOne({cId: customerID});
    //        console.log(currentCType);
    //        Session.set("currentCustomeCType", currentCType);
    //        if (currentCType) {
    //            if (currentCType.cType == '已签' || currentCType.cType == '已归档') {
    //                $(".ion-search").css("color", "#969696");
    //            }
    //        }
    //
    //    });
    //    //获得客户的基本信息，确定装修模式
    //Meteor.subscribe("CustomerID", customerID, function () {
    //    var CustInfo = Cust_Info.findOne({ACNumber: customerID});
    //    Session.set("CurrentCustomerInfo", CustInfo);
    //});
    //    //获得用户的方案信息
    //    Meteor.subscribe("shoppingcart", customerID, "shoppingcart", function () {
    //            var info = Solutions.findOne({acnumber: customerID, solutionType: "shoppingcart"});
    //            if (info) {
    //                Session.set("currentCustomerSolutionInfo", info);
    //            }
    //        }
    //    );
    //    Meteor.subscribe("newsInfo", function () {
    //    })
    //    Meteor.subscribe("AdvertisementPic", function () {
    //        console.log(AdvertisementPic.findOne())
    //    });
    //});
    //this.subscribe("images", function () {
    //});

    // this.subscribe("CustomerLogingetCustomerInfo", Meteor.userId(), function () {
    // });

};

Template.customerindex.helpers({
    headObj: function () {
        return {}
    },
    newmessage: function () {
        var info = newsInfo.find();
        return info;

    },
    titleName: function () {
        var customerInfo = Cust_Info.findOne({ACNumber: Session.get("customerLogin")});
        if (customerInfo) {
            return customerInfo.ACNumber + " " + customerInfo.CustName + " 欢迎您！"
        }
    },
    //滚动图片显示
    'AdvertisementPicList': function () {
        var AdvertisementPicInfo = AdvertisementPic.findOne();
        return AdvertisementPicInfo;
    },
    images: function (id) {

        var imageInfo = OtherImages.find({_id: id});
        return imageInfo;
    },
    isimage: function () {
        console.log();
        var MaterialListInfo = AdvertisementPic.findOne();
        if (MaterialListInfo && MaterialListInfo.picture) {
            return true;
        } else {
            return false;
        }
    },
    images1: function () {
        var materialInfo = AdvertisementPic.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("images1:", materialInfo.picture[0]);
            if (materialInfo.picture[0]) {
                return materialInfo.picture[0];
            } else {
                return false;
            }
        }
    }, images2: function () {
        var materialInfo = AdvertisementPic.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("images2:", materialInfo.picture[1]);
            if (materialInfo.picture[1]) {
                return materialInfo.picture[1];
            } else {
                return false;
            }
        }
    }, images3: function () {
        var materialInfo = AdvertisementPic.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("images3:", materialInfo.picture[2]);
            if (materialInfo.picture[2]) {
                return materialInfo.picture[2];
            } else {
                return false;
            }
        }
    }, images4: function () {
        var materialInfo = AdvertisementPic.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("images5:", materialInfo.picture[3]);
            if (materialInfo.picture[3]) {
                return materialInfo.picture[3];
            } else {
                return false;
            }
        }
    }, images5: function () {
        var materialInfo = AdvertisementPic.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("images5:", materialInfo.picture[4]);
            if (materialInfo.picture[4]) {
                return materialInfo.picture[4];
            } else {
                return false;
            }
        }
    }, images6: function () {
        var materialInfo = AdvertisementPic.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("images6:", materialInfo.picture[5]);
            if (materialInfo.picture[5]) {
                return materialInfo.picture[5];
            } else {
                return false;
            }
        }
    },
});

Template.customerindex.events({
    'click .goNewsMessageDetail': function (event) {
        var currentNewId = $(event.currentTarget).children("h3").attr("id").trim();
        Router.go("/newsList/" + currentNewId);
    }
});