/**
 * Created by pcbeta on 15-8-5.
 */
Template.projDetail.rendered = function () {
    //Session.set("classifyback", "projDetail");
    //
    ////根据方案的ID订阅数据
    //this.subscribe("selectedSolution", Session.get("solutionListID"));
    //this.subscribe("currentCurrentCType", Session.get("customerAcNumber"), function () {
    //
    //});
    //console.log("solutionsId:", Session.get("solutionListID"));
    //this.subscribe("images");


    this.subscribe("meterialDetailBySolutionid", Router.current().params._id, function (err, result) {
        var solutionsInfo = Solutions.findOne({_id: Router.current().params._id});
        var materialIds = [];
        _.each(solutionsInfo.selectMaterial, function (value) {
            materialIds.push(parseInt(value.materialID));
        });
        Meteor.call("getMaterialNameByIDs", materialIds, function (err, result) {
            console.log("result:", result);
            Session.set("materialNameByIds", result);
        })
    });

};

Template.projDetail.helpers({
    headObj: function () {
        return {
            leftButton: true,
            title: Solutions.findOne().solutionName,
            //backTemplate: 'customerList'
        }
    },
    //获取数据的列表
    'projectDeataiList': function () {
        var info = Solutions.findOne();
        console.log(info);
        if (info && info.selectMaterial) {
            return info.selectMaterial;
        }
    },
    'getMaterialName': function (mId) {
        var materialInfo = Session.get("materialNameByIds");
        if (mId && materialInfo) {
            return materialInfo[mId];
        }
    },

    'currentCustomerState': function () {
        var cType = Router.current().params.cType;
        if (cType == "已签" || cType == "已归档") {
            return false
        } else {
            return true;
        }
    }
});

Template.projDetail.events({
    'click .solutionListDel': function (event) {
        if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {

            var _id = Router.current().params._id;
        console.log(_id);
        var MaterialId = $(event.currentTarget).attr("id");
        console.log(MaterialId);
        Meteor.call("delSolutionMaterial", _id, MaterialId, function (err) {
            if (!err) {
                IonLoading.show({
                    customTemplate: "<h4>删除材料成功!</h4>",
                    duration: 1000
                });
            } else {
                IonLoading.show({
                    customTemplate: "<h4>删除材料失败!</h4>",
                    duration: 1000
                });
            }
        });}
        else{
            IonLoading.show({
                customTemplate: "<h4>您没有权限操作!</h4>",
                duration: 1000
            });
        }
    },
    "click .add_Material": function (event) {
        //新增材料
        if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {

            var currentSolution = Router.current().params._id;
        if (currentSolution) {
            var Info = Session.get("customerDecorateInfo");
            Info.sId = currentSolution;
            Session.set("customerDecorateInfo", Info);
        }
        Router.go("/DecorateArea");}else {
            IonLoading.show({
                customTemplate: "<h4>您没有权限操作!</h4>",
                duration: 1000
            });
        }
    },
    'click .saveButton': function () {
        //Router.go("/customerList")
        //customerDetail/customerType/已安排/customerId/3Dc7KrreTsoka5geX
        Router.go("/customerDetail/customerType/" + Session.get("customerDecorateInfo").cType + "/customerId/" + Session.get("customerDecorateInfo").cId);
    },
    'click .solutionList .item .nextDetailPage': function (event) {
        var currentMaterialId = $(event.currentTarget).children("h2").attr("id");
        var materialOrderType = $(event.currentTarget).children("input").attr("class");
        if (!materialOrderType) {
            materialOrderType = 0;
        }
        var currentSolutionId = Router.current().params._id;
        var customerType = Router.current().params.cType;
        Router.go("/updateProjDetail/solutionId/" + currentSolutionId + "/materialId/" + currentMaterialId + '/OrderType/' + materialOrderType + '/customerType/' + customerType);
    },
    'click .backMenuList': function () {
        Router.go("/customerDetail/customerType/" + Session.get("customerDecorateInfo").cType + '/customerId/' + Session.get("customerDecorateInfo").cId);
    }
});