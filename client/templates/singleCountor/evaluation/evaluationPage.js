/**
 * Created by zhangxuhui on 15/10/29.
 */
Template.evaluationPage.rendered = function () {
    var ACNumber;

    if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
        ACNumber = Session.get("customerDecorateInfo").ACNumber;
    } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
        ACNumber = Session.get("customerLogin");
    }
    this.subscribe("publishDesignerId", ACNumber, function () {
        Meteor.call("designerNames", ACNumber, function (err, result) {
            Session.set("designerNameInfo", result);
        });
    });
    this.subscribe("publishConstrationId", ACNumber, function () {
        Meteor.call("ConstrationNames", ACNumber, function (err, result) {
            console.log(result);
            Session.set("ConstrationNameInfo", result);
        });
    });
    this.autorun(function () {
        Meteor.subscribe("Evaluation", ACNumber, function () {
            var info = Evaluation.findOne({acnumber: ACNumber});
            if (info) {
                $(".designer").val(info.designerScore);
                Session.set("designerscole", info.designerScore);
                $(".shigongduizhang").val(info.constructionCaptainScore);
                Session.set("shigongduizhangscole", info.constructionCaptainScore);
                $(".evaluationContent").val(info.content);
            }
        });
    });


};

Template.evaluationPage.helpers({
    'shigongduizhangName': function () {
        return Session.get("ConstrationNameInfo");
    },
    headObj: function () {
        return {
            title: '评价',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    },
    'shigongduizhangCount01': function () {
        var scoreInfo = Session.get("shigongduizhangscole");
        if (scoreInfo == 1) {
            return true;
        }
    }, 'shigongduizhangCount02': function () {
        var scoreInfo = Session.get("shigongduizhangscole");
        if (scoreInfo == 2) {
            return true;
        }
    }, 'shigongduizhangCount03': function () {
        var scoreInfo = Session.get("shigongduizhangscole");
        if (scoreInfo == 3) {
            return true;
        }
    }, 'shigongduizhangCount04': function () {
        var scoreInfo = Session.get("shigongduizhangscole");
        if (scoreInfo == 4) {
            return true;
        }
    }, 'shigongduizhangCount05': function () {
        var scoreInfo = Session.get("shigongduizhangscole");
        if (scoreInfo) {
            if (scoreInfo == 5) {
                return true;
            }
        } else {
            return true;
        }
    }, 'designer01': function () {
        var scoreInfo = Session.get("designerscole");
        if (scoreInfo == 1) {
            return true;
        }
    }, 'designer02': function () {
        var scoreInfo = Session.get("designerscole");
        if (scoreInfo == 2) {
            return true;
        }
    }, 'designer03': function () {
        var scoreInfo = Session.get("designerscole");
        if (scoreInfo == 3) {
            return true;
        }
    }, 'designer04': function () {
        var scoreInfo = Session.get("designerscole");
        if (scoreInfo == 4) {
            return true;
        }
    }, 'designer05': function () {
        var scoreInfo = Session.get("designerscole");
        if (scoreInfo) {
            if (scoreInfo == 5) {
                return true;
            }
        } else {
            return true;
        }
    },
    "submitdisable": function () {
        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        var info = Evaluation.findOne({acnumber: ACNumber});

        if (info) {
            return false
        } else {
            return true;
        }
    },
    'evaluationInfo': function () {
        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        var info = Evaluation.findOne({acnumber: ACNumber});
        if (info) {
            $(".designer").options.add(info.designerScore);
        }
    },
    'issubmitButton': function () {
        if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
            return true;
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            return false;
        }
    },
    'designerName': function () {
        return Session.get("designerNameInfo");
    },


});

Template.evaluationPage.events({
    'change .shigongduizhang': function (event) {
        console.log($(event.currentTarget).val());
        Session.set("shigongduizhangscole", $(event.currentTarget).val());
    },
    'change .designer': function (event) {
        console.log($(event.currentTarget).val());
        Session.set("designerscole", $(event.currentTarget).val());
    },
    'click .evaluationSubmit': function (event) {
        var textContent = $(".evaluationContent").val().trim();
        console.log(textContent);
        var designerScore = Session.get("designerscole");
        var shigongduizhang = Session.get("shigongduizhangscole");
        if (!designerScore) {
            designerScore = 5
        }
        if (!shigongduizhang) {
            shigongduizhang = 5
        }
        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        var Designer = DesignerRelationClient.findOne({cId: ACNumber});
        var construtionInfo = construtionRelationClient.findOne({cId: ACNumber});
        if (!Designer) {
            IonLoading.show({
                customTemplate: "<h4>您还没有分配设计师,暂时不能评价~</h4>",
                duration: 1000
            });
            return;
        }
        if (!construtionInfo) {
            IonLoading.show({
                customTemplate: "<h4>您还没有分配施工队长,暂时不能评价~</h4>",
                duration: 1000
            });
            return;
        }


        //todo 施工队长没有加入
        Meteor.call("insertevaluation", ACNumber, Designer.dId, designerScore, construtionInfo.construtionId, shigongduizhang, textContent, function (err, result) {
            if (err) {
                IonLoading.show({
                    customTemplate: "<h4>评价失败!</h4>",
                    duration: 1000
                });

                console.log("评价失败，")
            } else {
                IonLoading.show({
                    customTemplate: "<h4>评价成功!</h4>",
                    duration: 1000
                });

                console.log("评价成功")
            }
        });

    },
    'click .Have_evaluation': function () {
        IonLoading.show({
            customTemplate: "<h4>已经评价过了，谢谢配合!</h4>",
            duration: 1000
        });

    }
});

//
//Schemas = {};
//Schemas.evaluationInfo = new SimpleSchema({
//    "designerScore": {
//        type: String, label: "给设计师评分:", optional: true, autoform: {
//            type: "select",
//            options: function () {
//                return [
//                    {label: "1分", value: "1"},
//                    {label: "2分", value: "2"},
//                    {label: "3分", value: "3"},
//                    {label: "4分", value: '4'},
//                    {label: "5分", value: "5"},
//                ];
//            }
//        }
//    }, "designerConter": {
//        type: String, label: "对设计师的评价:", optional: true,autoform:{type:text,rows:5}
//    },
//    'constructionCaptainScore': {
//        type: String, label: "给施工队长评分:", optional: true, autoform: {
//            type: "select",
//            options: function () {
//                return [
//                    {label: "1分", value: "1"},
//                    {label: "2分", value: "2"},
//                    {label: "3分", value: "3"},
//                    {label: "4分", value: '4'},
//                    {label: "5分", value: "5"},
//                ];
//            }
//        }
//    },
//    'constructionCaptainContent':{
//        type: String, label: "对设计师的评价:", optional: true,autoform:{type:text,rows:5}
//    }
//});
//Evaluation.attachSchema(
//    Schemas.evaluationInfo
//);
