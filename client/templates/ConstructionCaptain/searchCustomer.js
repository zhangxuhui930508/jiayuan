/**
 * Created by zhangxuhui on 16/4/20.
 */
Template.searchCustomer.rendered = function () {
    Session.set("currentCustomerSearchVal", null);
    IonLoading.show();
    this.subscribe("publishSearchCustomer", Session.get("currentConstructionId"), parseInt(Router.current().params.type), function () {
        IonLoading.hide();
    });

};

Template.searchCustomer.helpers({
    headObj: function () {
        return {
            leftButton: true,
            backTemplate: 'constructionCType'
        }
    },
    titleName: function () {
        var type = Router.current().params.type;
        var info = AC_Personnel.findOne();
        if (info && info.pName) {
            if (type == "1") {
                return info.pName + "的未质检工地";
            } else if (type == "3") {
                return info.pName + "的整改单工地"
            }
        }
    },
    construtionListInfo: function () {
        var info = Session.get("currentCustomerSearchVal");
        var select = {};
        if (info) {
            select.ACNumber = new RegExp(info, "i");
            select.CustName = new RegExp(info, "i");
        }
        return Cust_Info.find(select, {sort: {CreatTime: -1}});
    },
    'checkType': function () {
        var type = Router.current().params.type;
        if (type == 1) {
            return true;
        } else {
            return false;
        }
    },
    'checkProjectInfo': function (acnumber) {
        var info = Session.get("currentCustomerSearchVal");
        var select = {};
        if (info) {
            select.acnumber = new RegExp(info, "i");
        }
        return checkConstructionProject.find(select, {sort: {jumpTheQueue: -1, updateDate: 1}});
    },
    'customerCustNameInfo': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    'customerId': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info._id;
        }
    },
    'customerAddressInfo': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    },
    'checkReasultInfo': function (reasult) {
        if (reasult == 1) {
            return "待质检"
        } else if (reasult == 2) {
            return '质检通过'
        } else if (reasult == "A") {
            return "整改单A"
        } else if (reasult == "C") {
            return "协调单"
        } else if (reasult == "B") {
            return "整改单B"

        }
    },

    checkProject: function (value) {
        if (value) {
            switch (value) {
                case "completion":
                    return "竣工报检";
                    break;
                case "hydropower":
                    return "水电报检";
                    break;
                case "mudWood":
                    return "泥木报检";
                    break;
            }
        }
    },
    "overdueDate": function (id, date) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info) {
            if (info.jumpTheQueue) {
                return "#FFFF00";
            }
            if (info.checkDate < new Date().Format("yyyy-MM-dd")) {
                return "red"
            }
            if (info && !info.qcIsCheck) {
                return "#8C8C8C	"
            }
        }
    }, 'getCustomerMode': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info && info.BusiType == "整体翻新") {
            return "#0000CD"
        } else if (info && info.BusiType == "局部翻新") {
            return "#000000";
        }
    },


});

Template.searchCustomer.events({
    'click .gocustomerDetail': function (event) {
        var cid = $(event.currentTarget).children("h2").attr("id").trim();
        var type = Router.current().params.type;
        console.log(this._id);
        console.log(cid);
        Router.go("/customerDetail/customerType/" + type + '/customerId/' + cid);
    }, 'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();

        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
        console.log("-------------------------------------");
    },


})

Template.searchCustomer.onDestroyed(function () {
    Session.set("currentCustomerSearchVal", null);
})