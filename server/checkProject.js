/**
 * Created by zhangxuhui on 16/4/15.
 */
Meteor.methods({
    removeEditWorkSiteRepairInfo: function (id, errcode) {
        if (!id || !errcode) {
            throw new Meteor.Error("Incomplete information");
        }

        var reasonInfo = correctiveReason.findOne({_id: id});
        if (reasonInfo && reasonInfo.reason) {

            var arr = reasonInfo.reason;
            if (arr.length == 1) {
                correctiveReason.remove({_id: id});
                QcTemporary.remove({reasonId: id});
            } else {
                var reasonArr = [];
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].errorCode != errcode) {
                        var json = {};
                        json.area = arr[i].area;
                        json.supplementrea = arr[i].supplementrea;
                        json.errorCode = arr[i].errorCode;
                        json.picture = arr[i].picture;
                        reasonArr.push(json);
                    }
                }
                correctiveReason.update({_id: id}, {$set: {reason: reasonArr}})
            }
        }

    },
    /**
     * 创建报检
     * @param acnumber 受理编号
     * @param project 报检项目
     * @param sender 报检人
     */

    'createInspection': function (acnumber, project, send, ConstructionId) {
        if (!acnumber || !project) {
            throw new Meteor.Error("Incomplete information");
        }
        var assigner;
        var employeeInfo = AC_Personnel.find({pBranch2: {$nin: [null]}}).fetch();
        var custInfo = Cust_Info.findOne({ACNumber: acnumber});
        var assignerID;
        var status = 1;
        var InspectionPeople = this.userId;
        // console.log("===============employeeInfo==============================")
        // console.log(employeeInfo)
        // console.log("=============================================")
        //currentConstructionId
        var sender;
        console.log("ConstructionId:", ConstructionId);
        if (Roles.userIsInRole(this.userId, ["ConstructionCaptainBoss"], "user-group")) {
            // console.log("==============================")
            // console.log(Meteor.users.findOne({ID: parseInt(ConstructionId)}))
            // console.log("==============================")
            Meteor.users.find({ID: parseInt(ConstructionId)}).forEach(function (info) {
                // console.log(info);
                if (Roles.userIsInRole(info._id, ["ConstructionCaptain"], "user-group")) {
                    // console.log("--------------------------------")
                    sender = info._id;
                }
            });
        } else {
            sender = this.userId;
        }

        _.each(employeeInfo, function (info) {
            if (info.ID == 219 || info.ID == 333 || info.ID == 331 || info.ID == 332) {
                Meteor.users.find({ID: info.ID}).forEach(function (userinfo1) {
                    if (Roles.userIsInRole(userinfo1._id, ["qc", "qcManager"], "user-group")) {
                        var pBranch2Info = autoAssigner.bitConversion(info.pBranch2);
                        if (pBranch2Info) {
                            _.each(pBranch2Info, function (branchInfo) {
                                if (branchInfo == custInfo.Branch) {
                                    status = 2;
                                    assigner = userinfo1._id;
                                }
                            })
                        }
                    }
                });
            }
        });

        var info = checkConstructionProject.findOne({acnumber: acnumber, project: project});
        if (info) {
            checkConstructionProject.update({acnumber: acnumber, project: project}, {
                $set: {
                    status: status,
                    checkResult: 1,
                    sender: sender,
                    assigner: assigner,
                    createDate: new Date(),
                    checkDate: moment().add(3, "days").format('YYYY-MM-DD'),
                    bossCheck: 0,
                    checkDateInspection: moment().add(3, "days").format('YYYY-MM-DD'),
                    inspectionPeople: InspectionPeople,
                    isFinishCheck: false,
                    isDeleted: false

                }
            });
        } else {
            checkConstructionProject.insert({
                acnumber: acnumber,
                project: project,
                sender: sender,
                status: status,
                checkResult: 1,
                createDate: new Date(),
                assigner: assigner,
                checkDate: moment().add(3, "days").format('YYYY-MM-DD'),
                qcIsCheck: false,
                bossCheck: 0,
                checkDateInspection: moment().add(3, "days").format('YYYY-MM-DD'),
                inspectionPeople: InspectionPeople,
                isFinishCheck: false,
                isDeleted: false
            })

        }
        var messageInfo;
        switch (project) {
            case "completion":
                messageInfo = "竣工报检";
                break;
            case "hydropower":
                messageInfo = "水电报检";
                break;
            case "mudWood":
                messageInfo = "泥木报检";
                break;
        }
        var userInfo = Meteor.users.findOne({_id: sender});
        // console.log(userInfo, "userInfo")
        if (userInfo) {
            var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
            // console.log(employeeInfo, "employeeInfo")
            //
            if (employeeInfo) {
                systemMessage.insert({
                    acnumber: acnumber,
                    message: "施工队长" + employeeInfo.pName + "提交了" + messageInfo + "!",
                    date: new Date(),
                    state: "unread",
                    userInfo: {
                        name: '施工队长', icon: "/image/designer.jpg"
                    }
                })
            }
        }
    },
    'qcIsCheckInfo': function (id) {
        checkConstructionProject.update({_id: id}, {$set: {qcIsCheck: true}})
    },
    /**
     * 姜婷婷确认报检
     * @param id
     */
    bossCheckQcInfo: function (id) {
        console.log("=====================", id)
        if (!id) {
            throw new Meteor.Error("ID is null")
        }
        checkConstructionProject.update({_id: id}, {$set: {bossCheck: 1, bossCheckDate: new Date()}});
        var info = checkConstructionProject.findOne({_id: id})
        systemMessage.insert({
            acnumber: info.acnumber,
            message: "施工主管蒋婷婷审核通过了施工队长的报检信息,已提交质检部门",
            date: new Date(),
            state: "unread",
            userInfo: {
                name: '施工队长', icon: "/image/designer.jpg"
            }
        })
    },
    /**
     * 取消报检
     * @param acnumber 受理编号
     * @param project 取消报检项目
     */
    'cancelInspection': function (acnumber, project, sender) {
        if (!acnumber || !project) {
            throw new Meteor.Error("Incomplete information");
        }
        var info = checkConstructionProject.findOne({acnumber: acnumber, project: project});
        if (info) {
            checkConstructionProject.update({_id: info._id}, {$set: {isDeleted: true}});

            var messageInfo;
            switch (project) {
                case "completion":
                    messageInfo = "竣工报检";
                    break;
                case "hydropower":
                    messageInfo = "水电报检";
                    break;
                case "mudWood":
                    messageInfo = "泥木报检";
                    break;
            }
            var userInfo = Meteor.users.findOne({_id: sender});
            if (userInfo) {
                var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
                if (employeeInfo) {
                    systemMessage.insert({
                        acnumber: acnumber,
                        message: "施工队长" + employeeInfo.pName + "取消了" + messageInfo + "!",
                        date: new Date(),
                        state: "unread",
                        userInfo: {
                            name: '施工队长', icon: "/image/designer.jpg"
                        }
                    })
                }
            }

        } else {
            throw new Meteor.Error("This item does not exist");
        }
    },
    //姜婷婷有插队的权限
    'jumpTheQueueRole': function (acnumber, project, sender) {
        if (!acnumber || !project || !sender) {
            throw new Meteor.Error("info is null!")
        }
        var info = checkConstructionProject.findOne({acnumber: acnumber, project: project});
        if (info) {
            return checkConstructionProject.update({_id: info._id}, {$set: {jumpTheQueue: true, operator: this.userId}})
        } else {
            return false;
        }
    },
    /**
     * 分配质检人员
     * @param checkId 检查ID
     * @param assigner 分配人
     * @param date 分配日期
     */
    'assignQCInfo': function (checkId, assigner, date, uid) {
        var userInfo;
        userInfo = Meteor.users.findOne({
            ID: parseInt(assigner), "roles": {
                "user-group": [
                    "qc"
                ]
            }
        });
        if (!userInfo) {
            userInfo = Meteor.users.findOne({
                ID: parseInt(assigner), "roles": {
                    "user-group": [
                        "qcManager"
                    ]
                }
            });
        }
        if (userInfo) {
            console.log(userInfo, "userInfo");
            checkConstructionProject.update({_id: checkId}, {
                $set: {
                    status: 2,
                    assigner: userInfo._id,
                    checkDate: date,
                    updateDate: new Date()
                }
            });
            var info = checkConstructionProject.findOne({_id: checkId});

            var qcManager = Meteor.users.findOne({_id: uid});
            if (qcManager) {
                var employeeInfo = AC_Personnel.findOne({ID: qcManager.ID});
                var assign = AC_Personnel.findOne({ID: parseInt(assigner)});
                if (employeeInfo && assign) {
                    systemMessage.insert({
                        acnumber: info.acnumber,
                        message: "质检主管安排" + employeeInfo.pName + "安排了质检人员" + assign.pName + "在" + date + "进行质量检验!",
                        date: new Date(),
                        state: "unread",
                        userInfo: {
                            name: '质检人员', icon: "/image/designer.jpg"
                        }
                    })
                }
            }


        } else {
            throw new Meteor.Error("user is null");
        }
    },
    /**
     *
     * @param acnumberArr
     * @returns {{}}
     */
    'getCustInfo': function (acnumberArr) {
        var returncust = {};
        console.log(acnumberArr);
        _.each(acnumberArr, function (info) {
            var custinfo = Cust_Info.findOne({ACNumber: info});
            if (custinfo) {
                var value = {};
                value.CustName = custinfo.CustName;
                value.Address = custinfo.Address;
                value.Mobile = custinfo.Mobile;
                returncust[info] = value;
            }
        });
        console.log(returncust);
        return returncust;
    },
    /**
     * 更新检验日期
     * @param id
     * @param newDate
     */
    'updateCheckDate': function (id, newDate, sender) {
        if (!id) {
            throw new Meteor.Error("id is null")
        }
        checkConstructionProject.update({_id: id}, {$set: {checkDate: newDate, updateDate: new Date(newDate)}});
        var info = checkConstructionProject.findOne({_id: id});
        var userInfo = Meteor.users.findOne({_id: sender});
        if (userInfo) {
            var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
            if (employeeInfo) {
                systemMessage.insert({
                    acnumber: info.acnumber,
                    message: "质检人员" + employeeInfo.pName + "修改了上门质检时间为" + newDate + "!",
                    date: new Date(),
                    state: "unread",
                    userInfo: {
                        name: '质检人员', icon: "/image/designer.jpg"
                    }
                })
            }
        }
    },
    /**
     * 更新检验工地为合格
     * @param id
     */
    'updateCheckResultInspection': function (id, sender) {
        if (!id) {
            throw new Meteor.Error("id is null")
        }
        var info = checkConstructionProject.findOne({_id: id});
        if (info) {
            checkConstructionProject.update({_id: id}, {
                $set: {
                    checkResult: 2,
                    checkDateInspection: new Date(),
                    updateDate: new Date(),
                    checkDate: moment().format('YYYY-MM-DD')
                }
            })
            var messageInfo;
            var project = info.project;
            switch (project) {
                case "completion":
                    messageInfo = "竣工报检";
                    break;
                case "hydropower":
                    messageInfo = "水电报检";
                    break;
                case "mudWood":
                    messageInfo = "泥木报检";
                    break;
            }

            var userInfo = Meteor.users.findOne({_id: sender});
            if (userInfo) {
                var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
                if (employeeInfo) {
                    systemMessage.insert({
                        acnumber: info.acnumber,
                        message: "质检人员" + employeeInfo.pName + "检验" + messageInfo + "结果为合格!",
                        date: new Date(),
                        state: "unread",
                        userInfo: {
                            name: '质检人员', icon: "/image/designer.jpg"
                        }
                    })
                }
            }

        }

    },
    /**
     *
     * @param checkId
     * @param repairId
     * @param acnumber
     * @constructor
     */
    'IssueRectificationList': function (checkId, acnumber, sender) {
        if (!checkId || !acnumber) {
            throw new Meteor.Error("Incomplete information");
        }
        var info = checkConstructionProject.findOne({_id: checkId});
        // var repairInfo = correctiveReason.findOne({_id: repairId});
        var repairIdArr = [];
        QcTemporary.find({checkId: checkId}).forEach(function (info) {
            repairIdArr.push(info.reasonId);
        });

        if (info) {
            checkConstructionProject.update({_id: checkId}, {
                $set: {
                    checkResult: 3,
                    updateDate: new Date(),
                    checkDateInspection: new Date(),
                    reasonID: repairIdArr,
                    checkDate: moment().format('YYYY-MM-DD'),
                    isFinishCheck: true
                }
            });

            correctiveReason.find({checkId: checkId}).forEach(function (repairInfo) {
                console.log("==========================================")
                if (repairInfo && repairInfo.reason) {
                    var reasonCount = repairInfo.reason.length;
                    for (var i = 0; i < reasonCount; i++) {
                        CheckResultPunchListsToSQL.insert({
                            checkId: checkId,
                            reasonid: repairInfo._id,
                            acnumber: acnumber,
                            repairType: repairInfo.repairType,
                            area: repairInfo.reason[i].area,
                            supplementrea: repairInfo.reason[i].supplementrea,
                            errorCode1: repairInfo.reason[i].errorCode1,
                            errorCode2: repairInfo.reason[i].errorCode2,
                            createAt: new Date(),
                            isFinishCheck: true
                        })
                    }
                }
            });


            var messageInfo;
            var project = info.project;
            switch (project) {
                case "completion":
                    messageInfo = "竣工报检";
                    break;
                case "hydropower":
                    messageInfo = "水电报检";
                    break;
                case "mudWood":
                    messageInfo = "泥木报检";
                    break;
            }

            var userInfo = Meteor.users.findOne({_id: sender});
            if (userInfo) {
                var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
                if (employeeInfo) {
                    systemMessage.insert({
                        acnumber: acnumber,
                        message: "质检人员" + employeeInfo.pName + "检验" + messageInfo + "正在检验中……",
                        date: new Date(),
                        state: "unread",
                        userInfo: {
                            name: '质检人员', icon: "/image/designer.jpg"
                        }
                    })
                }
            }

            QcTemporary.remove({checkId: checkId});
        }
    }
});

var autoAssigner = {
    'assignQC': function (acnumber) {
        var employeeInfo = AC_Personnel.find({pBranch2: {$nin: [null]}}).fetch();

        var custInfo = Cust_Info.findOne({ACNumber: acnumber});

        _.each(employeeInfo, function (info) {
            if (info) {
                console.log("----------------------checkProject-------------")
                console.log(info);
                if (info._id) {
                    var userInfo = Meteor.users.findOne({ID: info._id});
                    console.log("-----------------------------------------------------------------")
                    console.log(userInfo);
                    console.log("-----------------------------------------------------------------")
                    if (Roles.userIsInRole(userInfo._id, ["qc"], "user-group")) {
                        var pBranch2Info = autoAssigner.bitConversion(info.pBranch2);
                        if (pBranch2Info) {
                            _.each(pBranch2Info, function (info2) {
                                if (info2 == custInfo.Branch) {
                                    console.log("******************info**************************");
                                    console.log(info.ID);

                                    return info.ID;
                                }
                            })
                        }
                    }
                }
            }
        });
    },
    'bitConversion': function (pBreach) {
        if (!(pBreach instanceof Number)) {
            pBreach = Number(pBreach)
        }
        var pbBit = pBreach.toString(2);
        var arr = [];
        _.each(_.range(pbBit.length), function (n) {
            // console.log("======", n, "=======");
            // console.log(pbBit.substring(n, n + 1));
            if (pbBit.substring(n, n + 1) !== "0") {
                // console.log("pow:", Math.pow(2, pbBit.length - n - 1));
                arr.push(Math.pow(2, pbBit.length - n - 1));
            }
        });
// console.log("arr:", arr);
        return arr;
    },
    'getServerDate': function () {
        var date = new Date();
        //console.log(date);
        return date;
    },

};