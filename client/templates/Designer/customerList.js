/**
 * Created by pcbeta on 15-8-4.
 *
 */
Template.customerList.onRendered(function () {
    Session.set("currentCustomerSearchVal", null);
    IonLoading.show();
    var designerId = Router.current().params.dId;
    var customerType = Router.current().params.type;
    this.subscribe("designerByCustomerType", designerId, customerType, function (err, result) {
        IonLoading.hide();
    });
});

Template.customerList.helpers({
    headObj: function () {
        return {
            title: '客户列表',
            leftButton: true,
            backTemplate: 'customerType'
        }
    },
    'Info': function () {
        var info = Session.get("currentCustomerSearchVal");
        var select = {};
        if (info) {
            select = {$or: [{ACNumber: new RegExp(info, "i")}, {CustName: new RegExp(info, "i")}, {Address: new RegExp(info, "i")}]};
        }
        var customerInfo = Cust_Info.find(select, {sort: {CreatTime: -1}});
        if (customerInfo) {
            return customerInfo;
        }
    }
});

Template.customerList.events({
    'click .item .gocustomerDetail': function (event) {
        var cid = $(event.currentTarget).children("h2").attr("id").trim();
        var ctype = Router.current().params.type;
        Router.go("/customerDetail/customerType/" + ctype + '/customerId/' + cid);
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    }
});

Template.customerList.onDestroyed(function () {
    Session.set("currentCustomerSearchVal", null);
});