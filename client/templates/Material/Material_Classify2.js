/**
 * Created by pcbeta on 15-8-10.
 */

Template.MaterialClassify2.rendered = function () {
    Session.set("NAV_two", null);
    Session.set("Nav_third", null);
    Session.set("backtemplate", "MaterialClassify2");
    Session.set('customerMaterialListCountListKey', {});

    var info = Session.get("first_level");
    var infoList = info.classList;
    if (infoList.length > 0) {
        var arr = [];
        _.each(infoList, function (t) {
            arr.push(t.ID)
        });

        Meteor.call("materialListCountList", arr, function (err, result) {
            if (!err) {
                console.log(result);
                Session.set("customerMaterialListCountListKey", result);
            }
        })
    }
};

Template.MaterialClassify2.helpers({
    'headObj': function () {
        return {
            title: '材料分类',
        }
    },
    // Session.get("first_level");表示经过第一级筛选后的数据
    'MaterialClassifyList': function () {
        var info = Session.get("first_level");
        if (info) {
            return info.classList;
        }
    },

    'oneupper_level': function () {
        var item = Router.current().params.mClassifyName;
        if (item) {
            return item;
        }
    },
    'titleName': function () {
        var parts = "材料分类";
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        if (Session.get("customerDecorateInfo")) {
            parts = Session.get("customerDecorateInfo").parts;
        } else {
            parts = "材料分类";
        }
        return parts;
    },
    getnextMaterialID: function (id, classList) {
        try {
            if (classList.length == 0) {
                var json = Session.get("customerMaterialListCountListKey");
                if (id && json[id]) {
                    return json[id];
                } else {
                    return "0";
                }
            } else {
                return classList.length;
            }
        } catch (e) {
            return "0";
        }
    }
});


Template.MaterialClassify2.events({
    'input .search1, change .search1': function (event) {
        var searchName = $(event.currentTarget).val();
        console.log(searchName);
        Session.set("searchVal2", searchName);
    },
    'click .materClassify2 .item': function (event) {
        var itemName = $(event.currentTarget).children("h2").text().trim();
        var id = $(event.currentTarget).children("i").text().trim();

        var classifyName = Session.get("classifyName");
        classifyName.two = itemName;
        Session.set("classifyName", classifyName);

        var info = Session.get("first_level");
        var getMaterialCount = $(event.currentTarget).children("span").children("span").text();
        if (getMaterialCount == "（ 0 ）") {
            IonLoading.show({
                customTemplate: "<h4>没有此类材料，请选择其他材料!</h4>",
                duration: 500
            });

            return;
        }

        var infolist = info.classList;
        var nextlistInfo;
        for (var i = 0; i < infolist.length; i++) {
            if (infolist[i].分类名称 == itemName) {
                if (infolist[i].classList.length == 0) {
                    Router.go("/materialList/" + id);
                } else {
                    nextlistInfo = infolist[i].classList;
                    Session.set("second_level", nextlistInfo);
                    var classify1 = Router.current().params.mClassifyName;
                    Router.go("/MaterialClassify3/Mclassify1/" + classify1 + "/Mclassify2/" + itemName);
                }
            }
        }

    },
    'click .upper_classify': function (event) {
        Router.go("/MaterialClassify");
    },
    'click .DecorateArea': function () {
        Router.go("/DecorateArea");
    },
    'click .backMenuList': function () {
        Router.go("/MaterialClassify");

    }
});

Template.MaterialClassify2.destroy = function () {
    Session.set("customerMaterialListCountListKey", {});
};