/**
 * Created by zhangxuhui on 16/4/17.
 */
Template.assignQC.rendered = function () {
    Session.set("currentCustomerSearchVal", null);

}
Template.assignQC.helpers({
    headObj: function () {
        return {
            title: '分配质检人员'
        }
    },
    Info: function () {
        var select = {};
        if (Session.get("currentCustomerSearchVal")) {
            select.acnumber = new RegExp(Session.get("currentCustomerSearchVal"), "i");
        }
        console.log(checkConstructionProject.find(select).fetch());
        return checkConstructionProject.find(select, {sort: {jumpTheQueue: -1, updateDate: 1}});
    },
    getCustInfoCustName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    getCustInfoAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    },
    getStoreInfo: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info && info.Branch) {
            var acSetInfo = acSet.findOne({ACNumb: info.Branch});
            if (acSetInfo) {
                return acSetInfo.ACName;
            }
        }
    },
    'constructionInfo': function (sender) {
        var userInfo = Meteor.users.findOne({_id: sender});
        if (userInfo) {
            var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
            if (employeeInfo) {
                return employeeInfo.pName;
            }
        }
    },
    'getCustomerMode': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info && info.BusiType == "整体翻新") {
            return "#0000CD"
        } else if (info && info.BusiType == "局部翻新") {
            return "#000000";
        }
    },
    'jumpTheQueueinfo': function (id) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info && info.jumpTheQueue) {
            return "red"
        }
    },
    "overdueDate": function (id, date) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info) {
            if (info.jumpTheQueue) {
                return "#FFFF00";
            }
            if (info.checkDate < new Date().Format("yyyy-MM-dd")) {
                return "red"
            }
            if (info && !info.qcIsCheck) {
                return "#8C8C8C	"
            }
        }
    },
    'checkProject': function (value) {
        if (value) {
            switch (value) {
                case "completion":
                    return "竣工报检";
                    break;
                case "hydropower":
                    return "水电报检";
                    break;
                case "mudWood":
                    return "泥木报检";
                    break;
            }
        }
    },
    'isCheckInfo': function (id) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info && !info.qcIsCheck) {
            return "#D9D9D9	"
        }
    }
});

Template.assignQC.events({
    'click .gocustomerDetail': function (event) {
        var id = $(event.currentTarget).children("h2").attr('class').trim();
        Router.go("/assignList/" + id);

    }, 'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/qualityLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    },
});