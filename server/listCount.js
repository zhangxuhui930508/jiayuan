/**
 * Created by zhangxuhui on 15/11/12.
 */
Meteor.methods({
    'MaterialListCount': function (ID) {
        var MateriaLDetailInfo = MaterialDetailList.find({分类ID: ID}).count();
        console.log("materialCount:", MateriaLDetailInfo);
        return MateriaLDetailInfo;
    },
    /**
     * 统计材料表中材料的数量材料分类
     * @param classdArr
     * @returns {{}}
     */
        materialListCountList: function (classdArr) {
        console.log(classdArr);
        var materJson = {};
        _.each(classdArr, function (c) {
            var count = MaterialDetailList.find({分类ID: c, 是否可用: true, 结束日期: {$gt: new Date()}}).count();
            materJson[c] = count;
        });
        return materJson;
    },

    //-------------materialClassify02------------------------------------


    /**
     * 标准材料
     * @param acnumber
     * @returns {*}
     */
    erpBudgetMaterial: function (acnumber) {
        var count = ERP_Budget_Material.find({受理编号: acnumber, 定单分类: 2}).count();
        var rationCount = erpBudgetRation.find({受理编号: acnumber}).count();
        return Number(count) + Number(rationCount);
    },
    'duizhangCount': function (acnumber) {
        return ERP_Budget_Material.find({受理编号: acnumber, 定单分类: 1}).count();
    },
    /**
     * 定制材料统计
     * @param acnumber
     * @returns {number}
     * @constructor
     */
    ERPProductOrder: function (acnumber) {
        var count1 = parseFloat(ERP_Product_Order01.find({受理编号: acnumber}).count());
        var count2 = parseFloat(ERP_Product_Order02.find({受理编号: acnumber}).count());
        var count3 = parseFloat(ERP_Product_Order03.find({受理编号: acnumber}).count());
        var count4 = parseFloat(ERP_Product_Order04.find({受理编号: acnumber}).count());
        var count5 = parseFloat(ERP_Product_Order05.find({受理编号: acnumber}).count());
        var count6 = parseFloat(ERP_Product_Order06.find({受理编号: acnumber}).count());
        return count1 + count2 + count3 + count4 + count5 + count6;
    },
    /**
     * 根据材料的ID获得材料的名称
     * @param materialIds
     * @returns {{}}
     */
    'getMaterialNameByIDs': function (materialIds) {
        var materialJson = {};
        _.each(materialIds, function (value) {
            var MaterialInfo = MaterialDetailList.findOne({ID: value});
            materialJson[value] = MaterialInfo.材料名称;
        });
        return materialJson;
    },

    'designerNames': function (acnumber) {
        var custInfo = DesignerRelationClient.findOne({cId: acnumber});
        if (custInfo) {
            var designer = AC_Personnel.findOne({ID: parseInt(custInfo.dId)});
            if (designer && designer.pName) {
                return designer.pName;
            }
        }
    },

    'ConstrationNames': function (acnumber) {
        var custInfo = construtionRelationClient.findOne({cId: acnumber});

        if (custInfo) {
            var designer = AC_Personnel.findOne({ID: parseInt(custInfo.construtionId)});
            console.log("------------ConstrationNames--------------------------------------")
            console.log(designer);
            console.log("--------------------------------------------------")

            if (designer && designer.pName) {
                return designer.pName;
            }
        }
    },

    /**
     *
     * @param dId
     * @param cType
     * @returns {*|{}}
     */
    'getDesignerRelationClientCount': function (dId, cType) {
        if (dId) {
            var count = DesignerRelationClient.find({dId: dId.toString(), cType: cType}).count();
            return count;
        }
    },


    /**
     *根据受理编号获得部位名称
     * @param ACNumber
     * @returns {{}}
     */
    'getPartsInfo': function (ACNumber) {
        var Ration = erpBudgetRation.find({受理编号: ACNumber}).fetch();
        var material = ERP_Budget_Material.find({受理编号: ACNumber}).fetch();
        var arr = [];
        var json = {};
        _.each(Ration, function (RationInfo) {
            arr.push(RationInfo.部位号)
        });
        _.each(material, function (materialInfo) {
            arr.push(materialInfo.部位号)
        });

        //console.log(arr);

        _.each(arr, function (arrinfo) {
            var location = erpBudgetLocation.findOne({受理编号: ACNumber, 部位号: arrinfo});
            json[arrinfo] = location.部位名称;
        });
        return json;
    },

    /**
     * 施工队长客户数量统计
     *
     * @param constructionId
     * @param type
     * @returns {*}
     */
    'getConstructionListCount': function (constructionId, type) {
        //var consotructionInfo = construtionRelationClient.find({construtionId: constructionId }).fetch();
        //var count;
        //if (consotructionInfo) {
        //    var select = {};
        //    var arr = [];
        //    _.each(consotructionInfo, function (info) {
        //        if (type == "未开工") {
        //            select.ACNumber = info.cId;
        //            select.startTime = {"$gt": new Date()};
        //        } else if (type == "已开工") {
        //            select.ACNumber = info.cId;
        //            select.startTime = {$gte: new Date()};
        //            select.endTime = {$lt: new Date()};
        //        } else if (type == "已完成") {
        //            select.ACNumber = info.cId;
        //            select.endTime = {$lt: new Date(), $gte: new Date("2016/01/01")};
        //        }
        //
        //        var custInfo = Cust_Info.find(select).fetch();
        //        if (custInfo.length > 0) {
        //            arr.push(custInfo)
        //        }
        //    });
        //    return arr.length;
        //}
        if (constructionId) {
            var count = construtionRelationClient.find({construtionId: constructionId, cType: type}).count();

            console.log(count);
            return count;
        }
    },

//    借款类型已发放和未发放数量统计
    'borrowingTypeCount': function (id, type) {

        var info = construtionRelationClient.find({construtionId: id}).fetch();
        var count;
        var arr = [];
        _.each(info, function (typeinfo) {
            var select = {};
            select.申请日期 = {$gte: new Date("2016/01/01")};
            select.受理编号 = typeinfo.cId;
            select.财务审核 = type;
            var erpProjectLoaninfo = erpProjectLoan.findOne(select);
            if (erpProjectLoaninfo) {
                arr.push(erpProjectLoaninfo);
            }
        });
        return arr.length;

        //var consotructionInfo = construtionRelationClient.find({construtionId: id}).fetch();
        //
        //if (consotructionInfo) {
        //    var select = {};
        //    var count;
        //    var arr = [];
        //    _.each(consotructionInfo, function (info) {
        //        select.申请日期 = {$gt: new Date("2016/01/01")};
        //        select.受理编号 = info.cId;
        //        select.财务审核 = type;
        //
        //        count = erpProjectLoan.find(select).count();
        //        //if (projectInfo.length > 0) {
        //        //    arr.push(projectInfo)
        //        //}
        //
        //    });
        //    return count;
        //
        //}
    },
    /**
     * 工程费类型 已发放和未发放数量统计
     * @param id
     * @param type
     * @returns {Number}
     * @constructor
     */
    'ConstrectionTypeCount': function (id, type) {
        var info = construtionRelationClient.find({construtionId: id}).fetch();
        var count;
        var arr = [];
        _.each(info, function (typeinfo) {
            var select = {};
            select.工程部审核日期 = {$gte: new Date("2016/01/01")};
            select.受理编号 = typeinfo.cId;
            select.工程部审核 = type;
            var erpProjectLoaninfo = erpCostAccountingPlan.findOne(select);
            if (erpProjectLoaninfo) {
                arr.push(erpProjectLoaninfo);
            }
        });
        return arr.length;
    },
    /**
     * todo 有问题
     * @param constructionId
     * @returns {{}}
     */
    'getConstructionCount': function (constructionId) {
        var id;
        if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain"], "user-group")) {
            var info = Meteor.users.findOne({_id: this.userId});
            id = info.ID.toString();
        } else {
            id = constructionId.toString()

        }
        // var info = Meteor.users.findOne({_id: this.userId});
        // if (info) {
        // var constructionId=AC_Personnel.findOne({ID:info.ID}).ID;
        console.log("----------", id);
        var cIdArr = [];
        var constructionInfo = construtionRelationClient.find({construtionId: id}).fetch();
        if (constructionInfo) {
            _.each(constructionInfo, function (info) {
                cIdArr.push(info.cId);
            });
            var returnJosn = {};
            var notServiceCount = 0;
            var ServiceCount = 0;
            var finishCount = 0;
            _.each(cIdArr, function (cIdInfo) {
                var select = {};
                select.ACNumber = cIdInfo;
                var info = Cust_Info.findOne(select);
                if (info && info.startTime) {
                    if (info.startTime > new Date()) {
                        notServiceCount = notServiceCount + 1;
                    }
                }
            });
            _.each(cIdArr, function (cIdInfo) {
                var select = {};
                select.ACNumber = cIdInfo;

                var info = Cust_Info.findOne(select);

                if (info && info.startTime && info.endTime) {
                    if (info.startTime <= new Date() && info.endTime > new Date()) {
                        ServiceCount = ServiceCount + 1;
                    }
                }
            });

            _.each(cIdArr, function (cIdInfo) {
                var select = {};
                select.ACNumber = cIdInfo;
                var info = Cust_Info.findOne(select);
                if (info && info.startTime && info.endTime) {
                    if (info.endTime <= new Date() && info.endTime > new Date("2016-01-01")) {
                        finishCount = finishCount + 1;
                    }
                }
            });
            console.log("================================")
            console.log(notServiceCount, ServiceCount, finishCount);
            console.log("================================")
            returnJosn.notServiceCount = notServiceCount;
            returnJosn.ServiceCount = ServiceCount;
            returnJosn.finishCount = finishCount;

            return returnJosn;
            // }
        }
    },
    "materialDeptConstructionListCount": function () {
        var userInfo = Meteor.users.findOne({_id: this.userId});
        if (userInfo) {
            var employee = AC_Personnel.findOne({ID: userInfo.ID});
            if (employee && employee.pName) {
                console.log(employee.pName);
                console.log("*****************************")
                var store;

                // switch (employee.pName) {
                //     case  "潘国荣":
                //         store = "浦东";
                //         break;
                //     case  "俞希凡":
                //         store = "旗舰";
                //         break;
                //     case  "归瑜佳":
                //         store = "西区";
                //         break;
                //     case  "孔建松":
                //         store = "北区";
                //         break;
                // }
                // var acSetInfo = acSet.findOne({"ACType": "营业网点", "ACName": store});
                console.log(employee);
                if (employee && employee.pBranch2) {
                    console.log(bitConversion(employee.pBranch2))
                    var notWork = Cust_Info.find({
                        Branch: {$in: bitConversion(employee.pBranch2)},
                        startTime: {$gt: new Date()}
                    }).count();


                    var work = Cust_Info.find({
                        Branch: {$in: bitConversion(employee.pBranch2)},
                        startTime: {$lte: new Date()},
                        endTime: {$gt: new Date()}
                    }).count();

                    var finish = Cust_Info.find({
                        Branch: {$in: bitConversion(employee.pBranch2)},
                        endTime: {$lte: new Date(), $gte: new Date("2016-01-01")}
                    }).count();

                    var count = {};
                    count.notWork = notWork;
                    count.work = work;
                    count.finish = finish;
                    return count;
                }
            }
        }
    }
});


function bitConversion(pBreach) {
    if (!(pBreach instanceof Number)) {
        pBreach = Number(pBreach)
    }
    var pbBit = pBreach.toString(2);
    var arr = [];
    _.each(_.range(pbBit.length), function (n) {
        if (pbBit.substring(n, n + 1) !== "0") {
            arr.push(Math.pow(2, pbBit.length - n - 1));
        }
    });
    return arr;
}