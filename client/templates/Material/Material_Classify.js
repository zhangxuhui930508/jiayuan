/**
 * Created by pcbeta on 15-8-7.
 */


Template.MaterialClassify.rendered = function () {
    Session.set("backtemplate", "MaterialClassify1");
    this.subscribe("MaterialClassifyList");
};
Template.MaterialClassify.helpers({
    'headObj': function () {
        return {
            title: "材料分类",
            //leftButton: true,
            //backTemplate: 'modeSelect'
        }
    },
    'MaterialClassifyList': function () {
        var classListInfo = Material_Classify_List.find();
        if (classListInfo) {
            return classListInfo;
        }
    },
    titleName: function () {
        var parts = "材料分类";
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        if (Session.get("customerDecorateInfo")) {
            parts = Session.get("customerDecorateInfo").parts;
        } else {
            parts = "材料分类";
        }
        return parts;
    },
    'materialCount': function (classList) {
        if (!classList || !_.isArray(classList)) {
            return '0';
        }
        return classList.length;
    }
});

Template.MaterialClassify.events({
    'input .search, change .search': function (event) {
        var searchName = $(event.currentTarget).val();
        Session.set("searchVal", searchName);
    },
    'click .materClasify1 .item': function (event) {
        var itemName = $(event.currentTarget).children("h2").text().trim();

        var classifyName = {};
        classifyName.one = itemName;
        Session.set("classifyName", classifyName);


        var id = $(event.currentTarget).children("i").text().trim();
        //Session.set("FC_classification", itemName);//第一级分类的名称
        var info = Material_Classify_List.findOne({分类名称: itemName});
        console.log("========第一级筛选后的数据,为第二级准备数据========", info);
        Session.set("first_level", info); //经过第一级分类后筛选出来的数据；

        var length = info.classList.length;
        console.log(length + '=======第一级分类后的长度==============', length);
        if (length == 0) {
            //Session.set("MaterialListID", id);
            Router.go("/materialList/" + id);
        } else {
            Router.go("/MaterialClassify2/" + itemName);
        }
    },
    'click .backMenuList': function () {
        //todo 从分类页面返回
        console.log("=======================")
        var back = Session.get("classifyback");
        switch (back) {
            case "customerindex":
                Router.go("/customerindex");
                break;
            case "mycustomer":
                Router.go("/mycustomer");
                break;
            case "CSolutionDetail":
                Router.go("/CSolutionDetail");
                break;
            case "projDetail":
                Router.go("/projDetail");
                break;
            case "DecorateArea":
            {
                Session.set("backtemplate", null);
                Router.go("/DecorateArea");
                break
            }
            case "customerDetail":
            {
                Router.go("/customerDetail/" + Session.get("currentCustomerID"));
                break;
            }
        }
    },
    'click .DecorateArea': function () {
        Router.go("/DecorateArea");
    },

});
