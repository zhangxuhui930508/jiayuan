/**
 * Created by zhangxuhui on 16/5/12.
 */

Template.materialDeptConstructionType.onRendered(function () {
    IonLoading.show({
        duration: 1500
    })
});


Template.materialDeptConstructionType.helpers({
    headObj: function () {
        return {
            title: '客户工地类型'
        }
    },
    "notWorkCount": function () {
        if (Session.get("materialDeptConstructionInfoCount") != null) {
            return Session.get("materialDeptConstructionInfoCount").notWork;
        } else {
            return "0"
        }
    },
    "WorkCount": function () {
        if (Session.get("materialDeptConstructionInfoCount") != null) {
            return Session.get("materialDeptConstructionInfoCount").work;
        } else {
            return "0";
        }
    },
    'CompletedCount': function () {
        if (Session.get("materialDeptConstructionInfoCount") != null) {
            return Session.get("materialDeptConstructionInfoCount").finish;
        } else {
            return "0";
        }
    }

});

Template.materialDeptConstructionType.events({
    'click .item': function (event) {
        Router.go("/materialDeptConstructionList/" + $(event.currentTarget).children("span").attr("class").trim());
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/materialDeptLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
});