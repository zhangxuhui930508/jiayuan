/**
 * Created by zhangxuhui on 16/6/25.
 */

Template.countDetail.onRendered(function () {
    var id = Router.current().params.id;
    var t;
    if (id == "arr") {
        t = Session.get("supplierTotalCount").arr;
    } else if (id == "arr2") {
        t = Session.get("supplierTotalCount").arr2;
    }
    this.subscribe("publishSupplierCountDetail", t);
})


Template.countDetail.helpers({
    materialInfo: function () {
        return ERP_Budget_Material.find();
    },
    getCustomerName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName
        }
    },
    getCustomerAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info)return info.Address;
    },
    headObj: function () {
        return {
            title: '统计详情',
            leftButton: true,
            backTemplate: 'supplierCount'
        }
    },
});
