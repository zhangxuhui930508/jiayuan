/**
 * Created by zhangxuhui on 15/11/10.
 */
var MaterialListCountListKey = "MaterialListCountKey";
Template.materialClassify02.rendered = function () {
    Session.set("customerSelectMaterial", "materialClassify02");
    //Session.set("materialListbacktemplate", "materialClassify02");
    Session.set(MaterialListCountListKey, {});
    this.autorun(function () {
        var mark = parseInt(Session.get("materialClassifyMark"));
        Meteor.subscribe("customerClassify", mark, function () {
            //console.log(CustomerMaterialclassify.find().fetch());
            var custClassifyInfo = CustomerMaterialclassify.find().fetch();
            var arr = [];
            for (var key in custClassifyInfo) {
                arr.push(custClassifyInfo[key].ID);
            }

            Meteor.call("materialListCountList", arr, function (err, result) {
                if (!err) {
                    console.log(result);
                    Session.set(MaterialListCountListKey, result);
                }
            })
        });
    });
};

Template.materialClassify02.helpers({
    'headObj': function () {
        return {}
    },
    titleName: function () {
        var parts = "材料分类";
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        if (Session.get("customerDecorateInfo")) {
            parts = Session.get("customerDecorateInfo").parts;
        } else {
            parts = "材料分类";
        }
        return parts;
    },
    materialListInfo: function () {
        var mark = parseInt(Session.get("materialClassifyMark"));
        var info = CustomerMaterialclassify.find({移动标志: mark});
        return info;
    },
    'nextPageMaterialInfoCout': function (id) {
        try {
            var countList = Session.get(MaterialListCountListKey);
            return countList[id];
        } catch (e) {
            return '0';
        }
    }
});

Template.materialClassify02.events({
    'click .backTemplates': function () {
        Router.go("materialClassify01");
    },
    'click .customerGoMaterialList': function (event) {
        var getMaterialCount = $(event.currentTarget).children("span").children("span").text();

        if (getMaterialCount == "（ 0 ）") {
            IonLoading.show({
                customTemplate: "<h4>没有此类材料，请选择其他材料!</h4>",
                duration: 1000
            });
            return;
        }
        var classifyId = $(event.currentTarget).children("span").attr("id");
        console.log(classifyId, "classifyId");
        Router.go("/materialList/" + classifyId);
    },
    'click .DecorateArea': function () {
        Router.go("/customerParts")
    }
});

Template.materialClassify02.onDestroyed(function () {
    Session.set(MaterialListCountListKey, {});
});