/**
 * Created by zhangxuhui on 15/10/14.
 */
Template.DecorateArea.rendered = function () {
    Session.set("classifyback", "DecorateArea");
};

Template.DecorateArea.helpers({
    'headObj': function () {
        return {
            title: '选择部位后选材',
        }
    },
    'Selectparts': function () {
        return [
            {parts: "主卧"},
            {parts: "次卧"},
            {parts: "客卧"},
            {parts: "主卫"},
            {parts: "次卫"},
            {parts: "厨房"},
            {parts: "厨房（二扇门）"},
            {parts: "阳台"},
            {parts: "工作阳台"},
            {parts: "进户门"},
            {parts: "保姆房"},
            {parts: "主卧阳台"},
            {parts: "客卧阳台"},
            {parts: "客厅阳台"},
            {parts: "客厅"},
            {parts: "餐厅"},
            {parts: "过道"},
            {parts: "书房"},
            {parts: "衣帽间"},
            {parts: "老人房"},
            {parts: "储藏间"},
            {parts: "盥洗间"}
        ]
    },
});

Template.DecorateArea.events({
    'click .item': function (event) {
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        if (customerDecorateInfo) {
            var parts = $(event.currentTarget).text().trim();
            customerDecorateInfo.parts = parts;
            Session.set("customerDecorateInfo", customerDecorateInfo);
        }
        //Session.set("Decoration_parts", parts);

        if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager"], "user-group")) {
            var backtemplate = Session.get("backtemplate");
            switch (backtemplate) {
                case "MaterialClassify1":
                    Router.go("/MaterialClassify");
                    break;
                case "MaterialClassify2":
                    Router.go("/MaterialClassify2/" + Session.get("classifyName").one);
                    break;
                case "MaterialClassify3":
                    Router.go("/MaterialClassify3/Mclassify1/" + Session.get("classifyName").one + "/Mclassify2/" + Session.get("classifyName").two);
                    break;
                case "MaterialClassify4":
                    Router.go("/MaterialClassify4/Mclassify1/" + Session.get("classifyName").one + '/Mclassify2/' + Session.get("classifyName").two + "/Mclassify3/" + Session.get("classifyName").three);
                    break;
                default:
                    Router.go("/MaterialClassify");
                    break;
            }
        }
    },
    'click .backTemplates': function (event) {
        //customerDetail/customerType/已安排/customerId/24192
        Router.go("/customerDetail/customerType/" + Session.get("customerDecorateInfo").cType + "/customerId/" + Session.get("customerDecorateInfo").cId)
    }
});