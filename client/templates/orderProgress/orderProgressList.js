/**
 * Created by zhangxuhui on 16/7/11.
 */

Template.orderProgressList.onRendered(function () {
    this.subscribe("publishSupplierImages");

    var groupArr = ["measure", "nuclearPrice", "prepareGoods", "deliverGoods", "install", "sendGoods"];
    var roleArr = Roles.getRolesForUser(Meteor.userId(), 'supplier');
    var groupRole = [];
    _.each(roleArr, function (info) {
        if (_.contains(groupArr, info)) {
            groupRole.push(info + "1");
            groupRole.push(info + "2");
        } else {
            groupRole.push(info);
        }
    });

    this.autorun(function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info) {
            var index = 0;
            if (info.orderState) {
                var currentOrder = json[info.orderState];
                // console.log("--------------------------------------")
                // console.log(currentOrder);
                // console.log("--------------------------------------")
                if (currentOrder) {
                    index = _.indexOf(groupRole, currentOrder);
                }
                if (index < groupRole.length - 1) {
                    index++;
                }
            }
            Session.set("orderStatusButton", roleJosn[groupRole[index]])
            Session.set("orderStatusButton1", divJson[groupRole[index]])
        }
    });

});

var json = {
    1: "measure1",
    2: "measure2",
    3: "nuclearPrice1",
    4: "nuclearPrice2",
    5: "nuclearPrice1",
    6: "prepareGoods1",
    7: "prepareGoods2",
    8: "deliverGoods1",
    9: "deliverGoods2",
    10: "install1",
    14: "install2",
    11: "sendGoods1",
    12: "sendGoods1",
    13: "sendGoods2",
};
var divJson = {
    "measure1": ["measure1"],
    "measure2": ["measure1"],
    "nuclearPrice1": ["nuclearPrice1"],
    "nuclearPrice2": ["nuclearPrice1"],
    "prepareGoods1": ["prepareGoods1"],
    "prepareGoods2": ["prepareGoods1"],
    "deliverGoods1": ["deliverGoods1"],
    "deliverGoods2": ["deliverGoods2", "deliverGoods1"],
    "install1": ["install1", "install2"],
    "install2": ["install1", "install2"],
    "sendGoods1": ["sendGoods1", "sendGoods2"],
    "sendGoods2": ["sendGoods2"],
};

var roleJosn = {
    "measure1": ["measure1"],
    "measure2": ["measure2"],
    "nuclearPrice1": ["nuclearPrice1"],
    "nuclearPrice2": ["nuclearPrice1"],
    "prepareGoods1": ["prepareGoods1"],
    "prepareGoods2": ["prepareGoods2"],
    "deliverGoods1": ["deliverGoods1",],
    "deliverGoods2": ["deliverGoods2"],
    "install1": ["install1", "install2"],
    "install2": ["install1", "install2"],
    "sendGoods1": ["sendGoods1", "sendGoods2"],
    "sendGoods2": ["sendGoods2"],
    // "sendGoods3": ["sendGoods3"],
};


Template.orderProgressList.helpers({
    headObj: function () {
        return {
            // title: '进度信息',
            leftButton: true,
            // backTemplate: 'StandardMaterialDetail'
        }
    },

    isLock: function (project) {
        var info = erpViewVerifyOrderAll.findOne({});
        if (info) {
            if (info.lockProject == project)
                return "lock.png";
            else
                return "unlock.png";
        }
    },

    titleName: function () {
        return "供应商整体进度查询";
    },
    buttonDisabled: function (status) {
        var buttonInfo = Session.get("orderStatusButton");
        return !(buttonInfo && _.contains(buttonInfo, json[status]));
    },
    divIsDisplay: function (status) {
        var buttonInfo = Session.get("orderStatusButton1");
        if (!(buttonInfo && _.contains(buttonInfo, json[status]))) {
            return "none";
        } else {

            return "";
        }
    },
    "putInStorageInfo": function () {
        var erpinfo = erpViewVerifyOrderAll.findOne();
        if (erpinfo && erpinfo.orderState == 12) {
            return true
        } else {
            return false
        }
    },
    'corePriceButton1': function (status) {
        var erpinfo = erpViewVerifyOrderAll.findOne();
        return erpinfo && (erpinfo.orderState == 3 || erpinfo.orderState == 5)

    },

    //送货
    'materialInfo': function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info) {
            return info;
        }
    },
    //测量按钮状态
    'measureInfo': function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info && info.orderState) {
            if (info.orderState >= 2) {
                return false;
            }
        }
        return true;
    },
    //确认测量
    'measureStatus': function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info && info.orderState) {
            if (info.orderState >= 1) {
                return false;
            }
        }
        return true;
    },
    //日期格式化
    dateFormat: function (date) {
        try {
            return date.Format("yyyy-MM-dd");
        } catch (e) {

        }
    },
    //上门测试时间是否存在
    'measureDate': function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info && info.measureDate) {
            return true;
        }
    },
    //材料信息
    info: function () {
        return erpViewVerifyOrderAll.findOne({});
    },
    //测量确认
    'MeasureInfo1': function (type) {
        var info = erpViewVerifyOrderAll.findOne();
        if (info && info.finishMeasureDate) {
            return true;
        } else {
            Session.set("controlButton", true);
            return false;
        }
    },

//备货按钮状态
    'prepareGoodsButton': function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info && info.beihuoDate) {
            return false;
        }
        return true;
    }
    ,
//送货按钮状态
    'deliverGoodsButton': function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info) {
            if (info.deliveryDate) {
                return false;
            } else {
                return true;
            }
        }
    }
    ,
//安装按钮状态
    'installStatus': function () {
        var info = erpViewVerifyOrderAll.findOne();
        if (info && info.insetalDate) {
            return false;
        }
        return true;
    }
    ,
//入库单类型
    supplierType: function () {
        var info = erpInfoSupplier.findOne();
        console.log(info);

        if (info && info.putInStorageType) {
            console.log(info);
            if (info.putInStorageType == "whole") {
                return true;
            }
        }
    },

    isputInStorage: function (value) {

        return true;

    },
    erpduizhang: function () {
        var info = ERP_Budget_Material.findOne();
        if (info && info.定单分类 == 1) {
            return false;
        }
        return true;
    }


});

Template.orderProgressList.events({
    'click .skipButton': function () {
        IonPopup.prompt({
            title: '温馨提示',
            template: '请输入登录密码',
            okText: '确定',
            cancelText: '取消',
            inputType: 'text',
            inputPlaceholder: '输入登录密码',
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = Router.current().params.orderNo;
                var pwd = $('.popup-body input').val().trim();

                Meteor.call("skipOrderMeasure", acnumber, orderNo, pwd, function (err, result) {
                    console.log(err, result);
                    if (err) {
                        IonLoading.show({
                            customTemplate: "<h4>密码错误,请检查密码!</h4>",
                            duration: 1000
                        });
                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>跳过测量成功!</h4>",
                            duration: 1000
                        });
                        Meteor.setTimeout(function () {
                            history.go(-2);
                        }, 1500)
                    }
                })
            }
        });
    },
    'click .ditto': function (event) {
        IonPopup.confirm({
            title: '温馨提示',
            template: "确认送货和安装同时进行吗?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = parseInt(Router.current().params.orderId);
                Meteor.call("updateOrderStatus", acnumber, orderNo, 14, function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>操作成功!</h4>",
                            duration: 1000
                        })
                        Meteor.setTimeout(function () {
                            history.go(-2);
                        }, 1500)

                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>操作失败!</h4>",
                            duration: 1000
                        })
                        Meteor.setTimeout(function () {
                            history.go(-2);
                        }, 1500)
                    }
                });
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    //提交测量
    'click .MeasureConfirm': function (event) {
        var timeDate = $(".toMeasureTime").val();
        if (!timeDate) {
            IonLoading.show({
                customTemplate: "<h4>上门测量时间不能为空!</h4>",
                duration: 1000
            });
        } else {
            var acnumber = Router.current().params.acnumber;
            var orderNo = parseInt(Router.current().params.orderId);
            Meteor.call("updateOrderStatus", acnumber, orderNo, 1, timeDate, "lockMeasure", function (err, result) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>提交测量成功!</h4>",
                        duration: 1000
                    });
                    Meteor.setTimeout(function () {
                        history.go(-2);
                    }, 1500)
                } else {
                    console.log(err);
                }
            })
        }
    },
    //完成测量
    'click .Measurefinish': function (event) {
        IonPopup.confirm({
            title: '温馨提示',
            template: "确定测量已经完成?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = parseInt(Router.current().params.orderId);
                Meteor.call("updateOrderStatus", acnumber, orderNo, 2, new Date(), function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>完成测量工作!</h4>",
                            duration: 1000
                        });
                        Meteor.setTimeout(function () {
                            history.go(-2);
                        }, 1500)

                    } else {
                        console.log(err);
                    }
                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    //设置备货天数
    'click .Stocking': function (event) {
        var date = $(".stockingDate option:selected").val();
        if (!date) {
            IonLoading.show({
                customTemplate: "<h4>请选择备货天数</h4>",
                duration: 1000
            });
        } else {
            var acnumber = Router.current().params.acnumber;
            var orderNo = parseInt(Router.current().params.orderId);

            Meteor.call("updateOrderStatus", acnumber, orderNo, 6, date, "lockStockUp", function (err, result) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>备货中……</h4>",
                        duration: 1000
                    });
                    Meteor.setTimeout(function () {
                        history.go(-2);
                    }, 1500)
                } else {
                    // alert(err.message)
                }
            })
        }
    },
    // 送货
    'click .deliverGoods': function (event) {
        var date = $(".deliverGoodsDate").val();
        if (!date) {
            IonLoading.show({
                customTemplate: "<h4>请选择送货日期!</h4>",
                duration: 1000
            });
        } else {
            var acnumber = Router.current().params.acnumber;
            var orderNo = parseInt(Router.current().params.orderId);
            Meteor.call("updateOrderStatus", acnumber, orderNo, 8, date, 'lockDeliverGoods', function (err, result) {
                if (!err)
                    Meteor.setTimeout(function () {
                        history.go(-2);
                    }, 1500)
            })
        }
    },
    //完成备货
    'click .finishStocking': function (event) {
        var id = Router.current().params.mId;
        IonPopup.confirm({
            title: '温馨提示',
            template: "备货完成?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = parseInt(Router.current().params.orderId);
                Meteor.call("updateOrderStatus", acnumber, orderNo, 7, new Date(), function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>备货完成!</h4>",
                            duration: 1000
                        });
                        Meteor.setTimeout(function () {
                            history.go(-2);
                        }, 1500)
                    }
                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    }
    ,
    'click .finishfdeliverGoods': function (event) {
        var id = Router.current().params.mId;
        IonPopup.confirm({
            title: '温馨提示',
            template: "确定送货已经完成?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = parseInt(Router.current().params.orderId);
                Meteor.call("updateOrderStatus", acnumber, orderNo, 9, new Date(), function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>完成送货工作!</h4>",
                            duration: 1000
                        });
                        Meteor.setTimeout(function () {
                            history.go(-2);
                        }, 1500)
                    }
                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .install': function (event) {
        var installDate = $(".installDate").val();
        if (!installDate) {
            IonLoading.show({
                customTemplate: "<h4>请设置安装日期!</h4>",
                duration: 1000
            });
        } else {
            var acnumber = Router.current().params.acnumber;
            var orderNo = parseInt(Router.current().params.orderId);
            Meteor.call("updateOrderStatus", acnumber, orderNo, 10, installDate, function (err, result) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>安装时间设置成功!</h4>",
                        duration: 1000
                    });
                    Meteor.setTimeout(function () {
                        history.go(-2);
                    }, 1500)
                }
            })
        }
    },
    'click .finishInstall': function () {
        var id = Router.current().params.mId;
        IonPopup.confirm({
            title: '温馨提示',
            template: "确定安装已经完成?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = parseInt(Router.current().params.orderId);
                Meteor.call("updateOrderStatus", acnumber, orderNo, 14, new Date(), function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>完成安装工作!</h4>",
                            duration: 1000
                        });
                        Meteor.setTimeout(function () {
                            history.go(-2);
                        }, 1500)
                    }
                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click .backMenuList': function () {
        history.back();
    },
    'click .measureUnlock': function (event) {
        IonPopup.confirm({
            title: '温馨提示',
            template: "确认解除测量的锁定?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = Router.current().params.orderId;
                Meteor.call("measureOrderUnlock", acnumber, orderNo, function (err, result) {
                    console.log(err, result);
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>操作成功!</h4>",
                            duration: 1000
                        });
                        // Meteor.setTimeout(function () {
                        //     history.back();
                        // }, 1500)
                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>操作失败!</h4>",
                            duration: 1000
                        })
                    }
                });
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });


    },
    'click .beihuoDatemeasureUnlock': function () {
        IonPopup.confirm({
            title: '温馨提示',
            template: "确认解除备货的锁定?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = Router.current().params.orderId;
                Meteor.call("beihuoOrderDatemeasureUnlock", acnumber, orderNo, function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>操作成功!</h4>",
                            duration: 1000
                        });
                        // Meteor.setTimeout(function () {
                        //     history.back();
                        // }, 1500)
                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>操作失败!</h4>",
                            duration: 1000
                        })
                    }
                });
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });

    },
    'click .deliverGoodsUnlock': function () {
        IonPopup.confirm({
            title: '温馨提示',
            template: "确认解除送货的锁定?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                var acnumber = Router.current().params.acnumber;
                var orderNo = Router.current().params.orderId;
                Meteor.call("deliverGoodsOrderUnlock", acnumber, orderNo, function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>操作成功!</h4>",
                            duration: 1000
                        });
                        // Meteor.setTimeout(function () {
                        //     history.back();
                        // }, 1500)
                    } else {
                        IonLoading.show({
                            customTemplate: "<h4>操作失败!</h4>",
                            duration: 1000
                        })
                    }
                });
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    }
});
//==============================================

Schemas = {};
Schemas.corePriceOrder = new SimpleSchema({
    "corePrice": {
        type: [String],
        label: '上传核价文件',
        // fix: it's optional
        optional: true
    },
    "corePrice.$": {
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'SupplierImages',
                accept: 'image/*',
                label: '上传文件'
            }
        }
    },
});
erpViewVerifyOrderAll.attachSchema(Schemas.corePriceOrder);

Schemas.installOrder = new SimpleSchema({
    "installFile": {
        type: [String],
        label: '上传安装单文件',
        // fix: it's optional
        optional: true
    },
    "installFile.$": {
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'SupplierImages',
                accept: 'image/*',
                label: '上传文件'
            }
        }
    },
});
erpViewVerifyOrderAll.attachSchema(Schemas.installOrder);

Schemas.putInStorageOrder = new SimpleSchema({

    "putInStorageFile": {
        type: [String],
        label: '上传入库单文件',
        // fix: it's optional
        optional: true
    },
    "putInStorageFile.$": {
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'SupplierImages',
                accept: 'image/*',
                label: '上传文件'
            }
        }
    },
    'putInStoragePrice': {
        label: "入库单价",
        type: String,
        optional: true,
        autoform: {
            afFieldInput: {
                type: "text"
            }
        }
    },
    putInStorageNumber: {
        label: "入库数量",
        type: String,
        optional: true,
        autoform: {
            afFieldInput: {
                type: "text"
            }
        }
    }
});
erpViewVerifyOrderAll.attachSchema(Schemas.putInStorageOrder);

AutoForm.hooks({
    erpBudgetCorePriceOrder: {
        onSuccess: function (formType, result) {
            var acnumber = Router.current().params.acnumber;
            var orderNo = parseInt(Router.current().params.orderId);
            Meteor.call("updateOrderStatus", acnumber, orderNo, 3, new Date(), function (err, result) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>核价中······</h4>",
                        duration: 1000
                    });
                    Meteor.setTimeout(function () {
                        history.go(-2);
                    }, 1500)
                }
            })

        },

        onError: function (formType, error) {
            console.log(error);
            console.log(formType, error);
        },
    },

    erpBudgetinstallOrder: {
        onSuccess: function (formType, result) {
            IonLoading.show({
                customTemplate: "<h4>安装单提交成功!</h4>",
                duration: 1000
            });
            Meteor.setTimeout(function () {
                history.go(-2);
            }, 1500)
        },

        // Called when any submit operation fails
        onError: function (formType, error) {
            console.log(formType, error);
        }
    },
    erpBudgetputInStorageOrder: {
        onSuccess: function (formType, result) {
            var acnumber = Router.current().params.acnumber;
            var orderNo = parseInt(Router.current().params.orderId);
            Meteor.call("updateOrderStatus", acnumber, orderNo, 11, new Date(), function (err, result) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>入库中······</h4>",
                        duration: 1000
                    });
                    Meteor.setTimeout(function () {
                        history.go(-2);
                    }, 1500)
                    // history.back();
                } else {
                    alert(err);
                }
            })

        }
    }
});


