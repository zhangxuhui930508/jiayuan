/**
 * Created by zhangxuhui on 16/3/20.
 */

Template.ConstrectionType.rendered = function () {
    IonLoading.show();

    this.subscribe("publishConstructionCostInfo", function (err, result) {
        IonLoading.hide()
    });
};


Template.ConstrectionType.helpers({
    headObj: function () {
        return {
            title: '工程费类型'
        }
    },
    constreCtionnotissued: function () {
        var info = erpCostAccountingPlan.find({财务审核: false}).count();
        return info;
    },
    constreCtionissuedCount: function () {
        var info = erpCostAccountingPlan.find({财务审核: true}).count();
        return info;
    }
});

Template.ConstrectionType.events({
    'click .constreCtionissued': function () {
        Router.go("/ConstructionCost/true");
    },
    'click .constreCtionnotissued': function () {
        Router.go("/ConstructionCost/false");
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/constructionLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
})
;