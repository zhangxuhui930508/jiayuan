/**
 * Created by zhangxuhui on 15/10/29.
 */
Template.contractlist.rendered = function () {
    var ACNumber;

    if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
        ACNumber = Session.get("customerDecorateInfo").ACNumber;
    } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
        ACNumber = Session.get("customerLogin");
    }
    this.autorun(function () {
        Meteor.subscribe("contractListInfo", ACNumber, "预算封面");
        Meteor.subscribe("contractListInfo", ACNumber, "预算明细");
        Meteor.subscribe("contractListInfo", ACNumber, "工程报价");
    });

};

Template.contractlist.helpers({
    headObj: function () {
        return {
            title: '合同详情',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    },
    'BudgetCover': function () {
        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        var info = PdfImages.find({AcceptanceNumber: ACNumber, OrderType: "预算封面"});
        if (info) {
            return info;
        }
    },

    'BudgetDetail': function () {
        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        var info = PdfImages.find({AcceptanceNumber: ACNumber, OrderType: "预算明细"});
        if (info) {
            return info;
        }
    },
    'EngineeringQuotation': function () {
        var ACNumber;

        if (Roles.userIsInRole(Meteor.userId(), ["designer","districtManager","vicePresident","StoreManager"], "user-group")) {
            ACNumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            ACNumber = Session.get("customerLogin");
        }
        var info = PdfImages.find({AcceptanceNumber: ACNumber, OrderType: "工程报价"});
        if (info) {
            return info;
        }
    },

});

