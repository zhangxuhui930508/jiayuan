/**
 * Created by pcbeta on 15-8-4.
 */
Template.login.rendered = function () {
    LogInfo.insert({uid:Meteor.userId(),"title":"login"})
    if (Meteor.userId()) {
        if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
            Router.go("/customerindex");
        } else {
            Router.go("/");
        }
    } else {
        Router.go("/");
    }
};

Template.login.events({
    'click .account-button': function (event, template) {
        var userName = template.$('[id=username]').val().replace(/\s+/g, "");
        console.log("userName:" + userName);
        var password = template.$('[id=pwd]').val().replace(/\s+/g, "");
        console.log("password:" + password);
        //让return起作用
        event.preventDefault();

        //
        //IonLoading.show({
        //    customTemplate: "<h4>正在登录, 请稍候...</h4>",
        //});
        Meteor.loginWithPassword(userName, password, function (error, result) {
            //IonLoading.hide();
            if (error) {
                console.log(error.message);
                IonLoading.show({
                    customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                    duration: 1000
                });
            } else {
                if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
                    Router.go("/customerindex");
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                        duration: 1000
                    });
                }
            }
        })
    }
});