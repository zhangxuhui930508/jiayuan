/**
 * Created by zhangxuhui on 1/9/16.
 */

QCImage.allow({
    insert: function (userId, doc) {
        //return pemissionCheck(userId);
        //console.log("=====insert image====", JSON.stringify(doc));
        return true;
    },
    update: function (userId, doc, fieldName, modifier) {
        //console.log("=====update image====", JSON.stringify(modifier));
        //return pemissionCheck(userId);
        return true;
    },

    download: function (userId) {
        //return pemissionCheck(userId);
        return true;
    }
});


ShoppingCart.allow({
    insert: function (userId, doc) {
        return true;
    },
    update: function (userId, doc, fieldName, modifier) {

        return true;
    },
});

Solutions.allow({
    insert: function (userId, doc) {
        return true;
    },
    update: function (userId, doc, fieldName, modifier) {

        return true;
    },
});
Images.allow({
    insert: function (userId, doc) {
        //return pemissionCheck(userId);
        //console.log("=====insert image====", JSON.stringify(doc));
        return true;
    },
    update: function (userId, doc, fieldName, modifier) {
        //console.log("=====update image====", JSON.stringify(modifier));
        //return pemissionCheck(userId);
        return true;
    },

    download: function (userId) {
        //return pemissionCheck(userId);
        return true;
    }
});


correctiveReason.allow({
    insert: function () {
        return true;
    }
});

ERP_Budget_Material.allow({
    insert: function (userId, doc) {
        //return pemissionCheck(userId);
        //console.log("=====insert image====", JSON.stringify(doc));
        return true;
    },
    update: function (userId, doc, fieldName, modifier) {
        //console.log("=====update image====", JSON.stringify(modifier));
        //return pemissionCheck(userId);
        return true;
    },
})


QcTemporary.allow({
    insert: function (userId, doc) {
        return true;
    },
    update: function (userId, doc, fieldName, modifier) {
        return true;
    }
});

LogInfo.allow({
    insert: function (userId, doc) {
        return true;
    },
    update: function (userId, doc, fieldName, modifier) {
        return true;
    }
})
