/**
 * Created by zhangxuhui on 16/3/22.
 */


Template.hejiadanDetail.helpers({
    headObj: function () {
        return {
            title: '核价单详情',
            leftButton: true,
            backTemplate: 'hejiadan'
        }
    },

    'hejiadanDetailInfo': function () {
        var info = erpRemoteFileStorage.find();
        if (info) {
            return info;
        }
    }
});

Template.hejiadanDetail.events({
    'click .item': function (event) {
        var t1 = Router.current().params.childrenType;
        var id = $(event.currentTarget).children("h2").attr("class").trim();
        Router.go("/pdfFiles/hejiadanDetail/" + t1 + "/pdfId/" + id);
    }
})