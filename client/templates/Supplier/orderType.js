/**
 * Created by zhangxuhui on 16/3/24.
 */

Template.orderType.onRendered(function () {
    // Session.set("orderNewOrderCount", null);
    // Meteor.call("orderTypeCount", Meteor.userId(), "newOrder", function (err, result) {
    //     if (!err) {
    //         Session.set("orderNewOrderCount", result)
    //     }
    // });
    // this.subscribe("unReadMessage", Meteor.userId());

    loadingSystemContent();
    Meteor.call("orderPageCount", function (err, result) {
        loadingHide();
        Session.set("orderNewOrderCount", result);
    })

});

Template.orderType.helpers({
    headObj: function () {
        return {
            title: '订单类型'
        }
    }, orderCount: function () {
        return Session.get("orderNewOrderCount");
    },

    'newOrderCount': function () {
        try {
            return Session.get("orderNewOrderCount").newOrder;
        } catch (e) {
            return "0";
        }
    },
    "proceedOrderCount": function () {
        try {
            return Session.get("orderNewOrderCount").proceedOrder;
        } catch (e) {
            return "0";
        }
    },
    "finishOrderCount": function () {
        try {
            return Session.get("orderNewOrderCount").finishOrder;
        } catch (e) {
            return "0";
        }
    },
    'suspendOrder': function () {
        try {
            return Session.get("orderNewOrderCount").suspendOrder;
        } catch (e) {

        }
    },

    cancelOrder: function () {
        try {
            return Session.get("orderNewOrderCount").cancelOrder;
        } catch (e) {

        }
    },
    'isUnReadInfo': function () {
        var info = MessageState.find().count();
        console.log("---------------------", info);
        if (info > 0) {
            return true;
        }
    },
    isUnReadChat: function () {
        var info = constructionAndSupplierChat.find().count();
        if (info > 0) {
            return true;
        }
        return false;
    }

});

Template.orderType.events({

    'click .newOrder': function () {
        Router.go("/orderList/newOrder");
    },
    'click .proceedOrder': function () {

        Router.go("/proceedOrderType");
        // Router.go("/orderList/proceedOrder");
    },
    'click .finishOrder': function () {
        Router.go("/finishOrderList");
    },
    'click .suspendOrder': function () {
        Router.go("/suspendOrderList");
    },

    'click .log-out': function () {
        Meteor.logout(function (err, result) {
            console.log(err, result);
            if (!err) {
                Router.go("/SupplierLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .materialChat': function (event) {
        Router.go("/materialChatList");
    },
    'click .rushMaterial': function () {
        Router.go("/CandSChat");
    },
    'click .cancelOrder': function () {
        Router.go("/cancelOrder");
    }

});

 