/**
 * Created by zhangxuhui on 15/11/10.
 */

Template.materialClassify01.rendered = function () {
    Session.set("customerSelectMaterial","materialClassify01");
    this.autorun(function () {
        Meteor.subscribe("customerClassifyall", function () {
            console.log(CustomerMaterialclassify.find().fetch());
        });
    });
};

Template.materialClassify01.helpers({
    'headObj': function () {
        return {

            //leftButton: true,
            //backTemplate: ''
        }
    },
    titleName: function () {
        var parts = "材料分类";
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        if (Session.get("customerDecorateInfo")) {
            parts = Session.get("customerDecorateInfo").parts;
        } else {
            parts = "材料分类";
        }
        return parts;
    },
    SelectmaterialClassify: function () {
        var info01 = CustomerMaterialclassify.find({"移动标志": 1}).count() ? CustomerMaterialclassify.find({"移动标志": 1}).count() : '0';
        var info02 = CustomerMaterialclassify.find({"移动标志": 2}).count() ? CustomerMaterialclassify.find({"移动标志": 2}).count() : '0';
        var info03 = CustomerMaterialclassify.find({"移动标志": 3}).count() ? CustomerMaterialclassify.find({"移动标志": 3}).count() : '0';
        var info04 = CustomerMaterialclassify.find({"移动标志": 4}).count() ? CustomerMaterialclassify.find({"移动标志": 4}).count() : '0';
        //if (info01) {
        //    info01 = CustomerMaterialclassify.find({"移动标志": 1}).count();
        //}

        return [
            {classifyName: "地板类", count: info01},
            {classifyName: "瓷砖类", count: info02},
            {classifyName: "洁具类", count: info03},
            {classifyName: "电器类", count: info04},
        ]
    },
    'materialCount01': function () {
        var info = CustomerMaterialclassify.find({"移动标志": 1}).count();
        console.log(info);
        return info;
    }
});

Template.materialClassify01.events({
    'click .backTemplates': function () {
        Router.go("/customerParts");
    },
    'click .nextPage': function (event) {
        var first = $(event.currentTarget).children("span").text().trim();
        console.log("currentCustomerSelectClassify", first);
        switch (first) {
            case '地板类':
                Session.set("materialClassifyMark", 1);
                break;
            case '瓷砖类':
                Session.set("materialClassifyMark", 2);
                break;
            case '洁具类':
                Session.set("materialClassifyMark", 3);
                break;
            case '电器类':
                Session.set("materialClassifyMark", 4);
                break;
        }
        console.log(Session.get("materialClassifyMark"));
        console.log("==================        Router.go(materialClassify02); ================")
        Router.go("/materialClassify02");
    },
    'click .DecorateArea':function(){
        Router.go("/customerParts")
    }

});