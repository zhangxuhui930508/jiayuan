/**
 * Created by zhangxuhui on 15/10/29.
 */
Template.SettlementType.helpers({
    headObj: function () {
        return {
            title: '结算单类型',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    },
});

Template.SettlementType.events({
    'click .paymentReminder': function () {
        //var ACNumber;
        //
        //if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
        //    ACNumber = Session.get("customerDecorateInfo").ACNumber;
        //} else if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
        //    ACNumber = Session.get("customerLogin");
        //}
        Router.go("/paymentReminder");

    },
    'click .settlementOrder': function () {
        //var ACNumber;
        //
        //if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
        //    ACNumber = Session.get("customerDecorateInfo").ACNumber;
        //} else if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
        //    ACNumber = Session.get("customerLogin");
        //}
        Router.go("/settlementOrder");
    },
});