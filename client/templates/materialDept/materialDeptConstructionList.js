/**
 * Created by zhangxuhui on 16/5/12.
 */

Template.materialDeptConstructionList.onRendered(function () {
    IonLoading.show();
    var type = Router.current().params.type;
    this.subscribe("meterialDeptConstructionType", Meteor.userId(), type, function () {
        IonLoading.hide();
    });

});
Template.materialDeptConstructionList.helpers({
    headObj: function () {
        return {

            leftButton: true,
            backTemplate: 'materialDeptConstructionType'
        }
    },
    'titleName': function () {
        var title = Router.current().params.type;
        if (title) {
            return title + "客户";
        }
    },
    'construtionListInfo': function () {
        var info = Session.get("currentCustomerSearchVal");
        var select = {};
        if (info) {
            select = {$or: [{ACNumber: new RegExp(info, "i")}, {CustName: new RegExp(info, "i")}]};
        }
        var customerInfo = Cust_Info.find(select, {sort: {startTime: -1}});
        if (customerInfo) {
            return customerInfo;
        }
    },
    'constructionName': function (acnumber) {
        var Relationship = construtionRelationClient.findOne({cId: acnumber});
        if (Relationship) {
            var employee = AC_Personnel.findOne({_id: Relationship.construtionId});
            if (employee) {
                return employee.pName
            }
        }
    },
    'storeInfo': function (acnumber) {
        var customerInfo = Cust_Info.findOne({ACNumber: acnumber})
        if (customerInfo) {
            var info = acSet.findOne({ACNumb: customerInfo.Branch, "ACType": "营业网点"});
            if (info)
                return info.ACName;
        }
    }
});


Template.materialDeptConstructionList.events({
    'click .gocustomerDetail': function () {
        console.log(this._id);
        var type = Router.current().params.type;
        Session.set("customerLogin", this.ACNumber);
        Session.set("materialDeptCustomerType", type);
        Router.go("/ContractListType");
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    }
});

Template.materialDeptConstructionList.onDestroyed(function () {
    Session.set("currentCustomerSearchVal", null);
});