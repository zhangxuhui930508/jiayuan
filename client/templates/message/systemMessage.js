/**
 * Created by zhangxuhui on 16/4/19.
 */

Template.systemMessage.rendered = function () {
    var ACNumber;
    if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
        ACNumber = Session.get("customerLogin");
    } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "test", "designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptainBoss"], "user-group")) {
        ACNumber = Session.get("customerDecorateInfo").ACNumber;
    }
    this.subscribe("systemMessageInfo", ACNumber)
};

Template.systemMessage.helpers({
    headObj: function () {
        return {
            title: '工地进度消息',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    },
    'messageInfo': function () {
        var info = systemMessage.find({}, {sort: {state: -1, date: -1}});
        if (info) {
            return info;
        }
    },
    'fontColor': function (state) {
        console.log(state);
        if (state == "unread") {
            return "blue"
        } else if (state == "read") {
            return "#D3D3D3"
        }
    },
    MessageIcon: function (id) {
        var info = systemMessage.findOne({_id: id});
        if (info && info.userInfo) {
            return info.userInfo.icon;
        }
        return "/image/jiayuanicon.jpeg"
    }
});

Template.systemMessage.events({
    'click .chatMessage': function (event) {
        var id = $(event.currentTarget).children("div").attr("class").trim();
        var info = systemMessage.findOne({_id: id});
        console.log(id);

        // Router.go('/chatMessage/' + id);
        if (info) {
            var date = info.date.Format("yyyy-MM-dd");
            IonPopup.show({
                title: date,
                template: info.message,
                buttons: [{
                    text: '已读',
                    type: 'button-positive',
                    onTap: function () {
                        Meteor.call("systemMessageRead", id, function (err, result) {
                            IonPopup.close();
                        });
                    }
                }]
            });
        }
    }
});


//================================================================================
