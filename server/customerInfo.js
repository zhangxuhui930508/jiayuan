/**
 * Created by zhangxuhui on 15/12/8.
 */
Meteor.methods({
    /**
     * 将相应的客户返回
     * @param designerId
     * @param customerType
     * @returns {Array}
     */
    'customerInfo': function (designerId, customerType) {
        var info = DesignerRelationClient.find({dId: designerId, cType: customerType}).fetch();
        var customer = [];

        _.each(info, function (t) {
            var customerACNumber = {};
            //console.log(t.cId)
            var customerInfo = Cust_Info.findOne({ACNumber: t.cId}, {sort: {CreatTime: -1}});
            if (customerInfo) {
                customerACNumber.CustName = customerInfo.CustName;
                customerACNumber.Address = customerInfo.Address;
                customerACNumber._id = customerInfo._id;
                customerACNumber.ACNumber = customerInfo.ACNumber;
            }
            if (customerACNumber.ACNumber) {
                customer.push(customerACNumber);
            }
        });

        return customer;
    },
    /**
     *
     * @param designerId
     * @param customerType
     * @param limit
     * @param skip
     * @returns {Array}
     */
    'customerPage': function (designerId, customerType, limit, skip) {
        var info = DesignerRelationClient.find({dId: designerId, cType: customerType}).fetch();
        var customer = [];
        _.each(info, function (t) {
            var customerACNumber = {};
            console.log(t.cId)
            var customerInfo = Cust_Info.findOne({ACNumber: t.cId}, {sort: {CreatTime: -1}}, {
                skip: skip,
                limit: limit
            });
            if (customerInfo) {
                customerACNumber.CustName = customerInfo.CustName;
                customerACNumber.Address = customerInfo.Address;
                customerACNumber._id = customerInfo._id;
                customerACNumber.ACNumber = customerInfo.ACNumber;
            }
            if (customerACNumber.ACNumber) {
                customer.push(customerACNumber);
            }
        });
        //console.log("customerPage");
        //console.log("==========================");
        //console.log(customer);
        //console.log("==========================");
        return customer;
    },
    'getSearchCustomer': function (designerId, customerType, searchConditions) {
        var acNumber = [];
        //获得受理编号
        var customerAcnumber = Cust_Info.find({CustName: new RegExp(searchConditions.CustName, "i")}).fetch();
        if (customerAcnumber) {
            _.each(customerAcnumber, function (t) {
                acNumber.push(t.ACNumber);
            })
        }
        //console.log("--------------------------------------------------");
        //console.log(acNumber);
        //console.log("---------------------------------------------");

        //筛选出受理编号属于此设计师的,此类型的客户
        var selectAcnumber = [];
        var info = DesignerRelationClient.find({dId: designerId, cType: customerType}).fetch();

        for (var key in info) {
            for (var keys in acNumber) {
                if (info[key].cId == acNumber[keys]) {
                    selectAcnumber.push(acNumber[keys]);
                }
            }
        }
        //console.log("-----------selectAcnumber------------------")
        //console.log("selectAcnumber:", selectAcnumber);
        //console.log("===========================================")

        //返回搜索结果的数据

        var backdata = Cust_Info.find({ACNumber: {$in: selectAcnumber}}, {sort: {"CreatTime": -1}}).fetch();

        return backdata;


        //var customer = [];
        //_.each(info, function (t) {
        //    var customerACNumber = {};
        //    var select = {};
        //    console.log(searchConditions);
        //    //if (searchConditions.ACNumber) {
        //    //    select.ACNumber = new RegExp(search.ACNumber, "i");
        //    //}
        //    if (searchConditions.CustName) {
        //        select.CustName = new RegExp(search.CustName, "i");
        //    }
        //    //if (searchConditions.Address) {
        //    //    select.Address = new RegExp(search.Address, "i");
        //    //}
        //
        //    var customerInfo = Cust_Info.findOne(select, {sort: {CreatTime: -1}});
        //    if (customerInfo) {
        //        customerACNumber.CustName = customerInfo.CustName;
        //        customerACNumber.Address = customerInfo.Address;
        //        customerACNumber._id = customerInfo._id;
        //        customerACNumber.ACNumber = customerInfo.ACNumber;
        //    }
        //    if (customerACNumber.ACNumber) {
        //        customer.push(customerACNumber);
        //    }
        //
        //    console.log("------------------getSearchCustomer---------------");
        //    console.log("customer:", customer);
        //    console.log("-----------------------------------------------------");
        //})

    }

});