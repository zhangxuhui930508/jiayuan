/**
 * Created by zhangxuhui on 16/6/3.
 */

Template.contractDeptViewCustomer.onRendered(function () {

    var self = this;
    this.autorun(function () {
        self.subscribe("contractCustomerInfo", Session.get("currentCustomerSearchVal"));

    })
})

Template.contractDeptViewCustomer.helpers({
    headObj: function () {
        return {
            title: '客户列表',
            // leftButton: true,
            // backTemplate: 'customerType'
        }
    },
    'customerInfo': function () {
        return Cust_Info.find();
    }
});

Template.contractDeptViewCustomer.events({
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    }
});

Template.contractDeptViewCustomer.events({
    'click .gocustomerDetail': function () {
        Session.set("customerLogin", this.ACNumber);
        Router.go("/mycustomer");
    }
})
