/**
 * Created by zhangxuhui on 5/26/16.
 */


Template.orderSortSupplierList.helpers({
    headObj: function () {
        return {
            title: '供应商信息',
            leftButton: true,
            backTemplate: 'StandardMaterial'
        }
    },
    supplierInfo: function () {
        return erpInfoSupplier.find();
    },
    "supploerMaterialInfo": function (ID) {
        return ERP_Budget_Material.find({供货商ID: ID});
    },

})
;


Template.orderSortSupplierList.events({
    'click .allow': function (event) {
        var thisClass = $(event.target).attr('class');
        if (thisClass == "icon ion-ios-arrow-down allow") {
            console.log($("[class='" + this._id + "MaterialDetail']"));
            $("[class='" + this._id + "MaterialDetail']").css('display', 'block');
            $(event.target).removeClass('ion-ios-arrow-down').addClass('ion-ios-arrow-up');
        } else {
            $("[class='" + this._id + "MaterialDetail']").css('display', 'none');
            $(event.target).removeClass('ion-ios-arrow-up').addClass('ion-ios-arrow-down');
        }
    },
    'click .ion-chatboxes': function (event) {
        console.log("===========================")
        Router.go("/typicalSentences1/acnumber/" + Router.current().params.acnumber + "/supplierId/" + this.ID);
        //
        // /typicalSentences1/acnumber/:acnumber/supplierId/:supplierId
        // Meteor.call("constructionAndSupplier", this.ID, Router.current().params.acnumber, function (err, result) {
        //     if (!err) {
        //         IonLoading.show({
        //             customTemplate: "<h4>催材料信息发送成功!</h4>",
        //             duration: 1000
        //         });
        //     } else {
        //         IonLoading.show({
        //             customTemplate: "<h4>催材料信息发送失败!</h4>",
        //             duration: 1000
        //         });
        //     }
        // })
    }


});