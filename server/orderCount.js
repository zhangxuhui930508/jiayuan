/**
 * Created by zhangxuhui on 16/3/25.
 */

Meteor.methods({
    /**
     * 统计订单类型的count
     * @param uid
     * @param type
     * @returns {(function(): *)|_subs.count|{}|*|number}
     */
    'orderTypeCount': function (uid) {
        try {
            var ID = Meteor.users.findOne({_id: uid}).ID;
            if (ID) {
                var newOrder = erpViewVerifyOrderAll.find({
                    供货商编号: ID,
                    // 受理编号:{$in:acnumber},
                    "orderType": "newOrder",
                    预警确认时间: {$gte: new Date("2016-08-20")}
                }).count();
                var proceedOrder = erpViewVerifyOrderAll.find({
                    供货商编号: ID,
                    // 受理编号:{$in:acnumber},
                    "orderType": "proceedOrder",
                    预警确认时间: {$gte: new Date("2016-08-20")}
                }).count();

                // var finishOrder = vieVerifyOrderStorageA.find({供货商编号: ID}).count() + ERP_Budget_Material.find({orderState: 12}).count();
                var finishOrder = erpViewVerifyOrderAll.find({
                    供货商编号: ID,
                    // 受理编号:{$in:acnumber},
                    "orderType": "finishOrder",
                    预警确认时间: {$gte: new Date("2016-08-20")}
                }).count();
                var suspendOrder = ERP_Budget_Material.find({供货商ID: ID, orderState: 13}).count();

                var returnJson = {};
                returnJson.newOrder = newOrder;
                returnJson.proceedOrder = proceedOrder;
                returnJson.finishOrder = finishOrder;
                returnJson.suspendOrder = suspendOrder;
                returnJson.cancelOrder = ERP_Budget_Material.find({供货商ID: ID, isCancel: true})
                // console.log(returnJson);
                return returnJson;
            }

        } catch (e) {
            throw new Meteor.Error("")
        }
    },
    /**
     * 更新变更单信息
     * @param id
     */
    updateChangeOrder: function (id) {
        if (!id) {
            throw  new Meteor.Error("id is null")
        }
        erpRemoteFileStorage.update({_id: id}, {$set: {changeOrderConfirm: true}})

    },
    /**
     * 更改变更单的公开与不公开
     * @param id
     * @param status
     */
    'updateOpenOrder': function (id, status) {
        if (!id) {
            throw  new Meteor.Error("id is null")
        }
        erpRemoteFileStorage.update({_id: id}, {$set: {openOrder: status}})

    },
    /**
     * 根据受理编号获得客户的姓名和电话
     * @param acnumberarrs
     * @returns {{}}
     */
    'getCustomerNameInfo': function (acnumberarrs) {
        var json = {};
        _.each(acnumberarrs, function (acnumber) {
            var info = Cust_Info.findOne({ACNumber: acnumber});
            if (info) {
                var select = {};
                select.CustName = info.CustName;
                select.Address = info.Address;
                json[acnumber] = select;
            }
        })

        return json;
    },


});