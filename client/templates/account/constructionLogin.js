/**
 * Created by zhangxuhui on 16/1/19.
 */

Template.constructionLogin.events({
    'click .account-button': function (event, template) {
        var userName = template.$('[id=username]').val().replace(/\s+/g, "");
        console.log("userName:" + userName);
        var password = template.$('[id=pwd]').val().replace(/\s+/g, "");
        console.log("password:" + password);
        //让return起作用
        event.preventDefault();

        Meteor.loginWithPassword(userName, password, function (error, result) {
            // console.log(error, result);
            // console.log("===============================");
            if (error) {
                // console.log(error.message);
                IonLoading.show({
                    customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                    duration: 1000
                });
            } else {
                //if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptainBoss"], "user-group")) {

                if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptainBoss"], "user-group")) {
                    Session.set("loginRole", "ConstructionCaptainBoss");
                    Router.go("/constructionCaptainList");
                } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain"], "user-group")) {
                    // console.log(result);
                    Session.set("loginRole", "ConstructionCaptain");
                    Router.go('/constructionCType');
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>用户名或密码验证不通过!</h4>",
                        duration: 1000
                    });
                }
            }

        })
    }
});
