/**
 * Created by zhangxuhui on 16/6/18.
 */

Template.qcCheckPage.onRendered(function () {
    this.subscribe("publishQcCheckPage");
});

Template.qcCheckPage.helpers({
    headObj: function () {
        return {
            title: '待检工地列表'
        }
    }, Info: function () {
        var select = {};
        if (Session.get("currentCustomerSearchVal")) {
            select.acnumber = new RegExp(Session.get("currentCustomerSearchVal"), "i");
        }
        return checkConstructionProject.find(select, {sort: {jumpTheQueue: -1, updateDate: 1}});
    },
    getCustInfoCustName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    getCustInfoAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    },
    getStoreInfo: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info && info.Branch) {
            var acSetInfo = acSet.findOne({ACNumb: info.Branch});
            if (acSetInfo) {
                return acSetInfo.ACName;
            }
        }
    },
    'constructionInfo': function (sender) {
        var userInfo = Meteor.users.findOne({_id: sender});
        if (userInfo) {
            var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
            if (employeeInfo) {
                return employeeInfo.pName;
            }
        }
    },
    'getCustomerMode': function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info && info.BusiType == "整体翻新") {
            return "#0000CD"
        } else if (info && info.BusiType == "局部翻新") {
            return "#000000";
        }
    },
    'jumpTheQueueinfo': function (id) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info && info.jumpTheQueue) {
            return "red"
        }
    },
    "overdueDate": function (id, date) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info) {
            if (info.jumpTheQueue) {
                return "#FFFF00";
            }
            if (info.checkDate < new Date().Format("yyyy-MM-dd")) {
                return "red"
            }
            if (info && !info.qcIsCheck) {
                return "#8C8C8C	"
            }
        }
    },
    'checkProject': function (value) {
        if (value) {
            switch (value) {
                case "completion":
                    return "竣工报检";
                    break;
                case "hydropower":
                    return "水电报检";
                    break;
                case "mudWood":
                    return "泥木报检";
                    break;
            }
        }
    },
    'isCheckInfo': function (id) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info && !info.qcIsCheck) {
            return "#D9D9D9	"
        }
    }

});

Template.qcCheckPage.events({
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    },
    'click .jiangttConfirmQC': function () {
        console.log(this._id);
        var id = this._id;
        IonPopup.confirm({
            title: "温馨提示",
            template: "确认报检?",
            okText: '确定',
            cancelText: "取消",
            onOk: function () {
                Meteor.call("bossCheckQcInfo", id, function (err, result) {
                    if (!err) {
                        IonLoading.show({
                            customTemplate: "<h4>报检成功!</h4>",
                            duration: 1000
                        });
                    }
                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });
    },
    'click #js-logout': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/constructionLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });

            }
        });
    },
    'click .gocustomerDetail': function () {
        console.log(this._id);
        Router.go("/qcCheckDetail/" + this._id);
    }
});