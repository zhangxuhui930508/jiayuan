/**
 * Created by zhangxuhui on 15/10/29.
 */

Template.settlementOrder.helpers({
    headObj: function () {
        return {
            title: '结算单详情',
            leftButton: true,
            backTemplate: 'SettlementType'
        }
    },
    'settlementOrderDetail': function () {
        var info = erpRemoteFileStorage.find({}, {sort: {完成时间: -1}});
        if (info) {
            return info;
        }
    }
});

Template.settlementOrder.events({
    'click .openfile': function (event) {
        var id = $(event.currentTarget).attr("id").trim();
        Session.set("backURL", "settlementOrder");
        Router.go("/pdfFiles/" + id);
    }
})