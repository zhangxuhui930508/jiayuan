/**
 * Created by zhangxuhui on 15/11/10.
 */

Template.CustomizeMaterial.onRendered(function () {
    var ACNumber;
    if (Roles.userIsInRole(Meteor.userId(), ["client", "materialDept", "contractDept"], "user-group")) {
        ACNumber = Session.get("customerLogin");
    } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "test", "designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptainBoss"], "user-group")) {
        ACNumber = Session.get("customerDecorateInfo").ACNumber;
    }
    IonLoading.show({});

    this.subscribe("erpCustomizeMaterialInfo", ACNumber, function () {
        IonLoading.hide();
    });
});

Template.CustomizeMaterial.helpers({
    headObj: function () {
        return {
            title: '定制材料',
            leftButton: true,
            backTemplate: 'ContractListType'
        }
    },
    materialList: function () {
        var productInfo = ERP_Product_Order.find({}, {sort: {"修改时间": -1}});
        if (productInfo) {
            return productInfo;
        }
    },
    getMaterialDetail01: function () {
        var info = ERP_Product_Order01.find();
        if (info) {
            return info;
        }
    },
    getMaterialDetail02: function () {
        var info = ERP_Product_Order02.find();
        if (info) {
            return info;
        }
    },
    getMaterialDetail03: function () {
        var info = ERP_Product_Order03.find();
        if (info) {
            return info;
        }
    },
    getMaterialDetail04: function () {
        var info = ERP_Product_Order04.find();
        if (info) {
            return info;
        }
    }, getMaterialDetail05: function () {
        var info = ERP_Product_Order05.find();
        if (info) {
            return info;
        }
    }, getMaterialDetail06: function () {
        var info = ERP_Product_Order06.find();
        if (info) {
            return info;
        }
    }
});

Template.CustomizeMaterial.events({
    'click .productDetail01': function (event) {
        console.log($(event.currentTarget).children("span").attr("id"));
        var id = $(event.currentTarget).children("span").attr("id");
        if (!Roles.userIsInRole(Meteor.userId(), ["contractDept"], "user-group")) {
            Router.go("/product01Detail/" + id);
        }
    }, 'click .productDetail02': function (event) {
        console.log($(event.currentTarget).children("span").attr("id"));
        var id = $(event.currentTarget).children("span").attr("id");
        if (!Roles.userIsInRole(Meteor.userId(), ["contractDept"], "user-group")) {
            Router.go("/product02Detail/" + id);
        }
    }, 'click .productDetail03': function (event) {
        console.log($(event.currentTarget).children("span").attr("id"));
        var id = $(event.currentTarget).children("span").attr("id");
        if (!Roles.userIsInRole(Meteor.userId(), ["contractDept"], "user-group")) {
            Router.go("/product03Detail/" + id);
        }
    }, 'click .productDetail04': function (event) {
        console.log($(event.currentTarget).children("span").attr("id"));
        var id = $(event.currentTarget).children("span").attr("id");
        if (!Roles.userIsInRole(Meteor.userId(), ["contractDept"], "user-group")) {
            Router.go("/product04Detail/" + id);
        }
    }, 'click .productDetail05': function (event) {
        console.log($(event.currentTarget).children("span").attr("id"));
        var id = $(event.currentTarget).children("span").attr("id");
        if (!Roles.userIsInRole(Meteor.userId(), ["contractDept"], "user-group")) {
            Router.go("/product05Detail/" + id);
        }
    }, 'click .productDetail06': function (event) {
        console.log($(event.currentTarget).children("span").attr("id"));
        var id = $(event.currentTarget).children("span").attr("id");
        if (!Roles.userIsInRole(Meteor.userId(), ["contractDept"], "user-group")) {
            Router.go("/product06Detail/" + id);
        }
    }
});
