/**
 * Created by zhangxuhui on 16/3/25.
 */

Template.orderMaterialDetailInfo.helpers({
    headObj: function () {
        return {
            title: '材料详情',
            leftButton: true,
            backTemplate: 'orderMaterialList'
        }
    },
    'orderMaterialDetail': function () {
        var info = MaterialDetailList.findOne({});
        if (info) {
            return info;
        }
    },
    imageInfo: function (id) {
        var imageInfo = Images.find({_id: id});
        return imageInfo;
    },
    isimage: function (id) {
        var MaterialListInfo = MaterialDetailList.findOne();
        if (MaterialListInfo && MaterialListInfo.picture) {
            return true;
        } else {
            return false;
        }
    },

    'images1': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image1:", materialInfo.picture[0]);
            if (materialInfo.picture[0]) {
                return materialInfo.picture[0];
            }
        }
    },
    'images2': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image2:", materialInfo.picture[1]);
            if (materialInfo.picture[1]) {
                return materialInfo.picture[1];
            }
        }
    },
    'images3': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image3:", materialInfo.picture[2]);
            if (materialInfo.picture[2]) {
                return materialInfo.picture[2];
            }
        }
    },
    'images4': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image4:", materialInfo.picture[3]);
            if (materialInfo.picture[3]) {
                return materialInfo.picture[3];
            }
        }
    },
    'images5': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image5:", materialInfo.picture[4]);
            if (materialInfo.picture[4]) {
                return materialInfo.picture[4];
            }
        }
    },
    'images6': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            console.log("image6:", materialInfo.picture[5]);
            if (materialInfo.picture[5]) {
                return materialInfo.picture[5];
            }
        }
    },
    'isimages1': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[0]) {
                return false
            } else {
                return true;
            }
        }
    }, 'isimages2': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[1]) {
                return false;
            } else {
                return true;
            }
        }
    }, 'isimages3': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[2]) {
                return false
            } else {
                return true;
            }
        }
    }, 'isimages4': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[3]) {
                return false
            } else {
                return true;
            }
        }
    }, 'isimages5': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[4]) {
                return false
            } else {
                return true;
            }
        }
    }, 'isimages6': function () {
        var materialInfo = MaterialDetailList.findOne();
        if (materialInfo && materialInfo.picture) {
            if (materialInfo.picture[5]) {
                return false
            } else {
                return true;
            }
        }
    },

});
