/**
 * Created by zhangxuhui on 16/4/19.
 */

Template.notExaminedList.rendered = function () {
    Session.set("currentCustomerSearchVal", null);
    $(".search").val("");

    var uid;
    if (Roles.userIsInRole(Meteor.userId(), ["qc", "qcManager"], "user-group")) { //质检人员
        uid = Meteor.userId();
        // console.log("==============")
    } else if (Roles.userIsInRole(Meteor.userId(), ["qcBoss"], "user-group")) {
        uid = null;
    }
    console.log(uid);
    // this.subscribe("construction", uid, "notExamined");
    this.subscribe("consturctionInfo", uid, "notExamined");

    Session.set("searchNowDateInfo", null);
    Session.set("selectDateInfo", null);
    Session.set("selectDateButton", false);
    Session.set("selectNowDate", false)
};


Template.notExaminedList.helpers({
    headObj: function () {
        return {
            title: "未检工地(" + checkConstructionProject.find().count() + ")"
        }
    },
    Info: function () {
        var select = {};
        var selectDate = Session.get("searchNowDateInfo");
        var selectDateButton = Session.get("selectDateInfo");
        if (selectDate) {
            select.checkDate = selectDate;
        }
        if (selectDateButton) {
            select.checkDate = selectDateButton;
        }
        if (Session.get("currentCustomerSearchVal")) {
            select.acnumber = new RegExp(Session.get("currentCustomerSearchVal"), "i");
        }

        return checkConstructionProject.find(select, {sort: {jumpTheQueue: -1, updateDate: 1}})
    },
    getCustInfoAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    },
    getStoreInfo: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        console.log("-------------------------------------------")
        console.log(info);
        console.log("-------------------------------------------")
        if (info && info.Branch) {
            var acSetInfo = acSet.findOne({ACNumb: info.Branch});
            if (acSetInfo) {
                return acSetInfo.ACName;
            }
        }
    },
    'jumpTheQueueinfo': function (id) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info && info.jumpTheQueue) {
            return "red"
        }
    },
    "overdueDate": function (id, date) {
        var info = checkConstructionProject.findOne({_id: id});
        if (info) {
            if (info.jumpTheQueue) {
                return "#FFFF00";
            }
            if (info.checkDate < new Date().Format("yyyy-MM-dd")) {
                return "red"
            }
            if (info && !info.qcIsCheck) {
                return "#8C8C8C	"
            }
        }
    },
    'getCustomerMode': function (acnumber) {
        // console.log(acnumber);
        // console.log("=========================");
        var info = Cust_Info.findOne({ACNumber: acnumber});
        // console.log(info);

        if (info && info.BusiType == "整体翻新") {
            return "#0000CD"
        } else if (info && info.BusiType == "局部翻新") {
            return "#000000";
        }
    },
    getStartTime: function (acnumber) {

        var info = Cust_Info.findOne({ACNumber: acnumber});
        console.log(acnumber);

        if (info && info.startTime) {
            return moment(info.startTime).format("YYYY-MM-DD");
        }
    }, getEndTime: function (acnumber) {

        var info = Cust_Info.findOne({ACNumber: acnumber});
        console.log(acnumber);

        if (info && info.endTime) {
            return moment(info.endTime).format("YYYY-MM-DD");
        }
    },
    // 'getStoreAreaInfo': function (acnumber) {
    //     var customerinfo = Cust_Info.findOne({ACNumber: acnumber});
    //
    //     if (customerinfo && customerinfo.Branch) {
    //         acSet.find({ACType: "营业网点", "ACCont": "0"}).forEach(function (info) {
    //             console.log(info);
    //             var areaInfo = bitConversionClient(info.ACNumb);
    //             console.log("=============================================");
    //             console.log(areaInfo);
    //             console.log("=============================================");
    //             _.each(areaInfo, function (branchInfo) {
    //                 if (branchInfo == info.Branch) {
    //                     return info.ACName
    //                 }
    //             })
    //         });
    //         return "";
    //     }
    // },
    'constructionInfo': function (sender) {
        var userInfo = Meteor.users.findOne({_id: sender});
        if (userInfo) {
            var employeeInfo = AC_Personnel.findOne({ID: userInfo.ID});
            if (employeeInfo) {
                return employeeInfo.pName;
            }
        }
    },
    'checkProject': function (value) {
        if (value) {
            switch (value) {
                case "completion":
                    return "竣工报检";
                    break;
                case "hydropower":
                    return "水电报检";
                    break;
                case "mudWood":
                    return "泥木报检";
                    break;
            }
        }
    },
    'nowOrAllButton': function () {
        var selectDate = Session.get("selectNowDate");
        if (selectDate) {
            return false
        } else {
            return true;
        }
    },
    'selectAllButton': function () {
        var selectDate = Session.get("selectDateButton");
        if (selectDate) {
            return false
        } else {
            console.log("---------")
            return true;
        }
    }

});


Template.notExaminedList.events({
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentCustomerSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentCustomerSearchVal", null);
        $(".search").val("");
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/qualityLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .gocustomerDetail': function (event) {
        var id = $(event.currentTarget).attr("id").trim();
        console.log(id);
        console.log("----------------------------------------");
        Router.go("/constructionDetail/constructionId/" + id + "/type/notExamined");
    },
    'click .nowDate': function () {
        Session.set("selectNowDate", true);
        Session.set("searchNowDateInfo", new Date().Format("yyyy-MM-dd"));
    },
    'click .nowAll': function () {
        Session.set("selectNowDate", false);
        Session.set("searchNowDateInfo", null);

    },
    'click .selectDate': function () {
        IonPopup.prompt({
            title: '设置搜索时间',
            template: '请设置搜索时间',
            okText: '确定',
            cancelText: '取消',
            inputType: 'date',
            onOk: function () {
                var newDate = $('.popup-body input').val().trim();
                Session.set("selectDateInfo", newDate);
                Session.set("selectDateButton", true);
            }
        });
    },
    'click .selectAll': function () {
        Session.set("selectDateInfo", null);
        Session.set("selectDateButton", false);
    }
});

Template.constructionList.onDestroyed(function () {
    Session.set("currentCustomerSearchVal", null);
    Session.set("custInfo", null);
    Session.set("searchNowDateInfo", null);
    Session.set("selectNowDate", null);
    Session.set("selectDateButton", null);
    Session.set("selectDateInfo", null);
});


