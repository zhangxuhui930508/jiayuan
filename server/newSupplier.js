/**
 * Created by zhangxuhui on 2016/9/29.
 */
Meteor.methods({
    orderPageCount: function () {
        var userInfo = Meteor.users.findOne({_id: this.userId});
        if (userInfo && userInfo.ID) {
            var option = {};
            option.newOrder = erpViewVerifyOrderAll.find({"供货商编号": userInfo.ID, "orderType": "newOrder"}).count();
            option.proceedOrder = erpViewVerifyOrderAll.find({
                "供货商编号": userInfo.ID,
                "orderType": "proceedOrder"
            }).count();
            option.finishOrder = erpViewVerifyOrderAll.find({"供货商编号": userInfo.ID, "orderType": "finishOrder"}).count();
            option.suspendOrder = ERP_Budget_Material.find({供货商ID: userInfo.ID, orderState: 13}).count();
            option.cancelOrder = ERP_Budget_Material.find({供货商ID: userInfo.ID, isCancel: true}).count();
            return option;
        }
    }

});