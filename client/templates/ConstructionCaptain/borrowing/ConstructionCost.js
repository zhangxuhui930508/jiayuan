/**
 * Created by zhangxuhui on 16/3/20.
 */

Template.ConstructionCost.onRendered(function () {
    IonLoading.show();

    this.subscribe("publishConstructionCost", Router.current().params.type, function () {
        IonLoading.hide();
    })
});


Template.ConstructionCost.helpers({
    headObj: function () {
        return {
            leftButton: true,
            backTemplate: 'ConstrectionType'
        }
    },
    'titleName': function () {
        var title = Router.current().params.type;
        if (title) {
            if (title == "true") {
                return "已发放"
            } else {
                return "未发放"
            }
        }
    },
    'constructionCostInfo': function () {
        var info = erpCostAccountingPlan.find({}, {sort: {工程部审核日期: -1}});
        if (info) {
            return info;
        }
    },
    getDateFormat: function (date) {
        if (date) {
            return date.Format("yyyy-MM-dd");
        }
    }
});