/**
 * Created by zhangxuhui on 16/6/3.
 */

Meteor.methods({
    'measureUnlock': function (id) {
        if (!id) {
            throw new Meteor.Error("id is null")
        }
        ERP_Budget_Material.update({_id: id}, {
            $unset: {measureDate: -1, orderState: -1},
            $set: {lockProject: "unLock"}
        });
    },
    beihuoDatemeasureUnlock: function (id) {
        if (!id) {
            throw new Meteor.Error("id is null")
        }
        ERP_Budget_Material.update({_id: id}, {$unset: {beihuoDate: -1}, $set: {orderState: 4, lockProject: "unLock"}})
    },
    deliverGoodsUnlock: function (id) {
        if (!id) {
            throw new Meteor.Error("id is null")
        }
        ERP_Budget_Material.update({_id: id}, {
            $unset: {deliveryDate: -1},
            $set: {orderState: 7, lockProject: "unLock"}
        })
    },
    'SupplierMergeOrder': function (acnumber, orderNo) {
        if (!acnumber || !orderNo) {
            throw new Meteor.Error("info is null")
        }
        console.log("------------------", acnumber, orderNo);

        ConsolidatedOrders.upsert({acnumber: acnumber, orderNo: parseInt(orderNo)}, {$set: {isDelete: false}});
    },
    cancelSupplierMergeOrder: function (acnumber, orderNo) {
        if (!acnumber || !orderNo) {
            throw new Meteor.Error("info is null")
        }
        ConsolidatedOrders.update({acnumber: acnumber, orderNo: parseInt(orderNo)}, {$set: {isDelete: true}});
    },
    constructionConfirmMaterial: function (id) {
        if (!id) throw new Meteor.Error("id is null");
        var customer = Cust_Info.findOne({_id: id});
        if (customer) {
            var acnumber = customer.ACNumber;
            ERP_Budget_Material.update({受理编号: acnumber}, {$set: {队长确认: true}}, {multi: true});
        }
    },
//    =================================================

    beihuoOrderDatemeasureUnlock: function (acnumber, orderNo) {
        if (!acnumber || !orderNo) {
            throw new Meteor.Error("操作失败!")
        }
        erpViewVerifyOrderAll.update({受理编号: acnumber, 定单号: parseInt(orderNo)}, {
            $unset: {beihuoDate: -1},
            $set: {orderState: 4, lockProject: "unLock"}
        })
    },
    deliverGoodsOrderUnlock: function (acnumber, orderNo) {
        if (!acnumber || !orderNo) {
            throw new Meteor.Error("操作失败!")
        }
        erpViewVerifyOrderAll.update({受理编号: acnumber, 定单号: parseInt(orderNo)}, {
            $unset: {deliveryDate: -1},
            $set: {lockProject: "unLock",orderState:7}
        })
    },
    'measureOrderUnlock': function (acnumber, orderNo) {

        if (!acnumber || !orderNo) {
            throw new Meteor.Error("操作失败!")
        }
        erpViewVerifyOrderAll.update({受理编号: acnumber, 定单号: parseInt(orderNo)}, {
            $unset: {measureDate: -1, orderState: -1},
            $set: {lockProject: "unLock"}
        });
    },

});