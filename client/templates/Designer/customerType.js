/**
 * Created by zhangxuhui on 15/10/14.
 */
Template.customerType.rendered = function () {
    var dId;
    var designerID;
    console.log(designerID, "---------------------------------", Session.get("currentDesignerID"));

    //todo 方法需要优化


    this.autorun(function () {
        Meteor.call("getDesignerRelationClientCount", Session.get("currentDesignerID"), "已安排", function (err, result) {
            if (!err) {
                Session.set("newCustomerCountInfo", result)
            }
        });
        Meteor.call("getDesignerRelationClientCount", Session.get("currentDesignerID"), "待签", function (err, result) {
            if (!err) {
                Session.set("OnSiteCustomerCountInfo", result);
            }
        });
        Meteor.call("getDesignerRelationClientCount", Session.get("currentDesignerID"), "预算中", function (err, result) {
            if (!err) {
                Session.set("InBudgetCountInfo", result);
            }
        });
        Meteor.call("getDesignerRelationClientCount", Session.get("currentDesignerID"), "已签", function (err, result) {
            if (!err) {
                Session.set("ContractCustomersCountInfo", result);
            }
        });
        Meteor.call("getDesignerRelationClientCount", Session.get("currentDesignerID"), "已归档", function (err, result) {
            if (!err) {
                Session.set("CompletionCustomerCountInfo", result);

            }
        });
    })

};

Template.customerType.onRendered(function () {
    this.subscribe("publishDesignerInfo");
});

Template.customerType.helpers({
    headObj: function () {
        var info = AC_Personnel.findOne();
        if (info && info.pName) {
            return {
                title: info.pName + '的客户'
            }
        } else {
            return {
                title: "客户"
            }
        }
    },
    'new_customerCount': function () {
        return Session.get("newCustomerCountInfo");
    },
    'On_site_customerCount': function () {
        return Session.get("OnSiteCustomerCountInfo");
    },
    'In_budgetCount': function () {
        return Session.get("InBudgetCountInfo");
    },
    'Contract_customersCount': function () {
        return Session.get("ContractCustomersCountInfo");
    },
    'Completion_customerCount': function () {
        return Session.get("CompletionCustomerCountInfo");
    },
    'isUnReadInfo': function () {
        var info = MessageState.find().count();
        console.log("---------------------", info);
        if (info > 0) {
            return true;
        } else {
            return false;
        }
    }
});
Template.customerType.events({

    'click .new_customer': function (event) {
        //新客户
        Router.go("/customerList/designerID/" + Session.get("currentDesignerID") + '/customerType/已安排');
    },
    'click .On_site_customer': function (event) {
        Router.go("/customerList/designerID/" + Session.get("currentDesignerID") + '/customerType/待签');
    },
    'click .In_budget': function (event) {
        Router.go("/customerList/designerID/" + Session.get("currentDesignerID") + '/customerType/预算中');
    },
    'click .Contract_customers': function (event) {
        Router.go("/customerList/designerID/" + Session.get("currentDesignerID") + '/customerType/已签');
    },
    'click .Completion_customer': function (event) {
        Router.go("/customerList/designerID/" + Session.get("currentDesignerID") + '/customerType/已归档');
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/designerLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .materialChat': function () {
        Router.go("/materialChatList");
    }
});

Template.customerType.onRendered(function () {
    Session.set("newCustomerCountInfo", null);
    Session.set("OnSiteCustomerCountInfo", null);
    Session.set("InBudgetCountInfo", null);
    Session.set("ContractCustomersCountInfo", null);
    Session.set("CompletionCustomerCountInfo", null);
})