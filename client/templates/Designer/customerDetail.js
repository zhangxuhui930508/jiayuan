/**
 * Created by pcbeta on 15-8-4.
 */
//var currItemroomlist;
var currItem;
Template.customerDetail.onRendered(function () {
    Session.set("customerDecorateInfo", null);
    IonLoading.show()
    this.subscribe("publishCustomerDetailPage", Router.current().params.cId, function () {
        IonLoading.hide();
    });


    var CustInfo = Cust_Info.findOne({_id: Router.current().params.cId});
    var Projectroles = [];
    if (CustInfo && CustInfo.BusiType) {
        if (CustInfo.BusiType == "整体翻新") {
            Projectroles.push("hydropower", "mudWood", "completion");
        } else if (CustInfo.BusiType == "局部翻新") {
            Projectroles.push("completion");
        }
    }

});


var json = {
    "hydropower": "hydropower",
    "mudWood": "mudWood",
    "completion": "completion",
};


Template.customerDetail.helpers({
    'headObj': function () {
        return {
            title: '客户详细信息',
            leftButton: true,
        }
    },
    'titleName': function () {
        var info = Cust_Info.findOne({_id: Router.current().params.cId});
        if (info && info.CustName) {
            return info.CustName + "的详细信息"
        }
    },
    'Info': function () {
        var info = Cust_Info.findOne({_id: Router.current().params.cId});
        if (info) {
            return info;
        }
    },
    'designerInfo': function () {
        var info = AC_Personnel.findOne();
        if (info) {
            return info.pName;
        }
    },
    'solution': function () {
        var solutionInfo = Solutions.find();
        if (solutionInfo) {
            return solutionInfo;
        }
    },
    "getSolutionTime": function (time) {
        if (time) {
            return time.Format("yyyyMMdd");
        }
    },
    'getStartTime': function (time) {
        if (time) {
            if (time.getFullYear() <= 1899) return "待定";
            return time.Format("yyyy-MM-dd");
        }
    },
    // 判断是否是合同客户
    'Completion_customer': function () {
        if (Roles.userIsInRole(Meteor.userId(), ['designer'], 'user-group')) {
            var cType = Router.current().params.cType;
            if (cType == '已签' || cType == "已归档") {
                return false;
            }
            return true;
        }
        return false;
    },

    'inspectionButtonState': function (project, acnumber) {
        var info = checkConstructionProject.findOne({project: project, acnumber: acnumber});
        if (info && info.checkResult) {
            if (info.checkResult == 1) {
                return false;
            } else if (info.checkResult == 3) {
                return true;
            }
        } else {
            return true;
        }
    },
    'buttonCancel': function (value, acnumber) {
        var info = checkConstructionProject.findOne({checkResult: 1, acnumber: acnumber});
        if (info) {
            return false;
        } else {
            return true;
        }
    },
    'modelInspection': function (value) {
        if (value == "局部翻新") {
            return false;
        } else {
            return true;
        }
    },
    'inspectionStatus': function (project, acnumber) {
        var erpType;
        console.log(project);
        if (project) {
            switch (project) {
                case "hydropower":
                    erpType = 9.7;
                    break;
                case "mudWood":
                    erpType = 15.5;
                    break;
                case "completion":
                    erpType = 99;
                    break;
            }
        }
        var erpCheckInfo = ERP_Quarantine_Acceptance.findOne({受理编号: acnumber, 报验类型: erpType});
        if (!erpCheckInfo) {
            var info = checkConstructionProject.findOne({project: project, acnumber: acnumber});
            if (info) {
                if (info.checkResult == 1 && info.qcIsCheck) {
                    return "质检人员已经查看,不能取消质检!"
                }
                if (info.checkResult == 1) {
                    return "等待检验!";
                } else if (info.checkResult == 2) {
                    return "审批通过!";
                } else if (info.checkResult == 3) {
                    return "整改单!";
                }
            }
        } else {
            switch (erpCheckInfo.质检合格) {
                case 1:
                    return "一次性合格!";
                    break;
                case 2:
                    return "整改后合格!";
                    break;
                case 3:
                    return "有整改!";
                    break;
                case -1:
                    return "不合格!";
                    break;
                case 9:
                    return "不质检!";
                    break;
                case 0:
                    return "未质检!";
                    break;
            }
        }
    },
    'isqcIsCheck': function (project, acnumber) {
        var info = checkConstructionProject.findOne({project: project, acnumber: acnumber});
        if (info && info.jumpTheQueue) {
            return "(已插队)"
        }
    },
    'isButtonInfo': function (value, acnumber) {
        var info = checkConstructionProject.findOne({project: value, acnumber: acnumber});
        if (info && info.checkResult) {
            console.log(info.checkResult);
            if (info.checkResult == 2 || info.checkResult == 3) {
                return false;
            }
        }
        return true;
    },
    'iconImage': function (value, acnumber) {
        var info = checkConstructionProject.findOne({project: value, acnumber: acnumber});
        if (info && info.checkResult) {
            if (info.checkResult == 2) {
                return "/image/trueIcon.jpg"
            } else if (info.checkResult == 3) {
                return "/image/falseIcon.jpg";
            }
        }
    },
    'customerType': function () {
        var type = Router.current().params.cType;
        if (type == "未开工") {
            return false;
        } else {
            return true;
        }
    },
    'CostAccountingPlan': function () {
        var info = erpCostAccountingPlan.findOne();
        if (info) {
            return info;
        }
    },

    'hydropowerButtom': function (value, acnumber) {
        var info = checkConstructionProject.findOne({project: value, acnumber: acnumber});
        if (info && info.checkResult) {
            if (info.checkResult == 1) {
                return false;
            }
        }
        return true;
    },
    buttonDisplay: function (acnumber, type) {
        var info = ERP_Quarantine_Acceptance.findOne({受理编号: acnumber, 报验类型: type});
        if (info) {
            return "none";
        }
        return "block";

    },
    'mudWoodButton': function (value, acnumber) {
        //先检测第一项是否报检,如果没有报检第二项不能报检
        var erpType;
        console.log(value);
        if (value) {
            switch (value) {
                case "hydropower":
                    erpType = 9;
                    break;
                case "mudWood":
                    erpType = 15.5;
                    break;
                case "completion":
                    erpType = 99;
                    break;
            }
        }

        var erpCheckInfo = ERP_Quarantine_Acceptance.findOne({受理编号: acnumber, 报验类型: erpType});
        console.log("----------------------------", erpType)
        console.log(erpCheckInfo);
        console.log("----------------------------", acnumber)
        if (!erpCheckInfo) {
            var info = checkConstructionProject.findOne({project: value, acnumber: acnumber});
            if (info && info.checkResult) {
                if (info.checkResult == 2 || info.checkResult == 3) {
                    return true;
                }
            }
            return false;
        }
        return true;
    },
    'checkStatus': function (project, acnumber) {
        var info = checkConstructionProject.findOne({project: project, acnumber: acnumber});
        if (info && info.checkResult) {
            if (info.checkResult == 2 || info.checkResult == 3) {
                return false
            }
        }
        return true;
    },
    isCancelButton: function (project, acnumber) {
        var info = checkConstructionProject.findOne({project: project, acnumber: acnumber});
        console.log(info.qcIsCheck);
        if (info && info.checkResult) {
            if (info.checkResult == 1 && info.qcIsCheck) {
                return false;
            }
        }
        return true;
    },
    isConstructionConfirm: function () {
        var erpBudgetMaterial = ERP_Budget_Material.findOne({});
        if (erpBudgetMaterial && erpBudgetMaterial.队长确认) {
            return false;
        }
        return true;
    }

});

Template.customerDetail.events({
    'click .select_mode': function (event) {
        if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
            Session.set("backtemplate", "customerDetail");
            var acnumber = $(".acnumberNumber").text();
            var customerDecorateInfo = {};
            var Customerinfo = Cust_Info.findOne({_id: Router.current().params.cId});
            customerDecorateInfo.ACNumber = acnumber;
            customerDecorateInfo.mode = Customerinfo.BusiType;
            customerDecorateInfo.cType = Router.current().params.cType;
            customerDecorateInfo.cId = Router.current().params.cId;
            Session.set("customerDecorateInfo", customerDecorateInfo);
            if (Customerinfo && Customerinfo.BusiType) {
                Router.go("/DecorateArea");
            } else {
                Router.go("/modeSelect");
            }
        } else {
            IonLoading.show({
                customTemplate: "<h4>您没有权限操作!</h4>",
                duration: 1000
            });
        }
    },
    'click .solutionList .row .item .solutionName': function (event) {
        //查看方案的材料信息

        var solutionsId = $(event.currentTarget).attr("id");
        console.log(solutionsId);
        var acnumber = $(".acnumberNumber").text();
        var customerDecorateInfo = {};
        var Customerinfo = Cust_Info.findOne({_id: Router.current().params.cId});
        customerDecorateInfo.ACNumber = acnumber;
        customerDecorateInfo.mode = Customerinfo.BusiType;
        customerDecorateInfo.cType = Router.current().params.cType;
        customerDecorateInfo.cId = Router.current().params.cId;
        Session.set("customerDecorateInfo", customerDecorateInfo);
        var cType = Router.current().params.cType;
        Router.go("/projDetail/customerType/" + cType + '/solutionId/' + solutionsId);

    },
    'click .solutionListDel': function (event) {
        if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
            var id = $(event.currentTarget).attr("id");
            console.log(id);
            Meteor.call("delSolution", id, function (err, result) {
                if (err) {
                    IonLoading.show({
                        customTemplate: "<h4>删除方案失败!</h4>",
                        duration: 1000
                    });
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>删除方案成功!</h4>",
                        duration: 1000
                    });
                }
            });
        } else {
            IonLoading.show({
                customTemplate: "<h4>您没有权限操作!</h4>",
                duration: 1000
            });
        }
    },
    'click .goMYCustomerDetail': function () {
        var acnumber = $(".acnumberNumber").text();
        var customerDecorateInfo = {};
        var Customerinfo = Cust_Info.findOne({_id: Router.current().params.cId});
        if (Customerinfo && Customerinfo.BusiType) {
            customerDecorateInfo.ACNumber = acnumber;
            customerDecorateInfo.mode = Customerinfo.BusiType;
            customerDecorateInfo.cType = Router.current().params.cType;
            customerDecorateInfo.cId = Router.current().params.cId;
            Session.set("customerDecorateInfo", customerDecorateInfo);
            Router.go("/mycustomer");
        }
    },
    'click .backMenuList': function () {
        if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
            var cType = Router.current().params.cType;
            if (cType == "designer") {
                Router.go("/designerSearch");
            } else
                Router.go("/customerList/designerID/" + Session.get("currentDesignerID") + '/customerType/' + cType);
        } else if (Roles.userIsInRole(Meteor.userId(), ['test'], 'user-group')) {
            Router.go("/shopManagerTestPage");
        } else if (Roles.userIsInRole(Meteor.userId(), ['ConstructionCaptain', 'ConstructionCaptainBoss'], 'user-group')) {
            var cType = Router.current().params.cType;
            if (cType == "1" || cType == "3") {
                Router.go("/searchCustomer/" + cType);
            } else {
                Router.go("/construtionList/" + cType);
            }

        } else if (Roles.userIsInRole(Meteor.userId(), ['materialDept'], 'user-group')) {
            var cType = Router.current().params.cType;
            Router.go("/materialDeptConstructionList/" + cType);
        } else {
            Router.go("/shopManagerSearch");
        }
    },
    //=======================================施工报检=========================
    'click .Inspection': function (event) {
        //      
        var custInfo = Cust_Info.findOne({_id: Router.current().params.cId});
        var type = $(event.currentTarget).children("span").attr('class');
        console.log(type);
        if (custInfo) {
            Meteor.call("createInspection", custInfo.ACNumber, type, Meteor.userId(), Session.get("currentConstructionId"), function (err, result) {
                console.log(err, result);
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>报检成功!</h4>",
                        duration: 1000
                    });
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>报检失败!</h4>",
                        duration: 1000
                    });
                }
            });
        }

    },
    'click .cancelInspection': function (event) {
        //取消报检
        var type = $(event.currentTarget).children("span").attr('class');
        var custInfo = Cust_Info.findOne({_id: Router.current().params.cId});
        if (custInfo && type) {
            //todo 质检人员只要已经查看后 就不可以取消
            Meteor.call("cancelInspection", custInfo.ACNumber, type, Meteor.userId(), function (err, result) {
                if (!err) {
                    IonLoading.show({
                        customTemplate: "<h4>取消报检成功!</h4>",
                        duration: 1000
                    });
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>取消报检失败!</h4>",
                        duration: 1000
                    });
                }
            })
        }
    },
    'click .notInspection': function () {
        IonLoading.show({
            customTemplate: "<h4>请其他查看项目是否已经审批通过!</h4>",
            duration: 1000
        });
    },
    'click .goRepairList': function (event) {
        var type = $(event.currentTarget).children("span").attr("id");
        var acnumber = $(event.currentTarget).children("span").attr("class");
        console.log(type, acnumber);

        var info = checkConstructionProject.findOne({project: type, acnumber: acnumber});
        if (info && info.checkResult) {
            if (info.checkResult == 3) {
                var select = {};
                select.cType = Router.current().params.cType;
                select.cId = Router.current().params.cId;
                Session.set("goRepairInfoBack", select);
                Router.go("/repairList/acnumber/" + info.acnumber + "/project/" + info.project);
            }
        }
    },
//    姜婷婷有插队的权限
    'click .jumpTheQueue': function (event) {
        var type = $(event.currentTarget).children("span").attr('class');
        var custInfo = Cust_Info.findOne({_id: Router.current().params.cId});
        Meteor.call("jumpTheQueueRole", custInfo.ACNumber, type, Meteor.userId(), function (err, result) {
            console.log(err, result);
            if (result) {
                IonLoading.show({
                    customTemplate: "<h4>插队成功!</h4>",
                    duration: 1500
                });
            } else {
                IonLoading.show({
                    customTemplate: "<h4>插队失败,请确认施工队长已经提交了报检信息!</h4>",
                    duration: 1500
                });
            }
        })
    },
    'click .constructionConfirm': function () {
        IonPopup.confirm({
            title: '温馨提示',
            template: "确定供应商的材料都已经到货了吗?",
            okText: "确定",
            cancelText: "取消",
            onOk: function () {
                Meteor.call("constructionConfirmMaterial", Router.current().params.cId, function () {

                })
            },
            onCancel: function () {
                console.log('Cancelled');
            }
        });


    }
});

