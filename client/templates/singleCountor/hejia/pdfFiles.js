/**
 * Created by zhangxuhui on 16/3/22.
 */

Template.pdfFiles.helpers({
    headObj: function () {
        return {
            title: '读取文件',
            leftButton: true,
            //backTemplate: 'hejiadan'
        }
    },
    'pdfInfo': function () {
        var info = erpRemoteFileStorage.findOne();
        if (info) {
            console.log(info);
            return info
        }
    }

});

Template.pdfFiles.events({
    'click .backMenuList': function () {
        if (Session.get("backURL") == "hejiadan") {
            Router.go("/hejiadan")
        } else if (Session.get("backURL") == "changeOrder") {
            Router.go("/ChangeOrder");
        } else if (Session.get("backURL") == "settlementOrder") {
            Router.go("/settlementOrder");
        } else if (Session.get("backURL") == "paymentReminder") {
            Router.go("/paymentReminder");
        }
    }
});
