/**
 * Created by zhangxuhui on 16/5/12.
 */

Template.constructionCaptainList.onRendered(function () {
    // IonLoading.show();
});

Template.constructionCaptainList.helpers({
    headObj: function () {
        return {
            title: "施工队长列表"
        }
    },
    'employeeInfo': function () {
        var info = Session.get("currentSearchVal");
        var select = {};
        if (info) {
            select.pName = new RegExp(info, "i");
        }
        return AC_Personnel.find(select, {sort: {pUser: 1, pName: 1,}});
    }
});

Template.constructionCaptainList.events({
    'click .selectConstruction': function (event) {
        console.log(this.ID);
        Session.set("currentConstructionId", this.ID.toString());
        Router.go("/constructionCType")
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/constructionLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentSearchVal", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentSearchVal", null);
        $(".search").val("");
    }
});

Template.constructionCaptainList.onDestroyed(function () {
    Session.set("currentSearchVal", null);

});
