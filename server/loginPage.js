/**
 * Created by zhangxuhui on 16/8/16.
 */

Meteor.methods({
    checkIndexPage: function () {
        var returnJson = {};
        if (this.userId) {
            if (Roles.userIsInRole(this.userId, ["vicePresident", 'districtManager', 'StoreManager', 'test'], "user-group")) { //副总
                returnJson.templatePage = "shopManagerSearch";
            }
            if (Roles.userIsInRole(this.userId, ["designer"], "user-group")) {
                returnJson.templatePage = "customerType";
            }
        }
        return returnJson;
    },
    //客户登录
    clientCheckIndex: function () {
        var returnJson = {};
        if (Roles.userIsInRole(this.userId, ["client"], "user-group")) {
            returnJson.templatePage = "customerindex";
        }
        return returnJson;
    },
    constructionCheckIndex: function () {
        var returnJson = {};
        if (this.userId) {
            if (Roles.userIsInRole(this.userId, ["ConstructionCaptainBoss"], "user-group")) {
                returnJson.templatePage = "constructionCaptainList";
                returnJson.loginRole = "ConstructionCaptainBoss";
            }
            if (Roles.userIsInRole(this.userId, ["ConstructionCaptain"], "user-group")) {
                returnJson.templatePage = "constructionCType";
                returnJson.loginRole = "ConstructionCaptain";
            }
        }
        return returnJson;
    },
    //材料部登录materialDept
    materialDeptCheckIndex: function () {
        var returnJson = {};
        if (this.userId) {
            if (Roles.userIsInRole(this.userId, ["materialDept"], "user-group")) {
                returnJson.templatePage = "materialDeptConstructionType";
                returnJson.loginRole = "materialDept";
            }
        }
        return returnJson;
    },
    //合约部门
    contractDeptCheckIndex: function () {
        var returnJson = {};
        if (this.userId) {
            if (Roles.userIsInRole(this.userId, ["contractDept"], "user-group")) {
                returnJson.templatePage = "contractPricing";
                returnJson.loginRole = "contractDept";
            }
        }
        return returnJson;
    },
    qcCheckIndex: function () {
        var returnJson = {};
        if (this.userId) {
            if (Roles.userIsInRole(this.userId, ["qc"], "user-group")) {
                returnJson.templatePage = "notExaminedList";
                returnJson.loginRole = "qc";
            }
            if (Roles.userIsInRole(this.userId, ["qcManager", "qcBoss"], "user-group")) {
                returnJson.templatePage = "assignQC";
                returnJson.loginRole = "qcManager";
            }
        }
        return returnJson;
    },
    SupplierCheckIndex: function () {
        var returnJson = {};
        if (this.userId) {
            if (Roles.userIsInRole(this.userId, ["Supplier"], "user-group")) {
                returnJson.templatePage = "orderType";
                returnJson.loginRole = "Supplier";
            }
        }
        return returnJson;
    }
});