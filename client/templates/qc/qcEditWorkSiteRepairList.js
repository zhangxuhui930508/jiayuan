/**
 * Created by zhangxuhui on 16/8/16.
 */

Template.qcEditWorkSiteRepairList.onCreated(function () {
    HeytzSlideButton.init({
        delCallback: function (event, template, that) {
            var id = that._id;
            var errcode = that.errcode;
            IonPopup.confirm({
                title: '确定删除?',
                template: '你确定 <strong>删除</strong> 此条信息吗?',
                okText: "删除",
                // okType: "button-assertive",
                cancelText: "取消",
                onOk: function () {
                    Meteor.call("removeEditWorkSiteRepairInfo", id, errcode, function (err, result) {
                        console.log(err, result)
                    })
                },
                onCancel: function () {
                    console.log('Cancelled');
                }
            });
        }
    })
});


Template.qcEditWorkSiteRepairList.onRendered(function () {
    var checkId = Router.current().params.checkId;
    this.subscribe("publishQCEditWorkSitePage", checkId);
})

Template.qcEditWorkSiteRepairList.helpers({
    headObj: function () {
        return {
            title: '整改单/协调单编辑',
            leftButton: true,
        }
    },
    errcodeNo: function () {
        var errcodeNo = [];
        correctiveReason.find().forEach(function (info) {
            if (info && info.reason) {
                var json = {};
                var arr = info.reason;
                var arr1 = [];
                for (var i = 0; i < arr.length; i++) {
                    errcodeNo.push({errcode: arr[i].errorCode1, _id: info._id});
                }
            }
        });
        return errcodeNo;
    },
    getReason: function (id) {
        var info = ERP_Info_Quality_Control.findOne({_id: id});
        if (info) {
            return info.内容;
        }
    }
});

Template.qcEditWorkSiteRepairList.events({
    'click .item': function () {
        // console.log(this._id);
        // console.log(this.errcode);
        // Meteor.call("removeEditWorkSiteRepairInfo", this._id, this.errcode, function (err, result) {
        //     console.log(err, result)
        // })
    },
    'click .backMenuList': function () {
        history.back();
    }
});