Template.materialList.rendered = function () {
    // IonLoading.show({
    //     duration: 1500
    // });

    // var MClassifyId = Router.current().params.id;
    // this.subscribe("materialListByclassifyId", MClassifyId, function (err, result) {
    //     IonLoading.hide();
    // });
    Session.set("backtemplate", null);
    if (Session.get("customerDecorateInfo")) {
        //当前客户购物车的数量
        this.subscribe("ShoppingCartInfo", Session.get("customerDecorateInfo").ACNumber, function () {
        });
    }

    //根据材料的分类ID在materialDetailList库中查找对应的产品；
    Session.set("shoppingbackTemplate", "materialList");


};

Template.materialList.helpers({
    headObj: function () {
        return {
            title: '材料列表页'
        }
    },
    'materiaList': function () {
        var info = MaterialDetailList.find().fetch();
        return info;
    },
    //第一级分类的名称
    'oneupper_level': function () {
        var classifyName = Session.get("classifyName");
        if (classifyName) {
            return classifyName.one;
        }
    },
    //第二级分类的名称
    'upper_level': function () {
        var classifyName = Session.get("classifyName");
        if (classifyName) {
            return classifyName.two;
        }
    },
    //第三级分类的名称
    'third_material': function () {
        var classifyName = Session.get("classifyName");
        if (classifyName) {
            return classifyName.three;
        }
    },
    //第四级分类的名称
    'four_level': function () {
        return Session.get("four_level");
    },
    titleName: function () {
        var parts = "材料分类";
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        if (Session.get("customerDecorateInfo")) {
            parts = Session.get("customerDecorateInfo").parts;
        } else {
            parts = "材料分类";
        }
        return parts;
    },
    'rightbutton': function () {
        if (Session.get("customerDecorateInfo").sId) {
            return false;
        } else {
            return true;
        }
    },
    shoppingcount: function () {
        var shoppingInfo = ShoppingCart.find({ACNumber: Session.get("customerDecorateInfo").ACNumber}).count();
        if (shoppingInfo) {
            return shoppingInfo;
        }
    }
});

Template.materialList.events({
    'click .materialList .item': function (event) {
        var id = $(event.currentTarget).children("h2").attr("id");
        var orderType = $(event.currentTarget).children("input").val();
        console.log(orderType, "订单类型");
        Session.set("currentMaterialOrderType", orderType);
        var materialClassifyId = Router.current().params.id;
        Router.go("/materialDetail/materialClassifyId/" + materialClassifyId + "/materialId/" + id);//跳转到材料详情页面
    },
    'click .saveButton': function (event) {
        if (Roles.userIsInRole(Meteor.userId(), ["client"], "user-group")) {
            Router.go("/CSolutionDetail/" + Session.get("customerDecorateInfo").sId);
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer"], "user-group")) {
            Router.go("/projDetail/customerType/" + Session.get("customerDecorateInfo").cType + '/solutionId/' + Session.get("customerDecorateInfo").sId);
        }
    },
    'click .backMenuList': function () {
        if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "test"], "user-group")) {
            Router.go("/MaterialClassify");
        } else if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
            Router.go("/materialClassify02");
        }
    },
    //跳转到第一级分类名称
    'click .go_marerialClassify1': function () {
        Router.go("/MaterialClassify");
    },
    //跳转到第二级分类名称
    'click .go_marerialClassify2': function () {
        var classifyName = Session.get("classifyName");
        Router.go("/MaterialClassify2/" + classifyName.one);
    },
    //  //跳转到第三级分类名称
    'click .go_marerialClassify3': function () {
        var classifyName = Session.get("classifyName");
        Router.go("/MaterialClassify3/Mclassify1/" + classifyName.one + "/Mclassify2/" + classifyName.two);
    },
    //  //跳转到第四级分类名称
    'click .go_marerialClassify4': function () {
        var classifyName = Session.get("classifyName");
        Router.go("/MaterialClassify4/Mclassify1/" + classifyName.one + "/Mclassify2/" + classifyName.two + "/Mclassify3/" + classifyName.three);
    },
    'click .title_shop': function () {
        console.log("==============shopping=============");
        var customerDecorateInfo = Session.get("customerDecorateInfo");
        var materialClassifyId = Router.current().params.id;
        Session.set("currentMaterialClassifyId", materialClassifyId);
        Router.go("/shopping/" + customerDecorateInfo.ACNumber);
    }
});


