/**
 * Created by zhangxuhui on 15/10/29.
 */
Template.ChangeOrder.rendered = function () {
    var Acnumber;
    if (Roles.userIsInRole(Meteor.userId(), ["client", "contractDept"], "user-group")) {
        Acnumber = Session.get("customerLogin");
    } else if (Roles.userIsInRole(Meteor.userId(), ["ConstructionCaptain", "districtManager", "vicePresident", "StoreManager", 'test', 'designer'], "user-group")) {
        Acnumber = Session.get("customerDecorateInfo").ACNumber;
    } else if (Roles.userIsInRole(Meteor.userId(), ['designer'], 'user-group')) {
        Acnumber = Session.get("customerDecorateInfo").ACNumber;
    }
    this.subscribe("NuclearPriceSheet", Acnumber, 2);

};

Template.ChangeOrder.helpers({
    headObj: function () {
        return {
            title: '变更单详情',
            leftButton: true,
            backTemplate: 'mycustomer'
        }
    },
    'ChangeOrderDetail': function () {
        var select = {};
        if (Roles.userIsInRole(Meteor.userId(), ["client,designer,ConstructionCaptain"], "user-group")) {
            select.openOrder = true
        }
        var info = erpRemoteFileStorage.find(select, {sort: {完成时间: -1}});
        if (info) {
            return info;
        }
    },
    'confirmButtonStatus': function (id) {
        var info = erpRemoteFileStorage.findOne({_id: id});

        if (info.changeOrderConfirm) {
            return false;
        } else {
            return true;
        }
    },
    'openInfo': function (id) {
        var info = erpRemoteFileStorage.findOne({_id: id});
        if (info.openOrder) {
            return false;
        } else {
            return true;
        }
    }

});

Template.ChangeOrder.events({
    'click .openfile': function (event) {
        var id = $(event.currentTarget).attr("id").trim();
        Session.set("backURL", "changeOrder");
        Router.go("/pdfFiles/" + id);
    },
    'click .submitConfirmInfo': function (event) {
        var id = $(event.currentTarget).attr("id");
        console.log(id);
        Meteor.call("updateChangeOrder", id, function (err, result) {
            if (!err) {
                IonLoading.show({
                    customTemplate: "<h4>确认变更单成功!</h4>",
                    duration: 1000
                });
            } else {
                IonLoading.show({
                    customTemplate: "<h4>确认变更单失败!</h4>",
                    duration: 1000
                });
            }
        })
    },
    'click .confirm': function () {
        IonLoading.show({
            customTemplate: "<h4>此变更单已经确认过了!</h4>",
            duration: 1000
        });
    },
    'click .openOrderInfo': function (event) {
        var id = $(event.currentTarget).attr("id");
        console.log(id);
        Meteor.call("updateOpenOrder", id, true, function (err, result) {
            if (!err) {
                IonLoading.show({
                    customTemplate: "<h4>公开成功!</h4>",
                    duration: 1000
                });
            } else {
                IonLoading.show({
                    customTemplate: "<h4>公开失败!</h4>",
                    duration: 1000
                });
            }
        })
    },
    'click .closeOrder': function (event) {
        var id = $(event.currentTarget).attr("id");
        console.log(id);
        Meteor.call("updateOpenOrder", id, false, function (err, result) {
            if (!err) {
                IonLoading.show({
                    customTemplate: "<h4>关闭成功!</h4>",
                    duration: 1000
                });
            } else {
                IonLoading.show({
                    customTemplate: "<h4>关闭失败!</h4>",
                    duration: 1000
                });
            }
        })
    }


});