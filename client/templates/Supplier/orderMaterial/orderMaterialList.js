/**
 * Created by zhangxuhui on 16/3/24.
 */

Template.orderMaterialList.helpers({
    headObj: function () {
        return {
            title: '材料列表'
        }
    },
    'orderMaterialInfo': function () {

        var searchinfo = Session.get("currentMaterialSearch");
        var select = {};
        if (searchinfo) {
            select = {$or: [{材料品牌: new RegExp(searchinfo, "i")}, {材料名称: new RegExp(searchinfo, "i")}]}
        }

        var info = MaterialDetailList.find(select, {sort: {材料名称: 1}});
        if (info) {
            return info;
        }
    }
});

Template.orderMaterialList.events({
    'click .materialDetailInfo': function (event) {
        var id = $(event.currentTarget).children("h2").attr('id').trim();
        Router.go("/orderMaterialDetailInfo/" + id);
    },
    'click .log-out': function () {
        Meteor.logout(function (err) {
            if (!err) {
                Router.go("/SupplierLogin");
            } else {
                IonLoading.show({
                    customTemplate: "<h4>注销失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'change .search, input .search': function (event) {
        var currentSearch = $(event.currentTarget).val();
        Session.set("currentMaterialSearch", currentSearch);
    },
    'click .closed': function () {
        Session.set("currentMaterialSearch", null);
        $(".search").val("");
        console.log("-------------------------------------");
    }
});

Template.orderMaterialList.onDestroyed(function () {
    Session.set("currentMaterialSearch", null);

})