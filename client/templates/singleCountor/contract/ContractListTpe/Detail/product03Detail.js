/**
 * Created by zhangxuhui on 15/11/11.
 */


Template.product03Detail.rendered=function(){
    var currentid=Router.current().params.id;
    this.subscribe("Product_Order03", currentid);
};

Template.product03Detail.helpers({
    headObj: function () {
        return {
            title: '材料详情',
            leftButton: true,
            backTemplate: 'CustomizeMaterial'
        }
    },
    'productList':function(){
        var currentid=Router.current().params.id;
        var info=ERP_Product_Order03.findOne({_id:currentid});
        return info;
    }
});

Template.product03Detail.events({
    'click .SNINFO': function (event) {
        var materialID = Router.current().params.id;
        var acnumber;
        if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
            acnumber = Session.get("orderBackURL").acnumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], 'user-group')) {
            acnumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ['client', "materialDept", "contractDept"], 'user-group')) {
            acnumber = Session.get("customerLogin");
        }

        var info = roomMessage.findOne({clientId: acnumber, materialId: materialID});
        console.log("------------------------SNINFO-----------------------------------------");
        console.log(info);
        var materialName = $(".materialName").text().trim();
        Session.set("materialName", materialName);
        console.log("------------------------------------------------------------------------")
        if (!info) {
            Meteor.call("createRoom", materialID, acnumber, Meteor.userId(), function (err, result) {
                console.log(err, result);
                if (!err) {
                    Router.go("/chat/roomId/" + result + "/materialId/" + materialID)
                } else {
                    IonLoading.show({
                        customTemplate: "<h4>创建对话失败,请重新进入!</h4>",
                        duration: 1000
                    });
                }
            });
        } else {
            Router.go("/chat/roomId/" + info._id + "/materialId/" + materialID)
        }
    },
    'click .rushFactory': function (event) {
        var materialID = Router.current().params.id;
        var acnumber;
        if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
            acnumber = Session.get("orderBackURL").acnumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], 'user-group')) {
            acnumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ['client', "materialDept", "contractDept"], 'user-group')) {
            acnumber = Session.get("customerLogin");
        }
        Meteor.call("sendMessageOrCreate", materialID, acnumber, Meteor.userId(), "催材料进场!", $(".materialName").text().trim(), function (err, result) {
            if (!err) {
                IonLoading.show({
                    customTemplate: "<h4>消息发送成功!</h4>",
                    duration: 1000
                });
            } else {
                IonLoading.show({
                    customTemplate: "<h4>消息发送失败!</h4>",
                    duration: 1000
                });
            }
        });
    },
    'click .typicalSentences': function () {
        var id = Router.current().params.id;
        var acnumber;
        if (Roles.userIsInRole(Meteor.userId(), ["Supplier"], "user-group")) {
            acnumber = Session.get("orderBackURL").acnumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ["designer", "districtManager", "vicePresident", "StoreManager", "ConstructionCaptain", 'test', "ConstructionCaptainBoss"], 'user-group')) {
            acnumber = Session.get("customerDecorateInfo").ACNumber;
        } else if (Roles.userIsInRole(Meteor.userId(), ['client', "materialDept", "contractDept"], 'user-group')) {
            acnumber = Session.get("customerLogin");
        }
        var materialName = $(".materialName").text().trim();
        Session.set("materialName", materialName);
        Router.go("/typicalSentences/materialId/" + id + "/acnumber/" + acnumber);
    }
});
