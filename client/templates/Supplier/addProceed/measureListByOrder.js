/**
 * Created by zhangxuhui on 16/7/12.
 */

Template.measureListByOrder.onRendered(function () {
    Session.set("measureState", "unMeasure");
    this.subscribe("publishMeasureListByOrder")

});

Template.measureListByOrder.helpers({
    headObj: function () {
        return {
            title: '待测量订单',
            leftButton: true,
            backTemplate: 'proceedOrderType'
        }
    },
    isActive: function (val) {
        var select = Session.get("measureState");
        if (select && select == val) {
            return "active";
        }
    },
    isMeasureList: function () {
        var info = Session.get("measureState");
        var select = {};
        if (info == "unMeasure") {
            select.measureDate = {$lt: new Date().Format("yyyy-MM-dd")}
        } else if (info == "todayMeasure") {
            select.measureDate = new Date().Format("yyyy-MM-dd");
        } else if (info == "futureMeasure") {
            select.measureDate = {$gt: new Date().Format("yyyy-MM-dd")}
        }

        var erpBudgetMaterialInfo = erpViewVerifyOrderAll.find(select);

        if (erpBudgetMaterialInfo) {
            return erpBudgetMaterialInfo;
        }
    },
    getCustomerName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    getCustomerAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    }
});

Template.measureListByOrder.events({
    "click .tab-item": function (event) {
        var val = $(event.currentTarget).attr("value");
        Session.set("measureState", val);
    },
    'click .gocustomerDetail': function () {
        //    调转到订单详情页面进行编辑
        var info = erpViewVerifyOrderAll.findOne();
        if (info)
            Router.go("/orderDetail/type/newOrder/acnumber/" + info.受理编号 + '/orderNo/' + info.定单号);

    }
});