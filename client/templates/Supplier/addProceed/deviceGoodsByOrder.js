/**
 * Created by zhangxuhui on 16/7/12.
 */

Template.deviceGoodsByOrder.onRendered(function () {
    IonLoading.show();
    Session.set("deliverGoodsList", "undeliverGoods");

    this.subscribe("publishGetOrderdeviceGoodsList", 8, function () {
        IonLoading.hide();
    });
});


Template.deviceGoodsByOrder.helpers({
    headObj: function () {
        return {
            title: '发货中订单',
            leftButton: true,
            backTemplate: 'proceedOrderType'
        }
    },
    isActive: function (val) {
        var select = Session.get("deliverGoodsList");
        if (select && select == val) {
            return "active";
        }
    },
    deviceGoodsInfo: function () {
        var info = Session.get("deliverGoodsList");
        var select = {};
        if (info == "undeliverGoods") {
            select.deliveryDate = {$lt: new Date().Format("yyyy-MM-dd")}
        } else if (info == "todaydeliverGoods") {
            select.deliveryDate = new Date().Format("yyyy-MM-dd");
        } else if (info == "futuredeliverGoods") {
            select.deliveryDate = {$gt: new Date().Format("yyyy-MM-dd")}
        }

        var erpViewVerifyOrderAllInfo = erpViewVerifyOrderAll.find(select);
        if (erpViewVerifyOrderAllInfo) {
            return erpViewVerifyOrderAllInfo;
        }
    },
    getCustomerName: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.CustName;
        }
    },
    getCustomerAddress: function (acnumber) {
        var info = Cust_Info.findOne({ACNumber: acnumber});
        if (info) {
            return info.Address;
        }
    }
});

Template.deviceGoodsByOrder.events({
    "click .tab-item": function (event) {
        var val = $(event.currentTarget).attr("value");
        Session.set("deliverGoodsList", val);
    },
    'click .gocustomerDetail': function (event) {
        var info = erpViewVerifyOrderAll.findOne();
        if (info)
            Router.go("/orderDetail/type/newOrder/acnumber/" + info.受理编号 + '/orderNo/' + info.定单号);
    }

});